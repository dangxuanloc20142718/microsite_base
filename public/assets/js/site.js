(function($) {
    "use strict";
    $.app = new function () {
        this.vars = {};
        var interval  = null;
        var tmpHtml  = null;
        var instance  = this;

        this.init = function (args) {
            //Set vars
            for (var i in args) {
                if (args.hasOwnProperty(i)) {
                    instance.vars[i] = args[i];
                }
            }

            // Another init for class
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $.app.vars.token
                }
            });

            this.facebook.init();
        };

        this.ajax = function (obj, url, formData, callback, method, alertClass) {
            var $button = $(obj);
            var $alert = alertClass ? $(alertClass) : $button.parent().closest('alert');
            method = method || "POST";

            $.app.tmpHtml = obj ? $button.html() : null;
            $button.html('<img style="width:20px" src="'+$.app.vars.url+'media/system/loading.gif"/>Đang xử lý...');

            $alert.addClass('d-none').removeClass('alert-danger').removeClass('alert-success');

            $.ajax({
                type: method,
                url: $.app.vars.url + url,
                data: formData,
                success: function (data) {
                    $alert.removeClass('d-none').html(data.message);
                    if(data.code){
                        $alert.addClass('alert-success');
                    }else{
                        $alert.addClass('alert-danger');
                    }

                    $button.html($.app.tmpHtml);

                    if (typeof callback === 'function') {
                        callback(data);
                    }
                },
                error: function () {
                    $alert.removeClass('d-none').addClass('alert-danger').html('<strong>Thông báo:</strong> Lỗi khi xử lý dữ liệu. Bạn vui lòng thử lại.');
                },
                done: function() {
                }
            });
        };

        this.checkLogin = function (redirectTo, callback, submitId) {
            var _cb = function(data){
                // Logged in, can continue
                if(data.code !== 2){
                    $('#loginModal').modal('hide');
                    if(redirectTo){
                        window.location.href = redirectTo;
                    }

                    if(callback){
                        callback(data);
                    }
                }

                // Logged out, login again
                if(data.code === 2){
                    // Check login
                    $('#loginModal').modal('show');
                    $('#loginModal .form').removeClass('d-none');
                    $('#loginModal .process').addClass('d-none');

                    $('.btn-fblogin-modal').unbind('click').bind('click', function(){
                        var _cbLogin = function(){
                            $('#loginModal').modal('hide');

                            if(redirectTo){
                                window.location.href = redirectTo;
                            }

                            if(callback){
                                callback(data);
                            }
                        };

                        $.app.facebook.login(_cbLogin);
                        return false;
                    });
                }
            };

            $.app.ajax(null, 'bai-du-thi/checkUser', {id: submitId}, _cb, 'get', '');
            return false;
        };

        // Modal functions
        this.modal = new function () {
            this.show = function (obj) {
                //alert($.app.vars.url);
            };

            this.contentModal = function (obj) {
                var $modal = $('#sysModal');
                var $obj = $(obj);

                $modal.modal('show');
                $modal.find('.modal-title').html( $obj.data('title') );

                var _cb = function (data) {
                    if(data.code){
                        $modal.find('.modal-body').html(data.data);
                    }
                };

                $.app.ajax(null, $obj.data('content-ajax'), {}, _cb, 'GET', '');
                return false;
            };

            this.videoModal = function (obj) {
                //alert($.app.vars.url);
            };
        };

        // Facebook SDK functions
        this.facebook = new function () {
            this.init = function(){
                window.fbAsyncInit = function () {
                    FB.init({
                        appId: $.app.vars.facebook_appid,
                        xfbml: true,
                        cookie: true,
                        version: 'v2.8'
                    });
                };

                (function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8&appId=" + $.app.vars.facebook_appid;
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
            };

            this.login = function (callback) {
                FB.login(function (response) {
                    if (response) {
                        if (response.authResponse && response.status == 'connected') {
                            var accessToken = response.authResponse.accessToken;

                            $('#loginModal .form').addClass('d-none');
                            $('#loginModal .process').removeClass('d-none');

                            FB.api('/me?fields=id,name,email', function (response) {
                                $.ajax({
                                    type: 'POST',
                                    url: $.app.vars.url + 'auth/fblogin?accessToken=' + accessToken,
                                    data: response,
                                    success: function (data) {
                                        if (data.code === 1) {
                                            if(callback){
                                                callback();
                                            }
                                        }else{
                                            alert(data.msg);
                                        }
                                    }
                                });
                            });
                        } else {
                            // Response error!!!
                        }
                    }
                }, {scope: 'email'});
            };

            this.share = function (shareLink, callback) {
                FB.ui({
                    method: 'share',
                    href: shareLink,
                    mobile_iframe: false,
                    hashtag: '#DaHuong_LanToaHanhPhuc'
                }, function (response) {
                    if(response && !response.error_message) {
                        if(callback){
                            callback();
                        }
                    }
                });
            };
        };

        // Submit function
        this.contest = new function () {
            this.submitId = null;
            this.modal = $('#voteModal');

            this.vote = function (submitId) {
                $.app.contest.submitId = submitId;

                var _cb = function(data){
                    $('#voteModal').modal('show');
                    $('#voteModal .modal-title').html('Xác nhận Vote');

                    // End vote time
                    if(data.code === 3){
                        $('#voteModal .alert-message').removeClass('hidden').html('Đã hết thời gian Vote cho bài thi');
                        $('#voteModal .alert-form').addClass('hidden');
                    }else{
                        $('#voteModal .alert-message').addClass('hidden');
                        $('#voteModal .alert-form').removeClass('hidden');

                        // Init vote form
                        var _cbCatpcha = function(captchaRespondCode){
                            if (captchaRespondCode !== ""){
                                $('#voteModal .alert-message').removeClass('hidden').html('Đang xử lý...');
                                $('#voteModal .alert-form').addClass('hidden');

                                var _cbVote = function(resVote){
                                    $('#voteModal .alert-message').html(resVote.message);

                                    if (resVote.code){
                                        $('.submit-votes-' + $.app.contest.submitId).html(resVote.count);
                                    }else{

                                    }
                                };

                                $.app.ajax(null, 'bai-du-thi/vote', {id: $.app.contest.submitId, 'g-recaptcha-response': captchaRespondCode}, _cbVote, 'get', '');
                            }
                        };

                        $.app.recaptcha.init('recaptchaVote', _cbCatpcha);
                    }
                };

                $.app.checkLogin(null, _cb, submitId);
                return false;
            };

            this.share = function (shareLink, submitId) {
                $.app.contest.submitId = submitId;

                var _cb = function(data){
                    var _cbShare = function(){
                        $('#voteModal').modal('show');
                        $('#voteModal .modal-title').html('Xác nhận Share');
                        $('#voteModal .alert-form').addClass('hidden');
                        $('#voteModal .alert-message').removeClass('hidden').html('Đang xử lý...');

                        var _cbShare = function(resShare){
                            $('#voteModal .alert-message').html(resShare.message);

                            if (resShare.code){
                                $('.submit-shares-' + $.app.contest.submitId).html(resShare.count);
                            }
                        };

                        $.app.ajax(null, 'bai-du-thi/share', {id: $.app.contest.submitId}, _cbShare, 'get', '');
                    };

                    $.app.facebook.share(shareLink, _cbShare);
                };

                $.app.checkLogin(null, _cb, submitId);
                return false;
            };
        };
        
        this.recaptcha = new function () {
            this.object = null;

            this.init = function(initDivId, verifyCallback){
                if($.app.recaptcha.object !== null) {
                    grecaptcha.reset($.app.recaptcha.object);
                }else{
                    $.app.recaptcha.object = grecaptcha.render(initDivId, {
                        'sitekey': $.app.vars.captcha_sitekey,
                        'theme': 'light',
                        'callback': verifyCallback
                    });
                }
            }
        };

        this.cookie = new function(){
            this.domain = '';

            this.getCoookie = function (name, defaultValue) {

            };

            this.setCoookie = function (name, defaultValue) {

            };
        };

        this.upload = new function(){
            this.initForm = function(){
                if(!$('#ajaxUploadForm').length){
                    $('body').append('<form id="ajaxUploadForm" action="process.php" class="hidden" method="post" enctype="multipart/form-data">\n' +
                        '        <input name="upload-file" id="upload-file" type="file"  />\n' +
                        '    </form>');
                }
            };

            this.uploadImage = function (progressClass, callback, conditions) {
                conditions = conditions || {};
                $.app.upload.initForm();
                $(progressClass).html('<div id="progress-wrp"><div class="progress-bar"></div ><div class="status">0%</div></div>').hide();

                var $uploadForm = $('#ajaxUploadForm');

                $uploadForm.find('#upload-file').unbind('change').bind('change', function () {
                    var file = $uploadForm.find('#upload-file').get(0).files[0],
                        formData = new FormData();

                    formData.append('upload-file', file);
                    formData.append('upType', 'image');

                    if (conditions) {
                        $.each(conditions, function (k, v) {
                            formData.append(k, v);
                        });
                    }

                    $.ajax( {
                        url        : $.app.vars.url + 'upload',
                        type       : 'POST',
                        contentType: false,
                        cache      : false,
                        processData: false,
                        data       : formData,
                        mimeType:"multipart/form-data",
                        xhr        : function () {
                            $(progressClass).show();

                            //upload Progress
                            var xhr = $.ajaxSettings.xhr();
                            if (xhr.upload) {
                                xhr.upload.addEventListener('progress', function(event) {
                                    var percent = 0;
                                    var position = event.loaded || event.position;
                                    var total = event.total;
                                    if (event.lengthComputable) {
                                        percent = Math.ceil(position / total * 100);
                                    }
                                    //update progressbar
                                    $("#progress-wrp .progress-bar").css("width", + percent +"%");
                                    $("#progress-wrp .status").text(percent +"%");
                                }, true);
                            }
                            return xhr;
                        },
                        success: function (res) {
                            res = $.parseJSON(res);

                            //Do something success-ish
                            $(progressClass).hide();

                            if(!res.code){
                                $(progressClass).show().html('<div class="alert alert-danger" style="padding:2px; maring:0px;">'+res.message+'</div>');
                            }

                            if(callback){
                                callback(res);
                            }
                        }
                    });

                    return false;
                });
                
                $uploadForm.find('#upload-file').val('');
                $uploadForm.find('#upload-file').trigger('click');

                return false;
            };
        };
    };
})(jQuery);

// Play video in home header
(function ($) {
    var width = jQuery('.home-video').width();
    var height = width * (9/16);

    jQuery('.youtube_player1').attr('width', width);
    jQuery('.youtube_player1').attr('height', height);

    // if(jQuery(window).width() < 768 ){
    //     document.querySelector('#youtube_player1').autoplay = false;
    // }else{
    //     document.querySelector('#youtube_player1').autoplay = true;
    // }

    if(jQuery('#youtube_player1').length) {
        var $player1 = new MediaElementPlayer('youtube_player1', {
            videoWidth: width,
            videoHeight: height,
            shimScriptAccess: 'always',
            poster: './assets/img_home/img.png',
            showPosterWhenEnded: true,
            loop: true,
            isMuted: true,
            success: function (media, node, player) {
                jQuery('#' + node.id + '-mode').html('mode: ' + media.pluginType);
                jQuery('.mejs__controls').css("visibility", "hidden");
                jQuery('.mejs__overlay-button').css("visibility", "hidden");
                 media.isMuted == false;


                media.addEventListener('play', function() {
                    // jQuery('.home-video__play').css('display','none');
                    // jQuery('.home-video__icon').css('display','none');
                    // jQuery('.mejs__controls').css("visibility", "inherit");
                    // jQuery('.mejs__overlay-button').css("visibility", "inherit");
                    player.setVolume(0);
                }, true);

                jQuery('.home-video__icon__2').click(function(){
                    var src_imgvd = jQuery('.sound').attr('src');
                    if(src_imgvd == './assets/img_home/sound.png') {
                        jQuery('.sound').attr('src','./assets/img_home/icon4.png');
                        player.setVolume(0);
                    }else{
                        jQuery('.sound').attr('src','./assets/img_home/sound.png');
                        player.setVolume(1);
                    }

                });

                jQuery('.home-video__play').click(function(){
                    // player.play();
                    jQuery("#myModal-vdheader").modal('show');
                });
            }
        });
    }
    // jQuery('.container--posi').click(function(){
    //     window.location.href="https://www.youtube.com/watch?v=A-pDVJP-4MQ&t=22s";
    // })


    // video header mobile
    var widths = jQuery('#myModal-vdheader').width() - 30;
    var heights = widths * (9/16);

    jQuery('.youtube_players').attr('width', widths);
    jQuery('.youtube_players').attr('height', heights);
    if(jQuery('#youtube_players').length) {
        var $player7 = new MediaElementPlayer('youtube_players', {
            videoWidth: widths,
            videoHeight: heights,
            shimScriptAccess: 'always',
            success: function (media, node, player) {
                jQuery('#' + node.id + '-mode').html('mode: ' + media.pluginType);
            }
        });
    }

    // End

    //video vechungtoi
    var width = jQuery('.home-video').width();
    var height = width * (9/16);

    jQuery('.youtube_player2').attr('width', width);
    jQuery('.youtube_player2').attr('height', height);

    if(jQuery('#youtube_player2').length) {
        var $player1 = new MediaElementPlayer('youtube_player2', {
            videoWidth: width,
            videoHeight: height,
            shimScriptAccess: 'always',
            poster: '../assets/img_home/img.png',
            showPosterWhenEnded: true,
            success: function (media, node, player) {
                jQuery('#' + node.id + '-mode').html('mode: ' + media.pluginType);
                jQuery('.mejs__controls').css("visibility", "hidden");
                jQuery('.mejs__overlay-button').css("visibility", "hidden");
                media.addEventListener('play', function() {
                    // jQuery('.home-video__icon').css('display','none');
                    jQuery('.home-video__play').css('display','none');
                    jQuery('.mejs__controls').css("visibility", "inherit");
                    jQuery('.mejs__overlay-button').css("visibility", "inherit");
                    //player.setVolume(0);
                    jQuery('.sound1').attr('src','../assets/img_home/sound.png');
                    player.setVolume(1);
                }, true);

                jQuery('.home-video__icon__2_1').click(function(){
                    var src_imgvd = jQuery('.sound1').attr('src');
                    console.log('dfdf');
                    if(src_imgvd == '../assets/img_home/icon4.png') {
                        jQuery('.sound1').attr('src','../assets/img_home/sound.png');
                        player.setVolume(1);
                    }else{
                        jQuery('.sound1').attr('src','../assets/img_home/icon4.png');
                        player.setVolume(0);
                    }
                });

                jQuery('.home-video__play').click(function(){
                    player.play();
                });
            }
        });
    }

    var width = jQuery('.ifram-container').width();
    var height = width * (9/16);
    jQuery('.youtube_player_detail').attr('width', width);
    jQuery('.youtube_player_detail').attr('height', height);

    if(jQuery('#youtube_player_detail').length) {
        var $youtube_player_detail = new MediaElementPlayer('youtube_player_detail', {
            videoWidth: width,
            videoHeight: height,
            shimScriptAccess: 'always',
            success: function (media, node, player) {
                jQuery('#' + node.id + '-mode').html('mode: ' + media.pluginType);
            }
        });
    }

})(jQuery);


// Play video in home content
(function ($) {
    jQuery('.home-box-four__item').click(function(){
        hoverVideo(this);
    });

    jQuery('.home-box-four__item').hover(function(){
        jQuery(this).css('background-color','#d9d9d9');
    },function(){
        if(!jQuery(this).hasClass('active')){
            jQuery(this).css('background-color','#e7e7e7');
        }
    });

    jQuery('.home-box-four__item:first-child').addClass('active');
    jQuery('.home-box-four__item:first-child').find('.hover-slide').css('display','flex');

    function hoverVideo(obj) {
        var src = jQuery(obj).find('.src').text();

        var box_left = jQuery(obj).parents('.home-box-four__right').prev('.home-box-four__left');
        var box_right = jQuery(obj).parents('.home-box-four__right');

        box_left.find('source').attr('src',src);
        if(box_left.find('#youtube_player7').length) {
            $player7.pause();
            $player7.setSrc(src);
            $player7.media.load();
            $player7.play();
        }else{
            $player7.pause();
            $player7.setSrc(src);
            $player7.media.load();
            $player7.play();
        }

        jQuery('.hover-slide').css('display','none');
        jQuery(obj).find('.hover-slide').css('display','flex');
        jQuery('.home-box-four__item').css('background-color','#e7e7e7');
        jQuery(obj).css('background-color','#d9d9d9').addClass('active');
    }


    var width = jQuery('.home-box-four__left').width();
    var height = width * (9/16);
    jQuery('.youtube_player').attr('width', width);
    jQuery('.youtube_player').attr('height', height);
    if(jQuery(window).width() < 768 ){
        jQuery('.home-box-four__right').css('height', 'inherit').css('max-height','600px').css('overflow','auto');
    } else {
        jQuery('.home-box-four__right').css('height', height);
    }


    if(jQuery('#youtube_player7').length) {
        var $player7 = new MediaElementPlayer('youtube_player7', {
            videoWidth: width,
            videoHeight: height,
            shimScriptAccess: 'always',
            success: function (media, node, player) {
                jQuery('#' + node.id + '-mode').html('mode: ' + media.pluginType);
            }
        });
    }

    // change to slide video in mobile
    if(jQuery(window).width() < 768 ){
        jQuery('.home-box-four__right').css('height','inherit');
        jQuery('.home-box-four__right__list').addClass('slick-0').css('overflow','hidden');

        // jQuery('.home-box-row-one').addClass('slick-3').css('overflow','hidden');

        jQuery('.home-box-four__item').unbind('click');
        jQuery('.home-box-four__item').hover(function(){
            hoverVideo(this);
        });
    }


    // slide
    function slick1Init0() {
        jQuery('.slick-0').slick({
            autoplay:true,
            autoplaySpeed:5000,
            infinite: true,
            slidesToShow: 3,
            infinite: false,
            prevArrow:'<div class="prev"  role="presentation" style="position: absolute; top:0; left:0;margin:auto;height:10px;z-index: 99; cursor: pointer"><img src="./assets/img_home/icon23.png" alt="image" class="image_prev"></div>',
            nextArrow:'<div class="next"  role="presentation" style="position: absolute; top:0;right:0;margin:auto;height:10px;z-index: 99; cursor: pointer"><img src="./assets/img_home/icon24.png" alt="image" class="image_prev"></div>',
            responsive: [
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                    }
                }
            ]
        });
    }

    function slickInit() {
        $('.frame-slider').slick({
            dots: false,
            infinite: true,
            // speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            // autoplaySpeed: 2000,
            arrows: true,
            responsive: [{
                breakpoint: 320,
                settings: {
                    arrows: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
                {
                    breakpoint: 767,
                    settings: {
                        arrows: true,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        arrows: true,
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }]
        });
    }

    function slick1Init2() {
        jQuery('.slick-2').slick({
            autoplay:true,
            autoplaySpeed:4000,
            infinite: true,
            slidesToShow: 4,
            infinite: false,
            arrows: true,
            prevArrow:'<div class="prev"  role="presentation" style="position: absolute; top:0; left:0;margin:auto;height:10px;z-index: 999; cursor: pointer"><img src="./assets/img_home/icon23.png" alt="image" class="image_prev"></div>',
            nextArrow:'<div class="next"  role="presentation" style="position: absolute; top:0;right:0;margin:auto;height:10px;z-index: 999; cursor: pointer"><img src="./assets/img_home/icon24.png" alt="image" class="image_prev"></div>',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                    }
                }
            ]
        });
    }

    function slick1Init3() {
        jQuery('.slick-3').slick({
            autoplay:true,
            autoplaySpeed:5000,
            infinite: true,
            slidesToShow: 3,
            infinite: false,
            prevArrow:'<div class="prev"  role="presentation" style="position: absolute; top:0; left:0;margin:auto;height:10px;z-index: 99; cursor: pointer"><img src="./assets/img_home/icon23.png" alt="image" class="image_prev"></div>',
            nextArrow:'<div class="next"  role="presentation" style="position: absolute; top:0;right:0;margin:auto;height:10px;z-index: 99; cursor: pointer"><img src="./assets/img_home/icon24.png" alt="image" class="image_prev"></div>',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                    }
                }
            ]
        });
    }

    function slick1Init4() {
        jQuery('.slick-4').slick({
            autoplay:true,
            autoplaySpeed:5000,
            infinite: true,
            slidesToShow: 4,
            infinite: false,
            prevArrow:'<div class="prev"  role="presentation" style="position: absolute; top:0; left:0;margin:auto;height:10px;z-index: 99; cursor: pointer"><img src="./assets/img_home/icon23.png" alt="image" class="image_prev"></div>',
            nextArrow:'<div class="next"  role="presentation" style="position: absolute; top:0;right:0;margin:auto;height:10px;z-index: 99; cursor: pointer"><img src="./assets/img_home/icon24.png" alt="image" class="image_prev"></div>',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                    }
                }
            ]
        });
    }

    slick1Init0();
    slickInit();
    slick1Init2();
    slick1Init3();
    slick1Init4();

    //điều chỉnh vị trí nút prev, next cùa slicklInit
    if(jQuery(window).width() < 768 ){
        var height_image = jQuery('.home-box-four-2-img').height();
        jQuery('.slick-2 .slick-arrow').css('top', (height_image - 26)/2);

        var height_image1 = jQuery('.home-box-one__img').height();
        jQuery('.slick-3 .slick-arrow').css('top', (height_image1 - 26)/2);

        var height_image2 = jQuery('.slick4-img').height();
        jQuery('.slick-4 .slick-arrow').css('top', (height_image2 - 26)/2);

        var height_image3 = jQuery('.home-box-four__right').height();
        jQuery('.slick-0 .slick-arrow').css('top', (height_image3 - 26)/2);
    }else{
        var height_image = jQuery('.home-box-four-2-img').height();
        jQuery('.slick-2 .slick-arrow').css('top', (height_image - 74)/2);

        var height_image1 = jQuery('.home-box-one__img').height();
        jQuery('.slick-3 .slick-arrow').css('top', (height_image1 - 74)/2);

        var height_image2 = jQuery('.slick4-img').height();
        jQuery('.slick-4 .slick-arrow').css('top', (height_image2 - 74)/2);

        var height_image3 = jQuery('.home-box-four__right').height();
        jQuery('.slick-0 .slick-arrow').css('top', (height_image3 - 74)/2);
    }



    // menu header
    // function classActive() {
    //     var width_max = jQuery('.header__menu__li:first-child').width();
    //     var width_const = width_max + 20;
    //     jQuery('.header__menu__li .active').css('height', width_const).css('width', width_const);
    //
    //     jQuery('.header .active').each(function(){
    //         var width = jQuery(this).parent().width();
    //         var left = (width_const - width) / 2;
    //
    //         if (left < 0) {
    //             jQuery(this).css('left', left+'px');
    //         } else {
    //             jQuery(this).css('left', -left+'px');
    //         }
    //         jQuery(this).parents('a').css('color','#dc5ca1');
    //     });
    // }
    // classActive();
    //
    // jQuery('.header__menu__li').hover(function(){
    //     console.log('hover');
    //     if(!jQuery(this).hasClass('li-active')) {
    //         jQuery(this).append('<div class="active"></div>');
    //         classActive();
    //     }
    // },function(){
    //     if(!jQuery(this).hasClass('li-active')){
    //         jQuery(this).find('.active').remove();
    //         jQuery(this).parents('a').css('color','#a974bd');
    //     }
    // });
    jQuery('.home-video__icon__3').click(function(){
        jQuery("html, body").animate({
            scrollTop: jQuery('.content__one').offset().top
        }, 1000);
    })
    //config box three home
    if(jQuery(window).width() < 768 ){
        var width_box_three = jQuery('.box-three').width() + 80;
        var height_box_three = jQuery('.box-three').height();
        jQuery('.box-three-left').css('height',height_box_three);
        jQuery('.box-three__background').css('width', width_box_three + 100);
        jQuery('.box-three__background').css('height', height_box_three + 90).css('top','-45px').css('left','-70px');
        jQuery('.box-three-top__img2').attr('src','./assets/img_home/img58.png');
        jQuery('.box-three-top__img2').css("z-index", 3);
    }else{
        var width_box_three = jQuery('.box-three').width() + 80;
        var height_box_three = jQuery('.box-three').height();
        jQuery('.box-three-left').css('height',height_box_three);
        jQuery('.box-three__background').css('width', width_box_three + 100);
        jQuery('.box-three__background').css('height', height_box_three + 90).css('top','-45px').css('left','-70px');
    }

})(jQuery);