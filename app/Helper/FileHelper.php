<?php

namespace App\Helper;

use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use File;

class FileHelper
{
    public static function processCropImage($inputImage){
        $check_url_image = strpos($inputImage, 'media/');
        if($check_url_image !== false) {
            return $inputImage;
        } else {
            // Image select and it's base64
            $imagebase64 = explode(',', $inputImage);
            if (count($imagebase64) >= 2) {
                $imagebase64 = base64_decode($imagebase64[1]);
                //$imagebase64 = imagecreatefromstring($imagebase64);

                if ($imagebase64) {
                    $fileName = time().'.jpg';
                    $filePath = 'media/'.date('Y-m').'/'.date('d').'/'.$fileName;

                    self::saveFile($filePath, $imagebase64);
                    return $filePath;
                }
            }
        }

        return null;
    }

    // Compress image return path image or base64 encode image
    public static function compressImage($source, $destination = null, $quality = 75)
    {
        $info = getimagesize($source);

        if ($info['mime'] == 'image/jpeg')
            $image = imagecreatefromjpeg($source);

        elseif ($info['mime'] == 'image/gif')
            $image = imagecreatefromgif($source);

        elseif ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source);

        $old_x = $info[0];
        $old_y = $info[1];

        $new_x = 1910;
        $new_y = 2000;

        if ($old_x > $old_y) {
            $thumb_w = $new_x;
            $thumb_h = $old_y * ($new_x / $old_x);
        }

        if ($old_x < $old_y) {
            $thumb_w = $old_x * ($new_y / $old_y);
            $thumb_h = $new_y;
        }

        if ($old_x == $old_y) {
            $thumb_w = $new_x;
            $thumb_h = $new_y;
        }

        $dst_img = ImageCreateTrueColor($thumb_w, $thumb_h);

        imagecopyresampled($dst_img, $image, 0, 0, 0, 0, $thumb_w, $thumb_h, $old_x, $old_y);

        if (!empty($destination)) {
            imagejpeg($dst_img, $destination, $quality);
        } else {
            ob_start();
            imagejpeg($dst_img, NULL, $quality);
            $image_data = ob_get_contents();
            ob_end_clean();
            $destination = base64_encode($image_data);
        }
        return $destination;
    }

    public static function imagecreatefromfile($filename)
    {
        if (!is_bool(strpos('http', $filename))) {
            return imagecreatefromjpeg($filename);
        } else {
            if (!file_exists($filename)) {
                return false;
//                throw new InvalidArgumentException('File "' . $filename . '" not found.');
            }
            switch (strtolower(pathinfo($filename, PATHINFO_EXTENSION))) {
                case 'jpeg':
                case 'jpg':
                    return imagecreatefromjpeg($filename);
                    break;

                case 'png':
                    return imagecreatefrompng($filename);
                    break;

                case 'gif':
                    return imagecreatefromgif($filename);
                    break;

                default:
                    throw new InvalidArgumentException('File "' . $filename . '" is not valid jpg, png or gif image.');
                    break;
            }
        }
    }

    function createThumbnail($image_name, $new_width, $new_height, $uploadDir, $moveToDir)
    {
        $path = $uploadDir . '/' . $image_name;

        $mime = getimagesize($path);

        if ($mime['mime'] == 'image/png') {
            $src_img = imagecreatefrompng($path);
        }
        if ($mime['mime'] == 'image/jpg' || $mime['mime'] == 'image/jpeg' || $mime['mime'] == 'image/pjpeg') {
            $src_img = imagecreatefromjpeg($path);
        }

        $old_x = imageSX($src_img);
        $old_y = imageSY($src_img);

        if ($old_x > $old_y) {
            $thumb_w = $new_width;
            $thumb_h = $old_y * ($new_height / $old_x);
        }

        if ($old_x < $old_y) {
            $thumb_w = $old_x * ($new_width / $old_y);
            $thumb_h = $new_height;
        }

        if ($old_x == $old_y) {
            $thumb_w = $new_width;
            $thumb_h = $new_height;
        }

        $dst_img = ImageCreateTrueColor($thumb_w, $thumb_h);

        imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $thumb_w, $thumb_h, $old_x, $old_y);


        // New save location
        $new_thumb_loc = $moveToDir . $image_name;

        if ($mime['mime'] == 'image/png') {
            $result = imagepng($dst_img, $new_thumb_loc, 8);
        }
        if ($mime['mime'] == 'image/jpg' || $mime['mime'] == 'image/jpeg' || $mime['mime'] == 'image/pjpeg') {
            $result = imagejpeg($dst_img, $new_thumb_loc, 80);
        }

        imagedestroy($dst_img);
        imagedestroy($src_img);

        return $result;
    }

    public static function uploadImage($fileName = 'image_upload', $uploadFolder = 'products', $arraySize = null, $ratio = null, $filename = null)
    {
        $mineTypes = [
            'mines' => ' image/png, image/gif, image/jpg, image/jpeg, image/bmp, image/x-bmp, image/x-bitmap',
            'ext' => ' jpg, jpeg, png, gif, bmp'
        ];

        if (Input::hasFile($fileName)) {
            $image = Input::file($fileName);

            // Get type mine and ext file image
            $mineTypeFile = strtolower($image->getClientMimeType());
            $fileExt = strtolower($image->getClientOriginalExtension());

            // Check type mine and ext file image
            if (!(strpos($mineTypes['mines'], $mineTypeFile) === false || strpos($mineTypes['ext'], $fileExt) === false)) {
                $filePath = 'uploads/' . $uploadFolder . '/' . date('Y-m') . '/';
                $filePathFull = StringHelper::getUploadPath($filePath);
                if (!File::isDirectory($filePathFull)) {
                    File::makeDirectory($filePathFull);
                    File::put($filePathFull . 'index.html', '');
                }

                if (!$filename) {
                    $filename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME) . time();
                }
                $filename = StringHelper::removeSign($filename, true, 50);

                $percentRatio = null;
                $imageSize = getimagesize($image);
                if ($ratio != null && is_string($ratio) && is_array(explode(':', $ratio))) {
                    $percentRatio = explode(':', $ratio);
                }

                if ($arraySize && count($arraySize) > 0) {
                    foreach ($arraySize as $s => $size) {
                        if (is_array($size)) {
                            if (is_array($percentRatio) && $size['w'] && $size['h']) {
                                if ($size['w'] > $size['h']) {
                                    $size['h'] = $size['w'] * $percentRatio[1] / $percentRatio[0];
                                } else {
                                    $size['w'] = $size['h'] * $percentRatio[0] / $percentRatio[1];
                                }
                            }
                            $arrayFile = array(
                                'filename' => $filename . $size['w'] . 'x' . $size['h'] . '-' . $s . '.' . $fileExt,
                                'width' => $size['w'],
                                'height' => $size['h']
                            );

                            $resultPathImage = $filePath . $arrayFile['filename'];

                            if (is_bool($ratio)) {
                                $make = Image::make($image->getRealPath())->resize($arrayFile['width'], $arrayFile['height'], function ($constraint) {
                                    $constraint->aspectRatio();
                                })->save(StringHelper::getUploadPath($resultPathImage));
                            } else {
                                $make = Image::make($image->getRealPath())->resize($arrayFile['width'], $arrayFile['height'])
                                    ->save(StringHelper::getUploadPath($resultPathImage));
                            }
                        }
                    }
                } else {
                    $resultPathImage = $filePath . $filename . '.' . $fileExt;
                    if (is_array($percentRatio)) {
                        if ($imageSize[0] > $imageSize[1]) {
                            $w = $imageSize[0];
                            $h = $imageSize[0] * $percentRatio[1] / $percentRatio[0];
                        } else {
                            $w = $imageSize[1];
                            $h = $imageSize[1] * $percentRatio[0] / $percentRatio[1];
                        }
                        $make = Image::make($image->getRealPath())->resize($w, $h)
                            ->save(StringHelper::getUploadPath($resultPathImage));
                    } else {
                        $make = Image::make($image->getRealPath())->save(StringHelper::getUploadPath($resultPathImage));
                    }

                }
            } else {
                // $make = $image->move(StringHelper::getUploadPath($filePath), $filename);
                // Not allow image
                $make = false;
            }

            if ($make) {
                return $resultPathImage;
            }
        }

        return false;
    }

    public static function createImageSubmit($userId, $image1, $image2, $catId)
    {
        return true;
        // Thiet lap cac thong tin kich thuoc chung
        $bg_w = 800;
        $bg_h = 420;
        $margin = 35;

        // Tao background chung cho image de merge
        $outputImage = imagecreatetruecolor($bg_w, $bg_h);

        $bgUrl = public_path() . '/assets/images/bgsubmit/bg-' . $catId . '.png';
        $image1 = public_path() . '/' . $image1;
        $image2 = public_path() . '/' . $image2;

        list($icon1_w, $icon1_h) = getimagesize($image1);
        list($icon2_w, $icon2_h) = getimagesize($image2);

        $first = @imagecreatefrompng($bgUrl);
        $second = @FileHelper::imagecreatefromfile($image1);
        $third = @FileHelper::imagecreatefromfile($image2);

        // thu nho anh 105x105
        $icon2_size_thumbnail = 105;
        $third_ = ImageCreateTrueColor($icon2_size_thumbnail, $icon2_size_thumbnail);
        imagecopyresampled($third_, $third, 0, 0, 0, 0, $icon2_size_thumbnail, $icon2_size_thumbnail, $icon2_w, $icon2_h);
        // cat vong tron anh
        $third__ = new ImageCircleCrop(null, $third_);
        $third__->circleCrop();

        imagecopyresampled($outputImage, $second, 77, 55, 0, 0, 310, 310, $icon1_w, $icon1_h);
        imagecopymerge($outputImage, $third__->img, 55, 263, 0, 0, $icon2_size_thumbnail, $icon2_size_thumbnail, 100);
        imagecopy($outputImage, $first, 0, 0, 0, 0, $bg_w, $bg_h);

        FileHelper::checkDirectory('uploads/submit');
        $des = '/uploads/submit/' . date('Y-m') . '/' . date('d') . '/';

        $fullFileName = public_path() . $des . $userId . '-sharefb-s1.png';
        imagepng($outputImage, $fullFileName);

        imagedestroy($first);
        imagedestroy($second);
        imagedestroy($third);
        imagedestroy($outputImage);

        return $des . $userId . '-sharefb-s1.png';
    }

    public static function createImageSubmit2($userAvatar, $userId, $submitId, $catId)
    {
        // Thiet lap cac thong tin kich thuoc chung
        $bg_w = 1001;
        $bg_h = 701;

        // Tao background chung cho image de merge
        $outputImage = imagecreatetruecolor($bg_w, $bg_h);
        $bgUrl = public_path() . '/assets/images/bg/bg-round2-' . $catId . '.png';

        $userAvatar = explode('?rand=', $userAvatar);
        $userAvatar = $userAvatar[0];
        if (!$userAvatar) return;

        $avatarUrl = $userAvatar;
        if (!is_bool(strpos('http', $userAvatar))) {
            $avatarUrl = public_path() . '/' . $userAvatar;
        }

        $first = @imagecreatefrompng($bgUrl);
        $second = @FileHelper::imagecreatefromfile($avatarUrl);

        // Set transparent image png
        imagealphablending($first, true);
        imagesavealpha($first, true);

        list($avatar_w, $avatar_h) = getimagesize($avatarUrl);

        imagecopyresampled($outputImage, $second, 145, 222, 0, 0, 376, 378, $avatar_w, $avatar_h);
        imagecopy($outputImage, $first, 0, 0, 0, 0, $bg_w, $bg_h);

        $des = StringHelper::getUploadPath('uploads/content/submit/');
        if (!File::isDirectory($des)) {
            File::makeDirectory($des);
            File::put($des . 'index.html', '');
        }
        imagejpeg($outputImage, $des . 'vong2-' . $userId . '.jpg');

        imagedestroy($first);
        imagedestroy($second);
        imagedestroy($outputImage);

        return $des . 'vong2-' . $userId . '.jpg';
    }

    public static function createImageFrame($submitId, $orgImage, $frame)
    {
        if(!$frame) return $orgImage;

        $bg_w = 600;
        $bg_h = 400;

        $bgUrl = public_path($orgImage);
        $image1 = public_path( 'assets/images/frame/frame-0' .$frame . '.png' );
        if(!File::exists($image1)){
            return $orgImage;
        }

        $first = imagecreatefrompng($bgUrl);
        $second = imagecreatefrompng($image1);

        imagealphablending($first, true);
        imagesavealpha($first, true);

        imagecopy($first, $second, 0, 0, 0, 0, 600, 400);

        FileHelper::checkDirectory('uploads/submit');
        $des = '/uploads/submit/' . date('Y-m') . '/' . date('d') . '/';

        $fullFileName = public_path() . $des . $submitId . '-share.png';
        imagepng($first, $fullFileName);

        imagedestroy($first);

        return $des . $submitId . '-share.png';
    }

    public static function checkDirectory($des, $checkDateFolder = true)
    {
        $folder = StringHelper::getUploadPath($des);
        if (!File::isDirectory($folder)) {
            File::makeDirectory($folder);
            File::put($folder . '/index.html', '');
        }

        if ($checkDateFolder) {
            $folder = StringHelper::getUploadPath($des . '/' . date('Y-m'));
            if (!File::isDirectory($folder)) {
                File::makeDirectory($folder);
                File::put($folder . '/index.html', '');
            }

            $folder = StringHelper::getUploadPath($des . '/' . date('Y-m') . '/' . date('d'));
            if (!File::isDirectory($folder)) {
                File::makeDirectory($folder);
                File::put($folder . '/index.html', '');
            }
        }
    }

    public static function checkStorageDirectory($des, $checkDateFolder = true)
    {
        $folder = storage_path($des);
        if (!File::isDirectory($folder)) {
            File::makeDirectory($folder);
            File::put($folder . '/index.html', '');
        }

        if ($checkDateFolder) {
            $folder = storage_path($des . '/' . date('Y-m'));
            if (!File::isDirectory($folder)) {
                File::makeDirectory($folder);
                File::put($folder . '/index.html', '');
            }

            $folder = storage_path($des . '/' . date('Y-m') . '/' . date('d'));
            if (!File::isDirectory($folder)) {
                File::makeDirectory($folder);
                File::put($folder . '/index.html', '');
            }
        }

        return [$folder, $des . ($checkDateFolder ? '/' . date('Y-m') . '/' . date('d') : '')];
    }

    public static function getStorage(){
        return Storage::disk('public');
    }

    public static function saveFile($savePath, $content){
        return self::getStorage()->put($savePath, $content);
    }

    public static function deleteFile($filePath){
        return self::getStorage()->delete($filePath);
    }

    public static function exists($filePath){
        return self::getStorage()->has($filePath);
    }

    public static function hasFile($filePath){
        return self::getStorage()->has($filePath);
    }

    public static function getFile($filePath){
        return self::getStorage()->get($filePath);
    }

    public static function mimeType($filePath){
        return self::getStorage()->mimeType($filePath);
    }

    public static function size($filePath){
        return self::getStorage()->size($filePath);
    }

    public static function url($filePath){
        return self::getStorage()->url($filePath);
    }
}