<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 7/23/2016
 * Time: 8:45 AM
 */

namespace App\Helper;

class StringHelper
{
    public static function getColorCategories($cat_id = null){
        $colors = [
            1 => ['text-theme-1', '#52e34a'],
            2 => ['text-theme-2', '#fdb418'],
            3 => ['text-theme-3', '#76e0f5']
        ];
        if($cat_id != null) {
            return $colors[$cat_id];
        }
        return $colors;
    }

    public static function orderStatusText($status){
        $orderStatus = \App\Helper\StringHelper::orderStatus();

        if(isset($orderStatus[$status])){
            $statusText = $orderStatus[$status];
            $class = '';
            switch($status){
                case -2 : {
                    $class = 'danger';
                    break;
                }
                case -1 : {
                    $class = 'warning';
                    break;
                }
                case 0 : {
                    $class = 'default';
                    break;
                }
                case 1 : {
                    $class = 'info';
                    break;
                }
                case 2 : {
                    $class = 'primary';
                    break;
                }
                case 3 : {
                    $class = 'success';
                    break;
                }
            }

            return "<label style='font-size: 12px; font-weight: normal' class='label label-{$class}'>{$statusText}</label>";
        }

        return '';
    }

    public static function getUploadPath($filePath){
        return public_path( str_replace('/', DIRECTORY_SEPARATOR, $filePath) );
    }

    public static function stateStatus($status='', $type=1, $id=''){
        if($type == 2) {
            $groupButton =
                '<div class="btn-group">' .
                    '<a data-active="btn-warning" data-link="field=status&value=-1&id='.$id.'" type="button" class="ajax-reload btn btn-xs  '.((is_numeric($status) && $status == -1) ? 'btn-warning' : 'btn-default') .'">Chờ duyệt</a>' .
                    '<a data-active="btn-danger" data-link="field=status&value=0&id='.$id.'" type="button" class="ajax-reload btn-ajax btn btn-xs '.((is_numeric($status) && $status == 0) ? 'btn-danger' : 'btn-default') .'">Tắt</a>' .
                    '<a data-active="btn-success" data-link="field=status&value=1&id='.$id.'" type="button" class="ajax-reload btn-ajax btn btn-xs '.((is_numeric($status) && $status == 1) ? 'btn-success' : 'btn-default') .'">Kích hoạt</a>' .
                '</div>';
            return $groupButton;
        }
        $statusArray = array(
            -1 => '<label class="btn btn-xs btn-default">Chờ duyệt</label>',
            1 => '<label class="btn btn-xs btn-success">Kích hoạt</label>',
            0 => '<label class="btn btn-xs btn-danger">Tắt</label>'
        );

        return (is_numeric($status)) ? $statusArray[$status] : $statusArray;
    }

    public static function stateYesNo($status = '', $type = 1, $id = '', $field = 'status')
    {
        if ($type == 2) {
            $groupButton = '
                <div class="btn-group">' .
                    '<a data-active="btn-success" data-link="field=' . $field . '&value=1&id=' . $id . '" type="button" class="ajax-reload btn-ajax btn btn-xs ' . ((is_numeric($status) && $status == 1) ? 'btn-success' : 'btn-default') . '">Có</a>' .
                    '<a data-active="btn-danger" data-link="field=' . $field . '&value=0&id=' . $id . '" type="button" class="ajax-reload btn-ajax btn btn-xs ' . ((is_numeric($status) && $status == 0) ? 'btn-danger' : 'btn-default') . '">Không</a>' .
                '</div>
            ';

            return $groupButton;
        }

        $statusArray = array(
            1 => '<label class="btn btn-xs btn-success">Có</label>',
            0 => '<label class="btn btn-xs btn-danger">Không</label>'
        );

        return (is_numeric($status)) ? $statusArray[$status] : $statusArray;
    }

    // Convert date
    public static function date($date, $format = 'd-m-Y H:i'){
        return date($format, strtotime($date));
    }

    // Convert to Vietnamese number format
    public static function money($string){
        return number_format($string, 0, ',', '.');
    }

    public static function limit($text, $limit = 30) {
        if(mb_strlen($text, 'utf-8') >= $limit){
            $text = mb_substr($text, 0, $limit, 'utf-8');
            $post = mb_strrpos($text, ' ');
            $text = mb_substr($text, 0, $post).'...';
        }

        return $text;
    }

    public static function trim($text){
        if(is_array($text)){
            foreach ($text as $k => $t){
                if(!is_array($t)) {
                    $text[$k] = trim($t);
                }
            }
        }else{
            $text = trim($text);
        }

        return $text;
    }

    // Convert string sang kieu khong dau
    public static function removeSign($string, $replaceSpace = true, $limit = false){
        $unicode = array(
            'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
            'd'=>'đ',
            'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
            'i'=>'í|ì|ỉ|ĩ|ị',
            'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
            'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
            'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
            'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
            'D'=>'Đ',
            'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
            'I'=>'Í|Ì|Ỉ|Ĩ|Ị',
            'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
            'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
            'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
            '-'=>'zzz',
            ' '=>'zzz'
        );

        foreach($unicode as $nonUnicode=>$uni){
            $string = preg_replace("/($uni)/i", $nonUnicode, $string);
        }
        $string = strtolower($string);

        $string = preg_replace("/[^A-Za-z0-9 ]/", '', $string);

        if($replaceSpace) {
            $string = str_replace(' ', '-', $string);
        }

        if($limit){
            if(strlen($string) >= $limit){
                $string = substr($string, 0, $limit);
            }
        }

        return $string;
    }
}