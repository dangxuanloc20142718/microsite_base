<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Pages extends Model
{
    public $table = 'pages';

    public function setAliasAttribute($alias){
        $this->attributes['alias'] = \App\Helper\StringHelper::removeSign($this->attributes['title']);
    }
}
