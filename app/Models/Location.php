<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

use DB;
use phpDocumentor\Reflection\DocBlock\Tag;

class Location extends Model
{
    public $timestamps = false;
    public $table = 'tags';
    //
    protected $fillable = [
        'title',
        'alias',
        'description',
        'seo_title',
        'seo_description',
        'is_hot',
        'status'
    ];

    public function scopeSearch($query, $keyword=null)
    {
        if($keyword){
            $query->where('title', 'like', '%'.$keyword.'%');
        }
        
        return $query;
    }

    public static function getProvince(){
        $items = DB::table('location_province')->orderBy('ordering', 'DESC')->orderBy(DB::raw('name COLLATE utf8_general_ci'), 'ASC')->pluck('name', 'provinceid');

        return $items;
    }

    public static function getDistrict($provinceId){
        $items = DB::table('location_district')->where('provinceid', $provinceId)
            ->orderBy('type', 'ASC')
            ->orderBy(DB::raw('name COLLATE utf8_general_ci'), 'ASC')
            ->pluck('name', 'districtid');

        return $items;
    }

    public static function getWard($districtId){
        $items = DB::table('location_ward')->where('districtid', $districtId)
            ->orderBy('type', 'ASC')
            ->orderBy(DB::raw('name COLLATE utf8_general_ci'), 'ASC')
            ->pluck('name', 'wardid');

        return $items;
    }

    public static function boot()
    {
        parent::boot();
    }

    public static function setStatus($id, $value){

    }
}
