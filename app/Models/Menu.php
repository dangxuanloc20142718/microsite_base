<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

use DB;
use Session;

class Menu extends Model
{
    //
    protected $fillable = [
        'parent_id',
        'title',
        'title_en',
        'position',
        'module',
        'module_id',
        'alias',
        'image',
        'banner',
        'fulltext',
        'seo_title',
        'seo_keyword',
        'seo_description',
        'created_at'
    ];

    public $table = 'menu';
    public $field = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->field = Session::get('lang', '');
    }

    public function getMenuPositions(){
        return [
            'main' => 'Menu Chính',
            'footer' => 'Menu Chân trang',
            'footer2' => 'Menu Chân trang 2',
        ];
    }

    public function getCategoryTree($level = 0, $prefix = '', $position='main') {
        $query = DB::table('menu')
            ->select('id', 'parent_id', 'title', 'image', 'status', 'ordering')
            ->where('parent_id', $level)
            ->orderBy('ordering', 'asc');
        if($position){
            $query->where('position', $position);
        }

        $rows = $query->get();

        $category = array();
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $row->title = $prefix . $row->title;
                $category[] = $row;
                $category = array_merge($category, $this->getCategoryTree($row->id, $prefix . ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ', $position));
            }
        }

        return $category;
    }

    public function getCategorySelect($position='main'){
        $selectCat = $this->getCategoryTree(0, '', $position);
        $parentCat = array();
        foreach ($selectCat as $row){
            $parentCat[$row->id] = $row->title;
        }
        $selectCat = array('' => '--- Chọn Menu ---') + $parentCat;

        return $selectCat;
    }

    public function getNewsMenus($parentId = 0) {
        $rows = DB::table('news_categories')
            ->select( DB::raw("id, title, title_en, alias") )
            ->where('parent_id', $parentId)
            ->where('status', 1)
            ->where('is_menu', 1)
            ->orderBy('ordering', 'asc')
            ->get();

        $category = array();
        if (count($rows) > 0) {
            foreach ($rows as $k => $row) {
                $row->alias = $row->alias.'-nc'.$row->id.'.html';
                $category[$k] = $row;
                $category[$k]->childs = $this->getNewsMenus($row->id);
            }
        }

        return $category;
    }

    public function getMenus($parentId = 0, $position = 'main') {
        $this->field = Session::get('lang', '');
        echo $this->field;

        $rows = DB::table('menu')
            ->select( DB::raw("id, title, title_en, alias") )
            ->where('parent_id', $parentId)
            ->where('position', $position)
            ->where('status', 1)
            ->orderBy('ordering', 'asc')
            ->get();

        $category = array();
        if (count($rows) > 0) {
            foreach ($rows as $k => $row) {
                $category[$k] = $row;
                $category[$k]->childs = $this->getMenus($row->id, $position);
            }
        }

        return $category;
    }

    public function scopeSearchFrontEnd($query, $alias=null){
        if($alias){
            $query->where('alias', $alias);
        }

        return $query;
    }

    public function scopeSearch($query)
    {
        if(Input::get('keyword')){
            return $query->where('title', 'like', '%'.Input::get('keyword').'%');
        }
    }

    public static function boot()
    {
        parent::boot();

        static::created(function($data)
        {
            $max = DB::table('menu')->where('position', $data->position)->where('parent_id', $data->parent_id)->max('ordering')+1;
            DB::table('menu')->where('id', $data->id)->update(['ordering' => $max]);
            return true;
        });

        static::deleting(function($data)
        {
            $items = DB::table('menu')->where('position', $data->position)->where('parent_id', $data->parent_id)->where('ordering', '>', $data->ordering)->get();
            foreach ($items as $item){
                DB::table('menu')->where('id', $item->id)->update([ 'ordering' => $item->ordering - 1 ]);
            }
            return true;
        });
    }

    public static function setOrder($id, $action='up'){
        if($action == 'up'){
            $item = Menu::findOrFail($id);
            if($item && $item->ordering > 1){
                $mustChange = DB::table('menu')->where('ordering', '<', $item->ordering)
                    ->where('parent_id', $item->parent_id)
                    ->where('position', $item->position)
                    ->orderBy('ordering', 'DESC')->first();
                if($mustChange) {
                    DB::table('menu')->where('id', $mustChange->id)->update(['ordering' => $item->ordering]);
                }

                $item->ordering = $item->ordering - 1;
                $item->save();
            }
        }

        if($action == 'down'){
            $item = Menu::findOrFail($id);
            if($item){
                //$item->ordering = $item->ordering + 1;
               // $item->save();
                $mustChange = DB::table('menu')->where('ordering', '>', $item->ordering)
                    ->where('parent_id', $item->parent_id)
                    ->where('position', $item->position)
                    ->orderBy('ordering', 'ASC')->first();
                if($mustChange) {
                    DB::table('menu')->where('id', $mustChange->id)->update(['ordering' => $item->ordering]);

                    $item->ordering = $item->ordering + 1;
                    $item->save();
                }
            }
        }
    }

    /*public function setAliasAttribute($alias){
        
        //$this->attributes['alias'] = \App\Helper\StringHelper::removeSign($this->attributes['title']);
    }*/

    public static function setStatus($id, $value){
        $item = Menu::findOrFail($id);
        $item->status = $value;
        $item->save();
    }
}
