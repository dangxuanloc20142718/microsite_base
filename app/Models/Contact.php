<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use App\Helper\StringHelper;
use App\Models\BaseModal;

use DB;
use Auth;

class Contact extends BaseModal
{
    // Có field created_at & updated_at?
    public $timestamps = true;
    // Tên bảng
    protected $table = 'contact';

    // Cac field cần kiểm tra trước khi tạo
    protected $fillable = [
        'type',
        'fullname',
        'email',
        'mobile',
        'idnumber',
        'birthday',
        'facebook',
        'status',
        'title',
        'content',
    ];

    protected $parentField = false;
    protected $orderField = false;

    public function search($filter, $orderField='id', $orderSort='DESC', $activeOnly=true)
    {
        $query = self::select('*');

        if(isset($filter['keyword']) && $filter['keyword']){
            $query->where('title', 'LIKE', "%".$filter['keyword']."%");
        }

        if(isset($filter['type_id']) && $filter['type_id']){
            $query->where('type', $filter['type_id']);
        }

        $query->orderBy($orderField, $orderSort);

        if($activeOnly){
            $query->where('status', '>=', 1);
        }

        return $query;
    }

    public function getTypes($type=0){
        $types = [
            1 => 'Type A',
            2 => 'Type B',
            3 => 'Type C',
            4 => 'Type D',
            5 => 'Type E',
        ];

        return isset($types[$type]) ? $types[$type] : $types;
    }
}
