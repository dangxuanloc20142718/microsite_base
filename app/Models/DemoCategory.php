<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use App\Helper\StringHelper;
use App\Models\BaseModal;

use DB;
use Auth;

class DemoCategory extends BaseModal
{
    // Có field created_at & updated_at?
    public $timestamps = true;
    // Tên bảng
    protected $table = 'demo_category';

    // Cac field cần kiểm tra trước khi tạo
    protected $fillable = [
        'parent_id',
        'title',
        'introtext',
        'fulltext',
        'icon',
        'image',
        'status',
    ];

    protected $parentField = 'parent_id';
    protected $orderField = 'ordering';

    public function search($query)
    {
        $query = self::where('status', '>=', 0);

        return $query;
    }

    public function getCategoryTree($level=0, $prefix='') {
        $query = self::where('parent_id', $level)
            ->orderBy('ordering', 'asc');

        $rows = $query->get();
        $category = array();
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $row->title = $prefix . $row->title;
                $category[] = $row;
                $category = array_merge($category, $this->getCategoryTree($row->id, $prefix . ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  '));
            }
        }

        return $category;
    }

    public function getCategorySelect(){
        $selectCat = $this->getCategoryTree(0, '');
        $parentCat = array();
        foreach ($selectCat as $row){
            $parentCat[$row->id] = $row->title;
        }
        $selectCat = array('0' => '--- Chọn Danh mục ---') + $parentCat;

        return $selectCat;
    }
}
