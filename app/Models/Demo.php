<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use App\Helper\StringHelper;
use App\Models\BaseModal;

use DB;
use Auth;

class Demo extends BaseModal
{
    // Có field created_at & updated_at?
    public $timestamps = true;
    // Tên bảng
    protected $table = 'demo';

    // Cac field cần kiểm tra trước khi tạo
    protected $fillable = [
        'type',
        'title',
        'introtext',
        'fulltext',
        'icon',
        'image',
        'status',
    ];

    protected $parentField = false;
    protected $orderField = 'ordering';

    public static function search($filter, $orderField='id', $orderSort='DESC', $activeOnly=true)
    {
        $query = self::select('*');

        if(isset($filter['keyword']) && $filter['keyword']){
            $query->where('title', 'LIKE', "%".$filter['keyword']."%");
        }

        if(isset($filter['type_id']) && $filter['type_id']){
            $query->where('type', $filter['type_id']);
        }

        $query->orderBy($orderField, $orderSort);

        if($activeOnly){
            $query->where('status', '>=', 1);
        }

        return $query;
    }

    public function getTypes($type=0){
        $types = [
            1 => 'Type A',
            2 => 'Type B',
            3 => 'Type C',
            4 => 'Type D',
            5 => 'Type E',
        ];

        return isset($types[$type]) ? $types[$type] : $types;
    }
}
