<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Pages extends Model
{
    //
    protected $fillable = [
        'title',
        'title_en',
        'alias',
        'image',
        'fulltext',
        'fulltext_en',
        'created_at'
    ];

    public static function boot()
    {
        parent::boot();

        static::created(function($data)
        {
            Pages::where('id', $data->id)->update(['alias' => \App\Helper\StringHelper::removeSign($data->title)]);
            return true;
        });

        static::updated(function($data)
        {
            Pages::where('id', $data->id)->update(['alias' => \App\Helper\StringHelper::removeSign($data->title)]);
            return true;
        });
    }

    public function scopeSearch($query)
    {
        if(Input::get('keyword')){
            return $query->where('title', 'like', '%'.Input::get('keyword').'%');
        }
    }

    public static function setStatus($id, $value){
        $item = Pages::findOrFail($id);
        $item->status = $value;
        $item->save();
    }
}
