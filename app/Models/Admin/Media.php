<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Media extends Model
{
    //
    protected $fillable = [
        'title',
        'link'
    ];

    public $timestamps = false;

    public function scopeSearch($query)
    {
        if(Input::get('keyword')){
            return $query->where('title', 'like', '%'.Input::get('keyword').'%');
        }
    }

    // Set attr
    public function setCreatedAtAttribute($date){
        //$this->attributes['created_at'] = Carbon::createFromFormat('Y-m-d',$date);
    }

    public static function setStatus($id, $value){
        $item = Pages::findOrFail($id);
        $item->status = $value;
        $item->save();
    }
}
