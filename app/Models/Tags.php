<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

use DB;
use phpDocumentor\Reflection\DocBlock\Tag;

class Tags extends Model
{
    public $timestamps = false;
    public $table = 'tags';
    //
    protected $fillable = [
        'title',
        'alias',
        'description',
        'seo_title',
        'seo_description',
        'is_hot',
        'status'
    ];

    public function scopeSearch($query, $keyword=null)
    {
        if($keyword){
            $query->where('title', 'like', '%'.$keyword.'%');
        }
        
        return $query;
    }

    public static function loadTags($newsId){
        $data = [];
        // Get tags_id
        $tags = DB::table('news_tags')->where('news_id', $newsId)->pluck('news_id', 'tags_id');
        if(count($tags)){
            $items = Tags::whereIn('id', array_keys($tags))->select('id', 'title', 'alias')->get();
            return $items;
        }

        return $data;
    }

    public static function saveTags($tags, $newsId){
        // Delete first
        DB::table('news_tags')->where('news_id', $newsId)->delete();
        $tags = explode(',', $tags);

        foreach ($tags as $tag){
            $tag = trim($tag);
            if(empty($tag)) continue;
            
            if(strpos($tag, ':') !== false){
                $tag = explode(':', $tag);
                DB::insert('insert into news_tags (news_id, tags_id) values (?, ?)', [$newsId, $tag[1]]);
            }else{
                $item = new Tags();
                $item = $item::create(['title' => $tag, 'alias' => $tag]);
                if($item){
                    DB::insert('insert into news_tags (news_id, tags_id) values (?, ?)', [$newsId, $item->id]);
                }
            }
        }
    }

    public static function removeTags($newsId){
        DB::table('news_tags')->where('news_id', $newsId)->delete();
    }

    public function setAliasAttribute($alias){
        $this->attributes['alias'] = \App\Helper\StringHelper::removeSign($this->attributes['title']);
    }

    public static function boot()
    {
        parent::boot();
    }

    public static function setStatus($id, $value){
        $item = Tags::findOrFail($id);
        $item->status = $value;
        $item->save();
    }
}
