<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

use DB;

class Comments extends Model
{
    //
    protected $fillable = [
        'type',
        'item_id',
        'comment',
        'reply',
        'fullname',
        'mobile',
        'email',
        'status',
        'created_at'
    ];

    public function scopeSearch($query, $types=null)
    {
        if(Input::get('keyword')){
            $query->where('title', 'like', '%'.Input::get('keyword').'%');
        }

        if($types){
            $query->whereIn('type', $types);
        }

        return $query;
    }

    public static function getComments($itemId = 0){
        $itemId = intval($itemId);

        $items = DB::table('comments')
            ->where('item_id', $itemId)
            ->where('status', 1)
            ->orderBy('id', 'DESC')
            ->paginate(20);

        return $items;
    }

    // Set attr
    /*public function setCreatedAtAttribute($date){
        //$this->attributes['created_at'] = Carbon::createFromFormat('Y-m-d',$date);
    }*/

    public static function setStatus($id, $value){
        $item = Comments::findOrFail($id);
        $item->status = $value;
        $item->save();
    }
}
