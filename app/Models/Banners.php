<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

use DB;

class Banners extends Model
{
    public $timestamps = false;
    //
    protected $fillable = [
        'position',
        'title',
        'link',
        'image',
        'created_at'
    ];

    public function scopeSearch($query, $position, $keyword)
    {
        if($position){
            $query->where('position', '=', $position);
        }

        if($keyword){
            $query->where('title', 'like', '%'.$keyword.'%');
        }

        return $query;
    }

    public static function getBanners($position='home1'){
        return DB::table('banners')
            ->where('position', $position)
            ->where('status', 1)
            ->orderBy('ordering', 'ASC')
            ->get();
    }

    public static function boot()
    {
        parent::boot();

        static::created(function($data)
        {
            $max = DB::table('banners')->where('position', $data->position)->max('ordering')+1;
            DB::table('banners')->where('id', $data->id)->update(['ordering' => $max]);
            return true;
        });

        static::deleting(function($data)
        {
            $items = DB::table('banners')->where('position', $data->position)->where('ordering', '>', $data->ordering)->get();
            foreach ($items as $item){
                DB::table('banners')->where('id', $item->id)->update([ 'ordering' => $item->ordering - 1 ]);
            }
            return true;
        });
    }

    public static function setStatus($id, $value){
        $item = Banners::findOrFail($id);
        $item->status = $value;
        $item->save();
    }

    public static function setOrder($id, $action='up'){
        if($action == 'up'){
            $item = Banners::findOrFail($id);
            if($item && $item->ordering > 1){
                $mustChange = DB::table('banners')->where('ordering', '<', $item->ordering)
                    ->where('position', $item->position)
                    ->orderBy('ordering', 'DESC')->first();
                DB::table('banners')->where('id', $mustChange->id)->update(['ordering' => $item->ordering]);

                $item->ordering = $item->ordering - 1;
                $item->save();
            }
        }

        if($action == 'down'){
            $item = Banners::findOrFail($id);
            if($item){
                $mustChange = DB::table('banners')->where('ordering', '>', $item->ordering)
                    ->where('position', $item->position)
                    ->orderBy('ordering', 'ASC')->first();
                if($mustChange) {
                    DB::table('banners')->where('id', $mustChange->id)->update(['ordering' => $item->ordering]);

                    $item->ordering = $item->ordering + 1;
                    $item->save();
                }
            }
        }
    }
}
