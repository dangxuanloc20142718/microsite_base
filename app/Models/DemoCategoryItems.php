<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use App\Helper\StringHelper;
use App\Models\BaseModal;

use DB;
use Auth;

class DemoCategoryItems extends BaseModal
{
    // Có field created_at & updated_at?
    public $timestamps = true;
    // Tên bảng
    protected $table = 'demo_category_items';

    // Cac field cần kiểm tra trước khi tạo
    protected $fillable = [
        'category_id',
        'title',
        'introtext',
        'fulltext',
        'gallery',
        'icon',
        'image',
        'status',
    ];

    protected $parentField = 'category_id';
    protected $orderField = 'ordering';

    public function search($filter, $orderField='id', $orderSort='DESC', $activeOnly=true)
    {
        $query = self::select('*');

        if(isset($filter['category_id']) && $filter['category_id']){
            $query->where('category_id', $filter['category_id']);
        }

        if(isset($filter['keyword']) && $filter['keyword']){
            $query->where('title', 'LIKE', "%".$filter['keyword']."%");
        }

        if($activeOnly){
            $query->where('status', '>=', 1);
        }

        $query->orderBy($orderField, $orderSort);

        return $query;
    }

    public function category(){
        return $this->hasOne('App\Models\DemoCategory', 'id', 'category_id')->withDefault(function ($user) {
            $user->title = 'N/A';
            $user->id = 0;
        });
    }

    public function getGalleryAttribute($value){
        return $value ? json_decode($value) : null;
    }
}
