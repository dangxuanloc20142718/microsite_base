<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use App\Helper\StringHelper;
use Illuminate\Support\Facades\Schema;

use DB;
use Auth;

class BaseModal extends Model
{
    protected $parentField = false;
    protected $orderField = false;

    public static function boot()
    {
        parent::boot();

        static::created(function($data) {
            $user = Auth::user();
            if(Schema::hasColumn($data->table, 'created_by')){
                self::where('id', $data->id)->update(['created_by' => $user ? $user->id : 0]);
            }

            if($data->orderField){
                if($data->parentField){
                    self::where('id', $data->id)
                        ->update(['ordering' => self::where($data->parentField, $data->{$data->parentField})->max($data->orderField) + 1]);
                }else{
                    self::where('id', $data->id)->update(['ordering' => self::max($data->orderField) + 1]);
                }
            }

            self::reOrder($data);
            return true;
        });

        static::deleting(function($data) {
            self::reOrder($data);
            return true;
        });

        static::updated(function($data) {
            $user = Auth::user();
            if($user && $user->group == 1 && Schema::hasColumn($data->table, 'updated_by')) {
                self::where('id', $data->id)->update(['updated_by' => $user ? $user->id : 0]);
            }
            return true;
        });
    }

    public static function setOrder($id, $action='up'){
        if($action == 'up'){
            $item = self::findOrFail($id);
            if($item && $item->ordering > 1){
                $mustChange = self::where('ordering', '<=', $item->ordering)
                    ->where('id', '!=', $item->id)
                    ->orderBy('ordering', 'DESC');
                if($item->parentField){
                    $mustChange = $mustChange->where($item->parentField, $item->{$item->parentField});
                }
                $mustChange = $mustChange->first();

                if($mustChange) {
                    self::where('id', $mustChange->id)->update(['ordering' => $item->ordering]);
                }

                $item->ordering = $item->ordering - 1;
                $item->save();

                self::reOrder($item);
            }
        }

        if($action == 'down'){
            $item = self::findOrFail($id);
            if($item){
                $mustChange = self::where('ordering', '>=', $item->ordering)
                    ->where('id', '!=', $item->id)
                    ->orderBy('ordering', 'ASC');
                if($item->parentField){
                    $mustChange = $mustChange->where($item->parentField, $item->{$item->parentField});
                }
                $mustChange = $mustChange->first();

                if($mustChange) {
                    self::where('id', $mustChange->id)->update(['ordering' => $item->ordering]);
                }

                $item->ordering = $item->ordering + 1;
                $item->save();
            }

            self::reOrder($item);
        }
    }

    public static function reOrder($data){

        if($data->orderField){
            $query = self::orderBy($data->orderField, 'ASC');

            if($data->parentField){
                $query = $query->where($data->parentField, $data->{$data->parentField});
            }

            $items = $query->get();
            if($items){
                foreach ($items as $k => $item){
                    self::where('id', $item->id)->update([$data->orderField => $k + 1]);
                }
            }
        }
    }

    public function setAliasAttribute($alias){
        $this->attributes['alias'] = StringHelper::removeSign($this->attributes['title']);
    }

    public static function setFieldValue($id, $field, $value){
        $item = self::findOrFail($id);
        $item->{$field} = $value;
        $item->save();
    }

    // Return items for select box
    public static function getSelectBox($titleField='title', $defaultTitle='--Chọn--'){
        $items = self::pluck($titleField, 'id')->toArray();
        return $defaultTitle ? [0 => $defaultTitle] + $items : $items;
    }

    public function created_by_user(){
        return $this->hasOne('App\Models\User', 'id', 'created_by')->withDefault(function ($user) {
            $user->name = 'N/A';
            $user->id = 0;
        });
    }

    public function updated_by_user(){
        return $this->hasOne('App\Models\User', 'id', 'updated_by')->withDefault(function ($user) {
            $user->name = 'N/A';
            $user->id = 0;
        });
    }
}
