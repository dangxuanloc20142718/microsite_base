<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use App\Helper\StringHelper;
use App\Models\BaseModal;

use DB;
use Auth;

class SubmitVote extends BaseModal
{
    // Có field created_at & updated_at?
    public $timestamps = false;
    // Tên bảng
    protected $table = 'submit_vote';

    // Cac field cần kiểm tra trước khi tạo
    protected $fillable = [
        'type',
        'user_id',
        'submit_id',
        'ip',
        'user_agent',
        'created_at'
    ];

    protected $parentField = false;
    protected $orderField = false;
}
