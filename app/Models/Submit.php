<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use App\Helper\StringHelper;
use App\Models\BaseModal;
use App\Models\SubmitVote;
use App\Helper\RequestHelper;

use DB;
use Auth;

class Submit extends BaseModal
{
    // Có field created_at & updated_at?
    public $timestamps = true;
    // Tên bảng
    protected $table = 'submit';

    // Cac field cần kiểm tra trước khi tạo
    protected $fillable = [
        'round',
        'user_id',
//        'cat_id',
        'title',
        'image',
        'desc_images',
        'introtext',
        'fulltext',
        'youtube',
        'views',
        'votes',
        'shares',
        'status'
    ];

    protected $parentField = 'cat_id';
    protected $orderField = false;

    public static function getSubmitLibraryInfo()
    {
        $result = [
            1 => ['name' => 'Hạ Long'],
            2 => ['name' => 'Hải Phòng'],
            3 => ['name' => 'Nghệ An'],
            4 => ['name' => 'Hà Tĩnh'],
            5 => ['name' => 'Quảng Bình'],
            6 => ['name' => 'Đà Nẵng'],
            7 => ['name' => 'Hội An'],
            8 => ['name' => 'Nha Trang'],
            9 => ['name' => 'Cần Thơ'],
            10 => ['name' => 'Phú Quốc']
        ];

        for ($i = 1; $i <= 10; $i++) {
            $result[$i]['count'] = self::where('location_id', $i)->where('status', 1)->count();
        }

        return $result;

    }

    public function getTypes($type=0){
        $types = [
            7 => 'Tháng 7',
            8 => 'Tháng 8',
            9 => 'Tháng 9',
        ];

        return isset($types[$type]) ? $types[$type] : $types;
    }

    public static function search($filter=[], $orderField='id', $orderSort='DESC', $activeOnly=true)
    {
        $query = self::select('*');

        if(isset($filter['cat_id']) && $filter['cat_id']){
            $query->where('cat_id', $filter['cat_id']);
        }

        if(isset($filter['user_id']) && $filter['user_id']){
            $query->where('user_id', $filter['user_id']);
        }

        if(isset($filter['location_id']) && $filter['location_id']){
            $query->where('location_id', $filter['location_id']);
        }

        if(isset($filter['keyword']) && $filter['keyword']){
            $query->where('title', 'LIKE', "%".$filter['keyword']."%");
        }

        if(isset($filter['round']) && $filter['round']){
            $query->where('round', intval($filter['round']));
        }

        if(isset($filter['featured']) && $filter['featured']){
            $query->where('featured', $filter['featured']);
        }

        if($activeOnly){
            $query->where('status', '>=', 1);
        }else{
            $query->where('status', '>=', -1);
        }

        $query->orderBy($orderField, $orderSort);

        return $query;
    }

    public function category(){
        return $this->hasOne('App\Models\SubmitCategory', 'id', 'cat_id')->withDefault(function ($user) {
            $user->title = 'N/A';
            $user->id = 0;
        });
    }

    public function voteSubmit($submitId, $userId, $updateTo='votes', $userAgent='')
    {
        $ip = RequestHelper::get_client_ip();
        $updateTo = $updateTo == 'votes' ? 1 : 2;
        $submit = Submit::find($submitId);

        $voteToday = false;
        if($userId) {
            $check = SubmitVote::where('submit_id', $submitId)
                ->where('user_id', $userId)
                ->where('type', $updateTo)
                ->orderBy('id', 'DESC')
                ->first();

            if ($check) {
                $voteToday = date('Y-m-d', strtotime($check->created_at)) == date('Y-m-d') ? true : false;

                if ($updateTo == 1 && intval(date('m')) < 9) {
                    $voteToday = (strtotime($check->created_at) > time() - 2 * 60 * 60) ? true : false;
                }
            }
        }

        $ipVoteToday = false;
        $check = SubmitVote::where('submit_id', $submitId)
            ->where('ip', $ip)
            ->where('type', $updateTo)
            ->orderBy('id', 'DESC')
            ->first();
        if($check){
            $ipVoteToday = date('Y-m-d', strtotime($check->created_at)) == date('Y-m-d') ? true : false;

            if($updateTo == 1 && intval(date('m')) < 9){
                $ipVoteToday = (strtotime($check->created_at) > time() - 2 * 60 * 60) ? true : false;
            }
        }

        if (!$ipVoteToday && !$voteToday && $submit) {
            SubmitVote::create([
                'submit_id' => $submitId,
                'user_id' => $userId,
                'type' => $updateTo,
                'ip' => RequestHelper::get_client_ip(),
                'user_agent' => $userAgent,
                'created_at' => date('Y-m-d H:i:s')
            ]);

            if($updateTo == 1) {
                $votes = $submit->votes + 1;
                Submit::where('id', $submitId)->update(['votes' => $votes]);

                //DB::select(DB::raw("UPDATE wp_postmeta SET meta_value = meta_value + 1 WHERE post_id = ".$submit->ref_id." AND meta_key = 'contest-video-points' "));

                return $votes;
            }else{
                $shares = $submit->shares + 1;
                $votes = $submit->votes;// + 1;
                Submit::where('id', $submitId)
                    ->update(['shares' => $shares, 'votes' => $votes]);

                // Update to wordpress database
                /*DB::table('wp_postmeta')
                    ->where('post_id', $submit->ref_id)
                    ->where('meta_key', 'contest-video-points')
                    ->update('meta_value');*/
                //DB::select(DB::raw("UPDATE wp_postmeta SET meta_value = meta_value + 3 WHERE post_id = ".$submit->ref_id." AND meta_key = 'contest-video-points' "));

                return $shares;
            }
        }

        return false;
    }

    public function getLinkAttribute(){
        return url((app()->getLocale() == 'vn' ? '' : 'en/') . 'bai-du-thi/'.$this->id);
    }

}
