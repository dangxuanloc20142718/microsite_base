<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

use DB;

class Faq extends Model
{
    public $timestamps = false;
    public $table = 'faq';
    //
    protected $fillable = [
        'title',
        'alias',
        'email',
        'fullname',
        'description',
        'status'
    ];

    public function scopeSearch($query, $keyword=null)
    {
        if($keyword){
            $query->where('title', 'like', '%'.$keyword.'%');
        }
        $query->orderBy('id', 'DESC');
        
        return $query;
    }

    public function setAliasAttribute($alias){
        $this->attributes['alias'] = \App\Helper\StringHelper::removeSign($this->attributes['title']);
    }

    public static function boot()
    {
        parent::boot();
    }

    public static function setStatus($id, $value){
        $item = Faq::findOrFail($id);
        $item->status = $value;
        $item->save();
    }

    public static function getListFaq($queryTitle, $returnResult = true, $orderField='id', $orderBy='DESC', $limit = 8) {
        $return = DB::table('faq')
            -> select('id', 'title', 'description');
        if($queryTitle){
            $return->where('title', $queryTitle);
        }
        $return-> where('status', 1)
            //-> orderBy('updated_at', 'DESC')
            -> orderBy($orderField, $orderBy);

        if($returnResult) {
            return $return->paginate($limit);
        } else {
            return $return;
        }
    }

    public function getFaqFeature($limit = 5) {
        $list = $this->getListFaq(null, false);
        return $list->limit($limit)->get();
    }
}
