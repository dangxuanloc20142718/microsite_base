<?php

namespace App\Models;

use App\Models\Admin\NewsCategory;
use DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Session;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'group', 'gender', 'birthday', 'building_id',
        'mobile', 'avatar', 'facebook_id', 'facebook', 'status', 'password', 'promotion',
        'address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeSearch($query, $group = null, $keyword = null, $promotion = null)
    {
        if ($keyword) {
            $query->where('name', 'like', '%' . $keyword . '%')
                ->orWhere('mobile', 'like', '%' . $keyword . '%')
                ->orWhere('facebook_id', $keyword)
                ->orWhere('id', $keyword)
                ->orWhere('email', 'like', '%' . $keyword . '%');
        }

        if ($promotion) {
            $query->where('promotion', 'like', '%' . $promotion . '%');
        }

        if ($group) {
            return $query->where('group', $group);
        }

        return $query;
    }

    public function getAllUsers($limit = 20, $paging = false, $orders = null, $wheres = null)
    {
        $users = User;
        if (!empty($wheres) && is_array($wheres) && count($wheres) > 0) {
            foreach ($wheres as $key => $value) {
                $users->where($key, $value);
            }
        }
        if (!empty($orders) && is_array($orders) && count($orders) > 0) {
            foreach ($orders as $field => $with) {
                $users->orderBy($field, $with);
            }
        }
        if ($paging) {
            $users->paginate($limit);
        } else {
            $users->limit($limit)->get();
        }
        return $users;
    }

    public function getNewestUserSubmit($limit = 6)
    {
        $round = config('custom.round', 1);
        for ($i = 1; $i <= 5; $i++) {
            $collections['cat' . $i] = Submit::where('cat_id', $i)
                ->where('round', $round)
                ->orderby('id', 'desc')
                ->limit($limit)
                ->user()
                ->get();
        }
        return $collections;
    }

    public function countUsersByCategory()
    {
        $round = config('custom.round', 1);
        for ($i = 1; $i <= 5; $i++) {
            $collections['cat' . $i] = Submit::where('cat_id', $i)
                ->where('round', $round)
                ->groupby('user_id')
                ->distinct()
                ->count();
        }
        return $collections;
    }

    public static function getUserGroups()
    {
        return array(
            '0' => '--- Kiểu Thành viên ---',
            1 => 'Admin',
            2 => 'Nhân viên',
            3 => 'Thành viên'
        );
    }

    public static function getUserGroup($id = 0)
    {
        return User::getUserGroups()[$id];
    }

    public static function getUserName($id = 0)
    {
        $user = User::find($id);
        if (is_object($user)) {
            return $user->name;
        } else {
            return '';
        }
    }

    public static function checkEmail($email, $id = 0)
    {
        $check = DB::table('users')->where('email', $email);
        if ($id) {
            $check->where('id', '!=', $id);
        }

        return $check->value('email');
    }

    public static function checkMobile($mobile, $id = 0)
    {
        $check = DB::table('users')->where('mobile', $mobile);
        if ($id) {
            $check->where('id', '!=', $id);
        }

        return $check->value('mobile');
    }

    public static function setPermission($id, $request)
    {
        $user = User::findOrFail($id);

        $newsCategory = new NewsCategory();
        $cats = $newsCategory->getCategoryTree(0, '');

        $arr = [];
        $catArr = array();
        $arrAction = ['view', 'edit', 'status', 'delete'];
        foreach ($cats as $cat) {
            foreach ($arrAction as $action) {
                if ($request->input('news-' . $cat->id . '-' . $id . '-' . $action) != '' || $request->input('news-' . $cat->id . '-0-' . $action) != '') {
                    $arr[] = 'news-' . $cat->id . '-' . $id . '-' . $action;
                    if ($action == 'view') {
                        $catArr[] = $cat->id;
                    }
                }
                if ($request->input('news-' . $cat->id . '-*-' . $action) != '') {
                    $arr[] = 'news-' . $cat->id . '-*-' . $action;
                }
            }
        }

        // Others
        $menus = ['comments', 'report', 'hairsalon', 'hair', 'salon', 'designer',
            'options', 'tags', 'news', 'newscategory', 'pages', 'banners', 'contact',
            'shopmanager', 'products', 'productscategory', 'brand', 'orders', 'statistic', 'shop', 'faq', 'submit', 'user'
        ];
        foreach ($menus as $menu) {
            foreach ($arrAction as $action) {
                if ($request->input($menu . '-' . $action) != '') {
                    $arr[] = $menu . '-' . $action;
                }
            }
        }

        $user->permission = implode("\n", $arr);
        $user->cats = implode(",", $catArr);
        $user->save();
    }

    public static function getUserNewsCats($id)
    {
        $user = User::find($id);
        if (is_object($user)) {
            return explode(',', $user->cats);
        }

        return array(0);
    }

    public static function getUserByName($name)
    {
        return User::where('name', $name);
    }

    public static function setIncognito($id)
    {
        Session::put('incognito', $id);
    }

    public static function stopIncognito()
    {
        Session::forget('incognito');
    }

    public static function isIncognito()
    {
        return Session::has('incognito');
    }
}
