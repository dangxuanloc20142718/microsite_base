<?php

namespace App\Http\Controllers;

use App\Helper\InputHelper;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Log;
use Validator;

use App\Models\Contact;
use App\Models\Pages;

use Session;
use Mail;

class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = Pages::find(3);
        return view('frontend.contact')->with('page', $page);
    }

    /**
     * Save contact
     *
     */
    public function store(Request $request)
    {
        $request->type = 1;
        $data = $request->only([
            'type',
            'fullname',
            'email',
            'mobile',
            'idnumber',
            'birthday',
            'facebook',
        ]);

        $rules = array(
            'fullname' => 'required',
            'email' => 'required|email',
            'mobile' => 'required',
            'idnumber' => 'required',
            'birthday' => 'required',
            'facebook' => 'required',
        );

        $validator = Validator::make($data, $rules);
        if ($validator->fails()){
            return Response::json(array(
                'code' => 0,
                'message' => 'Dữ liệu không hợp lệ, bạn vui lòng nhập đầy đủ lại!'
            ));
        }else {

            if (!Session::get('captcha', 0)) {
                $captcha = InputHelper::getCaptcha($request);
                if (!$captcha) {
                    return Response::json(array(
                        'code' => 0,
                        'message' => 'Bạn vui lòng tick và làm theo hướng dẫn để xác nhận mã bảo vệ.'
                    ));
                } else {
                    Session::put('captcha', 1);
                }
            }

            Session::forget('captcha');
            $item = new Contact();
            $item::create($data);

            /*try {
                // Send email to admin
                Mail::send('emails.contact.admin', ['item' => $data], function ($m) use ($data) {
                    $m->from(env('MAIL_FROM'), env('MAIL_FROM_NAME'));
                    $m->to(env('MAIL_ADMIN'), '')
                        ->subject('Tronniemvui.Vinpearl.com - Bạn có thông tin liên hệ mới từ website');
                });

                // Send email to customer
                Mail::send('emails.contact.customer', ['item' => $data], function ($m) use ($data) {
                    $m->from(env('MAIL_FROM'), env('MAIL_FROM_NAME'));
                    $m->to($data['email'], $data['fullname'])
                        ->subject('Tronniemvui.Vinpearl.com - Thông báo đăng ký thông tin thành công!');
                });
            }catch (\Exception $e) {
                Log::info($e->getMessage());
            }*/
        }

        return Response::json(array(
            'code' => 1,
            'message' => "Gửi thông tin thành công!<br/>Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất!"
        ));
    }
}
