<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

use App\Models\Location;

class LocationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index(Request $request){
        $location = new Location();
        $res['code'] = 1;

        if($request->type == 'district'){
            $items = $location->getDistrict($request->value);

            $html[] = '<option value="">--Chọn Quận/Huyện--</option>';
            foreach ($items as $key => $value){
                if($key == $request->default){
                    $html[] = "<option value='{$key}' selected>{$value}</option>";
                }else{
                    $html[] = "<option value='{$key}'>{$value}</option>";
                }
            }

            $res['data'] = implode('', $html);
            return Response::json($res);
        }

        if($request->type == 'ward'){
            $items = $location->getWard($request->value);

            $html[] = '<option value="">--Chọn Xã/Phường--</option>';
            foreach ($items as $key => $value){
                if($key == $request->default){
                    $html[] = "<option value='{$key}' selected>{$value}</option>";
                }else{
                    $html[] = "<option value='{$key}'>{$value}</option>";
                }
            }

            $res['data'] = implode('', $html);
            return Response::json($res);
        }
    }
}
