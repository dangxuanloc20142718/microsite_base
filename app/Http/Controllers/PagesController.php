<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Pages;
use Session;

class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function show($alias = null, $id = 0){
        $item = Pages::findOrFail($id);

        $lang = Session::get('lang');
        $item->title = $item->{'title'.$lang};
        $item->fulltext = $item->{'fulltext'.$lang};
        
        return view('frontend.pages', compact('item'));
    }
}
