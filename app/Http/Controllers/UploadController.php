<?php

namespace App\Http\Controllers;

use App\Helper\FileHelper;
use App\Helper\InputHelper;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Helper\StringHelper;
use Carbon\Carbon;

use Auth;
use File;
use DB;

use App\Models\Admin\Media;
use Symfony\Component\Console\Helper\Helper;

class UploadController extends Controller
{
    /**
     * List all news
     *
     */
    public function index()
    {

    }

    /**
     * Save item
     *
     */

    // Returns a file size limit in bytes based on the PHP upload_max_filesize and post_max_size
    public static function file_upload_max_size() {
        static $max_size = -1;

        if ($max_size < 0) {
            // Start with post_max_size.
            $max_size = self::parse_size(ini_get('post_max_size'));

            // If upload_max_size is less, then reduce. Except if upload_max_size is
            // zero, which indicates no limit.
            $upload_max = self::parse_size(ini_get('upload_max_filesize'));
            if ($upload_max > 0 && $upload_max < $max_size) {
                $max_size = $upload_max;
            }
        }
        return $max_size;
    }

    public static function parse_size($size) {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
        $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
        if ($unit) {
            // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        }
        else {
            return round($size);
        }
    }

    public static function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public function upload(Request $request)
    {
        $user = Auth::user();

        // Init message responsive for client
        $respond = array(
            'code' => 0,
            'message' => "Lỗi khi upload",
            'link' => ''
        );

        $code = 1;
        $msg = "Upload thành công!";

        // List file
        if ($request->action == 'get') {
            $list = Media::orderBy('id', 'DESC')->paginate(50);
            $respond['message'] = 'Danh sách media không tồn tại';
            if($list) {
                $respond['code'] = 1;
                $respond['message'] = 'Danh sách media';
                $respond['data'] = $list;
            }
            return Response::json($respond);
        }

        // Xóa file
        if ($request->action == 'delete' && $user->group > 0) {
            $deleted = FileHelper::deleteFile($request->file);

            $respond['link'] = $request->file;
            if ($deleted) {
                $respond['code'] = 1;
                $respond['message'] = 'Xoá file thành công';
            } else {
                $respond['message'] = "Lỗi khi xóa file." . $request->file;
            }

            $media = Media::find($request->id);
            if($media) $media->delete();

            return Response::json($respond);
        }

        // Delete old files
        /*$files = File::allFiles( StringHelper::getUploadPath('uploads/content') );
        if(count($files)){
            foreach ($files as $file){
                if($file->isFile() && $file->getExtension() != 'html' && strtotime($file->getCTime()) > (time() + 24*60*60) ){
                    File::delete( $file->getPathname() );
                }
            }
        }*/

        $upType = InputHelper::getRawString('upType', 'image');
        if(!in_array($upType, ['image', 'document', 'video', 'audio', 'media', 'pdf', 'cv', 'avatar'])){
            return Response::json([
                'code' => 0,
                'message' => 'Kiểu type upload không hợp lệ'
            ]);
        }

        if (Input::hasFile('upload-file') && Auth::check()) {
            $media = new Media();
            $image = Input::file('upload-file');

            // Init variables config in upload files
            if ($customMaxSize = InputHelper::getRawString('maxSize', 0)) {
                $isCustomMaxSize = TRUE;
                $maxSizeFile = InputHelper::toByteSize($customMaxSize);
            } else {
                $isCustomMaxSize = FALSE;
                $maxSizeFile = $this->file_upload_max_size();
            }

            $filename = time();

            // Type file upload generic for all upload
            $mineTypes = array(
                'image' => [
                    'mines' => 'image/x-icon, image/gif, image/png, image/jpg, image/jpeg',
                    'ext' => 'ico, gif, jpg, jpeg, png',
                    'folder' => 'uploads/image'
                ],
                'document' => [
                    'mines' => 'application/pdf, application/msword, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.openxmlformats-officedocument.wordprocessingml.template, application/vnd.ms-word.document.macroEnabled.12, application/vnd.ms-word.template.macroEnabled.12, application/vnd.ms-excel, application/vnd.ms-excel, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.openxmlformats-officedocument.spreadsheetml.template, application/vnd.ms-excel.sheet.macroEnabled.12, application/vnd.ms-excel.template.macroEnabled.12, application/vnd.ms-excel.addin.macroEnabled.12, application/vnd.ms-excel.sheet.binary.macroEnabled.12, application/vnd.ms-powerpoint, application/vnd.ms-powerpoint, application/vnd.ms-powerpoint, application/vnd.ms-powerpoint, application/vnd.openxmlformats-officedocument.presentationml.presentation, application/vnd.openxmlformats-officedocument.presentationml.template, application/vnd.openxmlformats-officedocument.presentationml.slideshow, application/vnd.ms-powerpoint.addin.macroEnabled.12, application/vnd.ms-powerpoint.presentation.macroEnabled.12, application/vnd.ms-powerpoint.template.macroEnabled.12, application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
                    'ext' => 'pdf, doc, dot, docx, dotx, docm, dotm, xls, xlt, xla, xlsx, xltx, xlsm, xltm, xlam, xlsb, ppt, pot, pps, ppa, pptx, potx, ppsx, ppam, pptm, potm, ppsm',
                    'folder' => 'uploads/content/document'
                ],
                'video' => [
                    'mines' => 'video/x-flv, video/mp4, application/x-mpegURL, video/MP2T, video/3gpp, video/quicktime, video/x-msvideo, video/x-ms-wmv',
                    'ext' => 'flv, mp4, m3u8, ts, 3gp, mov, avi, wmv',
                    'folder' => 'uploads/video'
                ],
                'audio' => [
                    'mines' => 'audio/basic, audio/basic, audio/mid, audio/mid, audio/mpeg, audio/mp4, audio/x-aiff, audio/x-aiff, audio/x-aiff, audio/x-mpegurl, audio/vnd.rn-realaudio, audio/vnd.rn-realaudio, audio/ogg, audio/vorbis, audio/vnd.wav',
                    'ext' => 'au, snd, mid, rmi, mp3, mp4 audio, aif, aifc, aiff, m3u, ra, ram, ogg vorbis, vorbis, wav',
                    'folder' => 'uploads/audio'
                ],
                'pdf' => [
                    'mines' => 'application/pdf',
                    'ext' => 'pdf',
                    'folder' => 'uploads/document'
                ]
            );

            if($upType == 'cv') {
                $mineTypes['cv'] = array(
                    'mines' => $mineTypes['image']['mines'] . $mineTypes['document']['mines'],
                    'ext' => $mineTypes['image']['ext'] . $mineTypes['document']['ext'],
                    'folder' => 'uploads/document'
                );
            }
            if($upType == 'media') {
                $mineTypes['media'] = array(
                    'mines' => $mineTypes['image']['mines'] .', '. $mineTypes['document']['mines'] .', '. $mineTypes['video']['mines'],
                    'ext' => $mineTypes['image']['ext'] .', '. $mineTypes['document']['ext'] .', '. $mineTypes['video']['ext'],
                    'folder' => 'uploads/media'
                );
            }
            if($upType == 'avatar') {
                $mineTypes['avatar'] = array(
                    'mines' => $mineTypes['image']['mines'],
                    'ext' => $mineTypes['image']['ext'],
                    'folder' => 'uploads/avatar'
                );
            }

            // Check get image from client send
            if ($image) {
                $mineTypeFile = $image->getClientMimeType();
                $fileExt = $image->getClientOriginalExtension();
                $fileExt = strtolower($fileExt);

                $filenameFull = StringHelper::removeSign($filename) . '.' . $fileExt;
                $uriPathFolder = 'media/'. date('Y-m') . '/'.date('d').'/';
                $filePath = $uriPathFolder . $filenameFull;

                // Check minetype file and type extend file
                if($upType != 'all' && is_bool(strpos($mineTypes[$upType]['mines'], $mineTypeFile)) || is_bool(strpos($mineTypes[$upType]['ext'], $fileExt))) {
                    $code = 0;
                    $msg = "File không hợp lệ!";
                }

                // Check size file
                if(File::size($image) > $maxSizeFile) {
                    $code = 0;
                    $msg = "File có kích thước quá giới hạn! Hãy tải file có dung lượng <= ";
                    $msg .= $isCustomMaxSize ? $customMaxSize : ($this->formatSizeUnits($maxSizeFile) . "MB");
                }

                // check min width
                if ($minWidth = InputHelper::getInt('minWidth', 0)) {
                    $imageInfo = getimagesize($image);
                    if ($imageInfo[0] < $minWidth) {
                        $code = 0;
                        $msg = "File có kích thước quá nhỏ! Hãy tải lên file có chiều rộng >= " . $minWidth . "px";
                    }
                }

                if($code != 0) {
                    $resize = InputHelper::getInt('resize', null);
                    // Nếu file upload là file ảnh thì kiểm tra có param resize không?
                    if(is_numeric(strpos($mineTypes['image']['ext'], $fileExt)) && ($fileExt == 'ico' || $fileExt == 'gif')) {
                        $image->move(StringHelper::getUploadPath($uriPathFolder), $filenameFull);
                    } else {
                        $watermark = $request->get('chk_watermark', false);
                        if(is_numeric(strpos($mineTypes['image']['ext'], $fileExt)) && ($resize && $resize != '1')) {
                            $imageMake = Image::make($image->getRealPath())->resize($resize, null, function ($constraint) {
                                $constraint->aspectRatio();
                            });
                            if($watermark == true || $watermark == 'true') {
                                $imageMake->insert('uploads/others/logo.png', 'bottom-right', 15, 15);
                            }
                            $imageMake->stream();
                            FileHelper::saveFile($filePath, $imageMake);

                        } else {
                            FileHelper::saveFile($filePath, file_get_contents($image->getRealPath()));
                        }
                    }
                }
            }

            // This is media upload in admin
            if($code != 0 && $user->group <= 2 && !InputHelper::getInt('front')) {
                $media->title = $filename;
                $media->link = $filePath;

                if(is_numeric(strpos($mineTypes['video']['ext'], $fileExt))) {
                    $media->type = 'video';
                } else if(is_numeric(strpos($mineTypes['image']['ext'], $fileExt))) {
                    $media->type = 'image';
                } else if(is_numeric(strpos($mineTypes['document']['ext'], $fileExt))) {
                    $media->type = 'document';
                } else $media->type = $upType;
                $respond['type'] = $media->type;

                $media->save();
            }

            $respond['code'] = $code;
            $respond['message'] = $msg;
            $respond['title'] = $filename;
            $respond['id'] = $media->id;
            $respond['link'] = $filePath;// . '?rand=' . time();
        }

        return Response::json($respond);
    }

    public static function uploadCompressImage(Request $request) {
        $file = $request->file('image_upload');
        $base64Image = FileHelper::compressImage($file, NULL, 100);
        return Response::json([
            'message' => 'base64 image',
            'image' => $base64Image
        ]);
    }
}
