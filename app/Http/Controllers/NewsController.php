<?php

namespace App\Http\Controllers;

use App\Helper\InputHelper;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheckNewsRequest;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

use Cache;
use DB;
use Nathanmac\Utilities\Parser\Parser;
use Session;
use App\Models\Tags;
use App\Models\News;
use App\Models\Admin\NewsCategory;

class NewsController extends Controller
{
    private $newsModel;
    /**
     * List all news
     * 
     */
    public function __construct()
    {
        $this->newsModel = new News();
    }

    public function index(Request $request){
        $ajax = InputHelper::getInt('ajax', 0);
        $news = $this->newsModel->getNewsFeatured(1, 1, true);

        if($ajax) {
            return Response::json([
                'status' => (count($news)) ? 1 : 2,
                'message' => 'return news list',
                'html' => view('frontend.news.index_items')->with('news', $news)->render()
            ]);
        }

        return view('frontend.news.index')->with('news', $news);
    }

    public function category($alias = null, $id = 0, Request $request){
        $newsModel = new News();
        $category = NewsCategory::findOrFail($id);

        $items  = $newsModel->getNewsByCategory($id);
        $hots   = $newsModel->getNewsHot(6);
        $navs   = array_reverse($newsModel->getNavigation($id));

        if($request->ajax){
            $block = new \stdClass();
            $block->items = $items;
            return Response::json(
                array(
                    'status' => 1,
                    'data'  => view('frontend.news.home', compact('block'))->render()
                )
            );
        }

        return view('frontend.news.category'.$category->layout)
            ->with('category', $category)
            ->with('hots', $hots)
            ->with('navs', $navs)
            ->with('items', $items);
    }

    public function tag($alias = null, $id = 0){
        $newsModel = new News();
        $tag = Tags::findOrFail($id);

        $items  = $newsModel->getNewsByTag($tag->id);
        $hots   = $newsModel->getNewsHot(6);

        return view('frontend.news.category')
            ->with('category', $tag)
            ->with('hots', $hots)
            ->with('items', $items);
    }
    
    public function show($alias = null, $id = 0){
        $newsModel = new News();
        $categoryModel = new NewsCategory();

        //Tìm article thông qua mã id tương ứng
        $item = $newsModel->findOrFail($id);
        $item->views = $item->views + 1;
        $item->save();

        $category = $categoryModel->find($item->cat_id);

        $related = array();
        if($item->cat_id){
            $related = $newsModel->getNewsByCategory($item->cat_id, false)->limit(11)->get();
        }

        $item->tags = Tags::loadTags($item->id);
        $relatedByTags = [];
        if(count($item->tags)){
            foreach ($item->tags as $tag){
                $relatedByTags += $newsModel->getNewsByTag($tag->id, 5, $item->id);
            }
        }

        if($item->prevpost){
            $array = explode(',', $item->prevpost);
            $item->prevpost = News::select('id', 'title', 'alias')->whereIn('id', $array)->orderBy('id', 'ASC')->get();
        }else{
            $item->prevpost = [];
        }
        if($item->nextpost){
            $array = explode(',', $item->nextpost);
            $item->nextpost = News::select('id', 'title', 'alias')->whereIn('id', $array)->orderBy('id', 'ASC')->get();
        }else{
            $item->nextpost = [];
        }

        // Navigation
        $navs = array_reverse($newsModel->getNavigation($item->cat_id));

        $hots   = $newsModel->getNewsHot(6);

        $news = [];$layout = '';
//        $layout = ($category && $category->layout == 'service') ? 'service' : '';
//        if($layout == 'service'){
            $news = $newsModel->getNewsByCategory($category->id, TRUE, 'id', 'DESC', 6, array(
                ['col' => 'id', 'opt' => '!=', 'val' => $id]
            ));
//        }
        // Gọi view edit.blade.php hiển thị bải viết
        return view('frontend.news.show'.$layout, compact('item', 'navs', 'related', 'relatedByTags', 'hots', 'news'));
    }

}
