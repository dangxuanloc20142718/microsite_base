<?php

namespace App\Http\Controllers;

use App\Helper\StringHelper;
use Illuminate\Support\Facades\Input;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Helper\FileHelper;
use App\Helper\RequestHelper;

use App\Models\Submit;
use App\Models\User;
use App\Models\News;
use App\Models\SubmitCategory;
use App\Helper\InputHelper;

use Purifier;
use DB;
use Auth;
use Session;
use File;
use Mail;

class SubmitController extends Controller
{
    private $submitModel;
    private $color_random;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->submitModel = new Submit();
        $this->color_random = StringHelper::getColorCategories();
    }

    public function index(Request $request)
    {
        $filter = InputHelper::getRawString('filter', 'latest');
        $layout = InputHelper::getRawString('layout', 'index_items');
        $keyword = InputHelper::getRawString('keyword', '');
        $catId = InputHelper::getInt('cat',  0);
        $limit = InputHelper::getInt('limit', 99999);
        $orderBy = InputHelper::getRawString('orderBy', 'desc');
        $orderBy = $orderBy == 'asc' ? 'asc' : 'desc';
        $page = InputHelper::getInt('page', 1);

        $submits = Submit::search([])->paginate($limit);
        if($submits){
            foreach ($submits as &$submit){
                $submit->username = User::find($submit->user_id) ? User::find($submit->user_id)->name : '';
            }
        }

        if ($request->ajax()) {
            $response = [
                'status' => (count($submits)) ? 1 : 2,
                'message' => 'Request success',
                'html' => view('frontend.submit.' . ($layout == 'index_items' ? 'index_items' : 'index_top') )
                    ->with('submits', $submits)
                    ->render()
            ];

            return Response::json($response);
        }

        Input::merge(['page' => 1]);
        
        return view('frontend.pages.trienlamhanhphuc')
            ->with('submits', $submits);
    }

    public function show($id, Request $request)
    {
        // Bai submit hien tai cua user nay
        $submit = Submit::where('id', $id)->where('status', 1)->first();
        if(!$submit) dd('Bài dự thi không tồn tại');

        $user = User::where('id', $submit->user_id)->first();

        $users = User::search([])->get();
        $usersList = [];
        foreach ($users as $u) {
            $usersList[$u->id] = $u;
        }

        return view('frontend.pages.baiduthi', array(
            'submit' => $submit,
            'user' => $user,
            'submits' => Submit::search([])->limit(9)->get(),
            'users' => $usersList
        ));
    }

    public function create(Request $request){
        $user = Auth::user();
        if(!$user) dd('Bạn phải đăng nhập trước!');

//        $submitUser = Submit::search(['user_id'=>$user->id])->get();
//        $limit = ($submitUser && count($submitUser) > config('custom.round_1_limit')) ? true : false;
//        $submits = Submit::search([])->limit(9)->get();

//        return view('frontend.pages.uploadbaiduthi', array('user' => $user, 'limit' => $limit, 'submits' => $submits));
        return view('frontend.pages.uploadbaiduthi');
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        if (!$user) dd('Access denied');
        $bugs = [];
        $code = 1;
        $data = [];
        $message = 'Đăng bài thành công';

        $round = config('custom.round', 1);

        $cover = InputHelper::getRawString('cover', '');
        if (!empty($cover)) {
            $imagebase64 = explode(',', $cover);
            if (count($imagebase64) >= 2) {

            } else {
                $bugs[] = '- File ảnh không đúng dữ liệu cho phép';
            }
        } else {
            $bugs[] = '- Bạn chưa chưa tải ảnh Cover lên';
        }

        $introText = InputHelper::getRawString('introtext', '');
        if (!$introText) {
            $bugs[] = '- Bạn chưa nhập "Hạnh phúc trong bạn là gì?".';
        } else {
            if (strlen($introText) > 80) $bugs[] = '- "Hạnh phúc trong bạn là gì?" dài quá 80 ký tự.';
        }

        $fullText = InputHelper::getRawString('fulltext', '');
        if (!$fullText) {
            $bugs[] = '- Bạn chưa nhập  "Câu chuyện hạnh phúc của bạn".';
        }

        if (count($bugs)) {
            $code = 0;
            $message = 'Lỗi khi gửi bài:<br/>' . join('<br/>', $bugs);
        } else {
            // Process crop image
            $imagebase64 = base64_decode($imagebase64[1]);
            $imagebase64 = imagecreatefromstring($imagebase64);
            if ($imagebase64) {
                imagejpeg($imagebase64, public_path() . '/media/tmp.jpg');
                $filename = 'media/'.date('Y-m').'/'.date('d').'/'.time().'.jpg';

                FileHelper::saveFile($filename, file_get_contents(public_path() . '/media/tmp.jpg'));
                $cover = $filename;
            } else {
                $bugs[] = '- File ảnh không đúng dữ liệu cho phép';
            }

            // Create submit
            $data['title'] = $user->name;
            $data['round'] = $round;
            $data['introtext'] = $introText;
            $data['fulltext'] = $fullText;
            $data['votes'] = 0;
            $data['round'] = 1;
            $data['user_id'] = $user->id;
            $data['image'] = $cover;
            $data['desc_images'] = InputHelper::getRawString('desc_imgs', '');
            $data['youtube'] = InputHelper::getRawString('youtube', '');

            Submit::create($data);
            $data['id'] = Submit::max('id');
            $data['avatar'] = $user->avatar;

            $message .= '<br/>Bạn vui lòng click vào đây để xem bài dự thi của mình:<br/>'
            .'<a href="'.url('bai-du-thi/'.$data['id']).'" target="_blank">'.url('bai-du-thi/'.$data['id']).'</a>';
        }

        return Response::json([
            'code' => $code,
            'data' => $data,
            'message' => $message
        ]);
    }

    public function share(Request $request){
        $submitId = InputHelper::getInt('id', 0);
        $user = Auth::user();

        $code = 0;
        $count = 0;
        $message = 'Xin chúc mừng, Bài dự thi đã được tăng thêm một điểm Share!';

        $submit = $this->submitModel->find($submitId);
        if (time() > config('custom.round_1_end_vote')) {
            $message = 'Cuộc thi đã kết thúc, bạn không được tính điểm share nữa!';
        }else{
            if ($submit) {
                $check = $this->submitModel->voteSubmit($submitId, 0, 'shares', $request->header('User-Agent'));

                if($check){
                    $code = 1;
                    $count = $check;
                }else{
                    $count = 0;
                    $message = 'Điểm Share không được tính. Mỗi ngày bạn có thể Share cho 1 bài thi 1 lần. Bạn đã Share cho bài thi này trong ngày hôm nay rồi, Hoặc địa chỉ IP máy tính của bạn đã Share trong ngày hôm nay rồi!';
                }
            }
        }

        return Response::json([
            'code' => $code,
            'count' => $count,
            'message' => $message
        ]);
    }

    public function vote(Request $request)
    {
        $submitId = InputHelper::getInt('id', 0);
        $user = Auth::user();

        $code = 1;
        $count = 1;
        $message = '<img src="/assets/img/done.png"/> Like bài thành công!<br/>Mỗi ngày bạn có thể Like 1 lần';

        $submit = Submit::find($submitId);
        if (time() > config('custom.round_1_end_vote')) {
            $code = 0;
            $message = '<img src="/assets/img/fail.png"/> Cuộc thi đã kết thúc, bạn không thể Like cho bài thi nữa!';
        } else {
            if ($user && $user->id) {
                $captcha = InputHelper::getCaptcha($request);
                if (!$captcha) {
                    return Response::json(array(
                        'code' => 0,
                        'message' => '<img src="/assets/img/fail.png"/> Lỗi khi xác thực người dùng. Vui lòng bấm thử lại!'
                    ));
                } else {
                    $count = $this->submitModel->voteSubmit($submitId, $user->id, 'votes', $request->header('User-Agent'));
                    if (!$count) {
                        $code = 0;
                        $message = '<img src="/assets/img/fail.png"/> Mỗi ngày bạn chỉ được Like 1 lần.<br/>Bạn đã Like bài thi này hôm nay rồi!<br/>';
                        /*if(in_array($submitId, Submit::getCheckId())){
                            $message = "Bạn đã Like bài dự thi thành công, Xin cảm ơn!<br/><br/>Ghi chú: Mỗi ngày bạn có thể Like cho 1 bài thi 1 lần.";
                        }*/
                    } else {
                        //$category = SubmitCategory::find($submit->cat_id);
                        //$category->update(['submit_vote' => ($category->submit_vote + 1)]);
                    }
                }
            } else {
                $code = 0;
                $message = '<img src="/assets/img/fail.png"/> Bạn phải đăng nhập để Like cho bài này.<br/>Click <a href="#" onclick="return fblogin();">vào đây</a> để đăng nhập bằng Facebook';
            }
        }

        return Response::json([
            'code' => $code,
            'message' => $message,
            'count' => $count,
            'ip' => RequestHelper::get_client_ip()
        ]);
    }

    public function edit($id, Request $request)
    {
        dd('Access denied');

        $round = config('custom.round', 1);
        $user = Auth::user();
        if (!$user) dd('Access denied');
        $submit = Submit::findOrFail($id);
        if ($submit->user_id != $user->id) dd('Access denied');
        $maxSize = UploadController::file_upload_max_size();
        $maxSize = UploadController::formatSizeUnits($maxSize);

        if ($round == 1) {
            $categories = SubmitCategory::all();
            $color = [];
            foreach ($categories as $i => $item) {
                $color[$item->id] = $this->color_random[$i];
            }
            return view('frontend.submit.edit', ['user' => $user, 'submit' => $submit, 'color' => $color, 'maxSize' => $maxSize]);
        }

        if ($round == 2) {
            if (strtotime(config('custom.round_2_end_create')) > time()) {
                if ($submit->cat_id == 1 || $submit->cat_id == 5)
                    $prefix_view = '1';
                else if ($submit->cat_id == 3 || $submit->cat_id == 4)
                    $prefix_view = '3';
                else $prefix_view = '2';
                return view('frontend.submit.edit_' . $prefix_view, ['user' => $user, 'submit' => $submit, 'maxSize' => $maxSize]);
            } else {
                return view('frontend.submit.create');
            }
        }
    }

    public function update($id, Request $request)
    {
        $round = config('custom.round', 1);

        return $this->updateFirstRound($id);
    }

    public function profile($userId, Request $request) {
        $user = User::where('id', $userId)->first();
        if(!$user) {
            dd('Không tồn tại đường dẫn này');
        }
        $submitModel = new Submit();
        $catId = InputHelper::getInt('cat',  0);

        if($catId < 0 || $catId > 3) {
            return redirect('/profile/'.$userId);
        }

        $submitsByUser = $submitModel->getSubmitsByUser($userId, 1, $catId);

        return view('frontend.submit.index')
            ->with('submits', $submitsByUser)
            ->with('user', $user)
            ->with('catId', $catId);
    }

    public function checkUser(Request $request){
        $user = Auth::user();
        $submitId = InputHelper::getInt('id', 0);
        $submit = Submit::where('id', $submitId)->where('status', 1)->first();

        if(!$user){
            return Response::json([
                'code' => 2,
                'message' => 'Bạn phải đăng nhập để tham gia cuộc thi!'
            ]);
        }

        return Response::json([
            'code' => 1,
            'message' => 'Xác nhận Vote'
        ]);
    }
}
