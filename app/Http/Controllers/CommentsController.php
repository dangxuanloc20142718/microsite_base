<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Validator;

use App\Models\Comments;

use Session;
use Mail;

class CommentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = Comments::getComments( $request->item_id );

        return Response::json(array(
            'code' => 1,
            'data' => view('frontend.comments.index')->with('items', $items)->render(),
            'msg' => "Lấy dữ liệu thành công!"
        ));
    }

    public function create(){
        $code=rand(1000,9999);
        Session::put('captcha', $code);

        $im = imagecreatetruecolor(50, 35);
        $bg = imagecolorallocate($im, 242, 242, 242); //background color blue
        $fg = imagecolorallocate($im, 0, 0, 0);//text color white
        imagefill($im, 0, 0, $bg);
        imagestring($im, 5, 5, 10,  $code, $fg);
        header("Cache-Control: no-cache, must-revalidate");
        header('Content-type: image/png');
        imagepng($im);
        return '';
    }

    /**
     * Save contact
     *
     */
    public function store(Request $request)
    {
        $data = $request->only(
            'type',
            'item_id',
            'fullname',
            'mobile',
            'comment'
        );

        $rules = array(
            'fullname' => 'required',
            'mobile' => 'required',
            'comment' => 'required'
        );

        if( Session::get('captcha') != $request->captcha ){
            return Response::json(array(
                'code' => 0,
                'html' => 'Mã bảo vệ không chính xác'
            ));
        }

        $validator = Validator::make($data, $rules);
        if ($validator->fails()){
            return Response::json(array(
                'code' => 0,
                'msg' => $validator->getMessageBag()->toArray()
            ));
        }else {
            //Session::forget('captcha');
            $data['status'] = ( config('custom.comment_approve') ) ? 1 : 0;
            
            $item = new Comments();
            $item::create($data);

            /*if( config('custom.mail_comment') ) {
                // Send email to admin
                Mail::send('emails.comments.admin', ['item' => $request], function ($m) use ($item) {
                    $m->from(config('custom.admin_email'), config('custom.admin_name'));
                    $m->to(config('custom.admin_email'), config('custom.admin_name'))
                        ->subject('Bạn có Comment mới từ website ' . url(''));
                });
            }*/
        }

        return Response::json(array(
            'code' => 1,
            'msg' => "Gửi thông tin thành công!"
        ));
    }
}
