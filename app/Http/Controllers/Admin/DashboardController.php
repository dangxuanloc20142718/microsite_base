<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

use App\Models\News;
use App\Models\User;
use App\Models\Contact;
use App\Models\Comments;

class DashboardController extends Controller
{
    //
    public function index(){
        $newNews = News::where('created_at', '>=', date('Y-m-d 00:00:00', strtotime('-7 days')))->select('id')->orderBy('id', 'DESC')->get();
        $newUser = User::where('created_at', '>=', date('Y-m-d 00:00:00', strtotime('-7 days')))->select('id')->orderBy('id', 'DESC')->get();

        $newContactCount = Contact::where('created_at', '>=', date('Y-m-d 00:00:00', strtotime('-7 days')))->select('id')->orderBy('id', 'DESC')->get();
        $newCommentsCount = Comments::where('created_at', '>=', date('Y-m-d 00:00:00', strtotime('-7 days')))->select('id')->orderBy('id', 'DESC')->get();

        $newContact = Contact::where('created_at', '>=', date('Y-m-d 00:00:00', strtotime('-7 days')))
            ->orderBy('id', 'DESC')->limit(5)->get();
        $newComments = Comments::where('created_at', '>=', date('Y-m-d 00:00:00', strtotime('-7 days')))
            ->orderBy('id', 'DESC')->limit(5)->get();

        return view('admin.dashboard.dashboard')
            ->with('newNews', $newNews)
            ->with('newUser', $newUser)
            ->with('newContactCount', count($newContactCount))
            ->with('newCommentsCount', count($newCommentsCount))
            ->with('newContact', $newContact)
            ->with('newComments', $newComments);
    }
}
