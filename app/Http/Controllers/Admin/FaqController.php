<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use App\Helper\RequestHelper;

use Session;
use App\Models\Faq;

class FaqController extends Controller
{
    public $view = 'admin.faq';
    public $link = 'admin/faq';

    public function index(Request $request){
        if($request->status != ''){
            Faq::setStatus($request->id, $request->status);
        }

        $limit = RequestHelper::getRequestSession('limit', 20);

        $model = new Faq();

        $items = $model->search($request->keyword)->paginate($limit);

        return view($this->view.'.index')->with('items', $items);
    }

    public function create(){

        return view($this->view.'.create');
    }

    public function edit($id){

        //Tìm article thông qua mã id tương ứng
        $item = Faq::findOrFail($id);
        // Gọi view edit.blade.php hiển thị bải viết
        return view($this->view.'.edit', compact('item'));
    }

    public function destroy($id){
        $news = Faq::find($id);
        $news->delete();

        Session::flash('message', 'Xóa thành công!');
        return redirect($this->link);
    }

    /**
     * Update record
     *
     * @param int $id
     * @param CheckNewsRequest $request
     * @return Ambigous
     */
    public function update($id, Request $request){

        $data = $request->all();
        $item = Faq::findOrFail($id);
        $item->update($data);

        Session::flash('message', 'Cập nhật thành công!');
        return redirect($this->link);
    }

    /**
     * Save news
     *
     */
    public function store(Request $request){

        $data = $request->all();
        $model = new Faq();
        $model::create($data);

        Session::flash('message', 'Tạo mới thành công!');
        return redirect($this->link);
    }
}
