<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Response;
use App\Helper\StringHelper;
use App\Helper\FileHelper;

use Session;
use DB;
use App\Models\Comments;
use App\Models\News;
use App\Models\Hair;
use App\Models\Salon;

class CommentsController extends Controller
{
    public $view = 'admin.comments';
    public $link = 'admin/comments';

    public function index(Request $request){
        if($request->count){
            $count = DB::table('comments')->where('admin_view', 0)->count();
            return Response::json([
                'code' => 1,
                'data' => $count
            ]);
        }
        if($request->status != ''){
            Comments::setStatus($request->id, $request->status);
        }
        
        $news = Comments::search(array(1))->orderBy('id', 'DESC')->paginate(20);
        if(count($news)){
           foreach ($news as $k=>&$new){
               $new->item_title = '';
               $new->item_alias = '';
               $new->link = '';

               if($new->item_id) {
                   if($new->type == 1) {
                       $item = News::find($new->item_id);
                   }elseif($new->type == 3){
                       $item = Hair::find($new->item_id);
                   }elseif($new->type == 4){
                       $item = Salon::find($new->item_id);
                   }

                   if (is_object($item)) {
                       $new->item_title = $item->title;
                       $new->item_alias = $item->alias;

                       if($new->type == 1){
                           $new->link = url($new->item_alias.'-n'.$new->item_id.'.html');
                       }elseif($new->type == 3){
                           $new->link = url($new->item_alias.'-h'.$new->item_id.'.html');
                       }elseif($new->type == 4){
                           $new->link = url($new->item_alias.'-s'.$new->item_id.'.html');
                       }
                   }
               }
            }
        }
        DB::table('comments')->where('admin_view', 0)->update(['admin_view' => 1]);

        return view($this->view.'.index')->with('news', $news);
    }

    public function create(){

        return view($this->view.'.create');
    }

    public function edit($id){

        //Tìm article thông qua mã id tương ứng
        $news = Comments::findOrFail($id);

        // Gọi view edit.blade.php hiển thị bải viết
        return view($this->view.'.edit', compact('news'));
    }

    public function destroy($id){
        $news = Comments::find($id);
        if($news->image){
            // Delete image
            @unlink(  StringHelper::getUploadPath($news->image));
        }
        if($news->banner){
            // Delete image
            @unlink(  StringHelper::getUploadPath($news->banner));
        }

        $news->delete();

        Session::flash('message', 'Xóa thành công!');
        return redirect($this->link);
    }

    /**
     * Update record
     *
     * @param int $id
     * @param CheckNewsRequest $request
     * @return Ambigous
     */
    public function update($id, Request $request){

        $data = $request->all();
        $news = Comments::findOrFail($id);

        $image = FileHelper::uploadImage('image_upload', 'news', 1200);
        if($image){
            if($news->image){
                // Delete image
                @unlink(  StringHelper::getUploadPath($news->image));
            }
            $data['image'] = $image;
        }
        // Xóa ảnh?
        if(empty($data['image']) && $news->image){
            @unlink(StringHelper::getUploadPath($news->image));
        }

        $news->update($data);

        Session::flash('message', 'Cập nhật thành công!');
        return redirect($this->link);
    }

    /**
     * Save news
     *
     */
    public function store(Request $request){

        $data = $request->all();

        $image = FileHelper::uploadImage('image_upload', 'news', 1200);
        if($image){
            $data['image'] = $image;
        }

        $news = new Comments();
        $news::create($data);

        Session::flash('message', 'Tạo mới thành công!');
        return redirect($this->link);
    }
}
