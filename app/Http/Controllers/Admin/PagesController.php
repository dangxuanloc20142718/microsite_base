<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheckPagesRequest;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Helper\StringHelper;
use App\Helper\FileHelper;

use Session;
use App\Models\Admin\Pages;

class PagesController extends Controller
{
    public $view = 'admin.pages';
    public $link = 'admin/pages';

    public function index(Request $request){
        if($request->status != ''){
            Pages::setStatus($request->id, $request->status);
        }
        
        $news = Pages::search()->orderBy('id', 'DESC')->paginate(20);

        return view($this->view.'.index')->with('news', $news);
    }

    public function create(){

        return view($this->view.'.create');
    }

    public function edit($id){

        //Tìm article thông qua mã id tương ứng
        $news = Pages::findOrFail($id);

        // Gọi view edit.blade.php hiển thị bải viết
        return view($this->view.'.edit', compact('news'));
    }

    public function destroy($id){
        $news = Pages::find($id);
        if($news->image){
            // Delete image
            @unlink(  StringHelper::getUploadPath($news->image));
        }
        if($news->banner){
            // Delete image
            @unlink(  StringHelper::getUploadPath($news->banner));
        }

        $news->delete();

        Session::flash('message', 'Xóa thành công!');
        return redirect($this->link);
    }

    /**
     * Update record
     *
     * @param int $id
     * @param CheckNewsRequest $request
     * @return Ambigous
     */
    public function update($id, CheckPagesRequest $request){

        $data = $request->all();
        $news = Pages::findOrFail($id);

        $image = FileHelper::uploadImage('image_upload', 'news', array(['w'=>1200, 'h'=>null]), true);
        if($image){
            if($news->image){
                // Delete image
                @unlink(  StringHelper::getUploadPath($news->image));
            }
            $data['image'] = $image;
        }
        // Xóa ảnh?
        if(empty($data['image']) && $news->image){
            @unlink(StringHelper::getUploadPath($news->image));
        }
        $data['alias'] = StringHelper::removeSign($data['title']);

        $news->update($data);

        Session::flash('message', 'Cập nhật thành công!');
        return redirect($this->link);
    }

    /**
     * Save news
     *
     */
    public function store(CheckPagesRequest $request){

        $data = $request->all();

        $image = FileHelper::uploadImage('image_upload', 'news', array(['w'=>1200, 'h'=>null]), true);
        if($image){
            $data['image'] = $image;
        }
        $data['alias'] = StringHelper::removeSign($data['title']);
        
        $news = new Pages();
        $news::create($data);

        Session::flash('message', 'Tạo mới thành công!');
        return redirect($this->link);
    }
}
