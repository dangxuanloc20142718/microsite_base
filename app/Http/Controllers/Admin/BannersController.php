<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheckBannersRequest;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Helper\StringHelper;
use App\Helper\RequestHelper;

use Session;
use App\Models\Banners;

class BannersController extends Controller
{
    public $view = 'admin.banners';
    public $link = 'admin/banners';

    public $positions = array(
        'home1' => 'Trang chủ (1140 x 400)',
        'popup' => 'Popup Trang chủ 600 x 400)',
        'promotion' => 'Khuyến mại Trang chủ (500 x 550)',
        'catalog' => 'Catalog Trang chủ (500 x 600)',
        'content' => 'Trang con bên phải (600 x 450)',
        'customer' => 'Logo Khách hàng (400 x 200)'
    );
    public $positionsSize = array(
        'home1' => [1140, 400],
        'popup' => [600, 400],
        'promotion' => [500, 550],
        'catalog' => [500, 200],
        'customer' => [400, 200],
        'content' => [600, 450]
    );
    public $allowImages = array('jpg', 'jpeg', 'png', 'bmp');
    public $allowExtensions = array('jpg', 'jpeg', 'png', 'bmp', 'gif', 'tiff');

    public function index(Request $request){
        if($request->order){
            Banners::setOrder($request->id, $request->order);
        }
        if($request->status != ''){
            Banners::setStatus($request->id, $request->status);
        }

        $position = RequestHelper::getRequestSession('position', 'home1');
        $keyword = Input::get('keyword');

        $news = Banners::search($position, $keyword)->orderBy('ordering', 'ASC')->paginate(20);
        return view($this->view.'.index')
            ->with('news', $news)
            ->with('position', $position)
            ->with('positions', $this->positions);
    }

    public function create(){
        $position = RequestHelper::getRequestSession('position', 'home1');

        return view($this->view.'.create')
            ->with('position', $position)
            ->with('positions', $this->positions);
    }

    public function edit($id){
        $position = RequestHelper::getRequestSession('position', 'home1');

        //Tìm article thông qua mã id tương ứng
        $news = Banners::findOrFail($id);

        // Gọi view edit.blade.php hiển thị bải viết
        return view($this->view.'.edit', compact('news'))
            ->with('position', $position)
            ->with('positions', $this->positions);
    }

    public function destroy($id){
        $news = Banners::find($id);
        if($news->image){
            // Delete image
            @unlink(  StringHelper::getUploadPath($news->image));
        }

        $news->delete();

        Session::flash('message', 'Xóa thành công!');
        return redirect($this->link);
    }

    /**
     * Update record
     *
     * @param int $id
     * @param CheckBannersRequest $request
     * @return Ambigous
     */
    public function update($id, CheckBannersRequest $request){

        $data = $request->all();
        $news = Banners::findOrFail($id);

        if(Input::hasFile('image_upload')){
            $image = Input::file('image_upload');
            $filename  = time() . '.' . $image->getClientOriginalExtension();

            $filePath = 'uploads/banners/';

            if(substr($image->getMimeType(), 0, 5) == 'image') {
                if( in_array(strtolower($image->getClientOriginalExtension()), $this->allowImages) ) {
                    Image::make($image->getRealPath())->resize($this->positionsSize[$request->position][0], $this->positionsSize[$request->position][1])
                        ->save(StringHelper::getUploadPath($filePath.$filename));
                }else{
                    $image->move(StringHelper::getUploadPath($filePath), $filename);
                }

                if($news->image){
                    // Delete image
                    @unlink(  StringHelper::getUploadPath($news->image));
                }
                $data['image'] = $filePath.$filename;
            }
        }

        $news->update($data);

        Session::flash('message', 'Cập nhật thành công!');
        return redirect($this->link);
    }

    /**
     * Save news
     *
     */
    public function store(CheckBannersRequest $request){

        $data = $request->all();
        if(Input::hasFile('image_upload')){
            $image = Input::file('image_upload');
            $filename  = time() . '.' . $image->getClientOriginalExtension();

            if(substr($image->getMimeType(), 0, 5) == 'image') {
                $filePath = 'uploads/banners/' ;

                if (in_array(strtolower($image->getClientOriginalExtension()), $this->allowImages)) {
                    Image::make($image->getRealPath())->resize($this->positionsSize[$request->position][0], $this->positionsSize[$request->position][1])
                        ->save(StringHelper::getUploadPath($filePath . $filename));
                } else {
                    $image->move(StringHelper::getUploadPath($filePath), $filename);
                }

                $data['image'] = $filePath . $filename;
            }
        }

        $news = new Banners();
        $news::create($data);

        Session::flash('message', 'Tạo mới thành công!');
        return redirect($this->link);
    }
}
