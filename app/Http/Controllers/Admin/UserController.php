<?php

namespace App\Http\Controllers\Admin;

use App\Helper\InputHelper;
use App\Models\News;
use Illuminate\Http\Request;
use App\Helper\FileHelper;
use App\Http\Requests;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheckUserRequest;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redirect;
use App\Helper\StringHelper;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Helper\RequestHelper;

use Session;
use DB;
use File;
use Auth;
use App\Models\User;
use App\Models\Submit;

class UserController extends Controller
{
    public $view = 'admin.user';
    public $link = 'admin/user';

    public function index(Request $request)
    {
        $user = Auth::user();
        if ($request->action == 'export') {
            return $this->export();
        }
        $group = (int)RequestHelper::getRequestSession('group', '');
        $keyword = Input::get('keyword');
        $userId = Input::get('user_id', 0);
        $promotion = Input::get('promotion');

        $users = User::search($group, $keyword, $promotion)->where('group', '>=', $user->group)->orderBy('id', 'DESC');
        if($userId){
            $users->where('id', $userId);
        }
        $users = $users->paginate(20);

        if ($request->ajax()) {
            $users = User::search($group, $keyword, $promotion)->orderBy('id', 'DESC')->limit(20)->get();
            return Response::json($users);
        }

        return view($this->view . '.index', compact('group', 'keyword', 'promotion'))
            ->with('news', $users)
            ->with('groups', User::getUserGroups());
    }

    public function create()
    {
        /*$newsCategory = new NewsCategory();*/
        $newsCats = [];//$newsCategory->getCategoryTree(0, '');

        return view($this->view . '.create', compact('newsCats'));
    }

    public function edit($id)
    {
        /*$newsCategory = new NewsCategory();*/
        $newsCats = [];//$newsCategory->getCategoryTree(0, '');

        //Tìm article thông qua mã id tương ứng
        $news = User::findOrFail($id);
        $news->permission = explode("\n", $news->permission);

        // Gọi view edit.blade.php hiển thị bải viết
        return view($this->view . '.edit', compact('news', 'newsCats'));
    }

    public function destroy($id)
    {
        $news = User::find($id);
        if ($news->image) {
            // Delete image
            @unlink(StringHelper::getUploadPath($news->image));
        }

        $news->delete();

        Session::flash('message', 'Xóa thành công!');
        return redirect($this->link);
    }

    /**
     * Update record
     *
     * @param int $id
     * @param CheckNewsRequest $request
     * @return Ambigous
     */
    public function update($id, CheckUserRequest $request)
    {
        $currUser = Auth::user();

        $data = $request->all();
        $user = User::findOrFail($id);
        $response = array(
            'code' => 1,
            'message' => 'Success update user profile'
        );

        if (!empty($data['email']) && User::checkEmail($data['email'], $id)) {
            if($request->ajax()) {
                $response['code'] = 0;
                $response['message'] = 'Fail update user profile';
                return Response::json($response);
            }
            return Redirect::back()->withInput()->withErrors(array('email' => 'Email: "' . $request->email . '" đã tồn tại. Vui lòng chọn email khác.'));
        }

        if (Input::file()) {
            $image = Input::file('image_upload');
            $filename = time() . '.' . $image->getClientOriginalExtension();

            $filePath = 'uploads/avatar/' . $filename;

            Image::make($image->getRealPath())->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(StringHelper::getUploadPath($filePath));

            if ($user->avatar) {
                // Delete image
                @unlink(StringHelper::getUploadPath($user->avatar));
            }
            $data['avatar'] = $filePath;
        }

        if ($request->password_new) {
            $data['password'] = Hash::make(trim($data['password_new']));
        }

        $data['group'] = $data['group'] >= $currUser->group ? $data['group'] : $currUser->group;

        $user->update($data);
        //User::setPermission($id, $request);

        if(!$request->ajax()) {
            Session::flash('message', 'Cập nhật thành công!');
            if ($request->action == 'save') {
                return redirect('admin/user');
            } else {
                return redirect('admin/user/' . $user->id . '/edit');
            }
        } else {
            $response['user'] = $user;
            return Response::json($response);
        }
    }

    /**
     * Save news
     *
     */
    public function store(CheckUserRequest $request)
    {
        $currUser = Auth::user();
        $data = $request->all();

        if (User::checkEmail($request->email)) {
            return Redirect::back()->withInput()->withErrors(array('email' => 'Email: "' . $request->email . '" đã tồn tại. Vui lòng chọn email khác.'));
        }

        if (Input::file()) {
            $image = Input::file('image_upload');
            $filename = time() . '.' . $image->getClientOriginalExtension();

            $filePath = 'uploads/avatar/' . $filename;

            Image::make($image->getRealPath())->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(StringHelper::getUploadPath($filePath));

            $data['avatar'] = $filePath;
        }
        if ($request->password_new) {
            $data['password'] = Hash::make(trim($data['password_new']));
        }

        $data['group'] = $data['group'] >= $currUser->group ? $data['group'] : $currUser->group;

        $news = new User();
        $news = $news::create($data);
        //User::setPermission($news->id, $request);

        Session::flash('message', 'Tạo mới thành công!');
        if ($request->action == 'save') {
            return redirect('admin/user');
        } else {
            return redirect('admin/user/' . $news->id . '/edit');
        }
    }

    public function export()
    {
        $groups = User::getUserGroups();
        $page = 1;
        $totalPage = round(User::count() / 10000) + 1;

        for ($i = 1; $i <= $totalPage; $i++) {
            Excel::create('Kenh14_' . $i, function ($excel) use ($groups, $page, $totalPage) {

                Input::merge(['page' => $page]);
                $items = User::search()->orderBy('id', 'DESC')->paginate(10000);

                $excel->sheet('Users_' . $page, function ($sheet) use ($groups, $items, $page) {
                    $sheet->row(1, array(
                        'STT', 'Họ và tên', 'Tuổi', 'Số ĐT', 'Email', 'FB ID', 'Địa chỉ', 'Nhóm', 'Ngày đăng ký'
                    ));
                    $sheet->cell('A1:E1', function ($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->freezeFirstRow();

                    if ($items && count($items)) {
                        foreach ($items as $k => $item) {
                            $sheet->row($k + 2, array(
                                10000 * ($page - 1) + $k + 1,
                                $item->name,
                                Carbon::parse($item->birthday)->age,
                                $item->mobile,
                                $item->email,
                                $item->facebook_id . ' ',
                                $item->address,
                                (isset($groups[$item->group])) ? $groups[$item->group] : '',
                                date('d-m-Y', strtotime($item->created_at))
                            ));
                        }
                    }
                });

            })->store('xls', public_path('uploads/content'));

            $page++;
        }

        $html = [];
        for ($i = 1; $i <= $totalPage; $i++) {
            $link = url('uploads/content/Kenh14_' . $i . '.xls');
            $html[] = '<a href="' . $link . '">' . $link . '</a>';
        }

        return join('<br/>', $html);
    }

    public function updateFbAvatar(Request $request)
    {
        $users_id = $request->get('users');
        $errors = [];
        $flag_avatar = true;

        foreach ($users_id as $i => $item) {
            $user = User::where('id', '=', $item)->where('group', '>', '2')->first();

            if ($user) {
                $flag_avatar = true;
                if ($user->avatar) {
                    if (File::exists(StringHelper::getUploadPath($user->avatar))) {
                        $flag_avatar = false;
                    }
                }

                if ($flag_avatar) {
                    // Save image into server
                    $avatar = "http://graph.facebook.com/" . $user->facebook_id . "/picture?type=large";
                    $saved = @Image::make($avatar)->save(public_path('uploads/avatar/' . $user->id . '.jpg'));
                    if ($saved) {
                        $avatar = 'uploads/avatar/' . $user->id . '.jpg';
                        $user->update(['avatar' => $avatar]);

                        // Generation image share fb
                        $submit = Submit::where('user_id', $item)->first();
                        if ($submit) {
                            $catClassFolder = StringHelper::getColorCategories()[(($submit->cat_id) - 1)][3];
                            try {
                                FileHelper::createImageSubmit(htmlspecialchars_decode($submit->introtext), $user->name, $user->avatar, $submit->id, $submit->image, $catClassFolder);
                            } catch (Exception $e) {
                                $errors[] = [$userId = $item, $userName = $user->name, $message = $e];
                            }
                        }
                    } else {
                        $errors[] = [$userId = $item, $userName = $user->name, $message = 'Chưa lưu được ảnh vào server'];
                    }
                }
            }
        }

        return Response::json([
            'status' => (count($errors)) ? 2 : 1,
            'message' => 'Request success',
            'data' => $errors
        ]);
    }

    // Change Password
    public function updatePassword(Request $request)
    {
        $this->validate($request,
            [
                'password_new' => 'required',
                'agree_change_password' => 'required'
            ],
            [
                'password_new.required' => 'Mật khẩu không được để trống!',
                'agree_change_password.required' => 'Bạn phải xác nhận đồng ý thay đổi mật khẩu!',
            ]
        );
        if (!Hash::check($request->password_old, Auth::user()->password)) {
            return Redirect::back()
                ->withInput()
                ->withErrors(array('password' => 'Mật khẩu cũ không đúng!'));
        }

        if ($request->password_new != $request->password_new_retype || empty($request->password_new)) {
            return Redirect::back()
                ->withInput()
                ->withErrors(array('password' => 'Hai mật khẩu mới không trùng nhau!'));
        }

        $password_new = $request->password_new;
        $user = new User();
        $user = User::findOrFail(Auth::user()->id);
        if ($request->password_new) {
            $user->password = Hash::make(trim($password_new));
        }
        $user->save();
        return redirect()->route('user.updatePassword')->with(['message' => 'Cập nhật mật khẩu thành công!']);
    }

    public function loginAsThisMember(Request $request)
    {
        $user = User::findOrFail($request->id);
        Auth::login($user, true);
        return redirect('/');
    }

    public function incognito($id)
    {
        $user = User::find($id);
        if (Auth::user() && (Auth::user()->group == 1 || Auth::user()->group == 2) && $user->group == 5) {
            User::setIncognito($user->id);
        } else {
            return 'Error! Bạn không thể xứ lý thao tác này!';
        }

        return redirect('/');
    }

    public function stopIncognito()
    {
        User::stopIncognito();
        return redirect()->back();
    }

}
