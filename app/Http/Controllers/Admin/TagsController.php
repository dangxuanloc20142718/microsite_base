<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use App\Helper\RequestHelper;

use Session;
use App\Models\Tags;

class TagsController extends Controller
{
    public $view = 'admin.tags';
    public $link = 'admin/tags';

    public function index(Request $request){
        if($request->status != ''){
            Tags::setStatus($request->id, $request->status);
        }

        $limit = RequestHelper::getRequestSession('limit', 20);

        $tagsModel = new Tags();
        if($request->ajax){
            $data = [];
            $items = $tagsModel->search($request->keyword)->select('id', 'title')->limit(1000)->get();
            foreach ($items as $item){
                $data[] = $item->title.' :'.$item->id;
            }
            return Response::json(
                $data
            );
        }

        $news = $tagsModel->search($request->keyword)->paginate($limit);

        return view($this->view.'.index')->with('news', $news);
    }

    public function create(){

        return view($this->view.'.create');
    }

    public function edit($id){

        //Tìm article thông qua mã id tương ứng
        $news = Tags::findOrFail($id);

        // Gọi view edit.blade.php hiển thị bải viết
        return view($this->view.'.edit', compact('news'));
    }

    public function destroy($id){
        $news = Tags::find($id);
        $news->delete();

        Session::flash('message', 'Xóa thành công!');
        return redirect($this->link);
    }

    /**
     * Update record
     *
     * @param int $id
     * @param CheckNewsRequest $request
     * @return Ambigous
     */
    public function update($id, Request $request){

        $data = $request->all();
        $news = Tags::findOrFail($id);
        $news->update($data);

        Session::flash('message', 'Cập nhật thành công!');
        return redirect($this->link);
    }

    /**
     * Save news
     *
     */
    public function store(Request $request){

        $data = $request->all();
        $news = new Tags();
        $news::create($data);

        Session::flash('message', 'Tạo mới thành công!');
        return redirect($this->link);
    }
}
