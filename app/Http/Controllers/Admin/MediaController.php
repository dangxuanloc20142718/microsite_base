<?php

namespace App\Http\Controllers\Admin;

use App\Helper\InputHelper;
use App\Http\Controllers\Controller;
use App\Models\Admin\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Session;

class MediaController extends Controller
{

    public function index(Request $request){

    }

    public function create(){

    }

    public function edit($id){

    }

    public function update($id){
        $media = Media::findOrFail($id);
        if($media) {
            $introtext = InputHelper::getRawString('introtext');
            if($introtext)
            $media->update(['title' => $introtext]);
        }
        return Response::json([
            'status' => 1,
            'message' => 'Request success',
            'data' => $media
        ]);
    }

    /**
     * Save news
     *
     */
    public function store(Request $request){

    }
}
