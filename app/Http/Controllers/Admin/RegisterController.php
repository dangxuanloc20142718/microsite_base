<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheckPagesRequest;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel as Excel;

use Session;
use App\Models\Contact;

class RegisterController extends Controller
{
    public $view = 'admin.register';
    public $link = 'admin/register';

    public function index(){
        $limit = Input::get('limit', Session::get('limit', 20));
        if(Input::get('limit')) {
            Session::put('limit', $limit);
        }

        $news = Contact::search(2)->orderBy('id', 'DESC')->paginate($limit);
        
        return view($this->view.'.index')->with('news', $news)->with('limit', $limit);
    }

    public function create(){

        return view($this->view.'.create');
    }

    public function edit($id){

        //Tìm article thông qua mã id tương ứng
        $news = Contact::findOrFail($id);

        // Gọi view edit.blade.php hiển thị bải viết
        return view($this->view.'.edit', compact('news'));
    }

    public function destroy($id){
        $news = Contact::find($id);
        if($news->image){
            // Delete image
            @unlink(  StringHelper::getUploadPath($news->image));
        }

        $news->delete();

        Session::flash('message', 'Xóa thành công!');
        return redirect($this->link);
    }

    /**
     * Update record
     *
     * @param int $id
     * @param CheckNewsRequest $request
     * @return Ambigous
     */
    public function update($id, Request $request){

        $data = $request->all();
        $news = Contact::findOrFail($id);

        $news->update($data);

        Session::flash('message', 'Cập nhật thành công!');
        return redirect($this->link);
    }

    /**
     * Save news
     *
     */
    public function store(Request $request){

        $data = $request->all();

        $news = new Contact();
        $news::create($data);

        Session::flash('message', 'Tạo mới thành công!');
        return redirect($this->link);
    }

    public function export(){
        Excel::create('Thucphamsach.Soha.vn', function($excel) {

            $excel->sheet('Đăng ký', function($sheet) {
                $sheet->row(1, array(
                    'STT', 'Họ và tên', 'Số ĐT', 'Email', 'Công ty', 'Ngày đăng ký'
                ));
                $sheet->cell('A1:F1', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->freezeFirstRow();

                $items = Contact::search(2)->orderBy('id', 'DESC')->paginate(10000);
                foreach ($items as $k=>$item){
                    $sheet->row($k+2, array(
                        $k+1,
                        $item->fullname,
                        $item->mobile,
                        $item->email,
                        $item->company,
                        $item->created_at
                    ));
                }
            });

            $excel->sheet('Đăng ký Kết nối', function($sheet) {
                $sheet->row(1, array(
                    'STT', 'Công ty', 'Số ĐT', 'Email', 'Vấn đề', 'Cần gặp', 'Ngày đăng ký'
                ));
                $sheet->cell('A1:G1', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->freezeFirstRow();

                $items = Contact::search(3)->orderBy('id', 'DESC')->paginate(10000);
                foreach ($items as $k=>$item){
                    $sheet->row($k+2, array(
                        $k+1,
                        $item->company,
                        $item->mobile,
                        $item->email,
                        $item->problem,
                        $item->person,
                        $item->created_at
                    ));
                }
            });

        })->download('xls');
    }
}
