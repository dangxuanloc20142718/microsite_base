<?php

namespace App\Http\Controllers\Admin;

use App\Helper\RequestHelper;
use App\Helper\InputHelper;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use App\Helper\StringHelper;
use App\Helper\FileHelper;
use Session;

use App\Models\Submit;
use App\Models\SubmitCategory;
use App\Models\Location as LocationsModel;

class SubmitCategoryItemsController extends Controller
{
    public $routeName = 'submit_category_items';
    public $model = null;
    public $parentModel = null;
    public $types = null;

    public function __construct(){
        $this->model = new Submit();
        $this->parentModel = new SubmitCategory();

        $this->types = [0 => '--Tất cả tháng--'] + $this->model->getTypes();
    }

    public function index(Request $request){
        if($request->ajax()){
            if($request->has('field')) {
                $this->model->setFieldValue($request->id, $request->field, $request->value);
            }

            if($request->has('order')) {
                $this->model->setOrder($request->id, $request->order);
            }
        }

        $keyword = InputHelper::getString('keyword', '');
        $limit = RequestHelper::getRequestSession('limit', 20);
        $catId = RequestHelper::getRequestSession('cat_id', 0);
        $typeId = InputHelper::getInt('type_id', 0);
        $orderBy = InputHelper::getInt('orderBy', 0);

        $orderField = 'id';
        $orderFieldOrder = 'DESC';
        if($orderBy == 1){
            $orderField = 'votes';
            $orderFieldOrder = 'DESC';
        }
        if($orderBy == 2){
            $orderField = 'ordering';
            $orderFieldOrder = 'ASC';
        }

        $items = $this->model->search(['cat_id' => $catId, 'round' => $typeId, 'keyword' => $keyword],
            $orderField, $orderFieldOrder, false)
            ->paginate($limit);

        $selectCat = $this->parentModel->getCategorySelect();

        if($request->ajax()){
            return Response::json([
                'code' => 1,
                'message' => '',
                'html' => view('admin.'.$this->routeName.'.index_table')->with('items', $items)->with('routeName', $this->routeName)->render(),
            ]);
        }

        return view('admin.'.$this->routeName.'.index')
            ->with('items', $items)
            ->with('selectCat', $selectCat)
            ->with('types', $this->types)
            ->with('routeName', $this->routeName);
    }

    /**
     * Update record
     *
     * @param int $id
     * @param Request $request
     * @return Ambigous
     */
    public function update($id, Request $request){

        $data = $request->all();
        $item = $this->model->findOrFail($id);

        $data['image'] = FileHelper::processCropImage($request->image);
        if($request->has('gallery')){
            $obj = [];
            foreach ($data['gallery'] as $link => $title){
                $gallery = new \stdClass();
                $gallery->link = $link;
                $gallery->title = $title;
                $obj[] = $gallery;
            }
            $data['gallery'] = json_encode($obj);
        }

        $data['round'] = 1;
        $item->update($data);

        Session::flash('message', 'Cập nhật thành công!');
        if($request->_action == 'save') {
            return redirect('admin/' . $this->routeName);
        }else{
            return redirect('admin/' . $this->routeName . '/' . $item->id . '/edit');
        }
    }

    /**
     * Save item
     */
    public function store(Request $request){
        $data = $request->all();

        $data['image'] = FileHelper::processCropImage($request->image);
        $data['status'] = -1;
        $data['round'] = 1;

        $item = $this->model->create($data);

        Session::flash('message', 'Tạo mới thành công!');
        if($request->_action == 'save') {
            return redirect('admin/' . $this->routeName);
        }else{
            return redirect('admin/' . $this->routeName . '/' . $item->id . '/edit');
        }
    }

    public function create(){
        $parents = $this->parentModel->getCategorySelect();

        return view('admin.'.$this->routeName.'.create')
            ->with('parents', $parents)
            ->with('types', $this->types)
            ->with('routeName', $this->routeName);
    }

    public function edit($id){
        $item = $this->model->findOrFail($id);
        $parents = $this->parentModel->getCategorySelect();

        return view('admin.'.$this->routeName.'.edit', compact('item'))
            ->with('parents', $parents)
            ->with('types', $this->types)
            ->with('routeName', $this->routeName);
    }

    public function destroy($id, Request $request){
        $item = $this->model->find($id);

        // Delete image
        if($item->image){
            @unlink(StringHelper::getUploadPath($item->image));
        }

        $item->status = -2;
        $item->save();

        if($request->ajax()){
            return $this->index($request);
        }

        Session::flash('message', 'Xóa thành công!');
        return redirect('admin/'.$this->routeName);
    }
}
