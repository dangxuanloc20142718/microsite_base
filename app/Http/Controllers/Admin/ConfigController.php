<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheckUserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redirect;
use App\Helper\StringHelper;

use Session;
use DB;
use File;
use Storage;
use App\Models\User;

class ConfigController extends Controller
{
    public $view = 'admin.config';
    public $link = 'admin/config';

    public function index(){

        $news = User::search()->orderBy('id', 'DESC')->paginate(20);

        return view($this->view.'.index')
            ->with('news', $news)
            ->with('groups', User::getUserGroups());
    }

    public function create(){

        return view($this->view.'.create');
    }

    public function edit($id){

        //Tìm article thông qua mã id tương ứng
        $news = User::find(1);

        // Gọi view edit.blade.php hiển thị bải viết
        return view($this->view.'.edit', compact('news'));
    }

    public function destroy($id){
        $news = User::find($id);
        if($news->image){
            // Delete image
            @unlink(  StringHelper::getUploadPath($news->image));
        }

        $news->delete();

        Session::flash('message', 'Xóa thành công!');
        return redirect($this->link);
    }

    /**
     * Update record
     *
     * @param int $id
     * @param CheckNewsRequest $request
     * @return Ambigous
     */
    public function update($id, Request $request){

        $config = $request->except('image_favicon', 'image_logo', 'image_fbimage', 'image_offline');

        if($request->hasFile('image_favicon')){
            $image = $request->file('image_favicon');
            $fileName  = 'favicon.ico';
            $filePath  = StringHelper::getUploadPath('uploads/others/');
            $image->move($filePath, $fileName);
        }
        if($request->hasFile('image_logo')){
            $image = $request->file('image_logo');
            $fileName  = 'logo.png';
            $filePath  = StringHelper::getUploadPath('uploads/others/');
            $image->move($filePath, $fileName);
        }
        if($request->hasFile('image_fbimage')){
            $image = $request->file('image_fbimage');
            $fileName  = 'fbimage.jpg';
            $filePath  = StringHelper::getUploadPath('uploads/others/');
            $image->move($filePath, $fileName);
        }
        if($request->hasFile('image_offline')){
            $image = $request->file('image_offline');
            $fileName  = 'offline.jpg';
            $filePath  = StringHelper::getUploadPath('uploads/others/');
            $image->move($filePath, $fileName);
        }

        $config['version'] = time();
        $content = '<?php return ' . var_export($config, true) . ';';
        File::put(config_path() . '/custom.php', $content);

        // Tạo file css
        if($config['theme']){
            $css[] = "html, body, .body-content { color:".$config['theme_web_color']."; background:url('".$config['theme_web_url']."') ".$config['theme_web_repeat']." ".$config['theme_web_position']." ".$config['theme_web_bgcolor']."; }";
            $css[] = "html a, body a, ul.news li a { color:".$config['theme_web_acolor']."; }";

            $css[] = "header { color:".$config['theme_header_color']."; background:url('".$config['theme_header_url']."') ".$config['theme_header_repeat']." ".$config['theme_header_position']." ".$config['theme_header_bgcolor']."; }";
            $css[] = "header a, header a{ color:".$config['theme_header_acolor']."; }";

            $css[] = ".navbar { color:".$config['theme_menu_color']."; background:url('".$config['theme_menu_url']."') ".$config['theme_menu_repeat']." ".$config['theme_menu_position']." ".$config['theme_menu_bgcolor']."; }";
            $css[] = ".navbar a, .navbar .navbar-nav > li > a{ color:".$config['theme_menu_acolor']."; }";

            $css[] = "footer { color:".$config['theme_footer_color']."; background:url('".$config['theme_footer_url']."') ".$config['theme_footer_repeat']." ".$config['theme_footer_position']." ".$config['theme_footer_bgcolor']."; }";
            $css[] = "footer a{ color:".$config['theme_footer_acolor']."; }";

            @File::put(public_path('assets/css') . '/theme.css', implode("\n", $css));
        }else{
            @File::put(public_path('assets/css') . '/theme.css', '');
        }

        Session::flash('message', 'Cập nhật thành công!');
        // Gọi view edit.blade.php hiển thị bải viết
        return redirect($this->link.'/0/edit?tab=' . $request->tab);
    }

    /**
     * Save news
     *
     */
    public function store(CheckUserRequest $request){

        $data = $request->all();

        if( User::checkEmail($request->email) ){
            return Redirect::back()->withInput()->withErrors(array('email'=>'Email: "'.$request->email.'" đã tồn tại. Vui lòng chọn email khác.'));
        }
        
        if(Input::file()){
            $image = Input::file('image_upload');
            $filename  = time() . '.' . $image->getClientOriginalExtension();

            $filePath = 'uploads/avatar/' . $filename;

            Image::make($image->getRealPath())->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save( StringHelper::getUploadPath($filePath) );

            $data['avatar'] = $filePath;
        }
        if($request->password_new){
            $data['password'] = Hash::make(trim($data['password_new']));
        }

        $news = new User();
        $news::create($data);

        Session::flash('message', 'Tạo mới thành công!');
        return redirect($this->link);
    }
}
