<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheckMenuRequest;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Helper\StringHelper;
use App\Helper\FileHelper;
use App\Helper\RequestHelper;

use Session;
use App\Models\Menu;
use App\Models\Pages;
use App\Models\Admin\NewsCategory;
use App\Models\Admin\ProductsCategory;

class MenuController extends Controller
{
    public $view = 'admin.menu';
    public $link = 'admin/menu';

    public function index(Request $request){
        $menu = new Menu();
        if($request->order){
            $menu->setOrder($request->id, $request->order);
        }
        if($request->status != ''){
            $menu->setStatus($request->id, $request->status);
        }

        $position   = RequestHelper::getRequestSession('mposition', 'main');
        $positions  = $menu->getMenuPositions();
        $news = $menu->getCategoryTree(0, '', $position);

        return view($this->view.'.index', compact('position', 'positions'))->with('news', $news);
    }

    public function create(){
        $menuModel = new Menu();
        $newscatModel = new NewsCategory();
        $procatModel = new ProductsCategory();

        $position   = RequestHelper::getRequestSession('mposition', 'main');
        $parents = $menuModel->getCategorySelect($position);
        $newscat = $newscatModel->getNewsCategorySelect();
        $procat = $procatModel->getCategorySelect();
        $pages = array('0' => '--- Chọn Trang tin ---') + Pages::where('status', 1)->pluck('title', 'id')->toArray();

        return view($this->view.'.create', compact('position', 'newscat', 'procat', 'pages'))
            ->with('parents', $parents);
    }

    public function edit($id){
        $menuModel = new Menu();
        $newscatModel = new NewsCategory();
        $procatModel = new ProductsCategory();

        $position   = RequestHelper::getRequestSession('mposition', 'main');
        $parents = $menuModel->getCategorySelect($position);
        $newscat = $newscatModel->getNewsCategorySelect();
        $procat = $procatModel->getCategorySelect();
        $pages = array('0' => '--- Chọn Trang tin ---') + Pages::where('status', 1)->pluck('title', 'id')->toArray();

        //Tìm article thông qua mã id tương ứng
        $news = Menu::findOrFail($id);

        // Gọi view edit.blade.php hiển thị bải viết
        return view($this->view.'.edit', compact('news', 'position', 'newscat', 'procat', 'pages'))
            ->with('parents', $parents);
    }

    public function destroy($id){
        $news = Menu::find($id);
        if($news->image){
            // Delete image
            @unlink(  StringHelper::getUploadPath($news->image));
        }

        $news->delete();

        Session::flash('message', 'Xóa thành công!');
        return redirect($this->link);
    }

    /**
     * Update record
     *
     * @param int $id
     * @param CheckNewsRequest $request
     * @return Ambigous
     */
    public function update($id, CheckMenuRequest $request){

        $data = $request->all();
        $news = Menu::findOrFail($id);

        $position   = RequestHelper::getRequestSession('mposition', 'main');
        $data['position'] = $position;
        $this->processLink($data, $request);

        $image = FileHelper::uploadImage('image_upload', 'news', 1200);
        if($image){
            if($news->image){
                // Delete image
                @unlink(  StringHelper::getUploadPath($news->image));
            }
            $data['image'] = $image;
        }
        // Xóa ảnh?
        if(empty($data['image']) && $news->image){
            @unlink(StringHelper::getUploadPath($news->image));
        }

        $news->update($data);

        Session::flash('message', 'Cập nhật thành công!');
        if($request->action == 'save'){
            return redirect($this->link);
        }else{
            return redirect($this->link.'/'.$news->id.'/edit');
        }
    }

    /**
     * Save news
     *
     */
    public function store(CheckMenuRequest $request){

        $data = $request->all();

        $image = FileHelper::uploadImage('image_upload', 'news', 1200);
        if($image){
            $data['image'] = $image;
        }

        $position   = RequestHelper::getRequestSession('mposition', 'main');
        $data['position'] = $position;
        $this->processLink($data, $request);

        $news = new Menu();
        $news::create($data);
        $news->id = Menu::max('id');

        Session::flash('message', 'Tạo mới thành công!');
        if($request->action == 'save'){
            return redirect($this->link);
        }else{
            return redirect($this->link.'/'.$news->id.'/edit');
        }
    }

    public function processLink(&$data, $request){
        $module = $request->input('module', 1);

        if($module == 1){
            $data['module_id'] = $request->newscat;
            $newscat = NewsCategory::find($request->newscat);
            if($newscat){
                $data['alias'] = '/'.$newscat->alias.'-nc'.$newscat->id.'.html';
            }
        }

        if($module == 2){
            $data['module_id'] = $request->procat;
            $procat = ProductsCategory::find($request->procat);
            if($procat){
                $data['alias'] = '/'.$procat->alias.'-pc'.$procat->id.'.html';
            }
        }

        if($module == 3){
            $data['module_id'] = $request->pages;
            $pages = Pages::find($request->pages);
            if($pages){
                $data['alias'] = '/trang/'.$pages->alias.'-p'.$pages->id.'.html';
            }
        }

        if($module == 4){
            $data['module_id'] = 0;
            $data['alias'] = '/lien-he';
        }
    }
}
