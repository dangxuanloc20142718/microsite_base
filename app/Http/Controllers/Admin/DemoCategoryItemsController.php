<?php

namespace App\Http\Controllers\Admin;

use App\Helper\RequestHelper;
use App\Helper\InputHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use App\Helper\StringHelper;
use App\Helper\FileHelper;
use Session;

use App\Models\DemoCategoryItems;
use App\Models\DemoCategory;

class DemoCategoryItemsController extends Controller
{
    public $routeName = 'demo_category_items';
    public $model = null;
    public $parentModel = null;

    public function __construct(){
        $this->model = new DemoCategoryItems();
        $this->parentModel = new DemoCategory();
    }

    public function index(Request $request){
        if($request->ajax()){
            if($request->has('field')) {
                $this->model->setFieldValue($request->id, $request->field, $request->value);
            }

            if($request->has('order')) {
                $this->model->setOrder($request->id, $request->order);
            }
        }

        $keyword = InputHelper::getString('keyword', '');
        $limit = RequestHelper::getRequestSession('limit', 20);
        $catId = RequestHelper::getRequestSession('category_id', 0);

        $items = $this->model->search(['category_id' => $catId, 'keyword' => $keyword], 'id','DESC', false)->paginate($limit);

        $selectCat = $this->parentModel->getCategorySelect();

        if($request->ajax()){
            return Response::json([
                'code' => 1,
                'message' => '',
                'html' => view('admin.'.$this->routeName.'.index_table')->with('items', $items)->with('routeName', $this->routeName)->render(),
            ]);
        }

        return view('admin.'.$this->routeName.'.index')
            ->with('items', $items)
            ->with('selectCat', $selectCat)
            ->with('routeName', $this->routeName);
    }

    /**
     * Update record
     *
     * @param int $id
     * @param Request $request
     * @return Ambigous
     */
    public function update($id, Request $request){

        $data = $request->all();
        $item = $this->model->findOrFail($id);

        $data['image'] = FileHelper::processCropImage($request->image_crop);
        if($request->has('gallery')){
            $obj = [];
            foreach ($data['gallery'] as $link => $title){
                $gallery = new \stdClass();
                $gallery->link = $link;
                $gallery->title = $title;
                $obj[] = $gallery;
            }
            $data['gallery'] = json_encode($obj);
        }
        $item->update($data);

        Session::flash('message', 'Cập nhật thành công!');
        if($request->_action == 'save') {
            return redirect('admin/' . $this->routeName);
        }else{
            return redirect('admin/' . $this->routeName . '/' . $item->id . '/edit');
        }
    }

    /**
     * Save item
     */
    public function store(Request $request){
        $data = $request->all();

        $data['image'] = FileHelper::processCropImage($request->image_crop);

        $item = $this->model->create($data);

        Session::flash('message', 'Tạo mới thành công!');
        if($request->_action == 'save') {
            return redirect('admin/' . $this->routeName);
        }else{
            return redirect('admin/' . $this->routeName . '/' . $item->id . '/edit');
        }
    }

    public function create(){
        $parents = $this->parentModel->getCategorySelect();

        return view('admin.'.$this->routeName.'.create')
            ->with('parents', $parents)
            ->with('routeName', $this->routeName);
    }

    public function edit($id){
        $item = $this->model->findOrFail($id);
        $parents = $this->parentModel->getCategorySelect();

        return view('admin.'.$this->routeName.'.edit', compact('item'))
            ->with('parents', $parents)
            ->with('routeName', $this->routeName);
    }

    public function destroy($id, Request $request){
        $item = $this->model->find($id);

        // Delete image
        if($item->image){
            @unlink(StringHelper::getUploadPath($item->image));
        }

        $item->delete();

        if($request->ajax()){
            return $this->index($request);
        }

        Session::flash('message', 'Xóa thành công!');
        return redirect('admin/'.$this->routeName);
    }
}
