<?php

namespace App\Http\Controllers\Admin;

use App\Helper\RequestHelper;
use App\Helper\InputHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use App\Helper\StringHelper;
use App\Helper\FileHelper;
use Session;

use App\Models\Demo;

class DemoController extends Controller
{
    public $routeName = 'demo';
    public $model = null;
    public $types = null;

    public function __construct(){
        $this->model = new Demo();
        $this->types = [0 => '--Tất cả--'] + $this->model->getTypes();
    }

    public function index(Request $request){
        if($request->ajax()){
            if($request->has('field')) {
                $this->model->setFieldValue($request->id, $request->field, $request->value);
            }

            if($request->has('order')) {
                $this->model->setOrder($request->id, $request->order);
            }
        }

        $keyword = InputHelper::getString('keyword', '');
        $limit = RequestHelper::getRequestSession('limit', 20);
        $catId = RequestHelper::getRequestSession('category_id', 0);
        $typeId = InputHelper::getInt('type_id', 0);

        $items = $this->model->search(['category_id' => $catId, 'type_id' => $typeId, 'keyword' => $keyword], 'id', 'DESC',false)->paginate($limit);

        if($request->ajax()){
            return Response::json([
                'code' => 1,
                'message' => '',
                'html' => view('admin.'.$this->routeName.'.index_table')
                    ->with('items', $items)
                    ->with('routeName', $this->routeName)
                    ->with('types', $this->types)
                    ->render(),
            ]);
        }

        return view('admin.'.$this->routeName.'.index')
            ->with('items', $items)
            ->with('types', $this->types)
            ->with('routeName', $this->routeName);
    }

    /**
     * Update record
     *
     * @param int $id
     * @param Request $request
     * @return Ambigous
     */
    public function update($id, Request $request){

        $data = $request->all();
        $item = $this->model->findOrFail($id);

        $data['image'] = FileHelper::processCropImage($request->image_crop);
        $item->update($data);

        Session::flash('message', 'Cập nhật thành công!');
        if($request->_action == 'save') {
            return redirect('admin/' . $this->routeName);
        }else{
            return redirect('admin/' . $this->routeName . '/' . $item->id . '/edit');
        }
    }

    /**
     * Save item
     */
    public function store(Request $request){
        $data = $request->all();

        $data['image'] = FileHelper::processCropImage($request->image_crop);

        $item = $this->model->create($data);

        Session::flash('message', 'Tạo mới thành công!');
        if($request->_action == 'save') {
            return redirect('admin/' . $this->routeName);
        }else{
            return redirect('admin/' . $this->routeName . '/' . $item->id . '/edit');
        }
    }

    public function create(){
        $parents = [];

        return view('admin.'.$this->routeName.'.create')
            ->with('parents', $parents)
            ->with('types', $this->types)
            ->with('routeName', $this->routeName);
    }

    public function edit($id){
        $item = $this->model->findOrFail($id);
        $parents = [];

        return view('admin.'.$this->routeName.'.edit', compact('item'))
            ->with('parents', $parents)
            ->with('types', $this->types)
            ->with('routeName', $this->routeName);
    }

    public function destroy($id, Request $request){
        $item = $this->model->find($id);

        // Delete image
        if($item->image){
            @unlink(StringHelper::getUploadPath($item->image));
        }

        $item->delete();

        if($request->ajax()){
            return $this->index($request);
        }

        Session::flash('message', 'Xóa thành công!');
        return redirect('admin/'.$this->routeName);
    }
}
