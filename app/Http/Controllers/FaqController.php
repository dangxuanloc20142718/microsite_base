<?php

namespace App\Http\Controllers;

use App\Helper\InputHelper;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use App\Helper\RequestHelper;

use Session;
use App\Models\Faq;

class FaqController extends Controller
{
    public $view = 'frontend.faq';
    public $link = 'faq';

    public function index(Request $request){
        // Get data request
        $query_title = InputHelper::getRawString('search-title', '');
        $items = Faq::getListFaq($query_title, true, 'id', 'DESC', 50);
        return view($this->view.'.index')->with('items', $items);
    }

    public function store(Request $request){
        $bugs = [];
        $code = 1;
        $data = [];
        $message = 'Đăng câu hỏi thành công. Chúng tôi sẽ kiểm tra và trả lời bạn trong thời gian sớm nhất';

        $name = InputHelper::getRawString('name');
        if(!$name){
            $bugs[] = 'Vui lòng nhập họ và tên';
        }

        $email = InputHelper::getEmail('email');
        if(!$email){
            $bugs[] = 'Địa chỉ email không hợp lệ';
        }

        $description = InputHelper::getRawString('description');
        if(!$description){
            $bugs[] = 'Vui lòng nhập câu hỏi';
        }

        $secret = config('custom.captcha_secret');;
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $request->{'g-recaptcha-response'});
        $responseData = json_decode($verifyResponse);
        if (!$responseData->success) {
            $bugs[] = 'Vui lòng nhập lại mã bảo vệ';
        }

        if(count($bugs)){
            $code = 0;
            $message = join('<br/>', $bugs);

        }else{

            Faq::create([
                'title' => $name,
                'fullname' => $name,
                'email' => $email,
                'description' => $description,
                'status' => 0
            ]);
        }

        return Response::json([
            'code' => $code,
            'message' => $message,
            'data' => $data
        ]);
    }
}
