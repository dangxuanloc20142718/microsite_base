<?php

namespace App\Http\Controllers;

use App;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\Submit;
use App\Models\SubmitCategory;
use App\Models\User;
use App\Models\News;
use App\Helper\InputHelper;
use Session;
use URL;

class MemberController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index(Request $request)
    {
        $user = Auth::user();

        $submits = Submit::where('user_id', $user->id)
            ->where('status', 1)
            ->paginate(100);

        return view('frontend.member.index', compact('user', 'submits'));
    }

    public function update(Request $request){
        $user = Auth::user();
        $bugs = [];
        $code = 1;
        $message = 'Cập nhật thông tin thành công!<br/>Vui lòng đợi trong giây lát, Website sẽ chuyển đến trang bài dự thi...';

        if(!$request->name){
            $bugs[] = 'Vui lòng nhập họ và tên';
        }
        if(!$request->mobile){
            $bugs[] = 'Vui lòng nhập số điện thoại';
        }
        if(!$request->facebook){
            $bugs[] = 'Vui lòng nhập địa chỉ Facebook';
        }
        if(!$request->address){
            $bugs[] = 'Vui lòng nhập địa chỉ';
        }

        if(count($bugs)){
            $code = 0;
            $message = 'Lỗi:<br/>' . join('<br/>', $bugs);
        }else{
            $user->mobile = InputHelper::getString('mobile');
            $user->facebook = InputHelper::getString('facebook');
            $user->name = InputHelper::getString('name');
            $user->address = InputHelper::getString('address');
            $user->save();
        }

        return Response::json([
            'code' => $code,
            'message' => $message
        ]);
    }
}
