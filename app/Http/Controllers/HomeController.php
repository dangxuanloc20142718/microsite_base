<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

use App;
use Auth;
use DB;
use Session;
use URL;

use App\Models\Kol;
use App\Models\Submit;
use App\Models\Videos;
use App\Models\News;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index(Request $request)
    {
        if(config('custom.offline')){
            if ($request->code == 'open') {
                Session::put('open', 1);
                return redirect('/');
            }

            if (!Session::get('open', 0)) {
                return view('layouts.offline');
            }
        }

        if($request->show == 'kol'){
            $item = Kol::find($request->id);
            return Response::json([
                'code' => 1,
                'data' => $item->fulltext
            ]);
        }

        if($request->show == 'news'){
            $item = News::find($request->id);
            return Response::json([
                'code' => 1,
                'data' => $item->fulltext
            ]);
        }

        $kols = Kol::search([])->limit(8)->get();
        $submits = Submit::search(['featured' => 1])->limit(9)->get();
        $videos = Videos::search([], 'ordering', 'ASC')->get();
        $news = News::search()->limit(6)->get();
        $totalSubmit = number_format(Submit::where('status', 1)->count(), 0, ',', '.');
        $totalVote = number_format((Submit::where('status', 1)->select(DB::raw(' (SUM(votes) + SUM(shares)) as total '))->value('total') + 1000), 0, ',', '.');

        if($submits){
            foreach ($submits as &$submit){
                $submit->username = User::find($submit->user_id) ? User::find($submit->user_id)->name : '';
            }
        }

        return view('frontend.home', compact('kols', 'submits', 'videos', 'news', 'totalSubmit', 'totalVote'));
    }
}
