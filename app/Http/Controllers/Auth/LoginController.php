<?php

namespace App\Http\Controllers\Auth;
/**
 * Class LoginController
 * @package App\Http\Controllers\Auth
 */

class LoginController
{
    public function showLoginForm()
    {
        return view('auth.login');
    }
}