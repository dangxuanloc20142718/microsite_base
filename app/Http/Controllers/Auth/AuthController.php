<?php

namespace App\Http\Controllers\Auth;

use App\Helper\FileHelper;
use App\Helper\InputHelper;
use App\Helper\StringHelper;
use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Config;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
//use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use File;
use Session;
use Validator;
use Facebook\Facebook;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    // Duong dan sau khi login / logout
    protected $redirectPath = '/admin';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware($this->guestMiddleware(), ['except' => 'getLogout']);
        //Config::set('app.debug', true);
    }

    /**
     * Return user if exists; create and return if doesn't
     *
     * @param $facebookUser
     * @return User
     */
    private function findOrCreateUser($facebookUser, $accessToken = '')
    {
        $fb = new \Facebook\Facebook([
            'app_id' => config('custom.facebook_appid'),
            'app_secret' => config('custom.facebook_appsecret'),
            'default_graph_version' => 'v2.7'
        ]);

        try {
            $response = $fb->get('/me?fields=id,name,email,link', $accessToken);
            if(!$response) die();
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            return 'Graph returned an error: ' . $e->getMessage();
            //die('Graph returned an error: ' . $e->getMessage());
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            return 'Facebook SDK returned an error: ' . $e->getMessage();
            //die('Facebook SDK returned an error: ' . $e->getMessage());
        }

        $facebookUser->email = (strpos($facebookUser->email, '@') === false) ? $facebookUser->id . '@facebook.com' : $facebookUser->email;

        $ext = explode('@', $facebookUser->email);
        $ext = explode('.', $ext[1]);
        $ext = $ext[0];
        if(!in_array($ext, ['facebook', 'gmail', 'yahoo', 'live', 'hotmail'])){
            //return 'Đăng nhập thất bại. BTC chỉ chấp nhận các tài khoản Facebook được đăng ký bằng số điện thoại, gmail, yahoo mail, live mail, hot mail!';
        }

        $fileContents = @file_get_contents("http://graph.facebook.com/".$facebookUser->id."/picture?redirect=false");
        if($fileContents){
            $fileContents = \GuzzleHttp\json_decode($fileContents);
            if(is_object($fileContents) && $fileContents->data && $fileContents->data->is_silhouette){
                return 'Tài khoản Facebook không hợp lệ!';
            }
        }

        $authUser = User::where('facebook_id', $facebookUser->id)->first();

        if ($authUser){
            return $authUser;
        }else{
            $authUser = User::where('email', $facebookUser->email)->first();
            if ($authUser){
                return $authUser;
            }
        }

        $des = 'media/' . date('Y-m') . '/' . date('d').'/'.$facebookUser->id.'.jpg';
        $fileContents = @file_get_contents("http://graph.facebook.com/".$facebookUser->id."/picture?type=large");
        FileHelper::saveFile($des, $fileContents);

        return User::create([
            'name' => $facebookUser->name,
            'email' => $facebookUser->email,
            'facebook_id' => $facebookUser->id,
            'facebook' => $facebookUser->link,
            'avatar' => $des,
            'group' => 3
        ]);
    }

    public function fbLogin(Request $request){
        $accessToken = InputHelper::getRawString('accessToken', '');
        $authUser = $this->findOrCreateUser($request, $accessToken);

        if(is_object($authUser)) {
            Auth::login($authUser, true);
            Session::flash('message', 'Bạn đã đăng nhập bằng Facebook thành công!');

            return Response::json(array(
                'code' => 1,
                'msg' => "Đăng nhập thành công!"
            ));
        }else{
            return Response::json(array(
                'code' => 0,
                'msg' => $authUser
            ));
        }
    }

    public function ajaxLogin(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials, $request->has('remember'))) {
            return response()->json([
                'code' => 1,
                'msg' => "<b>Đăng nhập thành công.</b><br/>Bạn vui lòng chờ để tải lại trang!"
            ]);
        }

        return response()->json([
            'code' => 0,
            'msg' => 'Thông tin đăng nhập không hợp lệ!'
        ]);
    }

    public function postLogin(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.

        //$throttles = $this->isUsingThrottlesLoginsTrait();

        if ($lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->credentials($request);

        /*$secret = config('custom.captcha_secret');;
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $request->{'g-recaptcha-response'});
        $responseData = json_decode($verifyResponse);
        if ($responseData != null) {
            if (!$responseData->success) {
                Session::flash('message', 'Bạn vui lòng tick và làm theo hướng dẫn để xác nhận mã bảo vệ.');
            } else {
                if (Auth::attempt($credentials, $request->has('remember'))) {
                    if ($request->has('remember')) {
                        Session::put('email', $request->get('email'));
                        Session::put('password', $request->get('password'));
                        Session::put('remember', $request->get('remember'));
                    } else {
                        Session::forget('email');
                        Session::forget('password');
                        Session::forget('remember');
                    }

                    //return $this->handleUserWasAuthenticated($request, $throttles);
                    return $this->sendLoginResponse($request);
                }
            }
        }*/

        if (Auth::attempt($credentials, $request->has('remember'))) {
            if ($request->has('remember')) {
                Session::put('email', $request->get('email'));
                Session::put('password', $request->get('password'));
                Session::put('remember', $request->get('remember'));
            } else {
                Session::forget('email');
                Session::forget('password');
                Session::forget('remember');
            }
            
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if (!$lockedOut) {
            $this->incrementLoginAttempts($request);
        }

        return $this->sendFailedLoginResponse($request);
    }


    public function ajaxRegister(Request $request)
    {
        $promotionModel = new Promotion();

        $formFields = $request->all();
        $userData = array(
            'name' => $formFields['name'],
            'email' => $formFields['email'],
            'mobile' => $formFields['mobile'],
            'group' => (isset($formFields['type']) && $formFields['type'] == 'salon') ? 3 : 5,
            'password' => $formFields['password'],
            'gender' => $formFields['gender'],
            'password_confirmation' => $formFields['password_confirmation'],
        );

        $rules = array(
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
        );

        $validator = Validator::make($userData, $rules);
        if ($validator->fails())
            return Response::json(array(
                'code' => 0,
                'msg' => $validator->getMessageBag()->toArray()
            ));
        else {
            //save password to show to user after registration
            $password = $userData['password'];
            //hash it now
            $userData['password'] = Hash::make($userData['password']);
            unset($userData['password_confirmation']);

            $userData['promotion'] = $promotionModel->getPromotionCode();
            //save to DB user details
            if (User::create($userData)) {
                // Login
                Auth::attempt(array('email' => $formFields['email'], 'password' => $password), 1);

                // Tạo Salon
                $redirect = '';
                if ($userData['group'] == 3) {
                    $salonModel = new Salon();
                    $salonModel->createSalonByUser(Auth::user()->id, Auth::user()->name);
                    $redirect = url('member');
                    Session::flash('message', 'Đăng ký thành công! Bạn vui lòng cập nhật thông tin Salon!');
                }
                //return success  message
                return Response::json(array(
                    'code' => 1,
                    'redirect' => $redirect,
                    'msg' => "<b>Đăng ký thành công.</b><br/>Bạn vui lòng chờ để tải lại trang!"
                ));
            }
        }
    }

    public function showRegistrationForm()
    {
        return redirect('login');
    }

    public function register()
    {
        die();
    }

    public function getLogout(Request $request)
    {
        $user = Auth::user();

        if ($user && $user->group > 2) {
            Auth::logout();
            $param = $request->get('redirect', url('/'));
            return redirect($param);
        } else {
            $param = $request->get('redirect', url('auth/login'));
            Auth::logout();
            return redirect($param);
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return;
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

}
