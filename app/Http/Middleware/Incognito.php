<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Incognito
{
    /**
     * Handle an incoming request.
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->has('incognito'))
        {
            Auth::onceUsingId($request->session()->get('incognito'));
        }

        return $next($request);
    }
}