<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;
use Illuminate\Support\Facades\App;

class LangMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->lang != '') {
            if ($request->lang == 'en') {
                Session::put('lang', '_en');
            }

            if ($request->lang == 'vn') {
                Session::put('lang', '');
            }
        }

        if (Session::get('lang', '') == '_en'){
            App::setLocale('en');
        }else{
            App::setLocale('vn');
        }

        /*
        $url_array = explode('.', parse_url($request->url(), PHP_URL_HOST));
        $subdomain = $url_array[0];

        $languages = ['en', 'vn'];
        Session::put('dblang', '');
        if($subdomain == 'en'){
            Session::put('dblang', '_en');
        }

        if (in_array($subdomain, $languages)) {
            App::setLocale($subdomain);
        }
        */
        return $next($request);
    }
}
