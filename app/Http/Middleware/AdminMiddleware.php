<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( !Auth::check() )
        {
            return redirect('/auth/login');
        }else{
            if( Auth::user()->group > 2 ){
                return redirect('/');
            }
        }
        
        return $next($request);
    }
}
