<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        $gate->define('admin.manage', function ($user, $checkView) {
            $allowViews = array('dashboard', 'news', 'newscategory', 'pages', 'register', 'registerjoin');

            if($user->group == 2){
                $permission = explode("\n", $user->permission);
                $checkView2 = explode('-', $checkView);
                if(count($checkView2) == 4) {
                    $checkView2 = $checkView2[0] . '-' . $checkView2[1] . '-*-' . $checkView2[3];
                }else{
                    $checkView2 = '';
                }

                if(count($permission) == 0 || !$user->permission){
                    return false;
                }

                return ( in_array($checkView, $allowViews) || in_array($checkView, $permission) || in_array($checkView2, $permission) );
            }

            return true;
        });
    }
}
