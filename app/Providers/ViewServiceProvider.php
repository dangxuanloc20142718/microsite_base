<?php

namespace App\Providers;
use Illuminate\Support\ServiceProvider;

use App\Models\Submit;
use App\Models\SubmitCategory;
use App\Helper\StringHelper;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Carbon\Carbon;

use View;
use Auth;
use Session;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        view()->composer('*', function ($view) use ($request)
        {

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
