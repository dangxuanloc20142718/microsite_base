<?php

use Illuminate\Database\Seeder;
//use Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        //Model::unguard();
        //DB::table('customers')->delete();
        $this->call(CustomersTableSeeder::class);
        //Model::reguard();
    }
}
