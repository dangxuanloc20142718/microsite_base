<?php
Route::group(['middleware' => 'incognito'], function() {
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('pages/{page}',['as' => 'the-le',function ($page) {
        //if(in_array($page, ['cong-thuc-kem-dinh', 'qua-tang', 'the-le']))
        return view('frontend.pages.' . $page);
    }]);
    Route::get('/tin-tuc', 'NewsController@index')->name('news');
    Route::get('/{alias?}nc{id}.html', 'NewsController@category')->where('id', '[0-9]+');
    Route::get('/{alias?}nt{id}.html', 'NewsController@tag')->where('id', '[0-9]+');
    Route::get('/{alias?}n{id}.html', 'NewsController@show')->where('id', '[0-9]+')->name('news');

    Route::get('/profile/{userId}', 'SubmitController@profile')->name('profile');
    Route::get('bai-du-thi/share', 'SubmitController@share');
    Route::get('bai-du-thi/vote', 'SubmitController@vote');
    Route::get('bai-du-thi/checkUser', 'SubmitController@checkUser');
    Route::resource('bai-du-thi', 'SubmitController');
    Route::get('tham-gia-cuoc-thi', 'SubmitController@create');

    Route::get('news', 'NewsController@index');
    Route::resource('lien-he', 'ContactController');
    Route::resource('comments', 'CommentsController');
    Route::resource('faq', 'FaqController');
    Route::post('upload', 'UploadController@upload');
    Route::post('upload/compress', 'UploadController@uploadCompressImage');
});

Route::group(['middleware' => 'auth', 'prefix'=> 'member'], function () {
    Route::get('', 'MemberController@index');
    Route::get('{id}/edit', 'MemberController@edit');
    Route::post('{id}', 'MemberController@update');
    Route::get('{id}/password', 'MemberController@password');
    Route::post('{id}/password', 'MemberController@password');
    Route::get('posts', 'MemberController@posts');
    Route::get('transactions', 'MemberController@posts');
});

//Custom Error
Route::get('404.htm', function () {return view('errors.404_');});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@showLoginForm');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::post('auth/fblogin', 'Auth\AuthController@fbLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::post('password/reset', 'Auth\PasswordController@postReset');

// Admin
Route::auth();
Route::group(['middleware' => 'admin', 'prefix'=> 'admin', 'namespace' => 'Admin'], function () {

    Route::resource('', 'DashboardController');

    Route::resource('submit_category_items', 'SubmitCategoryItemsController');
    Route::resource('submit_category', 'SubmitCategoryController');

    Route::resource('demo', 'DemoController');
    Route::resource('demo_category', 'DemoCategoryController');
    Route::resource('demo_category_items', 'DemoCategoryItemsController');

    Route::resource('faq', 'FaqController');

    Route::resource('contact', 'ContactController');

    Route::resource('news', 'NewsController');
    Route::resource('videos', 'VideosController');
    Route::resource('kol', 'KolController');
    Route::resource('pages', 'PagesController');

    Route::resource('media', 'MediaController');
    Route::resource('tags', 'TagsController');

    Route::get('user/update-password',['as' => 'user.updatePassword',function () { return view('admin.user.update-password'); }]);
    Route::post('user/update-password', ['as' => 'user.updatePassword' , 'uses' => 'UserController@updatePassword']);

    Route::get('login_as_this_member/{id}', ['as' => 'user.loginAsThisMember' , 'uses' => 'UserController@loginAsThisMember']);

    Route::resource('user', 'UserController');

    Route::group(['middleware' => 'App\\Http\\Middleware\\AdminSystemMiddleware'], function () {
        Route::resource('menu', 'MenuController');
        Route::post('user/update-fb-avatar', 'UserController@updateFbAvatar');
        Route::resource('config', 'ConfigController');
    });
});

/*Route::get('media/{year}-{month}/{date}/{name}', function($year, $month, $date, $name)
{
    $filePath =  "media/{$year}-{$month}/{$date}/{$name}";
    if(!App\Helper\FileHelper::hasFile($filePath)) {
        return Response::make("File does not exist.", 404);
    }

    $fileContents = App\Helper\FileHelper::getFile($filePath);
    $mimeType = App\Helper\FileHelper::mimeType($filePath);
    return Response::make($fileContents, 200, array('Content-Type' => $mimeType));
});*/

// Incognito
Route::get('/users/{id}/incognito', 'UserController@incognito');
Route::get('/users/incognito/logout', 'UserController@stopIncognito');

