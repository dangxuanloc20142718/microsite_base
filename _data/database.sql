-- MySQL dump 10.13  Distrib 5.6.35, for osx10.9 (x86_64)
--
-- Host: localhost    Database: lantoahanhphuc
-- ------------------------------------------------------
-- Server version	5.6.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `ordering` tinyint(4) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT '1' COMMENT '1: news, 2: product, 3: hair, 4: salon',
  `item_id` int(11) DEFAULT NULL,
  `parent_id` int(10) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `reply` text COLLATE utf8_unicode_ci,
  `like` int(11) DEFAULT '0',
  `admin_view` tinyint(1) DEFAULT '0',
  `status` tinyint(4) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) DEFAULT '1' COMMENT '1: Contact, 2: Subscribe',
  `fullname` varchar(255) DEFAULT '0',
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `idnumber` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `demo`
--

DROP TABLE IF EXISTS `demo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `demo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `introtext` text COLLATE utf8_unicode_ci,
  `fulltext` text COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introtext_en` text COLLATE utf8_unicode_ci,
  `fulltext_en` text COLLATE utf8_unicode_ci,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `params` text COLLATE utf8_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `ordering` tinyint(4) DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `demo`
--

LOCK TABLES `demo` WRITE;
/*!40000 ALTER TABLE `demo` DISABLE KEYS */;
INSERT INTO `demo` VALUES (13,NULL,'test','',NULL,'',NULL,NULL,NULL,'',NULL,NULL,NULL,1,2,0,'2018-07-27 02:53:09',0,'2018-07-30 08:12:19'),(14,NULL,'test','',NULL,'',NULL,NULL,NULL,'',NULL,NULL,NULL,1,5,0,'2018-07-27 02:53:44',1,'2018-07-30 08:12:19'),(15,NULL,'test','',NULL,'',NULL,NULL,NULL,'',NULL,NULL,NULL,1,3,1,'2018-07-27 02:53:55',1,'2018-07-30 08:12:19'),(16,NULL,'test moi','',NULL,'',NULL,NULL,NULL,'',NULL,NULL,NULL,1,4,1,'2018-07-27 03:24:43',1,'2018-07-30 08:12:19'),(17,NULL,'Ha Noi - City Tour','',NULL,'',NULL,NULL,NULL,'',NULL,NULL,NULL,1,1,1,'2018-07-27 03:25:53',1,'2018-07-30 08:37:57'),(18,NULL,'test9999','',NULL,'',NULL,NULL,NULL,'',NULL,NULL,NULL,1,6,1,'2018-07-27 04:21:45',1,'2018-07-30 08:44:38'),(19,NULL,'test 1111','',NULL,'',NULL,NULL,NULL,'',NULL,NULL,NULL,1,7,1,'2018-07-27 04:21:52',1,'2018-07-30 08:41:52'),(20,1,'test2222','',NULL,'',NULL,NULL,NULL,'',NULL,NULL,NULL,1,8,1,'2018-07-27 04:22:00',1,'2018-07-30 08:40:21');
/*!40000 ALTER TABLE `demo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `demo_category`
--

DROP TABLE IF EXISTS `demo_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `demo_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `introtext` text COLLATE utf8_unicode_ci,
  `fulltext` text COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introtext_en` text COLLATE utf8_unicode_ci,
  `fulltext_en` text COLLATE utf8_unicode_ci,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `params` text COLLATE utf8_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `ordering` tinyint(4) DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `demo_category`
--

LOCK TABLES `demo_category` WRITE;
/*!40000 ALTER TABLE `demo_category` DISABLE KEYS */;
INSERT INTO `demo_category` VALUES (1,0,'Walls','walls',NULL,'',NULL,NULL,NULL,NULL,'',NULL,NULL,1,1,0,'2017-07-03 10:03:48',1,'2018-07-30 08:43:54'),(3,1,'Cat 3','cat-3',NULL,'',NULL,NULL,NULL,'','',NULL,NULL,1,2,0,'2017-07-03 10:06:38',1,'2018-07-27 08:41:33'),(6,1,'test11111','',NULL,'',NULL,NULL,NULL,'/uploads/content/media/2017-08/kenh14-favicon.ico','',NULL,NULL,1,3,1,'2018-07-26 04:20:20',1,'2018-07-27 08:41:33'),(7,0,'test 3333333','',NULL,'',NULL,NULL,NULL,NULL,'',NULL,NULL,1,5,1,'2018-07-26 04:20:31',1,'2018-07-27 08:41:21'),(8,0,'test','',NULL,'',NULL,NULL,NULL,'/media/2018-07/27/00hanoinnnn.jpg',NULL,NULL,NULL,1,2,1,'2018-07-26 08:40:05',1,'2018-07-27 09:51:00'),(9,0,'test 456','',NULL,'',NULL,NULL,NULL,'',NULL,NULL,NULL,1,3,1,'2018-07-26 10:19:44',1,'2018-07-27 08:41:21'),(10,0,'test 3333','',NULL,'',NULL,NULL,NULL,'',NULL,NULL,NULL,1,4,1,'2018-07-27 03:26:09',1,'2018-07-27 08:41:21'),(11,1,'test 1','',NULL,'',NULL,NULL,NULL,'',NULL,NULL,NULL,1,1,1,'2018-07-27 03:26:23',1,'2018-07-30 08:35:01');
/*!40000 ALTER TABLE `demo_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `demo_category_items`
--

DROP TABLE IF EXISTS `demo_category_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `demo_category_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `introtext` text COLLATE utf8_unicode_ci,
  `fulltext` text COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introtext_en` text COLLATE utf8_unicode_ci,
  `fulltext_en` text COLLATE utf8_unicode_ci,
  `gallery` text COLLATE utf8_unicode_ci,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `params` text COLLATE utf8_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `ordering` tinyint(4) DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `demo_category_items`
--

LOCK TABLES `demo_category_items` WRITE;
/*!40000 ALTER TABLE `demo_category_items` DISABLE KEYS */;
INSERT INTO `demo_category_items` VALUES (1,8,'Walls','walls',NULL,'',NULL,NULL,NULL,NULL,'','',NULL,NULL,1,1,0,'2017-07-03 10:03:48',1,'2018-07-27 02:23:29'),(3,0,'Cat 3','cat-3',NULL,'',NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,1,2,0,'2017-07-03 10:06:38',1,'2018-07-27 03:31:20'),(6,0,'test11111','',NULL,'',NULL,NULL,NULL,NULL,'/uploads/content/media/2017-08/kenh14-favicon.ico','media/2018-07/26/1532594046.jpg',NULL,NULL,1,3,1,'2018-07-26 04:20:20',1,'2018-07-27 03:31:20'),(7,0,'test 3333333','',NULL,'',NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,1,4,1,'2018-07-26 04:20:31',1,'2018-08-10 02:13:25'),(8,0,'test','',NULL,'',NULL,NULL,NULL,NULL,NULL,'media/2018-07/26/1532594405.jpg',NULL,NULL,1,5,1,'2018-07-26 08:40:05',1,'2018-07-27 10:01:06'),(9,8,'test 555','',NULL,'',NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,1,0,0,'2018-07-26 10:20:14',0,'2018-07-26 10:20:14'),(10,7,'test 3333','',NULL,'',NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,1,0,0,'2018-07-26 10:21:01',1,'2018-07-30 08:38:03'),(12,0,'test 5','',NULL,'',NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,1,1,1,'2018-07-27 03:27:19',1,'2018-07-30 08:35:10'),(13,0,'test 6','',NULL,'',NULL,NULL,NULL,'[{\"link\":\"\\/media\\/2018-07\\/27\\/hoa-loa-ken5-1493395111183.jpg\",\"title\":\"\\u1ede \\u0111\\u00e2y th\\u1ea5y ok \\u0111\\u00f3\"},{\"link\":\"\\/media\\/2018-07\\/27\\/00hanoinnnn.jpg\",\"title\":\"M\\u1ed9t s\\u1ed1 c\\u00e1ch tho\\u00e1t kh\\u1ecfi\"}]','',NULL,NULL,NULL,1,6,1,'2018-07-27 03:31:20',1,'2018-07-30 08:35:35');
/*!40000 ALTER TABLE `demo_category_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq`
--

DROP TABLE IF EXISTS `faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` tinyint(4) DEFAULT '1',
  `ordering` int(2) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq`
--

LOCK TABLES `faq` WRITE;
/*!40000 ALTER TABLE `faq` DISABLE KEYS */;
/*!40000 ALTER TABLE `faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kol`
--

DROP TABLE IF EXISTS `kol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kol` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introtext` text COLLATE utf8_unicode_ci,
  `fulltext` text COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introtext_en` text COLLATE utf8_unicode_ci,
  `fulltext_en` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `ordering` tinyint(4) DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kol`
--

LOCK TABLES `kol` WRITE;
/*!40000 ALTER TABLE `kol` DISABLE KEYS */;
INSERT INTO `kol` VALUES (21,0,'NGUYỄN KHÁNH PHƯƠNG ANH','','- Con gái DN Đoàn Thu Thủy -','Hạnh phúc là được tiếp nối mẹ lưu truyền những giá trị của phụ nữ Việt','<p>test</p>\r\n',NULL,NULL,NULL,'media/2018-08/29/1535514946.jpg',1,2,1,'2018-08-29 03:55:47',1,'2018-08-31 02:25:38'),(22,0,'NGUYỄN KHÁNH PHƯƠNG ANH 1','','- Con gái DN Đoàn Thu Thủy -','Hạnh phúc là được tiếp nối mẹ lưu truyền những giá trị của phụ nữ Việt','<p>Hạnh phúc là được tiếp nối mẹ lưu truyền những giá trị của phụ nữ Việt</p>\r\n',NULL,NULL,NULL,'media/2018-08/29/1535515043.jpg',1,1,1,'2018-08-29 03:57:23',1,'2018-08-31 02:25:38'),(23,0,'NGUYỄN KHÁNH PHƯƠNG ANH','','- Con gái DN Đoàn Thu Thủy -','Hạnh phúc là được tiếp nối mẹ lưu truyền những giá trị của phụ nữ Việt','',NULL,NULL,NULL,'media/2018-08/31/1535682319.jpg',1,3,1,'2018-08-31 02:25:19',0,'2018-08-31 02:25:38'),(24,0,'NGUYỄN KHÁNH PHƯƠNG ANH','','- Con gái DN Đoàn Thu Thủy -','Hạnh phúc là được tiếp nối mẹ lưu truyền những giá trị của phụ nữ Việt','',NULL,NULL,NULL,'media/2018-08/31/1535682357.jpg',1,4,1,'2018-08-31 02:25:38',1,'2018-08-31 02:25:57');
/*!40000 ALTER TABLE `kol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `type` varchar(10) DEFAULT 'image',
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=182 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` VALUES (108,'00HaNoinnnn','media/2018-07/27/00hanoinnnn.jpg','image',NULL),(109,'hoa loa ken5 1493395111183','media/2018-07/27/hoa-loa-ken5-1493395111183.jpg','image',NULL),(110,'Thieu nu ben hoa loa ken ghi diem voi ve dep moc mac 3','media/2018-07/27/thieu-nu-ben-hoa-loa-ken-ghi-diem-voi-ve-dep-moc-mac-3.jpg','image',NULL),(112,'35804821_457254734719661_5901100462714650624_n','media/2018-08/14/358048214572547347196615901100462714650624n.jpg','image',NULL),(113,'35804821_457254734719661_5901100462714650624_n','media/2018-08/15/358048214572547347196615901100462714650624n.jpg','image',NULL),(114,'1536204995','media/2018-09/06/1536204995.jpg','image',NULL),(115,'1536205844','media/2018-09/06/1536205844.jpg','image',NULL),(116,'1536206110','media/2018-09/06/1536206110.jpg','image',NULL),(117,'1536206395','media/2018-09/06/1536206395.jpg','image',NULL),(118,'1536206404','media/2018-09/06/1536206404.jpg','image',NULL),(119,'1536206412','media/2018-09/06/1536206412.jpg','image',NULL),(120,'1536206444','media/2018-09/06/1536206444.jpg','image',NULL),(121,'1536206543','media/2018-09/06/1536206543.jpg','image',NULL),(122,'1536206564','media/2018-09/06/1536206564.jpg','image',NULL),(123,'1536206567','media/2018-09/06/1536206567.jpg','image',NULL),(124,'1536206571','media/2018-09/06/1536206571.jpg','image',NULL),(125,'1536206637','media/2018-09/06/1536206637.jpg','image',NULL),(126,'1536206644','media/2018-09/06/1536206644.jpg','image',NULL),(127,'1536206647','media/2018-09/06/1536206647.jpg','image',NULL),(128,'1536206653','media/2018-09/06/1536206653.jpg','image',NULL),(129,'1536206739','media/2018-09/06/1536206739.jpg','image',NULL),(130,'1536207046','media/2018-09/06/1536207046.jpg','image',NULL),(131,'1536207181','media/2018-09/06/1536207181.jpg','image',NULL),(132,'1536207187','media/2018-09/06/1536207187.jpg','image',NULL),(133,'1536207259','media/2018-09/06/1536207259.jpg','image',NULL),(134,'1536207262','media/2018-09/06/1536207262.jpg','image',NULL),(135,'1536207266','media/2018-09/06/1536207266.jpg','image',NULL),(136,'1536207347','media/2018-09/06/1536207347.jpg','image',NULL),(137,'1536207352','media/2018-09/06/1536207352.jpg','image',NULL),(138,'1536207405','media/2018-09/06/1536207405.jpg','image',NULL),(139,'1536207409','media/2018-09/06/1536207409.jpg','image',NULL),(140,'1536207412','media/2018-09/06/1536207412.jpg','image',NULL),(141,'1536207435','media/2018-09/06/1536207435.jpg','image',NULL),(142,'1536207819','media/2018-09/06/1536207819.jpg','image',NULL),(143,'1536207824','media/2018-09/06/1536207824.jpg','image',NULL),(144,'1536207898','media/2018-09/06/1536207898.jpg','image',NULL),(145,'1536207900','media/2018-09/06/1536207900.jpg','image',NULL),(146,'1536207922','media/2018-09/06/1536207922.jpg','image',NULL),(147,'1536207926','media/2018-09/06/1536207926.jpg','image',NULL),(148,'1536207946','media/2018-09/06/1536207946.jpg','image',NULL),(149,'1536207948','media/2018-09/06/1536207948.jpg','image',NULL),(150,'1536207976','media/2018-09/06/1536207976.jpg','image',NULL),(151,'1536208009','media/2018-09/06/1536208009.jpg','image',NULL),(152,'1536208044','media/2018-09/06/1536208044.jpg','image',NULL),(153,'1536208047','media/2018-09/06/1536208047.jpg','image',NULL),(154,'1536208051','media/2018-09/06/1536208051.jpg','image',NULL),(155,'1536208087','media/2018-09/06/1536208087.jpg','image',NULL),(156,'1536208090','media/2018-09/06/1536208090.jpg','image',NULL),(157,'1536208094','media/2018-09/06/1536208094.jpg','image',NULL),(158,'1536208373','media/2018-09/06/1536208373.jpg','image',NULL),(159,'1536208376','media/2018-09/06/1536208376.jpg','image',NULL),(160,'1536208379','media/2018-09/06/1536208379.jpg','image',NULL),(161,'1536236756','media/2018-09/06/1536236756.jpg','image',NULL),(162,'1536236761','media/2018-09/06/1536236761.jpg','image',NULL),(163,'1536238230','media/2018-09/06/1536238230.jpg','image',NULL),(164,'1536238233','media/2018-09/06/1536238233.jpg','image',NULL),(165,'1536239720','media/2018-09/06/1536239720.jpg','image',NULL),(166,'1536239724','media/2018-09/06/1536239724.jpg','image',NULL),(167,'1536242866','media/2018-09/06/1536242866.jpg','image',NULL),(168,'1536243015','media/2018-09/06/1536243015.jpg','image',NULL),(169,'1536243115','media/2018-09/06/1536243115.jpg','image',NULL),(170,'1536243125','media/2018-09/06/1536243125.jpg','image',NULL),(171,'1536243127','media/2018-09/06/1536243127.jpg','image',NULL),(172,'1536243130','media/2018-09/06/1536243130.jpg','image',NULL),(173,'1536243436','media/2018-09/06/1536243436.jpg','image',NULL),(174,'1536243444','media/2018-09/06/1536243444.jpg','image',NULL),(175,'1536243446','media/2018-09/06/1536243446.jpg','image',NULL),(176,'1536294265','media/2018-09/07/1536294265.jpg','image',NULL),(177,'1536295296','media/2018-09/07/1536295296.png','image',NULL),(178,'1536313128','media/2018-09/07/1536313128.jpg','image',NULL),(179,'1536549659','media/2018-09/10/1536549659.png','image',NULL),(180,'1536549663','media/2018-09/10/1536549663.png','image',NULL),(181,'1536549666','media/2018-09/10/1536549666.png','image',NULL);
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module` int(10) DEFAULT NULL,
  `module_id` int(10) DEFAULT NULL,
  `parent_id` int(10) DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fulltext` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `ordering` tinyint(4) DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_06_17_022518_create_news_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `introtext` text COLLATE utf8_unicode_ci,
  `fulltext` text COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introtext_en` text COLLATE utf8_unicode_ci,
  `fulltext_en` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `ordering` tinyint(4) DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (21,0,'Gặp những người phụ nữ tuyệt vời này','','Gặp những người phụ nữ tuyệt vời này, ai cũng học được điều bổ ích gì đó cho riêng mình\r\n','<p>test</p>\r\n',NULL,NULL,NULL,'media/2018-08/29/1535517679.jpg',1,1,1,'2018-08-29 04:40:08',1,'2018-08-30 02:17:40'),(23,0,'Cà phê hoài cổ khiến giới trẻ Sài Gòn mê mẩn','','Bước vào quán, du khách như lạc vào không gian hoài cổ, tách biệt hoàn toàn với vẻ sôi động và hiện đại bên ngoài. Những chiếc máy khâu cũ, bàn ghế kiểu xưa, chiếc máy đánh chữ hay chiếc xe máy từ thời xưa đã tạo nên vẻ cổ kính cho quán','<p>(Dân trí) - Bước vào quán, du khách như lạc vào không gian hoài cổ, tách biệt hoàn toàn với vẻ sôi động và hiện đại bên ngoài. Những chiếc máy khâu cũ, bàn ghế kiểu xưa, chiếc máy đánh chữ hay chiếc xe máy từ thời xưa đã tạo nên vẻ cổ kính cho quán</p>\r\n',NULL,NULL,NULL,'media/2018-08/30/1535595434.jpg',1,3,1,'2018-08-30 02:17:14',0,'2018-08-30 02:17:40'),(22,0,'Gặp những người phụ nữ tuyệt vời này','','Thầy trò HLV Park Hang-seo sẽ phải làm nên cuộc lật đổ vĩ đại trước Hàn Quốc mới có thể giành quyền vào chung kết Asiad.','<p>Thầy trò HLV Park Hang-seo sẽ phải làm nên cuộc lật đổ vĩ đại trước Hàn Quốc mới có thể giành quyền vào chung kết Asiad.</p>\r\n',NULL,NULL,NULL,'media/2018-08/29/1535517737.jpg',1,2,1,'2018-08-29 04:42:17',1,'2018-08-30 02:17:40'),(24,0,'Đồng hương Văn Toàn có một fan girl bóng đá xinh đẹp thế này!','','Hot girl Hải Dương từng nổi đình đám dân mạng khi đến sân vận động Mỹ Đình cổ vũ U23 Việt Nam.','<p>Hot girl Hải Dương từng nổi đình đám dân mạng khi đến sân vận động Mỹ Đình cổ vũ U23 Việt Nam.</p>\r\n',NULL,NULL,NULL,'media/2018-08/30/1535595460.jpg',1,4,1,'2018-08-30 02:17:40',0,'2018-08-30 02:17:40');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fulltext` text COLLATE utf8_unicode_ci NOT NULL,
  `fulltext_en` text COLLATE utf8_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'Thể lệ cuộc thi','','the-le-cuoc-thi','',NULL,'<div class=\"container\">\r\n<section class=\"section tl fs-16 py-6\">\r\n<h1 class=\"tl__header display-3 fw-7 text-uppercase mb-6\">Nội dung thể lệ cuộc thi</h1>\r\n</section>\r\n</div>\r\n','',1,0,0,'2017-07-07 08:45:24','2017-09-11 03:06:55');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('vinhchuvan@admicro.vn','bb1028827151b4a28462d7a774c022770c01e66e5794f072da227bed97efcfe3','2017-08-09 02:25:24');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submit`
--

DROP TABLE IF EXISTS `submit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `round` tinyint(1) DEFAULT '1',
  `user_id` int(11) DEFAULT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desc_images` text COLLATE utf8_unicode_ci NOT NULL,
  `introtext` text COLLATE utf8_unicode_ci NOT NULL,
  `fulltext` text COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `votes` int(11) DEFAULT '0',
  `shares` int(11) DEFAULT '0',
  `params` text COLLATE utf8_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submit`
--

LOCK TABLES `submit` WRITE;
/*!40000 ALTER TABLE `submit` DISABLE KEYS */;
INSERT INTO `submit` VALUES (22,1,1,1,'Admin','','/uploads/submit/2018-07/05/25-share.png','','Admin','Admin','',0,0,NULL,-2,0,0,1,'2018-07-05 10:43:23','2018-08-28 10:13:25'),(23,1,1,1,'Admin','','/uploads/submit/2018-07/05/25-share.png','','Admin','Admin','',0,0,NULL,-2,0,0,1,'2018-07-05 10:43:47','2018-08-28 10:13:27'),(24,1,1,1,'Admin','','/uploads/submit/2018-07/05/25-share.png','','Admin','Admin','',0,0,NULL,-2,0,0,1,'2018-07-05 10:44:34','2018-08-28 10:13:29'),(25,1,1,1,'Admin','','/uploads/submit/2018-07/05/25-share.png','','Admin','Admin','',0,0,NULL,-2,0,0,1,'2018-07-05 10:47:08','2018-08-28 10:17:58'),(26,1,1,1,'Admin','','/uploads/submit/2018-07/06/26-share.png','','Admin','Admin','',0,0,NULL,-2,0,0,1,'2018-07-06 09:10:39','2018-08-28 10:18:00'),(27,1,NULL,0,'Bài dự thi 1','','media/2018-08/30/1535595304.jpg','','','<p>test</p>\r\n','',0,0,NULL,0,0,1,1,'2018-08-29 04:18:11','2018-09-10 03:17:55'),(28,1,NULL,0,'Bài dự thi 2','','media/2018-08/30/1535595295.jpg','','','<p>test</p>\r\n','',0,0,NULL,0,0,1,1,'2018-08-29 04:19:21','2018-09-10 03:17:54'),(29,1,NULL,0,'Bài dự thi 3','','media/2018-08/30/1535595282.jpg','','test 1','test 2','',0,0,NULL,0,0,1,1,'2018-08-29 07:19:30','2018-09-10 03:17:53'),(30,1,NULL,0,'Bài dự thi 4','','media/2018-08/30/1535595274.jpg','','test 1','test 3','',0,0,NULL,0,0,1,1,'2018-08-29 07:19:53','2018-09-10 03:17:52'),(31,1,NULL,0,'Bài dự thi 5','','media/2018-08/30/1535595263.jpg','','test',' test','',0,0,NULL,0,0,1,1,'2018-08-29 07:20:51','2018-09-10 03:17:52'),(32,1,NULL,0,'Bài dự thi 6','','media/2018-08/29/1535528552.jpg','','test','test','',1,0,NULL,0,0,1,1,'2018-08-29 07:23:03','2018-09-10 03:17:51'),(33,1,1,NULL,'','','media/2018-09/06/1536243115.jpg','','asdfsadfsdafsadf','asdfsadfsadf','',0,0,NULL,0,0,1,1,'2018-09-06 14:12:12','2018-09-10 03:18:07'),(34,1,1,NULL,'','','media/2018-09/06/1536243115.jpg','media/2018-09/06/1536243125.jpg,media/2018-09/06/1536243127.jpg,media/2018-09/06/1536243130.jpg','asdfsadfsdafsadf','asdfsadfsadf','2323423423',0,0,NULL,0,0,1,1,'2018-09-06 14:13:38','2018-09-10 03:18:06'),(35,1,1,NULL,'','','media/2018-09/06/1536243436.jpg','media/2018-09/06/1536243444.jpg,media/2018-09/06/1536243446.jpg','rfgsedrgergreg','ergergrewgewr','32453534543',0,0,NULL,0,0,1,1,'2018-09-06 14:17:28','2018-09-10 03:18:07'),(36,1,1,NULL,'','','media/2018-09/07/1536313128.jpg','','asdfsadfsafsad','asdfsadfsadfsadf','https://www.youtube.com/watch?v=EKYYjyEwlv4',0,0,NULL,0,0,1,1,'2018-09-07 09:41:10','2018-09-10 03:17:48'),(37,1,1,NULL,'','','media/2018-09/07/1536313128.jpg','','asdfsadfsafsad','asdfsadfsadfsadf','https://www.youtube.com/watch?v=EKYYjyEwlv4',0,0,NULL,0,0,1,1,'2018-09-07 09:41:32','2018-09-10 03:17:47'),(38,1,1,NULL,'Admin','','media/2018-09/10/1536549679.png','media/2018-09/10/1536549659.png,media/2018-09/10/1536549663.png,media/2018-09/10/1536549666.png','Hạnh phúc trong bạn là gì? Hạnh phúc trong bạn là gì? Hạnh','asdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsg','',0,0,NULL,1,0,1,0,'2018-09-10 03:21:19','2018-09-10 03:21:19'),(39,1,1,NULL,'Admin','','media/2018-09/10/1536549679.png','media/2018-09/10/1536549659.png,media/2018-09/10/1536549663.png,media/2018-09/10/1536549666.png','Hạnh phúc trong bạn là gì? Hạnh phúc trong bạn là gì? Hạnh','asdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsg','',0,0,NULL,1,0,1,0,'2018-09-10 03:21:19','2018-09-10 03:21:19'),(40,1,1,NULL,'Admin','','media/2018-09/10/1536549679.png','media/2018-09/10/1536549659.png,media/2018-09/10/1536549663.png,media/2018-09/10/1536549666.png','Hạnh phúc trong bạn là gì? Hạnh phúc trong bạn là gì? Hạnh','asdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsg','',0,0,NULL,1,0,1,0,'2018-09-10 03:21:19','2018-09-10 03:21:19'),(41,1,1,NULL,'Admin','','media/2018-09/10/1536549679.png','media/2018-09/10/1536549659.png,media/2018-09/10/1536549663.png,media/2018-09/10/1536549666.png','Hạnh phúc trong bạn là gì? Hạnh phúc trong bạn là gì? Hạnh','asdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsg','',0,0,NULL,1,1,1,1,'2018-09-10 03:21:19','2018-09-10 03:50:55'),(42,1,1,NULL,'Admin','','media/2018-09/10/1536549679.png','media/2018-09/10/1536549659.png,media/2018-09/10/1536549663.png,media/2018-09/10/1536549666.png','Hạnh phúc trong bạn là gì? Hạnh phúc trong bạn là gì? Hạnh','asdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsg','',0,0,NULL,1,1,1,1,'2018-09-10 03:21:19','2018-09-10 03:50:53'),(43,1,1,NULL,'Admin','','media/2018-09/10/1536549679.png','media/2018-09/10/1536549659.png,media/2018-09/10/1536549663.png,media/2018-09/10/1536549666.png','Hạnh phúc trong bạn là gì? Hạnh phúc trong bạn là gì? Hạnh','asdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsg','',0,0,NULL,1,1,1,1,'2018-09-10 03:21:19','2018-09-10 03:50:52'),(44,1,1,NULL,'Admin','','media/2018-09/10/1536549679.png','media/2018-09/10/1536549659.png,media/2018-09/10/1536549663.png,media/2018-09/10/1536549666.png','Hạnh phúc trong bạn là gì? Hạnh phúc trong bạn là gì? Hạnh','asdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsg','',0,0,NULL,1,1,1,1,'2018-09-10 03:21:19','2018-09-10 03:50:49'),(45,1,1,NULL,'Admin','','media/2018-09/10/1536549679.png','media/2018-09/10/1536549659.png,media/2018-09/10/1536549663.png,media/2018-09/10/1536549666.png','Hạnh phúc trong bạn là gì? Hạnh phúc trong bạn là gì? Hạnh','asdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsg','',0,0,NULL,1,1,1,1,'2018-09-10 03:21:19','2018-09-10 03:50:48'),(46,1,1,NULL,'Admin','','media/2018-09/10/1536549679.png','media/2018-09/10/1536549659.png,media/2018-09/10/1536549663.png,media/2018-09/10/1536549666.png','Hạnh phúc trong bạn là gì? Hạnh phúc trong bạn là gì? Hạnh','asdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsg','',0,0,NULL,1,1,1,1,'2018-09-10 03:21:19','2018-09-10 03:50:47'),(47,1,1,NULL,'Admin','','media/2018-09/10/1536549679.png','media/2018-09/10/1536549659.png,media/2018-09/10/1536549663.png,media/2018-09/10/1536549666.png','Hạnh phúc trong bạn là gì? Hạnh phúc trong bạn là gì? Hạnh','asdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsgasdfsafsadfsadfasegasegaewsg asdfsafsadfsadfasegasegaewsg','',0,0,NULL,1,1,1,1,'2018-09-10 03:21:19','2018-09-10 03:50:37');
/*!40000 ALTER TABLE `submit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submit_categories`
--

DROP TABLE IF EXISTS `submit_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submit_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fulltext` text COLLATE utf8_unicode_ci NOT NULL,
  `params` text COLLATE utf8_unicode_ci,
  `submit_vote` int(11) DEFAULT NULL,
  `submit_count` int(11) DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `ordering` tinyint(4) DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submit_categories`
--

LOCK TABLES `submit_categories` WRITE;
/*!40000 ALTER TABLE `submit_categories` DISABLE KEYS */;
INSERT INTO `submit_categories` VALUES (1,0,'Walls','walls','',NULL,'',NULL,2,24,1,1,0,0,'2017-07-03 10:03:48','2018-07-06 09:10:40'),(2,0,'Cat 2','cat-2','',NULL,'',NULL,1,0,0,2,0,0,'2017-07-03 09:23:37','2018-07-02 10:51:18'),(3,0,'Cat 3','cat-3','',NULL,'',NULL,1,0,0,3,0,0,'2017-07-03 10:06:38','2018-07-02 10:51:14');
/*!40000 ALTER TABLE `submit_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submit_vote`
--

DROP TABLE IF EXISTS `submit_vote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submit_vote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT '1' COMMENT '1: Like, 2: Share',
  `user_id` int(11) DEFAULT NULL,
  `submit_id` int(11) DEFAULT NULL,
  `ip` varchar(25) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submit_vote`
--

LOCK TABLES `submit_vote` WRITE;
/*!40000 ALTER TABLE `submit_vote` DISABLE KEYS */;
INSERT INTO `submit_vote` VALUES (5,2,0,12,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36','2018-07-05 09:58:40'),(6,1,1,32,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36','2018-08-30 17:02:52');
/*!40000 ALTER TABLE `submit_vote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group` tinyint(1) DEFAULT '3',
  `balance` int(11) DEFAULT '0',
  `building_id` int(11) DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` tinyint(1) DEFAULT '0',
  `birthday` datetime DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `promotion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ward_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `cats` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permission` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin',NULL,'vinhchuvan@admicro.vn','$2y$10$o7HLTbkIb4DVxBSOlpAun.J0bzq0.Vx3FjJWY4A1ElbV/PQdE0Nx2','J2M7os9DVMXKjSKdEkJAPsUI7rtgN8hpRoLzKQQG11JcQd2Nb8bejKRhewYX','2017-07-03 07:42:44','2018-09-10 03:21:30',1,0,NULL,'uploads/avatar/1502789540.jpg',NULL,'vinhchuvan@admicro.vn',0,'1989-01-01 00:00:00','','rewqrew','asfsf',NULL,NULL,NULL,NULL,'','',1),(2,'Kenh14',NULL,'xmen@kenh14.vn','$2y$10$OfY9E9WHNYadYbgj/wbMB..V5agTMon0OhCMgXwGMw3v6oGDBZJpC','oVQ25FHMu3mTwdZxm0AzNOdY7N4MZJbo5600twYPuoHhWMrB7J3I2ZCQZ8Kz','2017-07-07 09:01:03','2017-08-08 03:45:15',1,0,NULL,NULL,NULL,'tttm@kenh14.vn',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introtext` text COLLATE utf8_unicode_ci,
  `fulltext` text COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introtext_en` text COLLATE utf8_unicode_ci,
  `fulltext_en` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `ordering` tinyint(4) DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
INSERT INTO `videos` VALUES (21,0,'Video title Da Huong 1','','qyGT4ks3qQs',NULL,'',NULL,NULL,NULL,'media/2018-08/29/1535517206.jpg',1,1,1,'2018-08-29 04:33:26',0,'2018-08-30 02:16:27'),(22,0,'Video title Da Huong 2','','0s8QWqd4aXE',NULL,'',NULL,NULL,NULL,'media/2018-08/29/1535517222.jpg',1,2,1,'2018-08-29 04:33:42',0,'2018-08-30 02:16:27'),(23,0,'Video da huong 2','','0s8QWqd4aXE',NULL,'',NULL,NULL,NULL,'media/2018-08/30/1535595337.jpg',1,3,1,'2018-08-30 02:15:37',1,'2018-08-30 02:16:27'),(24,0,'Video da huong 3','','0s8QWqd4aXE',NULL,'',NULL,NULL,NULL,'media/2018-08/30/1535595387.jpg',1,4,1,'2018-08-30 02:16:27',0,'2018-08-30 02:16:27');
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-10 10:58:31
