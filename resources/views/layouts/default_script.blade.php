<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<div id="loginModal" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content ">
            <div class="modal-body text-center" style="color: #000">
                <div class="form">
                    <img src="{{ url('/') }}/media/system/logo.png" style="display: inline-block; width: 200px; max-width: 100%;"/><br/><br/>
                    Sử dụng tài khoản Facebook để tham gia cuộc thi<br/><br/>
                    <a href="#" data-redirect="" class="btn-fblogin-modal btn btn-sm btn-primary" style="background-color: #253785;">
                        <i class="fa fa-facebook" style="margin-right: 20px"></i>
                        Đăng nhập bằng Facebook
                    </a>
                </div>
                <div class="process">
                    Đang xử lý...
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="voteModal" tabindex="-1" role="dialog" style="color:#000000;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="display: block">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Xác nhận Vote</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-message alert-success hidden"></div>

                <div class="alert-form">
                    Để Vote cho bài này, bạn vui lòng bấm nút tick xác nhận dưới đây:<br/>
                    <div id="recaptchaVote"></div>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="sysModal" class="modal fade">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h1 class="modal-title" style=""></h1>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary hidden">Save changes</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

{{ Html::script('assets/js/custom.js?v='.config('custom.version')) }}
<script>
    var site_url = '{{ url('/')  }}/',
        token = '{!! csrf_token() !!}',
        captcha_sitekey = '{{ config('custom.captcha_sitekey') }}',
        facebook_appid = '{{ config('custom.facebook_appid') }}';
</script>


{{ Html::script('assets/js/jquery-ui.min.js?v='.config('custom.version')) }}
{{ Html::script('assets/js/tether.min.js?v='.config('custom.version')) }}
{{--{{ Html::script('assets/js/bootstrap.min.js?v='.config('custom.version')) }}--}}

{{ Html::script('assets/js/site.js?v='.config('custom.version')) }}
<script>
    $.app.init({
        url: '{{ url('/') }}/',
        token: '{!! csrf_token() !!}',
        captcha_sitekey: '{{ config('custom.captcha_sitekey') }}',
        facebook_appid: '{{ config('custom.facebook_appid') }}'
    });
</script>
<script>
    (function ($) {
        jQuery('.sticky-kit').stick_in_parent();
        if(jQuery(window).width() < 768 ){
            jQuery('.sticky-kit').trigger("sticky_kit:detach");
        }
    })(jQuery);
</script>