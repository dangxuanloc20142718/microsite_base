<!DOCTYPE HTML>
<!--[if lt IE 7 ]> <html lang="en" class="ie ie6"> <![endif]-->
<!--[if IE 7 ]>	<html lang="en" class="ie ie7"> <![endif]-->
<!--[if IE 8 ]>	<html lang="en" class="ie ie8"> <![endif]-->
<!--[if IE 9 ]>	<html lang="en" class="ie ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>{{ config('custom.seo_title') }}</title>
    <meta name="description" content="{{ config('custom.seo_description') }}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}" />
    <meta property="og:image" content="{{ url('media/system/fbimage.jpg?'.config('custom.version'))  }}" />
    <meta property="og:title" content="{{ config('custom.seo_title') }}" />
    <meta property="og:description" content="{{ config('custom.seo_description') }}" />

    <meta name="robots" content="noindex, nofollow">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <link rel="shortcut icon" href="{{ url('media/system/favicon.ico?'.config('custom.version')) }}" />
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/offline/styles.css?'.config('custom.version')) }}">
</head>

<body id="home" style="background: url('{{ url('media/system/offline.jpg?'.config('custom.version')) }}') no-repeat center top; background-size:cover;">
<div id="Header">
    <div class="wrapper">
        <h1>{{ config('custom.offline_title') }}</h1>
    </div>
</div>
<div id="Content" class="wrapper">
    <h2>{!!  nl2br(config('custom.offline_description'))  !!}</h2>
    <div class="countdown styled"></div>
    <div id="subscribe">
        <h3>Unlock code</h3>
        <form action="" method="get" onsubmit="">
            <p><input name="code" value="" placeholder="Enter your Member code" type="text" id=""/>
                <input type="submit" value="Go" style="border:2px solid #fff"/></p>
        </form>
        <div id="socialIcons" style="display: none">
            <ul>
                <li><a href="" title="Twitter" class="twitterIcon"></a></li>
                <li><a href="" title="facebook" class="facebookIcon"></a></li>
                <li><a href="" title="linkedIn" class="linkedInIcon"></a></li>
                <li><a href="" title="Pintrest" class="pintrestIcon"></a></li>
            </ul>
        </div>
    </div>
</div>

<div id="overlay"></div>

<!--Scripts-->
<script type="text/javascript">
    var endDate = "{{ config('custom.offline_date') }}";
</script>
<script type="text/javascript" src="{{ url('assets/lib/offline/jquery-1.9.1.min.js?'.config('custom.version')) }}"></script>
<script type="text/javascript" src="{{ url('assets/lib/offline/Backstretch.js?'.config('custom.version')) }}"></script>
<script type="text/javascript" src="{{ url('assets/lib/offline/jquery.countdown.js?'.config('custom.version')) }}"></script>
<script type="text/javascript" src="{{ url('assets/lib/offline/global.js?'.config('custom.version')) }}"></script>

</body>
</html>
