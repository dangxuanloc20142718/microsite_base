<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="vi-VN" dir="LTR" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
    <meta charset="UTF-8">
    <meta name="author" content="{{ config('custom.seo_author') }}">
    <meta property="fb:app_id" content="{{ config('custom.facebook_appid') }}" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="{{ url('assets/img_home/logochuan.png?'.config('custom.version')) }}"/>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ url('assets/bootstrap/css/font-awesome.min.css?'.config('custom.version')) }}" type="text/css"/>

    <link type="text/css" href="{{ url('/') }}/assets/css/base.css?{{ config('custom.version') }}" rel="stylesheet">
    <link type="text/css" href="{{ url('/') }}/assets/css/system.css?{{ config('custom.version') }}" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="{{ url('assets/js/jquery.sticky-kit.js?'.config('custom.version')) }}"></script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PV4JWTT');</script>
    <!-- End Google Tag Manager -->

    {!! config('custom.embed_header') !!}
    @yield('header')
</head>

<body class="">
@if(Session::has('message'))
    <div class="container">
        <p class="main-msgout alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
    </div>
@endif

<!-- Header -->
<div class="header">
    <div class="container container--config">
        <div class="row">
            <div class="col-sm-12 header__menu">
                <a href="{{ url('pages/vechungtoi') }}">
                    <li class="header__menu__li hidden-mb li-active @if(request()->is('pages/vechungtoi*'))act @endif">
                        Về chiến dịch<br> Lan tỏa<br> hạnh phúc
                        {{--<div class="active"></div>--}}
                        @if(request()->is('pages/vechungtoi*'))<div class="active"></div> @endif
                    </li>
                </a>
                <a href="{{ url('bai-du-thi') }}">
                    <li class="header__menu__li hidden-mb @if(request()->is('bai-du-thi*'))act @endif">
                        Bài dự thi<br> Hạnh phúc<br> của tôi
                        {{--<div class="active"></div>--}}
                        @if(request()->is('bai-du-thi*'))<div class="active"></div> @endif
                    </li>
                    {{--@if(request()->is('bai-du-thi*'))<div class="active"></div> @endif--}}

                </a>

                <li class="li-logo">
                    <a href="{{ url('/') }}"><img src="{{ url('assets/img_home/logochuan.png') }}" alt="image"></a>
                </li>

                {{--<a href="{{ url('pages/workshop') }}">--}}
                <a href="#" data-toggle="modal" data-target=".coming-soon">
                    <li class="header__menu__li hidden-mb @if(request()->is('pages/workshop*'))act @endif">
                        Workshop<br> sống <br> hạnh phúc
                        @if(request()->is('pages/workshop*'))<div class="active"></div> @endif
                    </li>
                </a>
                <a href="{{ url('pages/thelegiaithuong') }}">
                    <li class="header__menu__li hidden-mb @if(request()->is('pages/thelegiaithuong*'))act @endif">
                        Thể lệ<br> và <br> giải thưởng
                        @if(request()->is('pages/thelegiaithuong*'))<div class="active"></div> @endif
                    </li>
                </a>
                @if(Auth::check())
                    @php $user = Auth::user(); @endphp
                    <li class="header__menu__li hidden-mb">
                        <img style="width: 20px; height: 20px; border-radius: 50%;" src="{{ url($user->avatar ? $user->avatar : '/assets/img/avatar.png') }}"/>
                        <b>{{ $user->name }}</b> <br/><a class="logout" href="{{ url('auth/logout') }}?redirect=">[Thoát]</a>
                    </li>
                @else
                    <a onclick="return $.app.checkLogin('{{ url('/') }}')" href="#">
                        <li class="header__menu__li hidden-mb">
                            Đăng<br> nhập
                        </li>
                    </a>
                @endif

                <!-- Menu mobile -->
                <a href="https://www.facebook.com/dahuonghoalinh/" target="_blank"><img src="{{ url('assets/img_home/img55.png') }}" alt="image" class="mb-header-icon mb-header-icon-facebook hidden-pc show-mb"></a>
                <a href="https://www.youtube.com/watch?v=A-pDVJP-4MQ" target="_blank"><img src="{{ url('assets/img_home/img54.png') }}" alt="image" class="mb-header-icon mb-header-icon-youtube hidden-pc show-mb"></a>
                <div class="menu-mobile hidden-pc show-mb">
                    <img src="{{ url('assets/img_home/img53.png') }}" alt="image" class="btt-mobile">
                </div>
                <div id="menu" class="panel-menu" role="navigation">
                    <div class="mb-login">
                        @if(Auth::check())
                            @php $user = Auth::user(); @endphp
                        <img src="{{ url($user->avatar ? $user->avatar : '/assets/img/avatar.png') }}" class="img-circle" alt="Cinque Terre" width="60%">
                        <div style="margin-top:10px">
                            <b>{{ $user->name }}</b>
                            <a href="{{ url('auth/logout') }}?redirect=" class="mb-logout">| Thoát</a>
                        </div>
                        @else
                            <a href="#" onclick="return $.app.checkLogin('{{ url('/') }}')" class="mb-logout" style="font-size: 20px">Đăng nhập</a>
                        @endif
                    </div>
                    <ul class="mb-menu">

                        <li @if(request()->is('pages/vechungtoi*')) class="active" @endif>
                            <a href="{{ url('pages/vechungtoi') }}">Về chiến dịch lan tỏa hạnh phúc</a>
                        </li>
                        <li @if(request()->is('bai-du-thi*')) class="active" @endif>
                            <a href="{{ url('bai-du-thi') }}">Bài dự thi Hạnh phúc của tôi</a>
                        </li>
                        <li @if(request()->is('pages/workshop*')) class="active" @endif>
                            <a href="{{ url('pages/workshop') }}">Workshop sống hạnh phúc</a>
                        </li>
                        <li @if(request()->is('pages/thelegiaithuong*')) class="active" @endif>
                            <a href="{{ url('pages/thelegiaithuong') }}">Thể lệ và giải thưởng</a>
                        </li>
                    </ul>
                </div>
                <!-- /End Menu mobile -->
            </div>
        </div>
    </div>
</div>
<!-- /End Header -->

@yield('content')

<!-- Footer -->
<footer id="footer" class="footer-ctn">
    <img src="{{ url('assets/img_home/lavender-left.png') }}" class="footer-left">
    <img src="{{ url('assets/img_home/lavender-right.png') }}" class="footer-right">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 content_six_logo col-xs-8 col-xs-offset-2">
                <p class="p1" style="margin:0">Chương trình <br>
                    được tổ chức <br>
                    và thực hiện bởi</p>
                <a href="https://dahuong.vn/" target="_blank" style="text-align: center;"><img class="logo-footer1" src="{{ url('assets/img_home/logochuan.png') }}" alt="image" style="width: 72%"></a>
                <p style="margin:0">và</p>
                <a href="http://afamily.vn/" target="_blank"><img src="{{ url('assets/img_home/afamily.png') }}" alt="image"></a>
            </div>
        </div>
    </div>
    <a onclick="return $.app.checkLogin('{{ url('bai-du-thi/create') }}');">
        <div class="home-box-one__img__more home-box-one__img__more--10 btn-botton-config" style="z-index:100">Tham gia ngay</div>
    </a>
</footer>
<!-- End Footer -->

<!-- Bigslide menu mobile -->
<script src="{{ url('assets/lib/bigslidemenu/bigSlide.js') }}"></script>
<script>
    jQuery(document).ready(function() {
        jQuery('.btt-mobile').bigSlide({
            menu: '#menu',
            easyClose: true,
        });

        // jQuery('.btt-mobile').bigSlide();
        // var bigSlideAPI = (jQuery('.btt-mobile').bigSlide()).bigSlideAPI;
        // jQuery('.btt-mobile').click(function() {bigSlideAPI.view.toggleClose();});
    });
</script>
<!-- End Bigslide menu -->

@include('layouts.default_script')
{!! config('custom.embed_footer') !!}

@yield('footer')
<div class="bg-menu-act"></div>

<div class="modal fade coming-soon" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">

                </h4>
            </div>
            <div class="modal-body">
                <p>
                    Coming soon...
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PV4JWTT"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

</body>
</html>