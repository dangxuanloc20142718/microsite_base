@extends('admin.layouts.admin')

@section('breadcrum')
    <h1>
        Trang quản trị
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
@stop

@section('content')
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{ count($newNews) }}</h3>
                    <p>Bài viết mới</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                @can('admin.manage', 'news-view')<a href="{{ url('admin/news') }}" class="small-box-footer">Xem thêm <i class="fa fa-arrow-circle-right"></i></a>@endcan
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{ $newCommentsCount }}<sup style="font-size: 20px"></sup></h3>

                    <p>Comment mới</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                @can('admin.manage', 'comments-view')<a href="{{ url('admin/comments') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>@endcan
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{ count($newUser) }}</h3>

                    <p>Thành viên mới</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                @can('admin.manage', 'user-view')<a href="{{ url('admin/user') }}" class="small-box-footer">Xem thêm <i class="fa fa-arrow-circle-right"></i></a>@endcan
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{ $newContactCount }}</h3>

                    <p>Liên hệ mới</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                @can('admin.manage', 'contact-view')<a href="{{ url('admin/contact') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>@endcan
            </div>
        </div>
        <!-- ./col -->
    </div>
    <!-- /.row -->

    <div class="row">
        @can('admin.manage', 'comments-view')
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Comment mới</h3>

                    <div class="box-tools pull-right">
                        <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
                        </button>
                        <button data-widget="remove" class="btn btn-box-tool" type="button"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th>Họ và tên</th>
                                <th>Nội dung</th>
                                <th>Ngày đăng</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($newComments as $item)
                                <tr>
                                    <td>
                                        <b>{{ $item->fullname }}</b><br/>
                                        {{ $item->mobile }}
                                    </td>
                                    <td>
                                        {{ $item->comment }}
                                    </td>
                                    <td>{{ \App\Helper\StringHelper::date($item->created_at)  }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a class="btn btn-sm btn-primary btn-flat pull-right" href="{{ url('admin/comments') }}">Xem tất cả</a>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
        @endcan

        @can('admin.manage', 'contact-view')
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Liên hệ mới</h3>

                    <div class="box-tools pull-right">
                        <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
                        </button>
                        <button data-widget="remove" class="btn btn-box-tool" type="button"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th>Họ và tên</th>
                                <th>Nội dung</th>
                                <th>Ngày đăng</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($newContact as $item)
                                <tr>
                                    <td>
                                        <b>{{ $item->fullname }}</b><br/>
                                        {{ $item->mobile }}<br/>
                                        {{ $item->email }}
                                    </td>
                                    <td>
                                        {{ $item->content }}
                                    </td>
                                    <td>{{ \App\Helper\StringHelper::date($item->created_at)  }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a class="btn btn-sm btn-primary btn-flat pull-right" href="{{ url('admin/contact') }}">Xem tất cả</a>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
        @endcan
    </div>

@stop