@extends('admin.layouts.admin')

@section('breadcrum')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Quản lý Đại sứ
		<small>Danh sách</small>
	</h1>
	<ol class="breadcrumb hidden">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header hidden">
				<h3 class="box-title">Data Table With Full Features</h3>
			</div>
			<!-- /.box-header -->
			{!! Form::open( ['url' => 'admin/'.$routeName, 'method' => 'get'] ) !!}
				<div class="box-body">
					<div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-6">
								<div id="example1_filter" class="dataTables_filter" style="text-align:left">
									{{ Form::select('type_id', $types, app('request')->input('type_id'), ['onchange'=>'this.form.submit();', 'class'=>'hidden form-control']) }}
									<label><input type="search" class="form-control input-sm" value="{{ app('request')->input('keyword') }}" placeholder="Từ khóa tìm kiếm" name="keyword"></label>
									<button class="btn btn-sm btn-primary"><i class="fa fa-fw fa-search"></i> Tìm kiếm</button>
								</div>
							</div>
							<div class="col-sm-6 text-right">
								@can('admin.manage', 'newscategory-edit')
								<a class="btn btn-success" href="<?php echo URL::to('admin/'.$routeName.'/create');?>"><i class="fa fa-plus-circle"></i> Thêm mới</a>
								@endcan
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 data-table">
								@include('admin.'.$routeName.'.index_table')
							</div>
						</div>
					</div>
				</div>
			{!! Form::close() !!}
			<!-- /.box-body -->
		</div>
	</div>
</div>
@stop