@extends('admin.layouts.admin')

@section('breadcrum')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Quản lý Bình luận
		<small>Bình luận</small>
	</h1>
	<ol class="breadcrumb hidden">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header hidden">
				<h3 class="box-title">Data Table With Full Features</h3>
			</div>
			<!-- /.box-header -->
			{!! Form::open( ['url' => 'admin/comments', 'method' => 'get'] ) !!}
			<div class="box-body">
				<div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
					<div class="row">
						<div class="col-sm-6">
							<div id="example1_filter" class="dataTables_filter" style="text-align:left">
								<label><input type="search" class="form-control input-sm" value="{{ app('request')->input('keyword') }}" placeholder="Từ khóa tìm kiếm" name="keyword"></label>
								<button class="btn btn-sm btn-primary"><i class="fa fa-fw fa-search"></i> Tìm kiếm</button>
							</div>
						</div>
						<div class="col-sm-6 text-right hidden">
							<a class="btn btn-success" href="<?php echo URL::to('/admin/comments/create');?>"><i class="fa fa-plus-circle"></i> Thêm mới</a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-bordered table-striped dataTable" id="example1" role="grid" aria-describedby="example1_info">
								<thead>
								<tr role="row" class="text-center">
									<th class="text-center" style="padding-right: 10px;">STT</th>
									<th class="sorting_asc1" aria-sort="ascending">Họ và tên</th>
									<th class="sorting1 text-center">Nội dung</th>
									<th class="sorting1 text-center">Trong bài viết</th>
									<th class="sorting1 text-center">Ngày tạo</th>
									<th class="sorting1 text-center">Trạng thái</th>
									<th class="sorting1 text-center">ID</th>
									<th class="sorting1 text-center">...</th>
								</tr>
								</thead>
								<tbody>
								@foreach( $news as $k => $item )
									<tr role="row" class="even">
										<td class="text-center">{{$k+1}}</td>
										<td>
											<b>
												@can('admin.manage', 'comments-edit')<a href="<?php echo URL::to('/admin/comments/'.$item->id);?>/edit">@endcan
													{{$item->fullname}}
													@can('admin.manage', 'comments-edit')</a>@endcan
											</b>
											<br/>{{ $item->mobile }}
										</td>
										<td>{{ $item->comment  }}</td>
										<td><a href="{{ $item->link }}" target="_blank">{{ $item->item_title }}</a></td>
										<td>{{ \App\Helper\StringHelper::date($item->created_at)  }}</td>
										<td class="text-center">
											@can('admin.manage', 'comments-status')<a href="{{ url('admin/comments?id='.$item->id.'&status='.( ($item->status) ? 0 : 1 )) }}">@endcan
											{!! \App\Helper\StringHelper::stateStatus($item->status)  !!}
											@can('admin.manage', 'comments-status')</a>@endcan
										</td>
										<td class="text-center">{{$item->id}}</td>
										<td class="text-center">
											@can('admin.manage', 'comments-edit')<a class="btn btn-success btn-xs" href="<?php echo URL::to('/admin/comments/'.$item->id);?>/edit"><i class="fa fa-pencil"></i> Sửa</a>@endcan
											@can('admin.manage', 'comments-delete')<a onclick="return deleteItem({{$item->id}});" class="btn btn-danger btn-xs" href="#"><i class="fa fa-trash"></i> Xóa</a>@endcan
										</td>
									</tr>
								@endforeach
								</tbody>
								<tfoot class="hidden">
								<tr>
									<th rowspan="1" colspan="1">Rendering engine</th>
									<th rowspan="1" colspan="1">Browser</th>
									<th rowspan="1" colspan="1">Platform(s)</th>
									<th rowspan="1" colspan="1">Engine version</th>
									<th rowspan="1" colspan="1">CSS grade</th>
								</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="row"><br/>
						<div class="col-sm-5">
							<div class="dataTables_length" id="example1_length">
								<label>Hiển thị <select onchange="this.form.submit();" name="limit" class="form-control input-sm">
										<option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> mục</label>
							</div>
							<div class="dataTables_info hidden" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
						</div>
						<div class="col-sm-7">
							<div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
								{!! $news->appends(array('keyword' => app('request')->input('keyword')))->links() !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		{!! Form::close() !!}
			<!-- /.box-body -->
		</div>
	</div>
</div>

<div class="hidden">
	<form action="{{ URL::route('comments.destroy', '') }}" id="form_delete" method="POST">
		<input type="hidden" name="_method" value="DELETE">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
	</form>
</div>
<script type="text/javascript">
	function deleteItem(id){
		if ( confirm('Bạn có chắc chắn muốn xóa?') ) {
			$('#form_delete').attr('action', $('#form_delete').attr('action') + "/" + id);
			$('#form_delete').submit();
		}
		return false;
	}
</script>
@stop