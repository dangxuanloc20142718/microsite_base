@extends('admin.layouts.admin')

@section('breadcrum')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Quản lý Bình luận
		<small>Bình luận</small>
	</h1>
	<ol class="breadcrumb hidden">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Thêm / Sửa thông tin</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			{!! Form::model($news,[ 'method' => 'PATCH', 'action' => ['Admin\\CommentsController@update', $news->id], 'name'=>'uploadform', 'class' => 'form-horizontal', 'files'=>true ]) !!}
			<div class="box-body" style="padding: 5px;">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#vn" aria-controls="vn" role="tab" data-toggle="tab">Tiếng Việt</a></li>
					<li role="presentation" class="hidden"><a href="#en" aria-controls="en" role="tab" data-toggle="tab">Tiếng Anh</a></li>
				</ul>

				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="vn">
						<div class="form-group">
							<label class="col-sm-2 control-label required">Tiêu đề</label>
							<div class="col-sm-10">
								{!! Form::text('fullname', $news->title, array('class' => 'form-control', 'required')) !!}
							</div>
						</div>
						<div class="form-group hidden">
							<label class="col-sm-2 control-label">Ảnh Banner</label>
							<div class="col-sm-10">
								{!! Form::file('image_upload') !!}
								@if($news->image != '')
									{!! Form::hidden('image', $news->image, array('id' => 'image')) !!}
									<div class="pull-left" style="position: relative;">
										<img src="<?php echo URL::to($news->image);?>" class="thumbnail pull-left" style="width: 80px; margin: 0px">
										<a onclick="if(!confirm('Bạn có chắc muốn xóa ảnh?')) return false; $(this).parent().remove(); $('#image').val('');" class="btn btn-danger btn-xs pull-left" href="#" style="position: absolute; bottom: 0; right: 0;">Xóa ảnh?</a>
									</div>
								@endif<div class="clearfix"></div>
								<small>Kích thước tối thiểu 1200px</small>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label required" for="inputPassword3">Nội dung</label>
							<div class="col-sm-10">
								{!! Form::textarea('comment', $news->fulltext, array('class' => 'form-control', 'id' => '', 'rows' => 3)) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="inputPassword3">Phản hồi</label>
							<div class="col-sm-10">
								{!! Form::textarea('reply', $news->fulltext, array('class' => 'form-control', 'id' => '', 'rows' => 3)) !!}
							</div>
						</div>

					</div>

					<div role="tabpanel" class="tab-pane hidden" id="en">
						<div class="form-group">
							<label class="col-sm-2 control-label">Tiêu đề</label>
							<div class="col-sm-10">
								{!! Form::text('title_en', $news->title_en, array('class' => 'form-control')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="inputPassword3">Mô tả đầy đủ</label>
							<div class="col-sm-10">
								{!! Form::textarea('fulltext_en', $news->fulltext_en, array('class' => 'form-control', 'id' => 'editor2', 'rows' => 3)) !!}
							</div>
						</div>

					</div>
				</div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<div class="col-sm-2"></div>
				<div class="col-sm-10">
					<button class="btn btn-primary" type="submit"><i class="fa fa-check-circle"></i>   Hoàn tất</button>
					<a class="btn btn-default" href="<?php echo URL::to('/admin/comments');?>">Quay lại</a>
				</div>
			</div>
			<!-- /.box-footer -->
			{!! Form::close() !!}

		</div>
	</div>
</div>

@stop