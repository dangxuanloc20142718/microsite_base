@extends('admin.layouts.admin')

@section('breadcrum')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Quản lý Hệ thống
		<small>Cấu hình</small>
	</h1>
	<ol class="breadcrumb hidden">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border hidden">
				<h3 class="box-title">Thêm / Sửa thông tin</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			{!! Form::model($news,[ 'method' => 'PATCH', 'action' => ['Admin\\ConfigController@update', 0], 'name'=>'uploadform', 'class' => 'form-horizontal', 'files'=>true ]) !!}
			<input type="hidden" name="tab" value="{{ app('request')->input('tab') }}" id="tab"/>
			<div class="box-body" style="padding: 5px;">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#vn" aria-controls="vn" class="vn" role="tab" data-toggle="tab">Cấu hình chung</a></li>
					<li role="presentation" class=""><a href="#en" aria-controls="en" class="en" role="tab" data-toggle="tab">SEO</a></li>
					<li role="presentation" class=""><a href="#mail" aria-controls="mail" class="mail" role="tab" data-toggle="tab">Gửi mail</a></li>
					<li role="presentation" class=""><a href="#embed" aria-controls="embed" class="embed" role="tab" data-toggle="tab">Mã nhúng</a></li>
					<li role="presentation" class=""><a href="#offline" aria-controls="offline" class="offline" role="tab" data-toggle="tab">Tắt Web</a></li>
					<li role="presentation" class=""><a href="#theme" aria-controls="theme" class="theme" role="tab" data-toggle="tab">Giao diện</a></li>
				</ul>

				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="vn">

						<div class="form-group">
							<label class="col-sm-3 control-label">Mở cho đăng ký dự thi</label>
							<div class="col-sm-9">
								{!! Form::text('open', config('custom.open', 0), array('class' => 'form-control')) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Vòng thi</label>
							<div class="col-sm-9">
								{!! Form::text('round', config('custom.round', 1), array('class' => 'form-control')) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">round_2_end_create</label>
							<div class="col-sm-9">
								{!! Form::text('round_2_end_create', config('custom.round_2_end_create', ''), array('class' => 'form-control')) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">round_2_start_vote</label>
							<div class="col-sm-9">
								{!! Form::text('round_2_start_vote', config('custom.round_2_start_vote', ''), array('class' => 'form-control')) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">round_2_end_vote</label>
							<div class="col-sm-9">
								{!! Form::text('round_2_end_vote', config('custom.round_2_end_vote', ''), array('class' => 'form-control')) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">enable_top_18</label>
							<div class="col-sm-9">
								{!! Form::text('enable_top_18', config('custom.enable_top_18', ''), array('class' => 'form-control')) !!}
							</div>
						</div>


						<div class="form-group">
							<label class="col-sm-3 control-label">Favicon</label>
							<div class="col-sm-9">
								{!! Form::file('image_favicon') !!}
								<div class="pull-left" style="position: relative;">
									<img src="{{ url('uploads/others/favicon.ico?').rand(1, 100) }}" class="thumbnail pull-left" style="max-width: 80px; margin: 0px">
								</div>
								<div class="clearfix"></div>
								<small>Kiểu file: ico, png, gif. Kích thước 16x16</small>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Logo</label>
							<div class="col-sm-9">
								{!! Form::file('image_logo') !!}
								<div class="pull-left" style="position: relative;">
									<img src="{{ url('uploads/others/logo.png?').rand(1, 100) }}" class="thumbnail pull-left" style="max-width: 200px; margin: 0px">
								</div>
								<div class="clearfix"></div>
								<small>Kiểu file: png, jpg. Kích thước 200x...</small>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Họ và tên Admin</label>
							<div class="col-sm-9">
								{!! Form::text('admin_name', config('custom.admin_name'), array('class' => 'form-control')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Email Admin</label>
							<div class="col-sm-9">
								{!! Form::text('admin_email', config('custom.admin_email'), array('class' => 'form-control')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Điện thoại Admin</label>
							<div class="col-sm-9">
								{!! Form::text('admin_mobile', config('custom.admin_mobile'), array('class' => 'form-control')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Địa chỉ Admin</label>
							<div class="col-sm-9">
								{!! Form::textarea('admin_address', config('custom.admin_address'), array('class' => 'form-control')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Facebook Fanpage</label>
							<div class="col-sm-9">
								{!! Form::text('admin_fanpage', config('custom.admin_fanpage'), array('class' => 'form-control')) !!}
							</div>
						</div>
					</div>

					<div role="tabpanel" class="tab-pane" id="en">
						<div class="form-group">
							<label class="col-sm-3 control-label">Tác giả (Author)</label>
							<div class="col-sm-9">
								{!! Form::text('seo_author', config('custom.seo_author'), array('class' => 'form-control')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Tiêu đề trang chủ</label>
							<div class="col-sm-9">
								{!! Form::text('seo_title', config('custom.seo_title'), array('class' => 'form-control')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Mô tả trang chủ</label>
							<div class="col-sm-9">
								{!! Form::textarea('seo_description', config('custom.seo_description'), array('class' => 'form-control', 'rows'=>3)) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Ảnh khi share trang chủ trên Facebook</label>
							<div class="col-sm-9">
								{!! Form::file('image_fbimage') !!}
								<div class="pull-left" style="position: relative;">
									<img src="{{ url('media/system/fbimage.jpg?').rand(1, 100) }}" class="thumbnail pull-left" style="max-width: 200px; margin: 0px">
								</div>
								<div class="clearfix"></div>
								<small>Kiểu file: jpg. Kích thước đề xuất: 1200x630</small>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Facebook App ID</label>
							<div class="col-sm-9">
								{!! Form::text('facebook_appid', config('custom.facebook_appid'), array('class' => 'form-control')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Facebook App Secret</label>
							<div class="col-sm-9">
								{!! Form::text('facebook_appsecret', config('custom.facebook_appsecret'), array('class' => 'form-control')) !!}
							</div>
						</div>
					</div>

					<div role="tabpanel" class="tab-pane" id="embed">
						<div class="form-group">
							<label class="col-sm-3 control-label">Mã nhúng đầu trang</label>
							<div class="col-sm-9">
								{!! Form::textarea('embed_header', config('custom.embed_header'), array('class' => 'form-control', 'rows'=>3)) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Mã nhúng cuối trang</label>
							<div class="col-sm-9">
								{!! Form::textarea('embed_footer', config('custom.embed_footer'), array('class' => 'form-control', 'rows'=>3)) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Google ReCaptcha Sitekey</label>
							<div class="col-sm-9">
								{!! Form::text('captcha_sitekey', config('custom.captcha_sitekey'), array('class' => 'form-control', 'rows'=>3)) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Google ReCaptcha Secret</label>
							<div class="col-sm-9">
								{!! Form::text('captcha_secret', config('custom.captcha_secret'), array('class' => 'form-control', 'rows'=>3)) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Google Map PlaceID</label>
							<div class="col-sm-9">
								{!! Form::text('gmap_placeid', config('custom.gmap_placeid'), array('class' => 'form-control', 'rows'=>3)) !!}
							</div>
						</div>
					</div>

					<div role="tabpanel" class="tab-pane" id="mail">
						<div class="form-group">
							<label class="col-sm-3 control-label">Gửi mail khi có Comment?</label>
							<div class="col-sm-9" style="padding-top: 5px;">
								<label>{!! Form::radio('mail_comment', 1, config('custom.mail_comment') ? true : false ) !!} Có</label> -
								<label>{!! Form::radio('mail_comment', 0, config('custom.mail_comment') ? false : true ) !!} Không</label>
								 ( Mail sẽ được gửi về email của Admin)
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Tự động bật Comment?</label>
							<div class="col-sm-9" style="padding-top: 5px;">
								<label>{!! Form::radio('comment_approve', 1, config('custom.comment_approve') ? true : false ) !!} Có</label> -
								<label>{!! Form::radio('comment_approve', 0, config('custom.comment_approve') ? false : true ) !!} Không</label>
								( Comment sẽ được hiển thị luôn, không phải qua kiểm duyệt?)
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Gửi mail khi có Liên hệ?</label>
							<div class="col-sm-9" style="padding-top: 5px;">
								<label>{!! Form::radio('mail_contact', 1, config('custom.mail_contact') ? true : false ) !!} Có</label> -
								<label>{!! Form::radio('mail_contact', 0, config('custom.mail_contact') ? false : true ) !!} Không</label>
								 ( Mail sẽ được gửi về email của Admin)
							</div>
						</div>
					</div>

					<div role="tabpanel" class="tab-pane" id="offline">
						<div class="form-group">
							<label class="col-sm-3 control-label">Website tạm dừng?</label>
							<div class="col-sm-9" style="padding-top: 5px;">
								<label>{!! Form::radio('offline', 1, config('custom.offline') ? true : false ) !!} Có</label> -
								<label>{!! Form::radio('offline', 0, config('custom.offline') ? false : true ) !!} Không</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Ngày mở</label>
							<div class="col-sm-9">
								{!! Form::text('offline_date', config('custom.offline_date'), array('class' => 'form-control datepicker')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Tiêu đề</label>
							<div class="col-sm-9">
								{!! Form::text('offline_title', config('custom.offline_title'), array('class' => 'form-control')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Mô tả</label>
							<div class="col-sm-9">
								{!! Form::textarea('offline_description', config('custom.offline_description'), array('class' => 'form-control', 'rows'=>3)) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Ảnh nền</label>
							<div class="col-sm-9">
								{!! Form::file('image_offline') !!}
								<div class="pull-left" style="position: relative;">
									<img src="{{ url('uploads/others/offline.jpg?').rand(1, 100) }}" class="thumbnail pull-left" style="max-width: 200px; margin: 0px">
								</div>
								<div class="clearfix"></div>
								<small>Kiểu file: jpg. Kích thước đề xuất: 1200x...</small>
							</div>
						</div>
					</div>

					<div role="tabpanel" class="tab-pane" id="theme">
						<div class="form-group">
							<label class="col-sm-3 control-label">Áp dụng thay đổi giao diện dưới đây?</label>
							<div class="col-sm-9" style="padding-top: 5px;">
								<label>{!! Form::radio('theme', 1, config('custom.theme') ? true : false ) !!} Có</label> -
								<label>{!! Form::radio('theme', 0, config('custom.theme') ? false : true ) !!} Không</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Cả website - Ảnh nền</label>
							<div class="col-sm-9" style="padding-top: 5px;">
								<div class="theme_web_url">
									@if(config('custom.theme_web_url'))
										<div style="width: 100px; height: 80px; overflow: hidden; float: left; margin-right: 10px; position:relative;" class="pull-left">
											<img style="width: 100px; margin-bottom: 0px;" class="thumbnail pull-left" alt="" src="{{ config('custom.theme_web_url') }}" />
											<a onclick="if(!confirm('Bạn có chắc muốn xóa ảnh?')) return false; $('#theme_web_url').val(''); $(this).parent().remove(); return false;" class="btn btn-danger btn-xs pull-left" href="#" style="position: absolute; bottom: 0; right: 0;">Xóa ảnh?</a></div>
									@endif
								</div>
								<input type="hidden" name="theme_web_url" id="theme_web_url" value="{{ config('custom.theme_web_url') }}"/>
								<button type="button" class="btn btn-success btn-sm" onclick="$('#resize').val(1); isInsertEditor = false; replaceTo='.theme_web_url'; valueTo='#theme_web_url';" data-toggle="modal" data-target="#mediaModal">
									<i class="glyphicon glyphicon-plus"></i> Chọn ảnh
								</button>
								{!! Form::select('theme_web_repeat', ['no-repeat'=>'no-repeat','repeat'=>'repeat','repeat-x'=>'repeat-x','repeat-y'=>'repeat-y'], config('custom.theme_web_repeat')) !!}
								{!! Form::select('theme_web_position', ['left top'=>'left top','center top'=>'center top','right top'=>'right top'], config('custom.theme_web_position')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Cả website - Màu sắc</label>
							<div class="col-sm-9" style="padding-top: 5px;">
								<table>
									<tr>
										<td>Màu nền<br/>
											<div class="input-group colorpicker " style="width: 100px;">
												<input value="{{ config('custom.theme_web_bgcolor') }}" type="text" class="form-control" name="theme_web_bgcolor">
												<div class="input-group-addon">
													<i style="background-color: {{ config('custom.theme_web_bgcolor') }};"></i>
												</div>
											</div>
										</td>
										<td style="padding-left: 15px;">Màu chữ<br/>
											<div class="input-group colorpicker " style="width: 100px;">
												<input value="{{ config('custom.theme_web_color') }}" type="text" class="form-control" name="theme_web_color">
												<div class="input-group-addon">
													<i style="background-color: {{ config('custom.theme_web_color') }};"></i>
												</div>
											</div>
										</td>
										<td style="padding-left: 15px;">Màu link<br/>
											<div class="input-group colorpicker " style="width: 100px;">
												<input value="{{ config('custom.theme_web_acolor') }}" type="text" class="form-control" name="theme_web_acolor">
												<div class="input-group-addon">
													<i style="background-color: {{ config('custom.theme_web_acolor') }};"></i>
												</div>
											</div>
										</td>
									</tr>
								</table>
							</div>
						</div>
						<hr/>

						<div class="form-group">
							<label class="col-sm-3 control-label">Header - Ảnh nền</label>
							<div class="col-sm-9" style="padding-top: 5px;">
								<div class="theme_header_url">
									@if(config('custom.theme_header_url'))
										<div style="width: 100px; height: 80px; overflow: hidden; float: left; margin-right: 10px; position:relative;" class="pull-left">
											<img style="width: 100px; margin-bottom: 0px;" class="thumbnail pull-left" alt="" src="{{ config('custom.theme_header_url') }}" />
											<a onclick="if(!confirm('Bạn có chắc muốn xóa ảnh?')) return false; $('#theme_header_url').val(''); $(this).parent().remove(); return false;" class="btn btn-danger btn-xs pull-left" href="#" style="position: absolute; bottom: 0; right: 0;">Xóa ảnh?</a></div>
									@endif
								</div>
								<input type="hidden" name="theme_header_url" id="theme_header_url" value="{{ config('custom.theme_header_url') }}"/>
								<button type="button" class="btn btn-success btn-sm" onclick="$('#resize').val(1); isInsertEditor = false; replaceTo='.theme_header_url'; valueTo='#theme_header_url';" data-toggle="modal" data-target="#mediaModal">
									<i class="glyphicon glyphicon-plus"></i> Chọn ảnh
								</button>
								{!! Form::select('theme_header_repeat', ['no-repeat'=>'no-repeat','repeat'=>'repeat','repeat-x'=>'repeat-x','repeat-y'=>'repeat-y'], config('custom.theme_header_repeat')) !!}
								{!! Form::select('theme_header_position', ['left top'=>'left top','center top'=>'center top','right top'=>'right top'], config('custom.theme_header_position')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Header - Màu sắc</label>
							<div class="col-sm-9" style="padding-top: 5px;">
								<table>
									<tr>
										<td>Màu nền<br/>
											<div class="input-group colorpicker " style="width: 100px;">
												<input value="{{ config('custom.theme_header_bgcolor') }}" type="text" class="form-control" name="theme_header_bgcolor">
												<div class="input-group-addon">
													<i style="background-color: {{ config('custom.theme_header_bgcolor') }};"></i>
												</div>
											</div>
										</td>
										<td style="padding-left: 15px;">Màu chữ<br/>
											<div class="input-group colorpicker " style="width: 100px;">
												<input value="{{ config('custom.theme_header_color') }}" type="text" class="form-control" name="theme_header_color">
												<div class="input-group-addon">
													<i style="background-color: {{ config('custom.theme_header_color') }};"></i>
												</div>
											</div>
										</td>
										<td style="padding-left: 15px;">Màu link<br/>
											<div class="input-group colorpicker " style="width: 100px;">
												<input value="{{ config('custom.theme_header_acolor') }}" type="text" class="form-control" name="theme_header_acolor">
												<div class="input-group-addon">
													<i style="background-color: {{ config('custom.theme_header_acolor') }};"></i>
												</div>
											</div>
										</td>
									</tr>
								</table>
							</div>
						</div>
						<hr/>

						<div class="form-group">
							<label class="col-sm-3 control-label">Menu - Ảnh nền</label>
							<div class="col-sm-9" style="padding-top: 5px;">
								<div class="theme_menu_url">
									@if(config('custom.theme_menu_url'))
										<div style="width: 100px; height: 80px; overflow: hidden; float: left; margin-right: 10px; position:relative;" class="pull-left">
											<img style="width: 100px; margin-bottom: 0px;" class="thumbnail pull-left" alt="" src="{{ config('custom.theme_menu_url') }}" />
											<a onclick="if(!confirm('Bạn có chắc muốn xóa ảnh?')) return false; $('#theme_menu_url').val(''); $(this).parent().remove(); return false;" class="btn btn-danger btn-xs pull-left" href="#" style="position: absolute; bottom: 0; right: 0;">Xóa ảnh?</a></div>
									@endif
								</div>
								<input type="hidden" name="theme_menu_url" id="theme_menu_url" value="{{ config('custom.theme_menu_url') }}"/>
								<button type="button" class="btn btn-success btn-sm" onclick="$('#resize').val(1); isInsertEditor = false; replaceTo='.theme_menu_url'; valueTo='#theme_menu_url';" data-toggle="modal" data-target="#mediaModal">
									<i class="glyphicon glyphicon-plus"></i> Chọn ảnh
								</button>
								{!! Form::select('theme_menu_repeat', ['no-repeat'=>'no-repeat','repeat'=>'repeat','repeat-x'=>'repeat-x','repeat-y'=>'repeat-y'], config('custom.theme_menu_repeat')) !!}
								{!! Form::select('theme_menu_position', ['left top'=>'left top','center top'=>'center top','right top'=>'right top'], config('custom.theme_menu_position')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Menu - Màu sắc</label>
							<div class="col-sm-9" style="padding-top: 5px;">
								<table>
									<tr>
										<td>Màu nền<br/>
											<div class="input-group colorpicker " style="width: 100px;">
												<input value="{{ config('custom.theme_menu_bgcolor') }}" type="text" class="form-control" name="theme_menu_bgcolor">
												<div class="input-group-addon">
													<i style="background-color: {{ config('custom.theme_menu_bgcolor') }};"></i>
												</div>
											</div>
										</td>
										<td style="padding-left: 15px;">Màu chữ<br/>
											<div class="input-group colorpicker " style="width: 100px;">
												<input value="{{ config('custom.theme_menu_color') }}" type="text" class="form-control" name="theme_menu_color">
												<div class="input-group-addon">
													<i style="background-color: {{ config('custom.theme_menu_color') }};"></i>
												</div>
											</div>
										</td>
										<td style="padding-left: 15px;">Màu link<br/>
											<div class="input-group colorpicker " style="width: 100px;">
												<input value="{{ config('custom.theme_menu_acolor') }}" type="text" class="form-control" name="theme_menu_acolor">
												<div class="input-group-addon">
													<i style="background-color: {{ config('custom.theme_menu_acolor') }};"></i>
												</div>
											</div>
										</td>
									</tr>
								</table>
							</div>
						</div>
						<hr/>

						<div class="form-group">
							<label class="col-sm-3 control-label">Footer - Ảnh nền</label>
							<div class="col-sm-9" style="padding-top: 5px;">
								<div class="theme_footer_url">
									@if(config('custom.theme_footer_url'))
										<div style="width: 100px; height: 80px; overflow: hidden; float: left; margin-right: 10px; position:relative;" class="pull-left">
											<img style="width: 100px; margin-bottom: 0px;" class="thumbnail pull-left" alt="" src="{{ config('custom.theme_footer_url') }}" />
											<a onclick="if(!confirm('Bạn có chắc muốn xóa ảnh?')) return false; $('#theme_footer_url').val(''); $(this).parent().remove(); return false;" class="btn btn-danger btn-xs pull-left" href="#" style="position: absolute; bottom: 0; right: 0;">Xóa ảnh?</a></div>
									@endif
								</div>
								<input type="hidden" name="theme_footer_url" id="theme_footer_url" value="{{ config('custom.theme_footer_url') }}"/>
								<button type="button" class="btn btn-success btn-sm" onclick="$('#resize').val(1); isInsertEditor = false; replaceTo='.theme_footer_url'; valueTo='#theme_footer_url';" data-toggle="modal" data-target="#mediaModal">
									<i class="glyphicon glyphicon-plus"></i> Chọn ảnh
								</button>
								{!! Form::select('theme_footer_repeat', ['no-repeat'=>'no-repeat','repeat'=>'repeat','repeat-x'=>'repeat-x','repeat-y'=>'repeat-y'], config('custom.theme_footer_repeat')) !!}
								{!! Form::select('theme_footer_position', ['left top'=>'left top','center top'=>'center top','right top'=>'right top'], config('custom.theme_footer_position')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Menu - Màu sắc</label>
							<div class="col-sm-9" style="padding-top: 5px;">
								<table>
									<tr>
										<td>Màu nền<br/>
											<div class="input-group colorpicker " style="width: 100px;">
												<input value="{{ config('custom.theme_footer_bgcolor') }}" type="text" class="form-control" name="theme_footer_bgcolor">
												<div class="input-group-addon">
													<i style="background-color: {{ config('custom.theme_footer_bgcolor') }};"></i>
												</div>
											</div>
										</td>
										<td style="padding-left: 15px;">Màu chữ<br/>
											<div class="input-group colorpicker " style="width: 100px;">
												<input value="{{ config('custom.theme_footer_color') }}" type="text" class="form-control" name="theme_footer_color">
												<div class="input-group-addon">
													<i style="background-color: {{ config('custom.theme_footer_color') }};"></i>
												</div>
											</div>
										</td>
										<td style="padding-left: 15px;">Màu link<br/>
											<div class="input-group colorpicker " style="width: 100px;">
												<input value="{{ config('custom.theme_footer_acolor') }}" type="text" class="form-control" name="theme_footer_acolor">
												<div class="input-group-addon">
													<i style="background-color: {{ config('custom.theme_footer_acolor') }};"></i>
												</div>
											</div>
										</td>
									</tr>
								</table>
							</div>
						</div>
						<hr/>

					</div>
				</div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<div class="col-sm-2"></div>
				<div class="col-sm-9">
					<button class="btn btn-primary" type="submit"><i class="fa fa-check-circle"></i>   Hoàn tất</button>
				</div>
			</div>
			<!-- /.box-footer -->
			{!! Form::close() !!}
			<link rel="stylesheet" href="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/colorpicker/bootstrap-colorpicker.min.css">
			<script src="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

			<script type="text/javascript">
				$('.nav-tabs a').click(function(){
					$('#tab').val( $(this).attr('aria-controls') );
				});
				setTimeout(
					function(){
						$('.nav-tabs li').removeClass('active');
						$('.nav-tabs a.{{ app('request')->input('tab', 'vn') }}').parent().addClass('active');
						$('.tab-pane').removeClass('active');
						$('#{{ app('request')->input('tab', 'vn') }}').addClass('active');
					}, 100);

				$(".colorpicker").colorpicker();
			</script>
		</div>
	</div>
</div>

@stop