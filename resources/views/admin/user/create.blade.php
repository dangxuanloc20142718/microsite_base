@extends('admin.layouts.admin')

@section('breadcrum')
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Quản lý Thành viên
			<small>Thành viên</small>
		</h1>
		<ol class="breadcrumb hidden">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Thêm / Sửa thông tin</h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				{!! Form::open( ['url' => 'admin/user', 'class' => 'form-horizontal', 'files'=>true] ) !!}
				<div class="box-body">
					<div class="form-group">
						<label class="col-sm-2 control-label">Nhóm</label>
						<div class="col-sm-10">
							{!! Form::select('group', \App\Models\User::getUserGroups(), old('group', 3), array('class'=>'form-control',  'style'=>'width:200px', 'onchange'=>'showGroup(this.value);')) !!}
						</div>
					</div>
					<div class="form-group group group2 hidden">
						<label class="col-sm-2 control-label">Phân quyền</label>
						<div class="col-sm-10">
							Chức năng này sẽ hiển thị khi bạn lưu xong thông tin người dùng
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Họ và tên</label>
						<div class="col-sm-10">
							{!! Form::text('name', old('name'), array('class' => 'form-control')) !!}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							{!! Form::text('email', old('email'), array('class' => 'form-control')) !!}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Điện thoại</label>
						<div class="col-sm-10">
							{!! Form::text('mobile', old('mobile'), array('class' => 'form-control')) !!}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Ảnh</label>
						<div class="col-sm-10">
							{!! Form::file('image_upload') !!}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Trạng thái</label>
						<div class="col-sm-10">
							{!! Form::select('status', array(1=>'Hoạt động', 0=>'Banned'), old('status'), array('class'=>'form-control')) !!}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Mật khẩu</label>
						<div class="col-sm-10">
							<div class="password">
								{!! Form::text('password_new', old('password_new'), array('class' => 'form-control', 'placeholder' => 'Nhập mật khẩu mới')) !!}
							</div>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<div class="col-sm-2"></div>
					<div class="col-sm-10">
						<button class="btn btn-primary" onclick="$('#action').val('save');" type="submit"><i class="fa fa-check-circle"></i>   Lưu & Quay lại</button>
						<button class="btn btn-primary" onclick="$('#action').val('apply');" type="submit"><i class="fa fa-save"></i> Lưu</button>
						<a class="btn btn-default" href="<?php echo URL::to('admin/user');?>">Quay lại</a>
					</div>
				</div>
				<!-- /.box-footer -->
				<input type="hidden" name="action" id="action" value="save">
				{!! Form::close() !!}

				<script>
					function showGroup(groupId){
						$('.group').addClass('hidden');
						$('.group' + groupId).removeClass('hidden');
					}
				</script>
			</div>
		</div>
	</div>

@stop