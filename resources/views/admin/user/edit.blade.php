@extends('admin.layouts.admin')

@section('breadcrum')
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Quản lý Thành viên
			<small>Thành viên</small>
		</h1>
		<ol class="breadcrumb hidden">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>
	<style>
		.table label { font-weight: normal; }
	</style>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Thêm / Sửa thông tin</h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				{!! Form::model($news,[ 'method' => 'PATCH', 'action' => ['Admin\\UserController@update', $news->id], 'class' => 'form-horizontal', 'files'=>true ]) !!}
				<div class="box-body">
					<div class="form-group">
						<label class="col-sm-2 control-label">Nhóm</label>
						<div class="col-sm-10">
							{!! Form::select('group', \App\Models\User::getUserGroups(), $news->group, array('class'=>'form-control', 'style'=>'width:200px', 'onchange'=>'showGroup(this.value);')) !!}
						</div>
					</div>
					<div class="form-group group group2 @if($news->group != 2) hidden @endif">
						<label class="col-sm-2 control-label">Phân quyền</label>
						<div class="col-sm-10">
							<label><input type="checkbox" onchange="$('.table-perm input').prop('checked', $(this).prop('checked'))"/> Chọn tất cả</label>
							<table class="table table-striped table-bordered table-hover table-perm">
								<thead>
								<th>Chức năng</th>
								<th>Xem</th>
								<th>Thêm, sửa</th>
								<th>Sửa trạng thái</th>
								<th>Xóa</th>
								</thead>
								<tbody>
								<tr>
									<td><b>FAQ</b></td>
									<td>
										<label><input type="checkbox" @if( in_array('faq-view', $news->permission) ) checked @endif value="1" name="faq-view"/> Xem Menu</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('faq-edit', $news->permission) ) checked @endif value="1" name="faq-edit"/> Thêm, sửa Bình luận</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('faq-status', $news->permission) ) checked @endif value="1" name="faq-status"/> Sửa Trạng thái Bình luận</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('faq-delete', $news->permission) ) checked @endif value="1" name="faq-delete"/> Xóa Bình luận</label>
									</td>
								</tr>
								<tr>
									<td><b>Bài dự thi</b></td>
									<td>

									</td>
									<td>
									</td>
									<td>
									</td>
									<td>
									</td>
								</tr>
								<tr>
									<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Quản lý Bài dự thi</td>
									<td>
										<label><input type="checkbox" @if( in_array('submit-view', $news->permission) ) checked @endif value="1" name="submit-view"/> Xem</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('submit-edit', $news->permission) ) checked @endif value="1" name="submit-edit"/> Thêm, Sửa</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('submit-status', $news->permission) ) checked @endif value="1" name="submit-status"/> Sửa Trạng thái</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('submit-delete', $news->permission) ) checked @endif value="1" name="submit-delete"/> Xóa</label>
									</td>
								</tr>
								<tr>
									<td><b>Tin tức</b></td>
									<td> <label><input type="checkbox" @if( in_array('news-view', $news->permission) ) checked @endif value="1" name="news-view"/> Xem Menu Tin tức</label> </td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								@foreach($newsCats as $cat)
									<tr>
										<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $cat->title }}</td>
										<td>
											<label><input type="checkbox" @if( in_array('news-'.$cat->id.'-'.$news->id.'-view', $news->permission) ) checked @endif value="1" name="news-{{ $cat->id }}-{{ $news->id }}-view"/> Bài viết</label><br/>
											{{--<label><input type="checkbox" @if( in_array('news-'.$cat->id.'-*-view', $news->permission) ) checked @endif value="1" name="news-{{ $cat->id }}-*-view"/> Bài của người khác</label>--}}
										</td>
										<td>
											<label><input type="checkbox" @if( in_array('news-'.$cat->id.'-'.$news->id.'-edit', $news->permission) ) checked @endif value="1" name="news-{{ $cat->id }}-{{ $news->id }}-edit"/> Bài của mình</label><br/>
											<label><input type="checkbox" @if( in_array('news-'.$cat->id.'-*-edit', $news->permission) ) checked @endif value="1" name="news-{{ $cat->id }}-*-edit"/> Bài của người khác</label>
										</td>
										<td>
											<label><input type="checkbox" @if( in_array('news-'.$cat->id.'-'.$news->id.'-status', $news->permission) ) checked @endif value="1" name="news-{{ $cat->id }}-{{ $news->id }}-status"/> Bài của mình</label><br/>
											<label><input type="checkbox" @if( in_array('news-'.$cat->id.'-*-status', $news->permission) ) checked @endif value="1" name="news-{{ $cat->id }}-*-status"/> Bài của người khác</label>
										</td>
										<td>
											<label><input type="checkbox" @if( in_array('news-'.$cat->id.'-'.$news->id.'-delete', $news->permission) ) checked @endif value="1" name="news-{{ $cat->id }}-{{ $news->id }}-delete"/> Bài của mình</label><br/>
											<label><input type="checkbox" @if( in_array('news-'.$cat->id.'-*-delete', $news->permission) ) checked @endif value="1" name="news-{{ $cat->id }}-*-delete"/> Bài của người khác</label>
										</td>
									</tr>
								@endforeach
								<tr>
									<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tags</td>
									<td>
										<label><input type="checkbox" @if( in_array('tags-view', $news->permission) ) checked @endif value="1" name="tags-view"/> Xem Tags</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('tags-edit', $news->permission) ) checked @endif value="1" name="tags-edit"/> Thêm, Sửa Tags</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('tags-status', $news->permission) ) checked @endif value="1" name="tags-status"/> Sửa Trạng thái Tags</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('tags-delete', $news->permission) ) checked @endif value="1" name="tags-delete"/> Xóa Tags</label>
									</td>
								</tr>
								<tr>
									<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Danh mục</td>
									<td>
										<label><input type="checkbox" @if( in_array('newscategory-view', $news->permission) ) checked @endif value="1" name="newscategory-view"/> Xem Danh mục</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('newscategory-edit', $news->permission) ) checked @endif value="1" name="newscategory-edit"/> Thêm, Sửa Danh mục</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('newscategory-status', $news->permission) ) checked @endif value="1" name="newscategory-status"/> Sửa Trạng thái Danh mục</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('newscategory-delete', $news->permission) ) checked @endif value="1" name="newscategory-delete"/> Xóa Danh mục</label>
									</td>
								</tr>
								<tr>
									<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Trang thông tin</td>
									<td>
										<label><input type="checkbox" @if( in_array('pages-view', $news->permission) ) checked @endif value="1" name="pages-view"/> Xem Trang tin</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('pages-edit', $news->permission) ) checked @endif value="1" name="pages-edit"/> Thêm, Sửa Trang tin</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('pages-status', $news->permission) ) checked @endif value="1" name="pages-status"/> Sửa Trạng thái Trang tin</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('pages-delete', $news->permission) ) checked @endif value="1" name="pages-delete"/> Xóa Trang tin</label>
									</td>
								</tr>
								<tr>
									<td><b>Thành viên</b></td>
									<td>
										<label><input type="checkbox" @if( in_array('user-view', $news->permission) ) checked @endif value="1" name="user-view"/> Xem</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('user-edit', $news->permission) ) checked @endif value="1" name="user-edit"/> Thêm, sửa</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('user-status', $news->permission) ) checked @endif value="1" name="user-status"/> Sửa Trạng thái</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('user-delete', $news->permission) ) checked @endif value="1" name="user-delete"/> Xóa</label>
									</td>
								</tr>
								<tr class="hidden">
									<td><b>Quản lý Banner</b></td>
									<td>
										<label><input type="checkbox" @if( in_array('banners-view', $news->permission) ) checked @endif value="1" name="banners-view"/> Xem Menu Banner</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('banners-edit', $news->permission) ) checked @endif value="1" name="banners-edit"/> Thêm, Sửa Banner</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('banners-status', $news->permission) ) checked @endif value="1" name="banners-status"/> Sửa Trạng thái Banner</label>
									</td>
									<td>
										<label><input type="checkbox" @if( in_array('banners-delete', $news->permission) ) checked @endif value="1" name="banners-delete"/> Xóa Banner</label>
									</td>
								</tr>
								<tr class="hidden">
									<td><b>Quản lý Liên hệ</b></td>
									<td>
										<label><input type="checkbox" @if( in_array('contact-view', $news->permission) ) checked @endif value="1" name="contact-view"/> Xem Menu Liên hệ</label>
									</td>
									<td>

									</td>
									<td>

									</td>
									<td>
										<label><input type="checkbox" @if( in_array('contact-delete', $news->permission) ) checked @endif value="1" name="contact-delete"/> Xóa Liên hệ</label>
									</td>
								</tr>

								</tbody>
							</table>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Họ và tên</label>
						<div class="col-sm-10">
							{!! Form::text('name', $news->name, array('class' => 'form-control')) !!}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							{!! Form::text('email', $news->email, array('class' => 'form-control')) !!}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Điện thoại</label>
						<div class="col-sm-10">
							{!! Form::text('mobile', $news->mobile, array('class' => 'form-control')) !!}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">FB ID</label>
						<div class="col-sm-10">
							{!! Form::text('facebook_id', $news->facebook_id, array('class' => 'form-control')) !!}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Ảnh</label>
						<div class="col-sm-10">
							{!! Form::file('image_upload') !!}
							@if($news->avatar != '')
								<img src="<?php echo URL::to($news->avatar);?>" class="thumbnail" style="width: 80px; margin: 0px">
							@endif
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Trạng thái</label>
						<div class="col-sm-10">
							{!! Form::select('status', array(1=>'Hoạt động', 0=>'Banned'), $news->status, array('class'=>'form-control', 'style'=>'width:200px')) !!}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Mật khẩu</label>
						<div class="col-sm-10">
							<label><input type="checkbox" onchange="$('.password').toggleClass('hidden');"/> Tôi muốn đặt lại mật khẩu dưới đây:</label>
							<div class="hidden password">
								{!! Form::text('password_new', old('password_new'), array('class' => 'form-control', 'placeholder' => 'Nhập mật khẩu mới')) !!}
							</div>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<div class="col-sm-2"></div>
					<div class="col-sm-10">
						<button class="btn btn-primary" onclick="$('#action').val('save');" type="submit"><i class="fa fa-check-circle"></i>   Lưu & Quay lại</button>
						<button class="btn btn-primary" onclick="$('#action').val('apply');" type="submit"><i class="fa fa-save"></i> Lưu</button>
						<a class="btn btn-default" href="<?php echo URL::to('admin/user');?>">Quay lại</a>
					</div>
				</div>
				<!-- /.box-footer -->
				<input type="hidden" name="action" id="action" value="save">
				{!! Form::close() !!}
				<script>
					function showGroup(groupId){
						$('.group').addClass('hidden');
						$('.group' + groupId).removeClass('hidden');
					}
				</script>
			</div>
		</div>
	</div>

@stop