@extends('admin.layouts.admin')

@section('breadcrum')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Quản lý Thành viên
		<small>Thành viên</small>
	</h1>
	<ol class="breadcrumb hidden">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header hidden">
				<h3 class="box-title">Data Table With Full Features</h3>
			</div>
			<!-- /.box-header -->
			{!! Form::open( ['url' => 'admin/user', 'method' => 'get'] ) !!}
			<div class="box-body">
				<div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
					<div class="row">
						<div class="col-sm-8">
							<div id="example1_filter" class="dataTables_filter" style="text-align:left">
								{!! Form::select('group', $groups, $group, array('class'=>'form-control', 'onchange' => 'this.form.submit()')) !!}
								<label><input type="search" class="form-control input-sm" value="{{ $keyword }}" placeholder="Từ khóa tìm kiếm" name="keyword"></label>
								<label><input type="search" class="form-control input-sm" value="{{ $promotion }}" placeholder="Mã khuyến mại" name="promotion"></label>
								<button class="btn btn-sm btn-primary"><i class="fa fa-fw fa-search"></i> Tìm kiếm</button>
							</div>
						</div>
						<div class="col-sm-4 text-right">
							<a class="btn btn-primary" href="<?php echo URL::to('/admin/user?action=export');?>"><i class="fa fa-cloud-download"></i> Excel</a>
							<a class="btn btn-success" href="<?php echo URL::to('/admin/user/create');?>"><i class="fa fa-plus-circle"></i> Thêm mới</a>
							<a class="btn btn-success hidden" href="#" onclick="update_avatar_fb(this); return false;"><i class="fa fa-chain"></i> Update avatar FB</a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-bordered table-striped dataTable" id="tableUser" role="grid" aria-describedby="example1_info">
								<thead>
								<tr role="row" class="text-center">
									<th class="text-center" style="padding-right: 10px;">STT</th>
									<th class="sorting_asc1" aria-sort="ascending">Họ và tên</th>
									<th class="sorting1 text-center">Phân quyền</th>
									<th class="sorting1 text-center">Avatar</th>
									<th class="sorting1 text-center">Ngày đăng ký</th>
									<th class="sorting1 text-center">Trạng thái</th>
									<th class="sorting1 text-center">ID</th>
									<th class="sorting1 text-center">Xử lý</th>
								</tr>
								</thead>
								<tbody>
								@foreach( $news as $k => $item )
									<tr role="row" class="even" data-userId="{{ $item->id }}">
										<td class="text-center">{{$k+1}}</td>
										<td>
											<a href="<?php echo URL::to('/admin/user/'.$item->id);?>/edit">{{$item->name}}</a>
											@if($item->mobile) <br/>{{$item->mobile}} @endif
											@if($item->email) <br/>{{$item->email}} @endif
											@if($item->facebook_id)
												<br/><a href="{{ $item->facebook }}" target="_blank"><i class="btn btn-xs btn-primary fa fa-facebook"></i> {{ $item->facebook_id }}</a>
											@endif
										</td>
										<td>
											{{ $groups[$item->group] }}<br/>
										</td>
										<td>
											@if($item->avatar)
												<img src="<?php echo URL::to($item->avatar);?>" class="thumbnail" style="width: 50px; margin: 0px">
											@endif
										</td>
										<td class="text-center">
											{{ \App\Helper\StringHelper::date($item->created_at)  }}<br/>
											@if($item->promotion)
												Mã khuyến mại: <b>{{ $item->promotion }}</b>
											@endif
										</td>
										<td class="text-center">
											@if($item->status)
												<a href="#" class="btn btn-xs btn-success">Hoạt động</a>
											@else
												<a href="#" class="btn btn-xs btn-danger">Banned</a>
											@endif
										</td>
										<td class="text-center">{{$item->id}}</td>
										<td class="text-center">
											<a class="btn btn-success btn-xs" href="<?php echo URL::to('/admin/user/'.$item->id);?>/edit"><i class="fa fa-pencil"></i> Sửa</a>
											@if( Auth::user()->id != $item->id )
											<a onclick="return deleteItem({{$item->id}});" class="btn btn-danger btn-xs" href="#"><i class="fa fa-trash"></i> Xóa</a>
											
											<div class="btn-group" style="background: #e4e3e3;">
                                            <a target="_blank" href="/users/{{ $item->id }}/incognito" class="btn-info btn btn-xs" data-toggle="tooltip" title="Đăng nhập với tư cách là thành viên này!">
                                                <i class="fa fa-sign-in"></i> Login
                                            </a>
                                            <a href="/users/incognito/logout" data-toggle="modal" class="btn-info btn btn-xs" data-toggle="tooltip" title="Thoát tài khoản sử dụng thành viên này">
                                                <i class="fa fa-trash "></i> Logout
                                            </a>
                                        </div>
											@endif
										</td>
									</tr>
								@endforeach
								</tbody>
								<tfoot class="hidden">
								<tr>
									<th rowspan="1" colspan="1">Rendering engine</th>
									<th rowspan="1" colspan="1">Browser</th>
									<th rowspan="1" colspan="1">Platform(s)</th>
									<th rowspan="1" colspan="1">Engine version</th>
									<th rowspan="1" colspan="1">CSS grade</th>
								</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="row"><br/>
						<div class="col-sm-5">
							<div class="dataTables_length" id="example1_length">
								<label> Hiển thị
									<select onchange="this.form.submit();" name="limit" class="form-control input-sm">
										<option value="10">10</option>
										<option value="25">25</option>
										<option value="50">50</option>
										<option value="100">100</option>
									</select> mục
								</label>
							</div>
							<div class="dataTables_info hidden" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
						</div>
						<div class="col-sm-7">
							<div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
								{!! $news->appends(array('keyword' => app('request')->input('keyword')))->links() !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		{!! Form::close() !!}
			<!-- /.box-body -->
		</div>
	</div>
</div>

<div class="hidden">
	<form action="{{ URL::route('user.destroy', '') }}" id="form_delete" method="POST">
		<input type="hidden" name="_method" value="DELETE">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
	</form>
</div>
<script type="text/javascript">
	function deleteItem(id){
		if ( confirm('Bạn có chắc chắn muốn xóa?') ) {
			$('#form_delete').attr('action', $('#form_delete').attr('action') + "/" + id);
			$('#form_delete').submit();
		}
		return false;
	}

    function update_avatar_fb(obj) {
        if ( confirm('Bạn có chắc chắn muốn cập nhật lại toàn bộ avatar cho user từ facebook (chỉ trường hợp ko tìm thấy ảnh trên server mới update)?') ) {
            $(obj).text('Đang cập nhật ...');
            var limit = $('select[name="limit"]').val();
            var submits_list_id = [];
            $('.dataTable tr[class="even"]').each(function(index, item){
                submits_list_id.push($(item).attr('data-userId'));
            });

            $.ajax({
                url: '/admin/user/update-fb-avatar',
                type: 'post',
                data: {users: submits_list_id},
                success: function(result) {
                    if(result.status == 1) {
                        alert('Cập nhật avatar user từ Facebook thành công');
                    } else {
                        alert('Cập nhật bị lỗi, xem log chi tiết lỗi kết quả trả về');
                    }

                    $(obj).text('Cập nhật img-submit');
                }
            });
        }
        return false;
    }
</script>
@stop