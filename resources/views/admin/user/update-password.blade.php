@extends('admin.layouts.admin')

@section('breadcrum')
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Thay đổi mật khẩu
			<small>Thành viên</small>
		</h1>
		<ol class="breadcrumb hidden">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Thay đổi mật khẩu</li>
		</ol>
	</section>
	<style>
		.table label { font-weight: normal; }
	</style>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Cập nhật mật khẩu mới</h3>
				</div>
  
				<div class="box-body">
					<form method="POST" name="" class="form-horizontal" action="">
						<input type="hidden" name="_token" value="{!! csrf_token() !!}">
						<div class="form-group">
	                        <label for="" class="col-sm-3 control-label required">Mật khẩu cũ</label>
	                        <div class="col-sm-9">
	                            {!! Form::password('password_old', array('class' => 'form-control', 'required')) !!}
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <label for="" class="col-sm-3 control-label required">Nhập mật khẩu mới</label>
	                        <div class="col-sm-9">
	                            {!! Form::password('password_new', array('class' => 'form-control', 'required')) !!}
	                        </div>        
	                    </div>
	                    <div class="form-group">
	                        <label for="" class="col-sm-3 control-label required">Nhắc lại mật khẩu mới</label>
	                        <div class="col-sm-9">
	                            {!! Form::password('password_new_retype', array('class' => 'form-control', 'required')) !!}
	                        </div>
	                    </div>
	                    <div class="form-group">
	                    	<label for="" class="col-sm-3 control-label required"></label>
	                    	<div class="col-sm-9">
	                        	<label><input checked="" type="checkbox" name="agree_change_password"/> Xác nhận thay đổi mật khẩu</label>
	                    	</div>
	                    </div>
	                    <div class="form-group">
	                        <label for="" class="col-sm-3 control-label"></label>
	                        <div class="col-sm-9">
	                            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-send"></i> Hoàn tất</button>
	                        </div>
	                    </div>
                    </form>
				</div>
			</div>
		</div>
	</div>

@stop