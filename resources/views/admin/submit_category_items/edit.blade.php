@extends('admin.layouts.admin')

@section('breadcrum')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Quản lý Bài dự thi
		<small>Sửa</small>
	</h1>
	<ol class="breadcrumb hidden">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Thêm / Sửa thông tin</h3>
			</div>
			<!-- /.box-header -->

			<!-- form start -->
			{!! Form::open( ['url' => "admin/{$routeName}/{$item->id}", 'method' => 'PATCH', 'class' => 'form-horizontal', 'name'=>'uploadform', 'files'=>true] ) !!}
			<div class="box-body" style="padding: 5px;">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#vn" aria-controls="vn" role="tab" data-toggle="tab">Tiếng Việt</a></li>
					<li role="presentation" class="hidden"><a href="#en" aria-controls="en" role="tab" data-toggle="en">Tiếng Anh</a></li>
					<li role="presentation" class=""><a href="#seo" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li>
				</ul>

				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="vn">
						<div class="form-group">
							<label class="col-sm-2 control-label">Danh mục cha</label>
							<div class="col-sm-10">
								{!! Form::select('cat_id', $parents, $item->cat_id, ['class' => 'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Tiêu đề</label>
							<div class="col-sm-10">
								{!! Form::text('title', $item->title, array('class' => 'form-control')) !!}
								{!! Form::hidden('alias', $item->alias, array('class' => 'form-control')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Ảnh đại diện</label>
							<div class="col-sm-10">
								{!! Form::hidden('image', $item->image, array('class' => 'form-controls image_crop')) !!}
								<a onclick="aspectRatio = [600,600]; insertTo='.image_crop';" data-toggle="modal" data-target="#modalEditorImage" href="#" class="btn btn-xs btn-default">Chọn File...</a>
								<br/><img class="image_crop_preview" src="{{ $item->image ? url($item->image) : url('assets/img/no-image.jpg') }}" style="height: 50px;"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="inputPassword3">Mô tả ngắn</label>
							<div class="col-sm-10">
								{!! Form::textarea('introtext', $item->introtext, array('class' => 'form-control', 'id' => 'editors', 'rows' => 5)) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="inputPassword3">Mô tả đầy đủ</label>
							<div class="col-sm-10">
								{!! Form::textarea('fulltext', $item->fulltext, array('class' => 'form-control', 'id' => 'editors', 'rows' => 5)) !!}
							</div>
						</div>
					</div>

					<div role="tabpanel" class="tab-pane hidden" id="en">
						<div class="form-group">
							<label class="col-sm-2 control-label">Tiêu đề</label>
							<div class="col-sm-10">
								{!! Form::text('title_en', $item->title_en, array('class' => 'form-control')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="inputPassword3">Mô tả đầy đủ</label>
							<div class="col-sm-10">
								{!! Form::textarea('fulltext_en', $item->fulltext_en, array('class' => 'form-control', 'id' => 'editor2', 'rows' => 3)) !!}
							</div>
						</div>
					</div>

					<div role="tabpanel" class="tab-pane" id="seo">
						<div class="form-group">
							<label class="col-sm-2 control-label">Tiêu đề</label>
							<div class="col-sm-10">
								{!! Form::text('seo_title', $item->seo_title, array('class' => 'form-control')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="">Mô tả trang</label>
							<div class="col-sm-10">
								{!! Form::textarea('seo_keyword', $item->seo_keyword, array('class' => 'form-control', 'rows' => 3)) !!}
							</div>
						</div>
					</div>

				</div>
			</div>

			<!-- /.box-body -->
			<div class="box-footer">
				<div class="col-sm-2"></div>
				<div class="col-sm-10">
					<input type="hidden" name="_action" value="save" id="_action"/>
					<button class="btn btn-success" type="submit" onclick="$('#_action').val('save');"><i class="fa fa-check-circle"></i> Lưu & Quay lại</button>
					<button class="btn btn-primary" type="submit" onclick="$('#_action').val('apply');"><i class="fa fa-save"></i> Lưu</button>
					<a class="btn btn-default" href="<?php echo URL::to('admin/'.$routeName);?>">Quay lại</a>
				</div>
			</div>
			{!! Form::close() !!}
			<!-- /.box-footer -->
			<script type="text/javascript">
				$('.nav-tabs a').click(function (e) {
					e.preventDefault();
					$(this).tab('show');
				})
			</script>
		</div>
	</div>
</div>

@stop