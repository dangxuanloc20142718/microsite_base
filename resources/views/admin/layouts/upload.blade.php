<script type="text/javascript">
    //configuration
    var max_file_size 			= 2048576; //allowed file size. (1 MB = 1048576)
    var allowed_file_types 		= ['image/png', 'image/gif', 'image/jpeg', 'image/pjpeg']; //allowed file types
    var result_output 			= '#output'; //ID of an element for response output
    var my_form_id 				= jQuery('.uploadformfooter'); //ID of an element for response output
    var total_files_allowed 	= 1; //Number files allowed to upload
    var progress_bar_id 		= '#progress-wrp'; //ID of an element for response output

    //on form submit
    $('.btnupload').on( "click", function(event) {
        event.preventDefault();
        var proceed = true; //set proceed flag
        var error = [];	//errors
        var total_files_size = 0;

        if(!window.File && window.FileReader && window.FileList && window.Blob){ //if browser doesn't supports File API
            error.push("Your browser does not support new File API! Please upgrade."); //push error text
        }else{
            var total_selected_files = $('#upload').length; //number of files

            //limit number of files allowed
            if(total_selected_files > total_files_allowed){
                error.push( "You have selected "+total_selected_files+" file(s), " + total_files_allowed +" is maximum!"); //push error text
                proceed = false; //set proceed flag to false
            }
            //iterate files in file input field
            //$('#upload').each(function(i, ifile){
            if(document.uploadformfooter.upload.value !== ""){ //continue only if file(s) are selected

                if(allowed_file_types.indexOf(document.uploadformfooter.upload.type) === -1){ //check unsupported file
                    //error.push( "<b>"+ document.uploadform.upload.value + "</b> is unsupported file type!"); //push error text
                    //proceed = false; //set proceed flag to false
                }

                total_files_size = total_files_size + document.uploadformfooter.upload.size; //add file size to total size
            }else{
                $(result_output).html('<div class="alert alert-danger">Vui lòng chọn File trước</div>');
                return;
            }
            //});

            //if total file size is greater than max file size
            if(total_files_size > max_file_size){
                error.push( "You have "+total_selected_files+" file(s) with total size "+bytesToSize(total_files_size)+", Allowed size is " + bytesToSize(max_file_size) +", Try smaller file!"); //push error text
                proceed = false; //set proceed flag to false
            }

            var submit_btn  = $('.btnupload'); //form submit button

            //if everything looks good, proceed with jQuery Ajax
            if(proceed){
                submit_btn.val("Please Wait...").prop( "disabled", true); //disable submit button
                var form_data = new FormData( document.uploadformfooter ); //Creates new FormData object
                var post_url = "{{ url('upload?upType=media') }}"; //get action URL of form

                //jQuery Ajax to Post form data
                $.ajax({
                    url : post_url,
                    method: "POST",
                    type: "POST",
                    data : form_data,
                    contentType: false,
                    dataType: 'json',
                    cache: false,
                    processData:false,
                    xhr: function(){
                        //upload Progress
                        var xhr = $.ajaxSettings.xhr();
                        if (xhr.upload) {
                            xhr.upload.addEventListener('progress', function(event) {
                                var percent = 0;
                                var position = event.loaded || event.position;
                                var total = event.total;
                                if (event.lengthComputable) {
                                    percent = Math.ceil(position / total * 100);
                                }
                                //update progressbar
                                $(progress_bar_id).show();
                                $(progress_bar_id +" .progress-bar").css("width", + percent +"%");
                                $(progress_bar_id + " .status").text(percent +"%");
                            }, true);
                        }
                        return xhr;
                    },
                    mimeType:"multipart/form-data"
                }).done(function(res){ //
                    //$(my_form_id)[0].reset(); //reset form
                    if(res.code) {

                        //CKEDITOR.instances['editor1'].insertHtml('<img src="{{ url('/') }}/' + res.link + '" />');

                        $(result_output).html(''); //output response from server
                        var img_thumbnail = res.link;
                        if(res.type == 'video') {
                            img_thumbnail = 'assets/img/movie-clip-icon.png';
                        } else if(res.type == 'document') {
                            img_thumbnail = 'assets/img/document-img.png';
                        }
                        $('#mediaModal .w-multi-img').prepend('<div class="thumbnail"><img alt="'+res.title+'" data-intro="'+res.title+'" title="'+res.title+'" data-link="'+ res.link +'" data-type="'+ res.type +'" data-id="'+res.id+'" src="/' + img_thumbnail + '" /></div>');
                        submit_btn.val("Upload").prop("disabled", false); //enable submit button once ajax is done
                        $(progress_bar_id).hide();
                    }else{
                        $(result_output).html( '<div class="alert alert-danger">Lỗi khi upload, vui lòng thử lại: '+ res.message +'</div>' );
                        submit_btn.val("Upload").prop("disabled", false); //enable submit button once ajax is done
                        $(progress_bar_id).hide();
                    }

                }).error(function(res){
                    $(result_output).html('<div class="alert alert-danger">Lỗi khi upload, vui lòng thử lại</div>');
                    submit_btn.val("Upload").prop("disabled", false);
                    $(progress_bar_id).hide();
                });
            }
        }

        document.uploadformfooter.upload.value = '';

        $(result_output).html(""); //reset output
        $(error).each(function(i){ //output any error to output element
            $(result_output).append('<div class="error">'+error[i]+"</div>");
        });

        //function to format bites bit.ly/19yoIPO
        function bytesToSize(bytes) {
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0) return '0 Bytes';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        }
    });
</script>
<style>
    #progress-wrp {
        background: #fff none repeat scroll 0 0;
        border: 1px solid #0099cc;
        border-radius: 3px;
        box-shadow: 1px 3px 6px rgba(0, 0, 0, 0.12) inset;
        margin: 5px 0px;
        padding: 1px;
        position: relative;
        text-align: left;
        width: 100%;
        display: none;
    }
    #progress-wrp .progress-bar {
        background-color: #62a60a;
        border-radius: 3px;
        box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.11) inset;
        height: 20px;
        width: 0;
    }
    #progress-wrp .status {
        color: #000000;
        display: inline-block;
        left: 40%;
        position: absolute;
        top: 3px;
    }
</style>