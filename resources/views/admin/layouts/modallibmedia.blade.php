<style>
    .modal-header {
        height: 80px !important;
    }
   .wrap-upload > form  {
       margin-bottom: 0;
   }
</style>

<div class="modal fade" id="mediaModal" tabindex="-1" role="dialog" style="z-index: 999999;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Media</h4>
                <div class="wrap-upload">
                    <form name="uploadformfooter" enctype="multipart/form-data" action="{{ url('/upload?upType=media') }}" accept-charset="UTF-8" method="POST">
                        <span style="line-height: 25px;">Chọn file upload - Kích thước:
                            <select name="resize" id="resize" class="">
                                <option value="1">Giữ nguyên</option>
                                <option value="400">400px</option>
                                <option value="500">500px</option>
                                <option value="600">600px</option>
                                <option value="700">700px</option>
                                <option value="800" selected>800px</option>
                                <option value="900">900px</option>
                                <option value="1000">1000px</option>
                                <option value="1200">1200px</option>
                            </select>
                        </span>
                        <span><input type="checkbox" name="chk_watermark" value="true"> Đóng watermark </span>
                        <a href="#" onclick="$('#upload').trigger('click'); return false;" class="btn btn-xs btn-success"><i class="fa fa-cloud-upload"></i> Up file...</a>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for=""></label>
                            <div class="col-sm-10">
                                <table cellpadding="5" cellspacing="5">
                                    <tr valign="top">
                                        <td>
                                            <input type="file" class="hidden" name="upload-file" onchange="$('.btnupload').trigger('click');" class="pull-left" id="upload"><br/>
                                        </td>
                                        <td>
                                            <button class="btn pull-left btn-xs btn-primary btnupload hidden"><i class="fa fa-cloud-upload"></i> Upload & Chèn vào bài viết</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </form>
                </div> <!-- /end .wrap-upload -->
            </div> <!-- /end .modal-header -->
            <div class="modal-body">
                <div class="w-full container-fluid">
                    <div class="row">
                        <div class="w-left w-multi-img col-xs-12 col-md-8">

                        </div>

                        <div class="w-right w-pre-img col-xs-12 col-md-4">
                            <div class="preview-img">
                                <img src="/assets/img/no-image-1.png" alt="image-preview" title="image preview" class="">
                            </div>
                            <div class="w-detail-img">
                                <form class="form-horizontal" type="POST" action="">
                                    <div class="form-group hidden">
                                        <label class="control-label col-xs-12 col-md-3">Tên file</label>
                                        <div class="col-xs-12 col-md-9">
                                            <input class="form-control" type="text" name="title" placeholder="" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-12 col-md-3">URL</label>
                                        <div class="col-xs-12 col-md-9">
                                            <input class="form-control" type="text" name="url" placeholder="" value="" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-12 col-md-3">Mô tả</label>
                                        <div class="col-xs-12 col-md-9">
                                            <textarea name="introtext" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group text-right">
                                        <button class="btn btn-success btn-sm" name="btn-insert-file" id="btn_insert_file" onclick="return false;">Sử dụng File này</button>
                                        <button class="btn btn-danger btn-sm" name="btn-delete-file" id="btn_delete_file" onclick="return false;">Xoá</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- /end .modal-body -->
            <div class="clearfix"></div>
            <div class="modal-footer">
                <div class="pull-left" style="width: 220px;">
                    <div id="output"></div>
                    <div id="progress-wrp" class="pull-left">
                        <div class="progress-bar"></div>
                        <div class="status">0%</div>
                    </div>
                </div>
                <button type="button" class="btn btn-primary hidden" data-dismiss="modal">Chọn File</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    var insertTo = '';
    $(function () {
        $('#btn_insert_file').on('click', function () {
            var selected_thumb = $('.thumbnail.active:not(".thumbnail_upload")');
            if(selected_thumb.length <= 0) {
                // Chưa có file nào được chọn
                // alert();
                return false;
            }
            if (selected_thumb.length > 0) {
                var src = selected_thumb.find('img').attr('src');
                var alt = selected_thumb.find('img').attr('alt');
                var id_media = selected_thumb.find('img').attr('data-id');
                var introtext = $('.w-detail-img > form textarea[name="introtext"]').val();
                var link = selected_thumb.find('img').attr('data-link');
                var type_media = selected_thumb.find('img').attr('data-type');
                var html_insert_to = '<figure><img src="'+ src +'" alt="'+ alt +'"><figcaption>'+ introtext +'</figcaption></figure><br/>';
                if(type_media == 'document') {

                    var data_link = selected_thumb.find('img').attr('data-link');
                    html_insert_to = '<p><a href="' + data_link + '" target="_blank">' + introtext + '</a></p><br/>';
                } else if(type_media == 'video') {
                    var data_link = selected_thumb.find('img').attr('data-link');
                    html_insert_to = '<div class="center"><figure><video width="480" height="360" controls>' +
                        '<source src="'+ data_link +'" type="video/mp4">' +
                        '<source src="'+ data_link +'" type="video/ogg">' +
                        '<source src="'+ data_link +'" type="video/webm">' +
                        'Your browser does not support the video tag.</video><figcaption>'+ introtext +'</figcaption></figure></div><br/>';
                }

                if (insertTo.indexOf('gallery') >= 0) {
                    $(insertTo).prepend('<div class="item" style="width: 150px; height: 150px; overflow: hidden; float: left; margin-right: 10px; position:relative;">' +
                        '<img style="width: 100%; margin-bottom: 0px; height: 100px;" class="thumbnail" alt="' + alt + '" src="' + src + '" />' +
                        '<textarea style="width: 100%;" rows="2" name="gallery[' + src + ']">'+ introtext +'</textarea>' +
                        '<a onclick="$(this).parent().prev(\'.item\').before($(this).parent()); return false;" class="btn btn-default btn-xs pull-left" href="#" style="position: absolute; top: 0; right: 60px;"><i class="fa fa-chevron-left"></i></a>' +
                        '<a onclick="$(this).parent().next(\'.item\').after($(this).parent()); return false;" class="btn btn-default btn-xs pull-left" href="#" style="position: absolute; top: 0; right: 40px;"><i class="fa fa-chevron-right"></i></a>' +
                        '<a onclick="if(!confirm(\'Bạn có chắc muốn xóa ảnh?\')) return false; $(this).parent().remove(); return false;" class="btn btn-danger btn-xs pull-left" href="#" style="position: absolute; top: 0; right: 0;">Xóa</a>' +
                        '</div>');
                }
                else
                if (insertTo.indexOf('video_') >= 0) {
                    if(type_media == 'video') {
                        $(insertTo).val(src);
                        $(insertTo + '_preview source').attr('src', src);
                        $(insertTo + '_preview')[0].load();
                        $(insertTo + '_remove').removeClass('hidden');
                        $('.preview-video source').attr('src', link);
                    } else {
                        alert('Bạn phải chọn file video!');
                        return false;
                    }
                }
                else
                if (insertTo.indexOf('image_') >= 0) {
                    if(type_media == 'image') {
                        $(insertTo).val(src);
                        $(insertTo + '_preview').attr('src', src);
                        $(insertTo + '_remove').removeClass('hidden');
                    } else {
                        alert('Bạn phải chọn file ảnh!');
                        return false;
                    }
                }else
                if (insertTo.indexOf('file_') >= 0) {
                    $(insertTo).val(src);
                    $(insertTo + '_preview').attr('href', src).html(src);
                    $(insertTo + '_remove').removeClass('hidden');
                } else
                if (insertTo.indexOf('editor') >= 0) {
                    CKEDITOR.instances[insertTo].insertHtml(html_insert_to);
                }

                // Request ajax
                $.ajax({
                    url: 'admin/media/' + id_media,
                    method: 'PUT',
                    data: {
                        'introtext': introtext
                    },
                    success: function(result) {
                        if(result.code == 1) {
                            // Message alert call ajax success
                        }
                    }
                });

                insertTo = '';
                $('#mediaModal').modal('toggle');
                return false;
            }
        });

        $('#btn_delete_file').on('click', function () {
            var selected_thumb = $('.thumbnail.active:not(".thumbnail_upload")');
            if(selected_thumb.length <= 0) {
               return false;
            }
            if (confirm('Bạn có chắc chắn muốn xóa ảnh này?')) {
                var link = selected_thumb.find('img').attr('data-link');
                var id = selected_thumb.find('img').attr('data-id');
                $.ajax({
                    method: "POST",
                    type: "POST",
                    url: site_front + 'upload',
                    data: {'action': 'delete', 'file': link, 'id': id},
                    success: function (res) {
                        if (res.code > 0) {
                            selected_thumb.remove();
                            // Reset form field preview
                            var $form = $('.w-detail-img > form');
                            $('.preview-img > img').attr('src', '/assets/img/no-image-1.png');
                            $form.find('input[name="title"]').val('');
                            $form.find('input[name="url"]').val('');
                            $form.find('textarea[name="introtext"]').val('');
                        } else {
                            alert(res.msg);
                        }
                    },
                    error: function (data) {

                    }
                });
            }
        });

        $('#mediaModal').on('click', '.thumbnail:not(".thumbnail_upload")', function () {
            var $img = $(this).find('> img'),
                $form = $('.w-detail-img > form'),
                url = $img.attr('src');

            $(this).parent().find('.active').removeClass('active');
            $(this).addClass('active');

            $('.preview-img > img').attr('src', url);
            var split_url = url.split('/');
            if($img.attr('data-type') == 'document' || $img.attr('data-type') == 'video') {
                url = $img.attr('data-link');
                split_url = url.split('/');
                $form.find('input[name="title"]').val(split_url[split_url.length-1]);
                $form.find('input[name="url"]').val(url);
            } else {
                $form.find('input[name="title"]').val(split_url[split_url.length-1]);
                $form.find('input[name="url"]').val(url);
            }
            $form.find('textarea[name="introtext"]').val($img.attr('title'));
        });

        var lastScrollTop = 80,
            indexPage = 1;
        $('.w-multi-img').scroll(function (event) {
            var st = $(this).scrollTop();
            if (st > lastScrollTop) {
                lastScrollTop = lastScrollTop + st;
                indexPage++;
                getMedia(indexPage);
            }

        });
    });
</script>