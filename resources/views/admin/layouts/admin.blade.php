<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="robots" content="noindex, nofollow">
	<link rel="shortcut icon" href="{{ url('media/system/favicon.ico?'.config('vc.version')) }}" />

	<title>Administrator</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?php echo URL::to('/assets/');?>/bootstrap/css/bootstrap3.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo URL::to('/assets/');?>/bootstrap/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo URL::to('/assets/');?>/bootstrap/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo URL::to('/assets/admin/adminlte/');?>/css/AdminLTE.min.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?php echo URL::to('/assets/admin/adminlte/');?>/css/skins/_all-skins.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/iCheck/flat/blue.css">
	<!-- Morris chart -->
	<link rel="stylesheet" href="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/morris/morris.css">
	<!-- jvectormap -->
	<link rel="stylesheet" href="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
	<!-- Date Picker -->
	<link rel="stylesheet" href="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/datepicker/datepicker3.css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/daterangepicker/daterangepicker.css">
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<link rel="stylesheet" href="<?php echo URL::to('/assets/admin/adminlte');?>/plugins/datatables/dataTables.bootstrap.css"/>

	<link rel="stylesheet" href="<?php echo URL::to('/assets/admin/');?>/admin.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<!-- jQuery 2.2.3 -->
	<script src="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/jQuery/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

	<header class="main-header">
		<!-- Logo -->
		<a href="<?php echo URL::to('/admin');?>" class="logo">
			<!-- mini logo for sidebar mini 50x50 pixels -->
			<span class="logo-mini"><b>A</b>LT</span>
			<!-- logo for regular state and mobile devices -->
			<span class="logo-lg"><b>Admin</b></span>
		</a>
		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top">
			<!-- Sidebar toggle button-->
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
			</a>

			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav ">
					<!-- Messages: style can be found in dropdown.less-->
					<li class="dropdown messages-menu hidden">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-envelope-o"></i>
							<span class="label label-success">4</span>
						</a>
						<ul class="dropdown-menu">
							<li class="header">You have 4 messages</li>
							<li>
								<!-- inner menu: contains the actual data -->
								<ul class="menu">
									<li><!-- start message -->
										<a href="#">
											<div class="pull-left">
												<img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
											</div>
											<h4>
												Support Team
												<small><i class="fa fa-clock-o"></i> 5 mins</small>
											</h4>
											<p>Why not buy a new awesome theme?</p>
										</a>
									</li>
									<!-- end message -->
									<li>
										<a href="#">
											<div class="pull-left">
												<img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
											</div>
											<h4>
												AdminLTE Design Team
												<small><i class="fa fa-clock-o"></i> 2 hours</small>
											</h4>
											<p>Why not buy a new awesome theme?</p>
										</a>
									</li>
								</ul>
							</li>
							<li class="footer"><a href="#">See All Messages</a></li>
						</ul>
					</li>
					<!-- Notifications: style can be found in dropdown.less -->
					<li class="dropdown notifications-menu hidden">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-bell-o"></i>
							<span class="label label-warning">10</span>
						</a>
						<ul class="dropdown-menu">
							<li class="header">You have 10 notifications</li>
							<li>
								<!-- inner menu: contains the actual data -->
								<ul class="menu">
									<li>
										<a href="#">
											<i class="fa fa-users text-aqua"></i> 5 new members joined today
										</a>
									</li>
								</ul>
							</li>
							<li class="footer"><a href="#">View all</a></li>
						</ul>
					</li>
					<!-- Tasks: style can be found in dropdown.less -->
					<li class="dropdown tasks-menu hidden">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-flag-o"></i>
							<span class="label label-danger">9</span>
						</a>
						<ul class="dropdown-menu">
							<li class="header">You have 9 tasks</li>
							<li>
								<!-- inner menu: contains the actual data -->
								<ul class="menu">
									<li><!-- Task item -->
										<a href="#">
											<h3>
												Design some buttons
												<small class="pull-right">20%</small>
											</h3>
											<div class="progress xs">
												<div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
													<span class="sr-only">20% Complete</span>
												</div>
											</div>
										</a>
									</li>
									<!-- end task item -->
								</ul>
							</li>
							<li class="footer">
								<a href="#">View all tasks</a>
							</li>
						</ul>
					</li>
					<!-- User Account: style can be found in dropdown.less -->
					<li class="dropdown user user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							{{ Html::image(Auth::user()->avatar, '', array('class' => 'user-image')) }}
							<span class="hidden-xs">Xin chào: {{ Auth::user()->name }} </span>
						</a>
						<ul class="dropdown-menu">
							<!-- User image -->
							<li class="user-header">

								{{ Html::image(Auth::user()->avatar, '', array('class img-circle' => 'user-image')) }}
								<p>
									{{ Auth::user()->name }}
									<small>{{ Auth::user()->created_at }}</small>
								</p>
							</li>
							<!-- Menu Body -->
							<li class="user-body hidden">
								<div class="row">
									<div class="col-xs-4 text-center">
										<a href="#">Followers</a>
									</div>
									<div class="col-xs-4 text-center">
										<a href="#">Sales</a>
									</div>
									<div class="col-xs-4 text-center">
										<a href="#">Friends</a>
									</div>
								</div>
								<!-- /.row -->
							</li>
							<!-- Menu Footer-->
							<li class="user-footer">
								<div class="pull-left">
									<a href="#" class="btn btn-default btn-flat hidden">Profile</a>
								</div>
								<div class="pull-left">
									<a href="{{ url('/admin/user/update-password/') }}" class="btn btn-danger btn-flat">Đổi mật khẩu</a>
								</div>
								<div class="pull-right">
									<a href="{{ url('/auth/logout/') }}" class="btn btn-danger btn-flat">Thoát</a>
								</div>
							</li>
						</ul>
					</li>
					<!-- Control Sidebar Toggle Button -->
					<li class="notifications-menu">
						<a href="{{ url('/') }}" target="_blank" ><i class="fa fa-external-link"> Web</i></a>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- Left side column. contains the logo and sidebar -->
	<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">

			<!-- search form -->
			<form action="#" method="get" class="sidebar-form hidden">
				<div class="input-group">
					<input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
				</div>
			</form>
			<!-- /.search form -->
			<!-- sidebar menu: : style can be found in sidebar.less -->

			<ul class="sidebar-menu">
				<li class="header hidden">MAIN NAVIGATION</li>
				<li class="treeview <?php if(Route::getCurrentRoute()->getName() == 'admin..index'): ?>active<?php endif;?>">
					<a href="<?php echo URL::to('/admin');?>">
						<i class="fa fa-dashboard"></i> <span>Trang chủ</span>
					</a>
				</li>

				@can('admin.manage', 'submit-view')
					<li class="treeview <?php if( strpos(Route::getCurrentRoute()->getName(), 'submit') !== false || strpos(Route::getCurrentRoute()->getName(), 'submit_category') !== false ): ?>active<?php endif;?>">
						<a href="#">
							<i class="fa fa-files-o"></i> <span>Quản lý Bài dự thi</span>
							<span class="pull-right-container">
						  <i class="fa fa-angle-right pull-right"></i>
						</span>
						</a>
						<ul class="treeview-menu">
							@can('admin.manage', 'submit-view')
								<li class=""><a href="<?php echo URL::to('/admin/submit_category_items');?>"><i class="fa fa-circle-o"></i> Bài Dự thi</a></li>@endcan
							@can('admin.manage', 'submit-category-view')
								<li class="hidden"><a href="<?php echo URL::to('/admin/submit_category');?>"><i class="fa fa-circle-o"></i> Danh mục Bài dự thi</a></li>@endcan
						</ul>
					</li>
				@endcan

				@can('admin.manage', 'news-view')
					<li class="treeview <?php if( strpos(Route::getCurrentRoute()->getName(), 'news') !== false  ): ?>active<?php endif;?>">
						<a href="<?php echo URL::to('admin/news');?>"><i class="fa fa-newspaper-o"></i> Tin tức</a>
					</li>
				@endcan

				@can('admin.manage', 'video-view')
					<li class="treeview <?php if( strpos(Route::getCurrentRoute()->getName(), 'videos') !== false  ): ?>active<?php endif;?>">
						<a href="<?php echo URL::to('admin/videos');?>"><i class="fa fa-video-camera"></i> Video</a>
					</li>
				@endcan

				@can('admin.manage', 'kol-view')
					<li class="treeview <?php if( strpos(Route::getCurrentRoute()->getName(), 'kol') !== false  ): ?>active<?php endif;?>">
						<a href="<?php echo URL::to('admin/kol');?>"><i class="fa fa-star"></i> Đại sứ</a>
					</li>
				@endcan

				@can('admin.manage', 'contact-view')
					<li class="treeview <?php if( strpos(Route::getCurrentRoute()->getName(), 'contact') !== false  ): ?>active<?php endif;?>">
						<a href="<?php echo URL::to('admin/contact');?>"><i class="fa fa-users"></i> Liên hệ</a>
					</li>
				@endcan

				@can('admin.manage', 'user-view')
					<li class="treeview <?php if( strpos(Route::getCurrentRoute()->getName(), 'user') !== false  ): ?>active<?php endif;?>">
						<a href="<?php echo URL::to('admin/user');?>"><i class="fa fa-user"></i> Người dùng</a>
					</li>
				@endcan

				@can('admin.manage', 'user')
					<li class="treeview hidden <?php if( strpos(Route::getCurrentRoute()->getName(), 'menu') !== false || strpos(Route::getCurrentRoute()->getName(), 'config') !== false ): ?>active<?php endif;?>">
						<a href="#">
							<i class="fa fa-laptop"></i> <span>Quản lý Hệ thống</span>
						<span class="pull-right-container">
						  <i class="fa fa-angle-right pull-right"></i>
						</span>
						</a>
						<ul class="treeview-menu">
							<li class="hidden"><a href="<?php echo URL::to('admin/menu');?>"><i class="fa fa-list"></i> Menu</a></li>
							<li class=""><a href="<?php echo URL::to('admin/config/0/edit');?>"><i class="fa fa-cog"></i> Cấu hình hệ thống</a></li>
						</ul>
					</li>
				@endcan

			</ul>
		</section>
		<!-- /.sidebar -->
	</aside>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			@yield('breadcrum')

			<div class="row">
				<div class="col-md-12">
					@if(Session::has('message'))
						<br/>
						<p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					@if ( $errors->any() )
						<br/>
						<ul class="alert alert-danger">
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					@endif
				</div>
			</div>
		</section>

		<!-- Main content -->
		<section class="content">
			@yield('content')
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	<footer class="main-footer">
		<div class="pull-right hidden-xs">
			<b>Version</b> 1.0
		</div>
		<strong>Copyright &copy; 2008-2017 <a href="http://admicro.vn">Admicro.vn</a>.</strong> All rights reserved.
	</footer>

	<!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
	<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo URL::to('/assets/');?>/bootstrap/js/bootstrap3.min.js"></script>
<!-- Morris.js charts -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>-->
<!--<script src="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/morris/morris.min.js"></script>-->

<!-- Sparkline -->
<script src="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/knob/moment.min.js"></script>
<script src="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo URL::to('/assets/admin/adminlte/');?>/plugins/fastclick/fastclick.js"></script>

<script src="<?php echo URL::to('/assets/lib/edt/');?>/ckeditor.js"></script>
<script type="text/javascript">
	$(function () {

		// Replace the <textarea id="editor1"> with a CKEditor
		if($('#editor1').length) {
            CKEDITOR.replace('editor1').on('focus', function(){
                insertTo = 'editor1';
            });
		}

		if($('#editor2').length)
			CKEDITOR.replace('editor2').on('focus', function(){
                insertTo = 'editor2';
            });

		if($('#editor3').length)
			CKEDITOR.replace('editor3').on('focus', function(){
                insertTo = 'editor3';
            });

		if($('#editor4').length)
			CKEDITOR.replace('editor4').on('focus', function(){
                insertTo = 'editor4';
            });

	});
</script>

<!-- daterange picker -->
<script src="<?php echo URL::to('/assets/admin/adminlte');?>/plugins/moment.min.js"></script>
<link rel="stylesheet" href="<?php echo URL::to('/assets/admin/adminlte');?>/plugins/daterangepicker/daterangepicker.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?php echo URL::to('/assets/admin/adminlte');?>/plugins/datepicker/datepicker3.css">
<script src="<?php echo URL::to('/assets/admin/adminlte');?>/plugins/daterangepicker/daterangepicker.js"></script>

<!-- AdminLTE App -->
<script src="<?php echo URL::to('/assets/admin/adminlte/');?>/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="<?php echo URL::to('/assets/admin/adminlte/');?>/js/pages/dashboard.js"></script>-->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo URL::to('/assets/admin/adminlte/');?>/js/demo.js"></script>
<link rel="stylesheet" href="{{ url('assets/lib/cropper/cropper.css?'.config('custom.version')) }}"/>
<script src="{{ url('assets/lib/cropper/cropper.js?'.config('custom.version')) }}"></script>

<script type="text/javascript">
	var site_url = '{{ url('admin')  }}/';
	var site_front = '{{ url('/')  }}/';
	var token = '{!! csrf_token() !!}';

	$('#daterangepicker').daterangepicker({locale: {
		format: 'DD-MM-YYYY'
	}});
	$('.datepicker').datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd'
	});
</script>
{{ Html::script('assets/admin/admin.js?v=1.0') }}

@include('admin.layouts.modallibmedia')
@include('admin.layouts.upload')
@include('admin.layouts.modaluploadcrop')

</body>
</html>
