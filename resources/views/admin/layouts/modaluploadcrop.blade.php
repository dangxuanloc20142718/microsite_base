<div class="modal fade" id="modalEditorImage" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalLabel">Upload ảnh</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <input type="hidden" name="logo_value" value="" id="logo_value"/>
                        {!! Form::file('logo', ['id'=>'logo', 'style' => 'padding-left: 15px;padding-bottom: 15px;']) !!}
                        <div class="col-md-10 logo_crop text-center" style="max-height: 364px;">
                            <img id="logo_crop" src="{{ url('assets/img/no-image.jpg') }}">
                        </div>

                        <div class="col-md-2 logo-show not-file">
                            <img src="{{ url('assets/img/no-image.jpg') }}" id="logo_thumbnail" class="thumbnail" style="width: 100%; margin: 0">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary hidden" name="save" id="btn-image-crop">Cắt và lưu file ảnh</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        var dataImageCrop = '';
        //var aspectRatio = [800,600];
        $('#logo').on('change', function (e) {
            var file = this.files[0];
            var reader = new FileReader();
            reader.addEventListener("load", function () {
                $('.logo_crop, .logo-show, #btn-image-crop').removeClass('hidden');

                $('#logo_crop').attr("src", reader.result);

                $('#logo_crop').cropper('destroy');
                $('#logo_crop').cropper({
                    dragMode: 'move',
                    viewMode: 1,
                    aspectRatio: aspectRatio[0]/aspectRatio[1],
                    autoCropArea: 1,
                    crop: function (e) {
                        var imageData = $('#logo_crop').cropper('getCroppedCanvas',
                            {
                                width: aspectRatio[0],
                                height: aspectRatio[1],
                                fillColor: '#fff',
                                imageSmoothingEnabled: false,
                                imageSmoothingQuality: 'high'
                            }
                        ).toDataURL();
                        $('#logo_thumbnail').attr('src', imageData);
                        dataImageCrop = imageData;
                    }
                });
            }, false);

            if (file) {
                reader.readAsDataURL(file);
            }
        });

        $('#btn-image-crop').click(function () {
            if(dataImageCrop){
                $(insertTo).val(dataImageCrop);
                $(insertTo + '_preview').attr('src', dataImageCrop);
            }
            $('#modalEditorImage').modal('hide');
        });
    });
</script>

