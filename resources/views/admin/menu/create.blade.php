@extends('admin.layouts.admin')

@section('breadcrum')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Quản lý Menu
		<small>Menu</small>
	</h1>
	<ol class="breadcrumb hidden">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Thêm / Sửa thông tin</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			{!! Form::open( ['url' => 'admin/menu', 'class' => 'form-horizontal', 'name'=>'uploadform', 'files'=>true] ) !!}
				<div class="box-body">
					<div style="padding: 5px;">
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#vn" aria-controls="vn" role="tab" data-toggle="tab">Nội dung</a></li>
							<li role="presentation" class=""><a href="#en" aria-controls="en" role="tab" data-toggle="en">Tiếng Anh</a></li>
							<li role="presentation" class=""><a href="#seo" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li>
						</ul>

						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="vn">
								<div class="form-group">
									<label class="col-sm-2 control-label">Danh mục cha</label>
									<div class="col-sm-10">
										{!! Form::select('parent_id', $parents, '') !!}
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Tiêu đề</label>
									<div class="col-sm-10">
										{!! Form::text('title', '', array('class' => 'form-control')) !!}
										{!! Form::hidden('alias', '', array('class' => 'form-control')) !!}
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Liên kết đến</label>
									<div class="col-sm-10">
										{!! Form::select('module', [1=>'Danh mục Bài viết', 2=>'Danh mục Sản phẩm', 3=>'Trang Thông tin', 4=>'Trang Liên hệ', 5=>'Link tự nhập'], 1, ['onchange'=>'changeModule(this.value);']) !!}
									</div>
								</div>
								<div class="form-group modules module-1">
									<label class="col-sm-2 control-label">Chọn Danh mục Bài viết</label>
									<div class="col-sm-10">
										{!! Form::select('newscat', $newscat, '') !!}
									</div>
								</div>
								<div class="form-group modules module-2 hidden">
									<label class="col-sm-2 control-label">Chọn Danh mục Sản phẩm</label>
									<div class="col-sm-10">
										{!! Form::select('procat', $procat, '') !!}
									</div>
								</div>
								<div class="form-group modules module-3 hidden">
									<label class="col-sm-2 control-label">Chọn Trang thông tin</label>
									<div class="col-sm-10">
										{!! Form::select('pages', $pages, '') !!}
									</div>
								</div>

								<div class="form-group modules module-5 hidden">
									<label class="col-sm-2 control-label">Link</label>
									<div class="col-sm-10">
										{!! Form::text('alias', '', array('class' => 'form-control input-alias')) !!}
									</div>
								</div>
								<div class="form-group hidden">
									<label class="col-sm-2 control-label">Ảnh Banner</label>
									<div class="col-sm-10">
										{!! Form::file('image_upload') !!}
									</div>
								</div>
								<div class="form-group hidden">
									<label class="col-sm-2 control-label" for="inputPassword3">Mô tả đầy đủ</label>
									<div class="col-sm-10">
										{!! Form::textarea('fulltext', '', array('class' => 'form-control', 'id' => 'editor1', 'rows' => 3)) !!}
									</div>
								</div>
								<div class="form-group hidden">
									<label class="col-sm-2 control-label" for="inputPassword3"></label>
									<div class="col-sm-10">
										<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#mediaModal">
											<i class="fa fa-cloud-upload"></i> Chèn ảnh, File
										</button>
									</div>
								</div>
								<!-- /.box-footer -->
							</div>

							<div role="tabpanel" class="tab-pane" id="en">
								<div class="form-group">
									<label class="col-sm-2 control-label">Tiêu đề</label>
									<div class="col-sm-10">
										{!! Form::text('title_en', '', array('class' => 'form-control')) !!}
									</div>
								</div>
							</div>

							<div role="tabpanel" class="tab-pane" id="seo">
								<div class="form-group">
									<label class="col-sm-2 control-label">Tiêu đề</label>
									<div class="col-sm-10">
										{!! Form::text('seo_title', '', array('class' => 'form-control')) !!}
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="">Mô tả trang</label>
									<div class="col-sm-10">
										{!! Form::textarea('seo_keyword', '', array('class' => 'form-control', 'rows' => 3)) !!}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- /.box-body -->
				<div class="box-footer">
					<div class="col-sm-2"></div>
					<div class="col-sm-10">
						<button class="btn btn-primary" onclick="$('#action').val('save');" type="submit"><i class="fa fa-check-circle"></i>   Lưu & Quay lại</button>
						<button class="btn btn-primary" onclick="$('#action').val('apply');" type="submit"><i class="fa fa-save"></i> Lưu</button>
						<a class="btn btn-default" href="<?php echo URL::to('/admin/newscategory');?>">Quay lại</a>
					</div>
				</div>
				<input type="hidden" name="action" id="action" value="save">
				{!! Form::close() !!}

				<script type="text/javascript">
					$('.nav-tabs a').click(function (e) {
						e.preventDefault();
						$(this).tab('show');
					})
					function changeModule(module) {
						$('.modules').addClass('hidden');
						$('.module-'+module).removeClass('hidden');

						if(module == 4){
							$('.input-alias').val('lien-he');
						}
						if(module == 5){
							$('.input-alias').val('');
						}
					}
				</script>

		</div>
	</div>
</div>

@stop