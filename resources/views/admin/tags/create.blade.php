@extends('admin.layouts.admin')

@section('breadcrum')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Quản lý Tags
		<small>Tags</small>
	</h1>
	<ol class="breadcrumb hidden">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Thêm / Sửa thông tin</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			{!! Form::open( ['url' => 'admin/tags', 'class' => 'form-horizontal', 'name'=>'uploadform', 'files'=>true] ) !!}
			<div class="box-body" style="padding: 5px;">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#vn" aria-controls="vn" role="tab" data-toggle="tab">Nội dung</a></li>
					<li role="presentation" class="hidden"><a href="#en" aria-controls="en" role="tab" data-toggle="tab">Tiếng Anh</a></li>
				</ul>

				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="vn">
						<div class="form-group">
							<label class="col-sm-2 control-label">Tiêu đề</label>
							<div class="col-sm-10">
								{!! Form::text('title', '', array('class' => 'form-control')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="inputPassword3">Mô tả đầy đủ</label>
							<div class="col-sm-10">
								{!! Form::textarea('description', '', array('class' => 'form-control', 'id' => 'editor11', 'rows' => 3)) !!}
							</div>
						</div>
						<div class="form-group hidden">
							<label class="col-sm-2 control-label" for="inputPassword3"></label>
							<div class="col-sm-10">
								<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#mediaModal">
									<i class="fa fa-cloud-upload"></i> Chèn ảnh, File
								</button>
							</div>
						</div>
					</div>

					<div role="tabpanel" class="tab-pane hidden" id="en">
						<div class="form-group">
							<label class="col-sm-2 control-label">Tiêu đề</label>
							<div class="col-sm-10">
								{!! Form::text('title_en', '', array('class' => 'form-control')) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="inputPassword3">Mô tả đầy đủ</label>
							<div class="col-sm-10">
								{!! Form::textarea('fulltext_en', '', array('class' => 'form-control', 'id' => 'editor2', 'rows' => 3)) !!}
							</div>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<div class="box-footer">
				<div class="col-sm-2"></div>
				<div class="col-sm-10">
					<button class="btn btn-primary" type="submit"><i class="fa fa-check-circle"></i> Hoàn tất</button>
					<a class="btn btn-default" href="<?php echo URL::to('/admin/tags');?>">Quay lại</a>
				</div>
			</div>
			<!-- /.box-footer -->
			{!! Form::close() !!}
		</div>
	</div>
</div>

@stop