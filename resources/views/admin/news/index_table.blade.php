<table class="table table-bordered table-striped dataTable" id="example1" role="grid" aria-describedby="example1_info">
    <thead>
        <tr role="row" class="text-center">
            <th class="text-center" style="padding-right: 10px;">STT</th>
            <th class="sorting_asc1" aria-sort="ascending">Tiêu đề</th>
            <th class="sorting1 text-center">Ảnh</th>
            {{--<th class="sorting1 text-center">Loại</th>--}}
            {{--<th class="sorting1 text-center">Thứ tự</th>--}}
            <th class="sorting1 text-center">Trạng thái</th>
            <th class="sorting1 text-center">ID</th>
            <th class="sorting1 text-center">Người tạo</th>
            <th class="sorting1 text-center">Người sửa</th>
            <th class="sorting1 text-center">...</th>
        </tr>
    </thead>
    <tbody>
    @foreach( $items as $k => $item )
        <tr role="row" class="even">
            <td class="text-center">{{$k+1}}</td>
            <td>
                <a href="<?php echo URL::to('admin/'.$routeName.'/'.$item->id);?>/edit">{{$item->title}}</a>
            </td>
            <td>
                @if($item->image)
                    <img src="<?php echo URL::to($item->image);?>" class="thumbnail" style="width: 50px; margin: 0px">
                @endif
            </td>
            {{--<td class="text-center">{{ isset($types[$item->type]) ? $types[$item->type] : '' }}</td>--}}
            {{--<td class="@if($item->parent_id) text-right @endif">
                <a href="#" data-link="order=up&id={{ $item->id }}" class="ajax-reload"><i class="glyphicon glyphicon-chevron-up"></i></a>
                {{ $item->ordering  }}
                <a href="#" data-link="order=down&id={{ $item->id }}" class="ajax-reload"><i class="glyphicon glyphicon-chevron-down"></i></a>
            </td>--}}
            <td class="text-center">
                {!! \App\Helper\StringHelper::stateStatus($item->status, 2, $item->id)  !!}
            </td>
            <td class="text-center">{{ $item->id }}</td>
            <td class="text-center">{{ $item->created_by_user->name }}<br/>{{ date('d-m-Y H:i', strtotime($item->created_at)) }}</td>
            <td class="text-center">{{ $item->updated_by_user->name }}<br/>{{ date('d-m-Y H:i', strtotime($item->updated_at)) }}</td>
            <td class="text-center">
                <a class="btn btn-success btn-xs" href="<?php echo URL::to('admin/'.$routeName.'/'.$item->id);?>/edit"><i class="fa fa-pencil"></i> Sửa</a>
                <a class="btn btn-danger btn-xs ajax-delete" data-name="{{ $routeName }}" data-id="{{ $item->id }}" href="#"><i class="fa fa-trash"></i> Xóa</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div class="row">
    <div class="col-sm-5">
        <div class="dataTables_length" id="example1_length">
            <label>
                Hiển thị
                {{ Form::select('limit', [10=>10, 20=>20, 50=>50, 100=>100], app('request')->input('limit', 20), ['class'=>'form-control input-sm', 'onchange' => 'this.form.submit();']) }}
                mục
            </label>
        </div>
        <div class="dataTables_info hidden" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
    </div>
    <div class="col-sm-7">
        <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
            {!! $items->appends(['keyword' => app('request')->input('keyword', '')])->links() !!}
        </div>
    </div>
</div>