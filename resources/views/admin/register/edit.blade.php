@extends('admin.layouts.admin')

@section('breadcrum')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Quản lý Tin bài
		<small>Trang</small>
	</h1>
	<ol class="breadcrumb hidden">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Thêm / Sửa thông tin</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			{!! Form::model($news,[ 'method' => 'PATCH', 'action' => ['Admin\\PagesController@update', $news->id], 'class' => 'form-horizontal', 'files'=>true ]) !!}
				<div class="box-body">
					<div class="form-group">
						<label class="col-sm-2 control-label">Tiêu đề</label>
						<div class="col-sm-10">
							{!! Form::text('title', $news->title, array('class' => 'form-control')) !!}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Ảnh</label>
						<div class="col-sm-10">
							{!! Form::file('image_upload') !!}
							@if($news->image != '')
								<img src="<?php echo URL::to($news->image);?>" class="thumbnail" style="width: 80px; margin: 0px">
							@endif
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="inputPassword3">Mô tả đầy đủ</label>
						<div class="col-sm-10">
							{!! Form::textarea('fulltext', $news->fulltext, array('class' => 'form-control', 'id' => 'editor1', 'rows' => 3)) !!}
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<div class="col-sm-2"></div>
					<div class="col-sm-10">
						<button class="btn btn-primary" type="submit"><i class="fa fa-check-circle"></i>   Hoàn tất</button>
						<a class="btn btn-default" href="<?php echo URL::to('/admin/pages');?>">Quay lại</a>
					</div>
				</div>
				<!-- /.box-footer -->
			{!! Form::close() !!}
		</div>
	</div>
</div>

@stop