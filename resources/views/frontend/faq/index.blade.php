@extends('layouts.default')
@section('header')
    <title>Hỏi đáp - {{ config('custom.seo_title') }}</title>
    <meta name="description" content="Hỏi đáp - {{ config('custom.seo_description') }}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}" />
    <meta property="og:image" content="{{ url('media/system/fbimage.jpg?'.config('version.version'))  }}" />
    <meta property="og:title" content="Hỏi đáp - {{ config('custom.seo_title') }}" />
    <meta property="og:description" content="Hỏi đáp - {{ config('custom.seo_description') }}" />
    <style>
        #facebook {
            display: none;
        }
    </style>
@stop

@section('content')
    <div class="container">

        <section id="s6" class="section s6 py-6">
            <div class="text-center">
                <h1 class="section__header mb-0 d-inline-block">
                    <span>FAQ</span>
                </h1>
            </div>

            <div class="row">

                <div class="col-8 offset-2 mb-6">
                    @foreach( $items as $i => $item )
                    <div class="faq bg-gray-lightest p-3 p-xl-4 mb-4">
                        <div class="media mb-3">
                            <div class="faq__dropcap d-flex justify-content-center align-items-center mr-3 mt-1">Q</div>
                            <div class="media-body fs-15">
                                <strong>{{ $item->title }}</strong>
                            </div>
                        </div>

                        <div class="media">
                            <div class="faq__dropcap d-flex justify-content-center align-items-center mr-3 mt-1">A</div>
                            <div class="media-body">{{ $item->description }}</div>
                        </div>
                    </div>
                    @endforeach
                </div>

                <div class="col-8 offset-2">
                    <div class="faq-form">
                        <div class="text-center mb-4">
                            <h2 class="section__header section__header--center faq-form__header mb-0 d-inline-block">
                                <span class="text-uppercase d-block mb-1">Gửi câu hỏi của bạn</span>
                                    <span class="fs-14 d-block fw-4">
                                        Hãy gửi câu hỏi cho chúng tôi, mọi thắc mắc sẽ được chúng tôi <br>
                                        giải đáp trong thời gian sớm nhất có thể.
                                    </span>
                            </h2>
                        </div>

                        <form class="form-material faqForm faq-form__form">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-0 fs-15 text-gray-lighter" for="fc-1">Họ tên</label>
                                        <input type="text" name="name" class="form-control form-control--material" id="fc-1">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb-0 fs-15 text-gray-lighter" for="fc-2">Email</label>
                                        <input type="email" name="email" class="form-control form-control--material" id="fc-2">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="mb-0 fs-15 text-gray-lighter" for="fc-3">Câu hỏi của bạn</label>
                                <textarea id="fc-3" name="description" class="form-control form-control--material" rows="4"></textarea>
                            </div>

                            <div class="form-group">
                                <script src='https://www.google.com/recaptcha/api.js'></script>
                                <label class="mb-0 fs-15 text-gray-lighter" for="fc-3">Mã bảo vệ</label>
                                <div class="g-recaptcha" data-sitekey="{{ config('custom.captcha_sitekey') }}"></div>
                            </div>

                            <div class="text-center mt-5">
                                <div class="alert alert-faq alert-success hidden text-left"></div>
                                <button onclick="return sendFAQ(this, '.faqForm');" type="button" class="btn btn-lg btn-gray-dark faq-form__btn-send fs-24 px-5">
                                    Gửi câu hỏi
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </section>
    </div>
@stop

@section('footer')
    <script type="text/javascript">
        function sendFAQ(obj, formClass){
            var $form = $(formClass);
            var _success = function(data){
                $('.alert-faq').removeClass('hidden').html(data.message);

                if(data.code == 1){
                    $form.find("input[type=text], input[type=email], textarea").val("");
                }
            };

            ajax(obj, 'faq', $form.serialize(), _success, 'POST');

            return false;
        }
    </script>
@stop