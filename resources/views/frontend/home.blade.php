@extends('layouts.default')
@section('header')
    <title>{{ config('custom.seo_title') }}</title>
    <meta name="description" content="{{ config('custom.seo_description') }}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}" />
    <meta property="og:image" content="{{ url('media/system/fbimage.jpg?'.config('custom.version'))  }}" />
    <meta property="og:title" content="{{ config('custom.seo_title') }}" />
    <meta property="og:description" content="{{ config('custom.seo_description') }}" />

    <script src="{{ url('assets/lib/slick/slick.min.js?'.config('custom.version')) }}"></script>
    <link type="text/css" href="{{ url('assets/css/home.css?'.config('custom.version')) }}" rel="stylesheet"/>
    <link type="text/css" href="{{ url('assets/css/custom.css?'.config('custom.version')) }}" rel="stylesheet"/>
    <!-- carousel slide -->
    <link rel="stylesheet" href="{{ url('assets/lib/owl.carousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/lib/owl.carousel/owl.theme.default.min.css') }}">
    <script src="{{ url('assets/lib/owl.carousel/owl.carousel.js') }}"></script>
    <style>

        .home-video__icon img{
            width: auto;
        }
        .home-text__1{
            display: inline-block;
            vertical-align: top;
            font-family: 'Gilroy-Bold1';
            color: #fff;
        }
        .home-text__1 h2{
            font-size: 52px;
            text-shadow: 4px 4px 11px rgba(0,0,0,.6);
            margin-top: 0px;
            margin-bottom:0px;
        }
        .home-text__1 p{
            font-size: 13px;
            text-shadow: 4px 4px 11px rgba(0,0,0,.6);
        }
        .home-video__icon__3 img{
            -webkit-animation: sdb05 1.5s infinite;
            animation: sdb05 1.5s infinite;
        }
        @-webkit-keyframes sdb05 {
            0% {
                -webkit-transform: translate(0, 0);
                opacity: 0;
            }
            50% {
                opacity: 1;
            }
            100% {
                -webkit-transform: translate(0px, 20px);
                opacity: 0;
            }
        }
        @keyframes sdb05 {
            0% {
                transform: translate(0, 0);
                opacity: 0;
            }
            50% {
                opacity: 1;
            }
            100% {
                transform: translate(0px, 20px);
                opacity: 0;
            }
        }
        .mt-10{
            margin-top: 10px;
        }
        .home-header-one__left__text{
            font-size: 1.8rem;
        }
    </style>
@stop

@section('content')
    <div class="content home-video">
        <video class="youtube_player1" autoplay playsinline id="youtube_player1">
            <source src="https://www.youtube.com/watch?v=A-pDVJP-4MQ" type="video/youtube">
            Your browser does not support the video tag.
        </video>
        <div class="container container--config container--posi">
            <div class="row">
                <div class="col-sm-5 home-video__icon hidden-mb">
                    <div class="row">
                        <div class="col-sm-12 home-video__icon__1">
                            <img src="{{ url('assets/img_home/img44.png') }}" alt="image" width="120%">
                        </div>
                        <div class="col-sm-12 mt-10">
                            <img src="{{ url('assets/img_home/icon1.png') }}" alt="image">
                            <div class="home-text__1">
                                <h2>{{ $totalSubmit }}</h2>
                                <p>Câu chuyện hạnh phúc</p>
                            </div>
                        </div>
                        <div class="col-sm-12 mt-10">
                            <img src="{{ url('assets/img_home/icon2.png') }}" alt="image">
                            <div class="home-text__1">
                                <h2>{{ $totalVote }}</h2>
                                <p>Hạnh phúc được lan tỏa</p>
                            </div>
                        </div>
                        <div class="col-sm-12 home-video__icon__2">
                            <a href="{{ url('pages/vechungtoi') }}">
                                <img src="{{ url('assets/img_home/icon3.png') }}" alt="image">
                            </a>
                            <img src="{{ url('assets/img_home/icon4.png') }}" alt="image" class="sound">
                        </div>
                        <div class="col-sm-12 home-video__icon__3">
                            <a href="#content"><img src="{{ url('assets/img_home/icon5.png') }}" alt="image"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <a href="https://www.youtube.com/watch?v=EKYYjyEwlv4" target="_blank" class="target-youtube"></a>

        <div class="home-video__play hidden-pc show-mb">
            <img src="{{ url('assets/img_home/icon_play.png') }}" alt="image" class="player">
            <div class="home-video__play__text">Xem video</div>
        </div>
        <!-- popup video header mobile-->
        <div class="modal fade" id="myModal-vdheader" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" style="padding: 10px 5px">
                        <video class="youtube_players" playsinline id="youtube_players">
                            <source src="https://www.youtube.com/watch?v=EKYYjyEwlv4" type="video/youtube">
                            Your browser does not support the video tag.
                        </video>
                    </div>
                </div>

            </div>
        </div>
        <!-- /end -->
    </div>

    <div class="content content__one" id="content">
        <div class="container container--config">
            <!-- Mobile -->
            <div class="row m-header-box">
                <div class="col-xs-10 col-xs-offset-1 text-center">
                    <div class="m-header">Một chiến dịch của Dạ Hương</div>
                </div>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-5">
                            <img src="{{ url('assets/img_home/img56.png') }}" alt="image" width="100%">
                        </div>
                        <div class="col-xs-7 m-pl-0">
                            <div class="m-number">{{ $totalSubmit }}</div>
                            <div class="m-text">Câu chuyện hạnh phúc</div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-5">
                            <img src="{{ url('assets/img_home/img57.png') }}" alt="image" width="100%">
                        </div>
                        <div class="col-xs-7 m-pl-0">
                            <div class="m-number">{{ $totalVote }}</div>
                            <div class="m-text">Hạnh phúc được lan tỏa</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End -->

            <div class="row">
                <div class="col-sm-12">
                    <div class="home-header-one">
                        <div class="home-header-one__left">
                            <img class="home-header-one__left__img" src="{{ url('assets/img_home/cauchuyen.png') }}" width="100%" alt="image">
                            <div class="home-header-one__left__text">
                                Cùng lắng nghe những câu chuyện <br class="break-ip6">hạnh phúc truyền cảm hứng
                            </div>
                        </div>
                        <div class="home-header-one__right">
                            <img class="home--header-one__right_img" src="{{ url('assets/img_home/icon7.png') }}" alt="image" width="100%">
                        </div>
                    </div>
                </div>
            </div>

            <!-- Mobile -->
            @foreach ($kols as $k => $item)
                @if ($k == 0)
                    <div class="row hidden-pc show-mb">
                        <div class="col-sm-12">
                            <div class="mb-box-one-header">
                                <div class="col-xs-6 m-pl-0 m-pr-10">
                                    <a href="#" onclick="return $.app.modal.contentModal(this);" data-content-ajax="/?show=kol&id={{ $item->id }}" data-title="{{ $item->title }}"><img src="{{ url($item->image) }}" alt="image" width="100%"></a>
                                </div>
                                <div class="col-xs-6 m-pl-0 m-pr-10">
                                    <div class="home-box-one__content">
                                        <h2 class="home-box-one__content__name" style="margin-top: 10px">
                                            <a href="#" onclick="return $.app.modal.contentModal(this);" data-content-ajax="/?show=kol&id={{ $item->id }}" data-title="{{ $item->title }}">{{ $item->title }}</a>
                                        </h2>
                                        <div class="home-box-one__content__mother">{{ $item->note }}</div>
                                        <img class="home-box-one__content__img" src="{{ url('assets/img_home/icon9.png') }}">
                                        <div class="home-box-one__content__text">
                                            {{ $item->introtext }}
                                        </div>
                                        @if ($item->id == 22)
                                            <a href="http://afamily.vn/ly-nguyen-6-thang-song-o-bhutan-da-giup-minh-hoc-duoc-cach-giu-niem-tin-su-binh-an-trong-tam-hon-20180909205201159.chn" target="_blank"><div class="home-box-one__img__more home-box-one__img__more--1 m-mt-10 spe">Xem ngay</div></a>
                                        @else
                                            <a href="#" onclick="return $.app.modal.contentModal(this);" data-content-ajax="/?show=kol&id={{ $item->id }}" data-title="{{ $item->title }}"><div class="home-box-one__img__more home-box-one__img__more--1 m-mt-10 spe">Xem ngay</div></a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
            <!-- End -->

            <div class="row slick-config" style="position: relative">
                <div class="col-sm-12 home-box-row-one">
                    <div class="row slick-3">
                        @foreach($kols as $k => $item)
                        <div class="col-sm-3 col-xs-6{{ ($k == 0) ? ' hidden-mb show-pc' : '' }}">
                            <div class="home-box-one">
                                <div class="home-box-one__img">
                                    <a href="#" onclick="return $.app.modal.contentModal(this);" data-content-ajax="/?show=kol&id={{ $item->id }}" data-title="{{ $item->title }}"><img src="{{ url($item->image) }}" alt="image" width="100%"></a>
                                    <div class="home-box-one__img__moreposition">
                                        @if ($item->id == 22)
                                            <a href="http://afamily.vn/ly-nguyen-6-thang-song-o-bhutan-da-giup-minh-hoc-duoc-cach-giu-niem-tin-su-binh-an-trong-tam-hon-20180909205201159.chn" target="_blank"><div class="home-box-one__img__more home-box-one__img__more--1">Xem ngay</div></a>
                                        @else
                                            <a href="#" onclick="return $.app.modal.contentModal(this);" data-content-ajax="/?show=kol&id={{ $item->id }}" data-title="{{ $item->title }}"><div class="home-box-one__img__more home-box-one__img__more--1">Xem ngay</div></a>
                                        @endif
                                    </div>
                                </div>
                                <div class="home-box-one__content">
                                    <h2 class="home-box-one__content__name">
                                        <a href="#" onclick="return $.app.modal.contentModal(this);" data-content-ajax="/?show=kol&id={{ $item->id }}" data-title="{{ $item->title }}">{{ $item->title }}</a>
                                    </h2>
                                    <div class="home-box-one__content__mother">{{ $item->note }}</div>
                                    <img class="home-box-one__content__img" src="{{ url('assets/img_home/icon9.png') }}">
                                    <div class="home-box-one__content__text">
                                        {{ $item->introtext }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            {{-----------------------------SECTION3-DESKTOP------------------------}}
            <div id="section3" class="row">
                <div class="col-sm-12">
                    <div class="home-header-two">
                        <div class="home-header-two__left col-xs-5">
                            <div class="home-header-two__left__img">
                                <img src="{{ url('assets/img_home/hpct.png') }}" alt="image" width="100%">
                                <!-- <img src="{{ url('assets/img_home/icon10.png') }}" alt="image" class="home-header-two__left__imgposi"> -->
                            </div>
                            <a href="#" onclick="return $.app.checkLogin('{{ url('bai-du-thi/create') }}');"><div class="home-box-one__img__more home-box-one__img__more--2">Tham gia ngay</div></a>
                            <div class="home-hr">
                                <div class="home-hr-child"></div>
                            </div>
                        </div>
                        <div class="home-header-two__right col-xs-7">
                            <div class="home-header-two__right__box1 col-xs-6 no-padding">
                                <span><a href="#">{{ strlen($totalSubmit) < 3 ? ('00' . $totalSubmit) : $totalSubmit }}</a></span><br>
                                <a href="{{ url('pages/thelegiaithuong') }}"><div class="home-box-one__img__more home-box-one__img__more--8">Xem thể lệ</div></a>
                            </div>
                            <div class="home-header-two__right__box2 col-xs-6 no-padding">
                                <p>
                                    phụ nữ Việt đã chia sẻ câu<br/>
                                    chuyện hạnh phúc của họ. <br/>
                                    Hạnh phúc của bạn là gì? 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-----------------------------END-SECTION3-DESKTOP------------------------}}
        <!-- section123-mobile -->
        {{-----------------------------SECTION3-MOBILE------------------------}}
            <div id="section3-mb" class="row ">
                <div class="col-xs-12">
                    <div class="home-header-two-mb">
                        <div class="row up-part">
                            <div class="title-left col-xs-8">
                                <div class="home-header-two__left__img-mb">
                                    <img src="{{ url('assets/img_home/hpct.png') }}" alt="image" width="100%">
                                    <!-- <img src="{{ url('assets/img_home/icon10.png') }}" alt="image" class="home-header-two__left__imgposi"> -->
                                </div>
                            </div>
                            <div class="title-right col-xs-4 m-pl-0">
                                <a href="#" onclick="return $.app.checkLogin('{{ url('bai-du-thi/create') }}');"><div class="home-box-one__img__more home-box-one__img__more--2">Tham gia ngay</div></a>
                            </div>
                        </div>
                        <div class="row bottom-part ">
                            <div class="sec3-left col-xs-8">
                                <div class="left-number col-xs-6"><a href="#">{{ strlen($totalSubmit) < 3 ? ('00' . $totalSubmit) : $totalSubmit }}</a></div>
                                <div class="right-text col-xs-6" style="padding:12px 0;">
                                    <p >phụ nữ Việt đã chia sẻ câu<br/>
                                    chuyện hạnh phúc của họ. <br/>
                                    Hạnh phúc của bạn là gì?
                                    </p>
                                </div>
                            </div> 
                            <div class="sec3-right col-xs-4 m-pl-0">
                                <a href="{{ url('pages/thelegiaithuong') }}"><div class="home-box-one__img__more right-btn">Xem thể lệ</div></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {{-----------------------------END-SECTION3-MOBILE------------------------}}
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="sc">
                        <img src="{{ url('assets/img_home/sechia.png') }}" alt="image" width="100%">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content content__two">
        <div class="container container--config">
            <div class="row slick-config">
                <div class="col-sm-12 home-box-row-two">
                    <div class="row cnt-bdt">
                        @foreach($submits as $submit)
                            <div class="col-sm-4 col-xs-6 box-two">
                                <a href="{{ $submit->link }}"><div style="border-radius:20px"><img src="{{ url($submit->image) }}" width="100%" class="slick4-img"></div></a>
                                <div class="row">
                                    <div class="col-sm-9 col-xs-9">
                                        <div class="box-two__text">
                                            <a href="{{ $submit->link }}">{{ str_limit($submit->introtext, $limit=75, $end = '...') }}</a>
                                        </div>
                                        <div class="box-two__name">
                                            <div class="circle"></div>{{ $submit->username }}
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 text-center pl-0">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <a class="hover-vote" onclick="return $.app.contest.vote({{ $submit->id }});" href="#"><img src="{{ url('assets/img_home/vote.png')  }}" alt="image" width="100%" class="mt-10"></a>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="box-two__number submit-votes-{{ $submit->id }}">{{ $submit->votes }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-sm-12">
                    <a href="{{ url('bai-du-thi') }}"><div class="home-box-one__img__more home-box-one__img__more--3">Xem tất cả >></div></a>
                </div>
            </div>
        </div>
    </div>

    <div class="content content__three">
        <div class="container container--config">
            <div class="row" style="display: flex; align-items: center">
                <div class="col-sm-8 col-sm-offset-1">
                    <div class="home-header-three">
                        <img src="{{ url('assets/img_home/img15.png') }}" alt="image" width="100%">
                        {{--<img src="{{ url('assets/img_home/left-lavender.png') }}" alt="image" class="home-header-two__left__imgposi home-header-two__left__imgposi--1">--}}
                        <img src="{{ url('assets/img_home/img16.png') }}" alt="image" class="home-header-two__left__imgposi home-header-two__left__imgposi--2">
                        <div class="home-header-two__left__textposi hidden-pc show-mb">Vì bạn xứng đáng để luôn hạnh phúc mỗi ngày</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-3 hidden-mb">
                    <div class="home-header-two__left__textposi">Vì bạn xứng đáng để<br> luôn hạnh phúc mỗi ngày</div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 list-text-menu">
                    <div class="col-sm-4 col-xs-4 m-pr-0 m-pl-10">
                        <div class="text-menu icon-hover icon-hover--1">
                            Tham gia <span class="font-gilroy-bold">#sốnghạnhphúc</span> <br>
                            để khám phá bí quyết sống <br>
                            hạnh phúc
                            <img src="{{ url('assets/img_home/icon22.png') }}" alt="image">
                        </div>
                    </div>
                    <div class="col-sm-5 col-xs-5 m-pr-0 m-pl-10">
                        <div class="text-menu icon-hover icon-hover--1">
                            Đừng bỏ lỡ <span class="font-gilroy-bold">100 tấm vé</span> <br>
                            tham dự workshop dành cho 100 bài dự thi xuất sắc nhất
                            <img src="{{ url('assets/img_home/icon22.png') }}" alt="image">
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-3 m-pl-0 m-pr-0">
                        {{--<a href="{{ url('pages/workshop') }}"><div class="home-box-one__img__more home-box-one__img__more--9">Chi tiết workshop</div></a>--}}
                        <a href="#" data-toggle="modal" data-target=".coming-soon"><div class="home-box-one__img__more home-box-one__img__more--9">Chi tiết workshop</div></a>
                        <a onclick="return $.app.checkLogin('{{ url('bai-du-thi/create') }}');" href="#"><div class="home-box-one__img__more home-box-one__img__more--4" style="display: block; width: 100%; text-align: center">Gửi bài dự thi</div></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="box-threes">
            <div class="box-three-left"></div>
            <div class="box-three">
                <div class="m-box-img hidden-pc show-mb">
                    <img src="{{ url('assets/img_home/img60.png') }}" alt="image" width="100%">
                </div>
                <div class="box-three-top">
                    <div class="box-three-top__date">
                        <div class="box-three-top__time">Thời gian</div>
                        <p class="box-three-top__text1">Workshop diễn ra từ</p>
                        <!-- <img src="{{ url('assets/img_home/img38.png') }}" alt="image" class="box-three-top__img"> -->
                        <a href="#">14:30</a>
                        <p class="box-three-top__text1">đến</p>
                        <!-- <img src="{{ url('assets/img_home/img37.png') }}" alt="image" class="box-three-top__img"> -->
                        <a href="#">17:00</a>
                    </div>
                    <div class="box-three-top__where">
                        <div class="box-three-top__time">Địa điểm</div>
                        <img src="{{ url('assets/img_home/img40_1.png') }}" alt="image" class="box-three-top__img1">
                        <div class="box-three-top__text2">tại <span class="box-three-top__span">HÀ NỘI</span></div>
                    </div>
                </div>
                <div class="box-three-bottom">
                    <div class="box-three-bottom__title">Hãy đến với workshop để...</div>
                    <ul class="box-three-bottom__ul">
                        <li>
                            Gặp gỡ, lắng nghe <b>chia sẻ hạnh phúc</b> cùng những<br> phụ nữ xung quanh
                        </li>
                        <li>
                            Talkshow và giao lưu cùng những <b>người nổi tiếng</b>
                        </li>
                        <li>
                            Trải nghiệm nhiều <b>hoạt động thú vị</b> trong khuôn khổ<br> workshop như: nấu ăn, cắm hoa, nhiếp ảnh
                        </li>
                        <li>
                            Nhận được nhiều <b>quà tặng hấp dẫn</b> từ nhà tài trợ<br> Dạ Hương
                        </li>
                    </ul>
                </div>
                <div  class="box-three__background">

                    <img src="{{ url('assets/img_home/img39.png') }}" alt="image" width="100%" height="100%" style="z-index:-2">
                    <img src="{{ url('assets/img_home/ddvspn1.png') }}" alt="image" class="box-three-top__img2">
                </div>
            </div>
        </div>
    </div>

    <link rel="stylesheet" href="{{ url('assets/lib/mediaplayer/mediaelementplayer.min.css') }}" type="text/css"/>
    <script src="{{ url('assets/lib/mediaplayer/mediaelement-and-player.js') }}"></script>
    <div class="content content__four">
        <div class="container container--config">
            <div class="row home-header-four"><br/>
                <div class="col-sm-7 home-header-four__left" style="position: relative">
                    <img src="{{ url('assets/img_home/img17.png') }}" class="home-header-four__img" alt="image">
                    <img src="{{ url('assets/img_home/left-lavender.png') }}" alt="image" class="home-header-four__left__imgposi">
                </div>
                <div class="col-sm-5">
                    <div class="home-header-four__right">Là phụ nữ bạn đã biết bí quyết để luôn hạnh phúc, lạc quan và tâm hồn tươi trẻ</div>
                </div>
            </div>
            {{--<div class="row home-box-four">--}}
                {{--<div class="col-sm-7 home-box-four__left">--}}
                    {{--@foreach($videos as $k => $item)--}}
                        {{--@if($k == 0)--}}
                            {{--<video class="youtube_player" playsinline id="youtube_player7">--}}
                                {{--<source src="https://www.youtube.com/watch?v={{ $item->youtube }}" type="video/youtube">--}}
                                {{--Your browser does not support the video tag.--}}
                            {{--</video>--}}
                        {{--@endif--}}
                    {{--@endforeach--}}
                {{--</div>--}}

                {{--<div class="col-sm-5 col-xs-12 home-box-four__right">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-sm-12">--}}
                            {{--<div class="row slick-0-mb">--}}
                                {{--<div class="home-box-four__right__list ">--}}
                                    {{--@foreach($videos as $item)--}}
                                        {{--<div class="col-sm-12 col-xs-6 home-box-four__item">--}}
                                            {{--<div class="src hiden">https://www.youtube.com/watch?v={{ $item->youtube }}</div>--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-sm-6">--}}
                                                    {{--<div style="position: relative">--}}
                                                        {{--<img src="{{ url($item->image) }}" alt="image" width="100%" class="slick0-img">--}}
                                                        {{--<div class="hover-slide">Đang xem</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-sm-6 home-box-four__item__text">--}}
                                                    {{--{{ $item->title }}--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--@endforeach--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="row">
                <div class="col-sm-12 home-header-four__2">
                    <img src="{{ url('assets/img_home/img19.png') }}" alt="image" class="tt">
                    <img src="{{ url('assets/img_home/icon22.png') }}" alt="image" class="posi">
                </div>
            </div>
            <div class="row margin-bottom-40">
                @foreach($news as $k => $item)
                    @if($k == 0)
                    <div class="col-sm-8 col-xs-12">
                        <a href="#" onclick="return $.app.modal.contentModal(this);" data-content-ajax="/?show=news&id={{ $item->id }}" data-title="{{ $item->title }}">
                            <img src="{{ $item->image ? url($item->image) : '' }}" alt="image" width="100%"></a>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <a href="#" onclick="return $.app.modal.contentModal(this);" data-content-ajax="/?show=news&id={{ $item->id }}" data-title="{{ $item->title }}" class="home-box-four-2-text__title">
                            {{ $item->title }}</a>
                        <div class="home-box-four-2-text__month">{{ date('d-m-Y H:i', strtotime($item->created_at)) }}</div>
                        <div class="home-box-four-2-text__content">
                            {{ str_limit($item->introtext, $limit=200, $end = '...') }}
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
            <div class="row home-four-row">
                <div class="col-sm-12 home-four-slide">
                    <div class="row slick-2" style="overflow: hidden">
                        @foreach($news as $item)
                            <div class="col-sm-3 col-xs-6">
                                <a href="#" onclick="return $.app.modal.contentModal(this);" data-content-ajax="/?show=news&id={{ $item->id }}" data-title="{{ $item->title }}">
                                    <img class="home-box-four-2-img" alt="image" src="{{ url($item->image) }}" width="100%">
                                    <div class="home-box-four-2-text">
                                        <div class="home-box-four-2-text__month">{{ $item->title }}</div>
                                        <div class="home-box-four-2-text__content">
                                            {{ str_limit($item->introtext, $limit=50, $end = '...') }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content content__five">
        <div class="container container--config">
            <div class="row content__five__row">
                <div class="col-sm-12">
                    <img alt="image" src="{{ url('assets/img_home/img20.png') }}" width="100%">
                </div>
            </div>
            <div class="row home-box-five">
                <div class="col-sm-4 home-box-five__img hidden-mb" style="position: relative;">
                    <img alt="image" src="{{ url('assets/img_home/sub-ft.png?v=1') }}" width="100%">
                </div>
                <div class="col-sm-8 home-box-five__text">
                    <ul class="home-box-five__text__ul">
                        <li>
                            Phụ nữ Việt thường nghĩ gì khi nói về hạnh phúc? <br>
                            <p></p>
                            Xuất phát từ mong muốn giúp tất thảy những phụ nữ Việt Nam, dù là ai, ở độ tuổi nào, có cuộc sống ra sao, vẫn có thể sống lạc quan, vui vẻ với những niềm hạnh phúc dù là bình dị nhất.
                            <p></p>
                            Những câu chuyện về niềm hạnh phúc của mỗi cá nhân sẽ được lan tỏa, truyền cảm hứng cho những người phụ nữ khác. Tạo động lực cho họ cùng sẻ chia, nhân lên niềm vui hạnh phúc ngày một lan tỏa rộng khắp.
                            <p></p>
                            Chiến dịch “Lan tỏa hạnh phúc” là chuỗi hành trình của những câu truyện hạnh phúc đến từ mọi phụ nữ Việt, từ đó tạo dựng được tiếng vang và giúp cộng đồng hướng tới cuộc sống tích cực hơn.
                        </li>
                    </ul>
                    <div class="home-box-five__text__posi">
                        <a href="{{ url('pages/vechungtoi') }}"><div class="home-box-one__img__more home-box-one__img__more--7">Xem thêm</div></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer')
    <script src="https://chat.bizfly.vn/js/chatboxfacebook.js"></script>
    <script>
        new PluginChatBoxFacebook({
            fanpage_id: '533243890026838',
            text_greeting: 'Chào mừng bạn đến với cuộc thi Hạnh phúc của tôi',
            theme_color: '#0099FF',
            ref: 'from_website'
        }).run();
    </script>

    {{--<div class="fb-customerchat" page_id="533243890026838"></div>--}}
@stop