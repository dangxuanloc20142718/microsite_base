@extends('layouts.default')

@section('header')
    <title>{{$item->name}}</title>
    <meta name="description" content="{{$item->name}}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}" />
    <meta property="og:title" content="{{$item->name}}" />
    <meta property="og:description" content="{{$item->name}}" />
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
@stop

@section('breadcrum')
@stop

@section('content')
    <div class="container">
        <div class="member">
            <div class="row">
                <div class="col-md-3">
                    @include('frontend.member.menu')
                </div>
                <div class="col-md-9">
                    <h1 class="heading">Quản lý Đơn hàng </h1>
                    <div class="row">
                        <div class="col-md-12">
                            @if ( $errors->any() )
                                <br/>
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>

                    <table class="table table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th class="text-center">Người đặt</th>
                                <th class="text-center">Ngày đặt</th>
                                <th class="text-center">Tổng tiền</th>
                                <th class="text-center">Trạng thái</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($items as $k=>$order)
                            <tr>
                                <td class="text-center">{{ $k+1 }}</td>
                                <td ><b>{{ $order->fullname }}</b></td>
                                <td class="text-center">{{ \App\Helper\StringHelper::date($order->created_at)  }}</td>
                                <td class="text-right">{{ \App\Helper\StringHelper::money($order->total)  }}</td>
                                <td class="text-center">{!! \App\Helper\StringHelper::orderStatusText($order->status) !!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {!! $items->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer')
@stop
