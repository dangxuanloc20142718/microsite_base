<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="tabuser login-tab"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Đăng nhập</a></li>
    <li role="presentation" class="tabuser register-tab"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Đăng ký</a></li>
    <li role="presentation" class="tabuser register-salon-tab hidden"><a href="#professional" aria-controls="professional" role="tab" data-toggle="tab">Đăng ký là Chủ Salon</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">

    <div role="tabpanel" class="tab-pane active" id="home">
        <div class="alert alert-success hidden alert-login"></div>
        <form class="form-horizontal form-login" role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-3 control-label required">Email</label>
                <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-3 control-label required">Mật khẩu</label>
                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" checked="checked"> Ghi nhớ
                        </label>
                        <label>- <a class="btn btn-link" href="{{ url('/password/reset') }}">Quên mật khẩu?</a> </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                    <button type="submit" onclick="return ajaxLogin(this);" class="btn btn-success ">
                        <i class="glyphicon glyphicon-send"></i> Đăng nhập
                    </button>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                    <a href="{{ url('auth/facebook') }}" class="btn btn-primary btn-sm">Đăng nhập qua Facebook</a>
                    <a href="{{ url('auth/google') }}" class="btn btn-danger btn-sm">Đăng nhập qua Google</a>
                </div>
            </div>
        </form>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
        <div class="alert alert-success hidden alert-register"></div>
        <form class="form-horizontal form-register" role="form" method="POST" action="{{ url('/register') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label required">Họ và tên</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label required">Email</label>
                <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="mobile" class="col-md-4 control-label required">Điện thoại</label>
                <div class="col-md-6">
                    <input id="mobile" type="text" class="form-control" name="mobile" value="{{ old('mobile') }}">
                    @if ($errors->has('mobile'))
                        <span class="help-block">
                            <strong>{{ $errors->first('mobile') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                <label for="gender" class="col-md-4 control-label required">Giới tính</label>
                <div class="col-md-6">
                    <label><input type="radio" name="gender" value="1" checked> Nam</label> -
                    <label><input type="radio" name="gender" value="0"> Nữ</label>
                    @if ($errors->has('gender'))
                        <span class="help-block">
                            <strong>{{ $errors->first('gender') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label required">Mật khẩu</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label for="password-confirm" class="col-md-4 control-label required">Nhắc lại mật khẩu</label>

                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" onclick="return ajaxRegister(this, '.form-register');" class="btn btn-success">
                        <i class="glyphicon glyphicon-send"></i> Đăng ký
                    </button>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-9 col-md-offset-4">
                    <a href="{{ url('auth/facebook') }}" class="btn btn-primary btn-sm">Đăng ký qua Facebook</a>
                    <a href="{{ url('auth/google') }}" class="btn btn-danger btn-sm">Đăng ký qua Google</a>
                </div>
            </div>
        </form>
    </div>

    <div role="tabpanel" class="tab-pane hidden" id="professional">
        Nếu bạn là chủ Salon, bạn có thể đăng ký thành viên ở đây để có thể đăng thông tin Salon, các mẫu tóc và nhiều ưu đãi khác!
        <br/><br/>

        <div class="alert alert-success hidden alert-register"></div>
        <form class="form-horizontal form-register-salon" role="form" method="POST" action="{{ url('/register') }}">
            <input type="hidden" class="form-control" name="type" value="salon">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label required">Họ và tên</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label required">Email</label>
                <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="mobile" class="col-md-4 control-label required">Điện thoại</label>
                <div class="col-md-6">
                    <input id="mobile" type="text" class="form-control" name="mobile" value="{{ old('mobile') }}">
                    @if ($errors->has('mobile'))
                        <span class="help-block">
                            <strong>{{ $errors->first('mobile') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                <label for="gender" class="col-md-4 control-label required">Giới tính</label>
                <div class="col-md-6">
                    <label><input type="radio" name="gender" value="1" checked> Nam</label> -
                    <label><input type="radio" name="gender" value="0"> Nữ</label>
                    @if ($errors->has('gender'))
                        <span class="help-block">
                            <strong>{{ $errors->first('gender') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label required">Mật khẩu</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label for="password-confirm" class="col-md-4 control-label required">Nhắc lại mật khẩu</label>

                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" onclick="return ajaxRegister(this, '.form-register-salon');" class="btn btn-success">
                        <i class="glyphicon glyphicon-send"></i> Đăng ký là chủ Salon
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>