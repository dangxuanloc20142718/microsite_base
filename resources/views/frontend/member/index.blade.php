@extends('layouts.default')

@section('header')
    <title>{{$user->name}}</title>
    <meta name="description" content="{{$user->name}}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}" />
    <meta property="og:title" content="{{$user->name}}" />
    <meta property="og:description" content="{{$user->name}}" />
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

    <link type="text/css" href="{{ url('/') }}/assets/css/baiduthi.css?{{ config('custom.version') }}" rel="stylesheet"/>
@stop

@section('breadcrum')
@stop

@section('content')
    <div class="container">
        <div class="member top15"  style="background: #fff; margin-top: 30px;">
            <div class="row">
                <div class="col-md-3">
                    @include('frontend.member.menu')
                </div>
                <div class="col-md-9">
                    @if(Session::has('message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                    @endif
                    <h1 class="heading">Trang thành viên </h1>
                    <table class="table table-noborder table-stripeds">
                        <tbody>
                            <tr>
                                <td width="130"><b>Họ và tên:</b></td>
                                <td>{{$user->name}}</td>
                                <td width="100"><b>Điện thoại:</b></td>
                                <td>{{$user->mobile}}</td>
                            </tr>
                            <tr>
                                <td><b>Địa chỉ:</b></td>
                                <td> </td>
                                <td><b>Email:</b></td>
                                <td>{{$user->email}}</td>
                            </tr>
                            <tr>
                                <td><b>Facebook Link:</b></td>
                                <td colspan="3">
                                    <a href="{{ $user->facebook }}" target="_blank">{{ $user->facebook }}</a><br/>
                                    <small><i>Chúng tôi sẽ dùng địa chỉ Facebook này để liên hệ trong trường hợp bạn là người thắng cuộc</i></small>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>

                    <h1 class="heading">Bài dự thi</h1>
                    <div class="">
                        @include('frontend.submit.index_items')
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer')
<script>
    function checkScroll(){
        var $height = $('body > section').outerHeight() + $('body > header').outerHeight();

        $("body").css('height', $height + 100);
        $("body").css('background-size', '100% '+ ($height + 150) + 'px');
    }
    checkScroll();
    window.onresize = function(e){
        checkScroll();
    }
</script>
@stop
