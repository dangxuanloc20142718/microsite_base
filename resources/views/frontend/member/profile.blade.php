@extends('layouts.default')

@section('header')
    <title>Trang profile - {{$user->name}}</title>
    <meta name="description" content="Trang profile - {{$user->name}}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}" />
    <meta property="og:title" content="Trang profile - {{$user->name}}" />
    <meta property="og:description" content="{{$submit->introtext}}" />
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
@stop

@section('breadcrum')
@stop

@section('content')
    <div class="container">
        <div class="row">
            <p>Team: {{ $submit->cat_id }}</p>
            <p>Tên: {{ $user->name }}</p>
            <p>Nghề nghiệp: {{ $user->address }}</p>
            <p>Tuổi: {{ $user->birthday }}</p>
        </div>
        <div class="row">
            <img class="image1" name="image1" src="/{{ $submit->image1 }}" style="width: 200px; float: left;">
            <img class="image2" name="image2" src="/{{ $submit->image2 }}" style="width: 200px; float: left;">
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <p>Số lượt vote: {{ $submit->votes }}</p>
        </div>

        <hr>

        <div class="row">
            <img class="image" name="image" src="/{{ $submit->image }}">
        </div>

        <hr>

        <div class="top5-submit">
            <h3>top submit vote cao nhat</h3>
            @foreach($topVote as $item)
                <div class="item-submit" style="border:1px solid #333; float: left;">
                    <p>{{ $item->title }}</p>
                </div>
            @endforeach
            <div class="clearfix"></div>
        </div>
        <div class="other-submit">
            <h3>top submit moi nhat</h3>
            @foreach($topLater as $item)
                <div class="item-submit" style="border:1px solid #333; float: left;">
                    <p>{{ $item->title }}</p>
                </div>
            @endforeach
            <div class="clearfix"></div>
        </div>
    </div>
@stop

@section('footer')
@stop
