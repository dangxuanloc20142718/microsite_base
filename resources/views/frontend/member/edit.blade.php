@extends('layouts.default')

@section('header')
    <title>{{$item->name}}</title>
    <meta name="description" content="{{$item->name}}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}" />
    <meta property="og:title" content="{{$item->name}}" />
    <meta property="og:description" content="{{$item->name}}" />
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
@stop

@section('breadcrum')
@stop

@section('content')
    <div class="container">
        <div class="member top15">
            <div class="row">
                <div class="col-md-3">
                    @include('frontend.member.menu')
                </div>
                <div class="col-md-9">
                    <h1 class="heading">Sửa thông tin cá nhân </h1>
                    <div class="row">
                        <div class="col-md-12">
                            @if ( $errors->any() )
                                <br/>
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                    {!! Form::open( ['url' => 'member/0', 'class' => 'form-horizontal', 'files'=>true] ) !!}
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label required">Họ và tên</label>
                        <div class="col-sm-9">
                            {!! Form::text('name', old('name', $item->name), array('class' => 'form-control', 'required' => '1')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Ảnh đại diện</label>
                        <div class="col-sm-9">
                            {!! Form::file('image_upload') !!}
                            @if($item->avatar != '')
                                <img src="<?php echo URL::to($item->avatar);?>" class="thumbnail" style="width: 80px; margin: 0px">
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label required">Điện thoại</label>
                        <div class="col-sm-9">
                            {!! Form::text('mobile', old('mobile', $item->mobile), array('class' => 'form-control', 'required' => '1')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label required">Email</label>
                        <div class="col-sm-9">
                            {!! Form::text('email', old('email', $item->email), array('class' => 'form-control', 'required' => '1')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-send"></i> Hoàn tất</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer')
@stop
