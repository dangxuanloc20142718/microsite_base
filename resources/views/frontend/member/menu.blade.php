<div class="text-center">
    <a href="{{ url('member') }}">
    {{ Html::image( ($user->avatar) ? $user->avatar : 'assets/img/no-image.jpg', $user->name, array('class' => 'thumbnail', 'style'=>'display:inline-block; width:200px'))  }}
    <br/>
    <b>{{ $user->name }}</b>
        {{--<br/><span class="text-gray">Số dư: {{ number_format($user->balance) }}</span><br/>--}}
    </a>
</div>
{{--
<ul class="nav nav-pills nav-stacked top15">
    <li role="presentation" class="@if(Route::getCurrentRoute()->getPath() == 'member') active @endif"><a href="{{ url('member') }}">Trang Thành viên</a></li>
    <li role="presentation" class="@if(Route::getCurrentRoute()->getPath() == 'member/{id}/edit') active @endif"><a href="{{ url('member/0/edit') }}"> &nbsp;&nbsp;&nbsp; &raquo; Sửa thông tin cá nhân</a></li>
    <li role="presentation" class="@if(Route::getCurrentRoute()->getPath() == 'member/{id}/password') active @endif"><a href="{{ url('member/0/password') }}"> &nbsp;&nbsp;&nbsp; &raquo; Đổi mật khẩu</a></li>
    <li role="presentation" class="hidden @if(Route::getCurrentRoute()->getPath() == 'member/posts') active @endif"><a href="{{ url('member/posts') }}">Quản lý Tin đăng</a></li>
    <li role="presentation" class="hidden @if(Route::getCurrentRoute()->getPath() == 'member/transactions') active @endif"><a href="{{ url('member/transactions') }}">Giao dịch & Nạp tiền</a></li>
</ul>--}}
