@extends('layouts.default')

@section('header')
    <title>{{$item->name}}</title>
    <meta name="description" content="{{$item->name}}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}" />
    <meta property="og:title" content="{{$item->name}}" />
    <meta property="og:description" content="{{$item->name}}" />
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
@stop

@section('breadcrum')
@stop

@section('content')
    <div class="container">
        <div class="member top15">
            <div class="row">
                <div class="col-md-3">
                    @include('frontend.member.menu')
                </div>
                <div class="col-md-9">
                    <h1 class="heading">Đổi mật khẩu </h1>
                    @if(Session::has('message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            @if ( $errors->any() )
                                <br/>
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br/>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                    {!! Form::open( ['url' => 'member/0/password', 'class' => 'form-horizontal', 'files'=>true] ) !!}
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label required">Mật khẩu cũ</label>
                        <div class="col-sm-9">
                            {!! Form::password('password_old', array('class' => 'form-control', 'required')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label required">Mật khẩu mới</label>
                        <div class="col-sm-9">
                            {!! Form::password('password_new', array('class' => 'form-control', 'required')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label required">Nhắc lại mật khẩu mới</label>
                        <div class="col-sm-9">
                            {!! Form::password('password_new_retype', array('class' => 'form-control', 'required')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-send"></i> Hoàn tất</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer')
@stop
