@extends('layouts.default')
@section('header')
    <title>{{ config('custom.seo_title') }}</title>
    <meta name="description" content="{{ config('custom.seo_description') }}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}" />
    <meta property="og:image" content="{{ url('media/system/fbimage.jpg?'.config('custom.version'))  }}" />
    <meta property="og:title" content="{{ config('custom.seo_title') }}" />
    <meta property="og:description" content="{{ config('custom.seo_description') }}" />
    <link type="text/css" href="{{ url('assets/css/home.css?'.config('custom.version')) }}" rel="stylesheet"/>
    <link type="text/css" href="{{ url('assets/css/workshop.css?'.config('custom.version')) }}" rel="stylesheet"/>
    <script src="{{ url('assets/lib/slick/slick.min.js?'.config('custom.version')) }}"></script>
@stop

@section('content')
    <div class="workshop">
        <div class="workshop-title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ws-before">
                            <div class="workshop-img-title">
                                <img src="{{url('assets/images/workshop/title-workshop.png')}}" alt="" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="workshop-form hidden">
            <div class="container-fluid">
                <div class="row flex-box">
                    <div class="col-md-6 col-sm-6 no-padding-left mb">
                        <div class="ws-form">
                            <h5>THÔNG TIN ĐĂNG KÝ WORK SHOP</h5>
                            <form id="contactForm1" class="form-horizontal" autocomplete="off">
                                <div class="form-group">
                                    <label class="col-xs-4 control-label" for="name">Họ và tên</label>
                                    <div class="col-xs-8">
                                        <input class="form-control" type="text" id="name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label" for="dob">Ngày sinh</label>
                                    <div class="col-xs-8">
                                        <input class="form-control" type="date" id="dob" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label" for="phone-number">Số điện thoại</label>
                                    <div class="col-xs-8">
                                        <input class="form-control" type="number" id="phone-number" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label" for="email">Email</label>
                                    <div class="col-xs-8">
                                        <input class="form-control" type="email" id="email" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label" for="fb">Facebook</label>
                                    <div class="col-xs-8">
                                        <input class="form-control" type="text" id="fb" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label" for="identify-nb">Số CMT</label>
                                    <div class="col-xs-8">
                                        <input class="form-control" type="number" id="identify-nb" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label" for="identify-nb">Xác nhận</label>
                                    <div class="col-xs-8">
                                        <div id="captchaForm1" style="transform:scale(0.50);-webkit-transform:scale(0.50);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label" for="identify-nb"></label>
                                    <div class="col-xs-8">
                                        <div class="alert alert-contact d-none"></div>
                                        <a onclick="return $.app.ajax(this, 'lien-he', $('#contactForm1').serialize(), _cbSubmit1, 'POST', '.alert-contact')" class="btn btn-danger">Đăng ký</a>
                                    </div>
                                </div>
                            </form>
                            <script>
                                $(document).ready(function(){
                                    setTimeout(function(){
                                        $.app.recaptcha.init('captchaForm1');
                                    }, 2000);
                                });

                                _cbSubmit1 = function (data) {
                                    if(data.code){
                                        $('#contactForm1 input').val('');
                                        $('#contactForm1 textarea').val('');
                                    }
                                }
                            </script>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 no-padding-left">
                        <img src="{{url('assets/images/workshop/girls.png')}}" alt="" class="img-responsive ws-form-img" >
                    </div>
                    <div class="col-md-6 col-sm-6 no-padding-left desktop">
                        <div class="ws-form">
                            <h5>THÔNG TIN ĐĂNG KÝ WORK SHOP</h5>
                            <form id="contactForm" class="form-horizontal" autocomplete="off">
                                <div class="form-group">
                                    <label class="col-xs-4 control-label" for="name">Họ và tên</label>
                                    <div class="col-xs-8">
                                        <input class="form-control" type="text" name="fullname" id="name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label" for="dob">Ngày sinh</label>
                                    <div class="col-xs-8">
                                        <input class="form-control" type="date" name="birthday" id="dob" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label" for="phone-number">Số điện thoại</label>
                                    <div class="col-xs-8">
                                        <input class="form-control" type="number" name="mobile" id="phone-number" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label" for="email">Email</label>
                                    <div class="col-xs-8">
                                        <input class="form-control" type="email" name="email" id="email" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label" for="fb">Facebook</label>
                                    <div class="col-xs-8">
                                        <input class="form-control" type="text" name="facebook" id="fb" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label" for="identify-nb">Số CMT</label>
                                    <div class="col-xs-8">
                                        <input class="form-control" type="number" name="idnumber" id="identify-nb" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label" for="identify-nb">Xác nhận</label>
                                    <div class="col-xs-8">
                                        <div id="captchaForm" style="transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label" for="identify-nb"></label>
                                    <div class="col-xs-8">
                                        <div class="alert alert-contact d-none"></div>
                                        <a onclick="return $.app.ajax(this, 'lien-he', $('#contactForm').serialize(), _cbSubmit, 'POST', '.alert-contact')" class="btn btn-danger">Đăng ký</a>
                                    </div>
                                </div>
                            </form>
                            <script>
                                $(document).ready(function(){
                                    setTimeout(function(){
                                        $.app.recaptcha.init('captchaForm');
                                    }, 2000);
                                });

                                _cbSubmit = function (data) {
                                    if(data.code){
                                        $('#contactForm input').val('');
                                        $('#contactForm textarea').val('');
                                    }
                                }
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="col-sm-12 list-text-menu">
                <div class="col-md-4 col-xs-4 m-pl-0 m-pr-0">
                    <div class="text-menu icon-hover icon-hover--1">
                        Tham gia <span class="font-gilroy-bold">#sốnghạnhphúc</span> <br>
                        để khám phá bí quyết sống <br>
                        hạnh phúc
                        <img src="{{ url('assets/img_home/icon22.png') }}" alt="image">
                    </div>
                </div>
                <div class="col-md- col-xs-5 m-pl-0 m-pr-0">
                    <div class="text-menu icon-hover icon-hover--1">
                        Đừng bỏ lỡ <span class="font-gilroy-bold">100 tấm vé</span> <br>
                        tham dự workshop dành cho 100 bài dự thi xuất sắc nhất
                        <img src="{{ url('assets/img_home/icon22.png') }}" alt="image">
                    </div>
                </div>
                <div class="col-md-3 col-xs-3 m-pl-0 m-pr-0">
                    <a href="http://dahuong.local/pages/workshop"><div class="hidden home-box-one__img__more home-box-one__img__more--9">Chi tiết workshop</div></a>
                    <a onclick="return $.app.checkLogin('{{ url('bai-du-thi/create') }}');" href="#"><div class="home-box-one__img__more home-box-one__img__more--4" style="display: block; width: 100%; text-align: center">Gửi bài dự thi</div></a>
                </div>
            </div>
        </div>

        <div class="workshop-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-list-cnt">
                            <div class="title-img-bf-af">
                                <img src="{{url('assets/images/workshop/title-workshop2.png')}}" alt="" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-ws">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <img src="{{url('assets/images/workshop/workshop-images2.png')}}" alt="" class=" into-ws-mb-img">
                            <h1 class="title-ws">GIỚI THIỆU WORKSHOP</h1>
                            <p>Workshop “Dạ Hương - Giúp phụ nữ Việt sống hạnh phúc mỗi ngày” là hoạt động trải nghiệm thực tế
                                kết lại những điều Dạ Hương gửi gắm trong suốt chiến dịch “Lan tỏa hạnh phúc” của mình. </p>
                            <p>Workshop mang đến một không gian cởi mở để phụ nữ ở nhiều tầng lớp, ngành nghề, bối cảnh sống
                                khác nhau đều có thể chia sẻ những câu chuyện của bản thân về những trải nghiệm, những điều
                                tích cực khiến họ trở nên lạc quan, hạnh phúc hơn mỗi ngày.</p>
                            <p>Đồng thời người tham gia sẽ có cơ hội lắng nghe câu chuyện của các khách mời về những phương pháp
                                chăm sóc bảo vệ sức khỏe và giữ lửa hạnh phúc hàng ngày cùng Dạ Hương, cách họ vượt qua cẳng thẳng lo lắng,
                                tạo ra đông lực mỗi ngày để đạt được mục tiêu và cảm nhận được niềm vui, hạnh phúc trong cuộc sống. </p>
                            <p>Đặc biệt, mọi phụ nữa tham gia buổi Workshop sẽ có cơ hội tham gia vào các hoạt động trải nghiệm
                                vẽ tranh nghệ thuật với Tipsy Art và tư vấn chăm sóc sức khỏe cá nhân với bác sĩ chuyên gia.</p>
                        </div>
                    </div>
                </div>
                <div class="intro-ws-img">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 no-padding"></div>
                            <div class="col-md-6 col-sm-6 no-padding">
                                <img src="{{url('assets/images/workshop/workshop-images2.png')}}" alt="" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="speaker">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <img src="{{url('assets/images/workshop/chuyengiatamli.png')}}" alt="" class="into-ws-mb-img">
                            <h1 class="title-ws">Diễn giả</h1>
                            <p>Để theo đuổi đam mê giúp phụ nữ sống hạnh phúc, chị Nguyễn Thị Thu Giao quyết định bỏ một chức vụ cao, có thu nhập khủng để sang lập ra G-lounge,
                                nơi chuyên tổ chức các chương trình huấn luyện kỹ năng và phong cách sống cho phụ nữ.
                                Từ tháng năm 2016 tới nay, chị Giao đã tổ chức được hơn 50 sự kiện giúp phụ nữ hạnh phúc hơn, tự tin và mạnh mẽ hơn.</p>
                            <p>Trong buổi Workshop này,  chị Thu Giao sẽ chia sẻ những câu chuyện thực tế để giúp những người phụ nữ đón nhận và trải nghiệm
                                những hạnh phúc xung quanh họ, truyền cảm hứng hạnh phúc lan tỏa trong cộng đồng phụ nữ Việt Nam.
                            </p>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <img src="{{url('assets/images/workshop/workshop-images3_01.png')}}" alt="" class="img-responsive hidden-xs">
                        </div>
                    </div>
                </div>
                <div class="activities-in-ws">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="title-ws">Hoạt động trong WS</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="first-col-act">
                                <div class="first-col-img-box">
                                    <img src="{{url('assets/images/workshop/workshop-images4.png')}}" alt="" class="img-responsive">
                                    <div class="title-img-box-ws">
                                        <img src="{{url('assets/images/workshop/Zone mỹ thuật.png')}}" alt="" class="img-responsive">
                                    </div>
                                </div>
                                <div class="first-col-act-cnt">
                                    <p>Là zone nghệ thuật dành cho những người chưa cầm cọ bao giờ có thể vẽ được một bức tranh treo tường hoàn chỉnh.</p>
                                    <p>Chỉ trong 3 tiếng đồng hồ, các họa sĩ của Tipsy Art sẽ cho người tham dự những hướng dẫn chi tiết và cụ thể để
                                        họ có vẽ theo, học được những kĩ thuật hội họa cơ bản nhất, và hoàn thiện một bức tranh mang về treo tường
                                        hoặc tặng những người thân yêu.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="second-col-act">
                                <div class="second-col-img-box">
                                    <img src="{{url('assets/images/workshop/workshop-images5.png')}}" alt="" class="img-responsive">
                                    <div class="title-img-box-ws">
                                        <img src="{{url('assets/images/workshop/Zone chăm sóc sức khỏe.png')}}" alt="" class="img-responsive">
                                    </div>
                                </div>
                                <div class="second-col-act-cnt">
                                    <p>Với sự tham gia của Giáo sư, Bác sĩ chuyên gia Vũ Bá Quyết, Nguyên Giám đốc Bệnh viện Đại học Y Hà Nội,
                                        Nguyên Phó Giám đốc Bệnh viện Việt Đức, Zone tư vấn chăm sóc sức khỏe dành cho những phụ nữ tham
                                        dự chương trình được cung cấp những phương pháp, kiến thức chăm sóc cho sức khỏe bản thân và những người xung quanh. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="time-location">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="title-ws">Thời gian địa điểm</h1>
                        </div>
                    </div>
                    <div class="row flex-box">
                        <div class="col-md-6 col-sm-6">
                            <div class="left-col-time-location">
                                <img src="{{url('assets/images/workshop/woman.png')}}" alt="" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="right-col-time-location">
                                <p>Workshop sẽ được diễn ra vào ngày <span> 21/10</span></p>
                                <p>tại địa điểm: <span style="font-size:15px;">BKhup, Tầng 3, 17A, <br/>Tạ Quang Bửu, Bách Khoa, Hai Bà Trưng, Hà Nội</span></p>
                                <p>từ <span class="time">14:30</span> đến <span class="time">17:00</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="join-btn">
                    <a onclick="return $.app.checkLogin('{{ url('bai-du-thi/create') }}');" href="#"><span>Đăng kí tham gia ngay</span></a>
                </div>
            </div>
        </div>

    </div>
@stop

@section('footer')
@stop