@extends('layouts.default')
@section('header')
    <title>{{ config('custom.seo_title') }}</title>
    <meta name="description" content="{{ config('custom.seo_description') }}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}" />
    <meta property="og:image" content="{{ url('media/system/fbimage.jpg?'.config('custom.version'))  }}" />
    <meta property="og:title" content="{{ config('custom.seo_title') }}" />
    <meta property="og:description" content="{{ config('custom.seo_description') }}" />

    <link type="text/css" href="{{ url('assets/css/home.css?'.config('custom.version')) }}" rel="stylesheet"/>
    <link type="text/css" href="{{ url('assets/css/hanhphuccuatoila.css?'.config('custom.version')) }}" rel="stylesheet"/>
    <script src="{{ url('assets/lib/slick/slick.min.js?'.config('custom.version')) }}"></script>
@stop

@section('content')
<div class="hanhphucctl">
    <div class="container">
        <div class="row">
            <div class="img-emotion">
                <div class="col-sm-12">
                    <div class="col-sm-5 col-xs-12 my-image ">
                        <h5>Gửi ảnh của bạn<br>tại đây nhé!</h5>
                        <div class="img-view-box">
                            <img src="{{url('assets/images/hanhphuccuatoila/pink-bg.png')}}" class="img-responsive" alt="">
                            <div class="overlay-view-box"></div>
                            <div class="chonanh">
                                <a href="#">
                                Chọn ảnh 
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-xs-12 my-happy">
                    <h5>Hạnh phúc<br> trong bạn là gì?</h5>
                        <div class="happy-view-box">
                            <div class="happy-text"> <textarea name="" id="lined" cols="50" rows="5"></textarea></div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="last-row-btn">
                <div class="col-sm-12">
                    <div class="custom-btn">
                        <a href="{{ url('pages/thelegiaithuong') }}"><span>Thể lệ và cách thức tham gia</span></a>
                    </div>
                </div>
            </div>  
        </div>
        <div class="row">
            <div class="list-frame">
                <div class="col-sm-12">
                    <div class="slide-title"> <h5>Lựa chọn khung ảnh hạnh phúc của bạn tại đây nhé !</h5><span><img src="{{ url('assets/images/hanhphuccuatoila/camera.png ') }}" alt=""></span></div>
                    <div class="frame-slider">
                        @for ($i = 0; $i < 6; $i++)
                            <div class="item">
                                @for ($j = 0; $j < 2; $j++)
                                    <div class="item-content">
                                        <div class="frame-container">
                                            <img src="{{ url('assets/images/hanhphuccuatoila/frame.jpg') }}" width="100%"/>
                                            <div class="overlay"></div>
                                            <div class="select-button"><a href="#"> Chọn </a></div>
                                        </div>
                                    </div>
                                @endfor
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="hanhphuc-title">
                <div class="container">
                    <div class="col-sm-12">
                        <div class="join-btn hidden">
                            <a href="#"><span>Đăng nhập và chia sẻ hạnh phúc này ngay cho mọi người nào</span></a>
                        </div>
                    <div class="hanhphuc-img-title">
                            <img src="{{url('assets/images/hanhphuccuatoila/trienlam-title.png')}}" alt="">
                        </div>
                        <h5>Lưu giữ và lan tỏa những hạnh phúc mỗi ngày cùng dạ hương nhé</h5>
                        
                    </div>
                </div>
            </div> 
            <div class="trienlam-post-list">
                <div class="col-sm-12 tl-container">
                    @foreach($submits as $submit)
                        <div class="col-sm-4">
                            <div class="trienlam-list-item" >
                                <a href="#" target="_blank">
                                    <div class="trienlam-list-image-box">
                                        <img src="{{url($submit->image)}}" width="100%"/>
                                    </div>
                                    <div class="trienlam-list-ctn-box">
                                        <div class="like-share">
                                            <div class="like-nub">
                                                <a onclick="return $.app.contest.vote({{ $submit->id }});" href="#">
                                                    <img src="{{url('assets/images/hanhphuccuatoila/like-icon.png')}}" alt="">
                                                    <span class="submit-votes-{{ $submit->id }}">{{ $submit->votes }}</span>
                                                </a>
                                            </div>
                                            <div class="share-nub">
                                                <a onclick="return $.app.contest.share('{{ url($submit->link) }}', '{{ $submit->id }}');" href="#">
                                                    <img src="{{url('assets/images/hanhphuccuatoila/share-icon.png')}}" alt="">
                                                    <span class="submit-shares-{{ $submit->id }}">{{ $submit->shares }}</span>
                                                </a>
                                            </div>

                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            <div class="last-row-btn">
                <div class="custom-btn">
                    <a href="{{ url('bai-du-thi') }}"><span>Xem tất cả</span></a>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        function slickInit() {
            $('.frame-slider').slick({
                dots: false,
                infinite: true,
                // speed: 500,
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: false,
                // autoplaySpeed: 2000,
                arrows: true,
                responsive: [{
                breakpoint: 320,
                settings: {
                    arrows: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
                },
                {
                breakpoint: 767,
                settings: {
                    arrows: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
                },
                {
                breakpoint: 768,
                settings: {
                    arrows: true,
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
                }]
            });
        }
        slickInit();

    });
</script>
@stop

@section('footer')
@stop