@extends('layouts.default')
@section('header')
    <title>{{ config('custom.seo_title') }}</title>
    <meta name="description" content="{{ config('custom.seo_description') }}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}" />
    <meta property="og:image" content="{{ url('media/system/fbimage.jpg?'.config('custom.version'))  }}" />
    <meta property="og:title" content="{{ config('custom.seo_title') }}" />
    <meta property="og:description" content="{{ config('custom.seo_description') }}" />
    <link type="text/css" href="{{ url('assets/css/home.css?'.config('custom.version')) }}" rel="stylesheet"/>
    <link type="text/css" href="{{ url('assets/css/trienlamhanhphuc.css?'.config('custom.version')) }}" rel="stylesheet"/>
    <script src="{{ url('assets/lib/slick/slick.min.js?'.config('custom.version')) }}"></script>
@stop

@section('content')
<div class="trienlamhp">
    <div class="trienlam-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="trienlam-img-title">
                        <img src="{{url('assets/images/trienlamhanhphuc/trienlam-title.png')}}" alt="">
                    </div>
                    <h5>Lưu giữ và lan tỏa những hạnh phúc mỗi ngày cùng Dạ Hương nhé</h5>
                    <div class="join-btn">
                        <a onclick="return $.app.checkLogin('{{ url('bai-du-thi/create') }}')" href="#"><span>Chia sẻ hạnh phúc của bạn ngay nhé</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="trienlam-post">--}}
        {{--<div class="container">--}}
            {{--@foreach($submits as $k => $submit)--}}
                {{--@if($k == 0 || $k == 6)--}}
                    {{--<div class="row flex-box list-row">--}}
                        {{--<div class="col-md-5 col-sm-5">--}}
                            {{--<div class="bg-pink post-lg">--}}
                                {{--<div class="trienlam-post-box">--}}
                                    {{--<div class="trienlam-img-box">--}}
                                        {{--<a href="{{ $submit->link }}">--}}
                                            {{--<img src="{{url($submit->image)}}" alt="" class="img-responsive">--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                    {{--<div class="trienlam-cnt-box">--}}
                                        {{--<div class="col-md-9 no-padding">--}}
                                            {{--<h5><a href="{{ $submit->link }}">{{ $submit->title }}</a></h5>--}}
                                            {{--<p>{{ $submit->introtext }}</p>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-3 no-padding padding-15 like-share-checked">--}}
                                            {{--<div class="like-number">--}}
                                                {{--<a onclick="return $.app.contest.vote({{ $submit->id }});" href="#">--}}
                                                    {{--<img src="{{url('assets/images/trienlamhanhphuc/like-icon.png')}}" alt="">--}}
                                                    {{--<span class="submit-votes-{{ $submit->id }}">{{ $submit->votes }}</span>--}}
                                                {{--</a>--}}
                                            {{--</div>--}}
                                            {{--<div class="share-number">--}}
                                                {{--<a onclick="return $.app.contest.share('{{ url($submit->link) }}', {{ $submit->id }});" href="#">--}}
                                                    {{--<img src="{{url('assets/images/trienlamhanhphuc/share-icon.png')}}" alt="">--}}
                                                    {{--<span class="submit-shares-{{ $submit->id }}">{{ $submit->shares }}</span>--}}
                                                {{--</a>--}}
                                            {{--</div>--}}
                                            {{--<div class="checked-number hidden">--}}
                                                {{--<a href="#">--}}
                                                    {{--<img src="{{url('assets/images/trienlamhanhphuc/checked-icon.png')}}" alt="">--}}
                                                    {{--1000--}}
                                                {{--</a>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="clear"></div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-7 col-sm-7">--}}
                            {{--@foreach($submits as $j => $submit)--}}
                                {{--@if( ($j == 1 || $j == 2) && $j <= $k+3 )--}}
                            {{--<div class="bg-pink post-sm">--}}
                                {{--<div class="row flex-box">--}}
                                    {{--<div class="col-md-6 col-sm-6">--}}
                                        {{--<div class="trienlam-post-box">--}}
                                            {{--<div class="trienlam-img-box">--}}
                                                {{--<a href="{{ $submit->link }}">--}}
                                                    {{--<img src="{{ url($submit->image) }}" alt="" class="img-responsive">--}}
                                                {{--</a>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-6 col-sm-6">--}}
                                        {{--<div class="trienlam-cnt-box">--}}
                                            {{--<div class="row ">--}}
                                                {{--<div class="col-md-12">--}}
                                                    {{--<h5><a href="{{ $submit->link }}">{{ $submit->title }}</a></h5>--}}
                                                    {{--<p>{{ $submit->introtext }}</p>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-md-12 like-share-checked">--}}
                                                    {{--<div class="like-number">--}}
                                                        {{--<a onclick="return $.app.contest.vote({{ $submit->id }});" href="#">--}}
                                                            {{--<img src="{{url('assets/images/trienlamhanhphuc/like-icon.png')}}" alt="">--}}
                                                            {{--<span class="submit-votes-{{ $submit->id }}">{{ $submit->votes }}</span>--}}
                                                        {{--</a>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="share-number">--}}
                                                        {{--<a onclick="return $.app.contest.share('{{ url($submit->link) }}', '{{ $submit->id }}');" href="#">--}}
                                                            {{--<img src="{{url('assets/images/trienlamhanhphuc/share-icon.png')}}" alt="">--}}
                                                            {{--<span class="submit-shares-{{ $submit->id }}">{{ $submit->shares }}</span>--}}
                                                        {{--</a>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="checked-number hidden">--}}
                                                        {{--<a href="#">--}}
                                                            {{--<img src="{{url('assets/images/trienlamhanhphuc/checked-icon.png')}}" alt="">--}}
                                                            {{--1000--}}
                                                        {{--</a>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="clear"></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                                {{--@endif--}}
                            {{--@endforeach--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--@elseif($k == 3 || $k == 9)--}}
                    {{--<div class="row flex-box list-row">--}}
                        {{--<div class="col-md-7 col-sm-7">--}}
                            {{--@foreach($submits as $j => $submit)--}}
                            {{--@if(($j == 3 || $j == 4 || $j == 9 || $j == 10) && $j <= $k+2 )--}}
                                {{--<div class="bg-pink post-sm">--}}
                                    {{--<div class="row flex-box">--}}
                                        {{--<div class="col-md-6 col-sm-6">--}}
                                            {{--<div class="trienlam-post-box">--}}
                                                {{--<div class="trienlam-img-box">--}}
                                                    {{--<a href="{{ $submit->link }}">--}}
                                                        {{--<img src="{{ url($submit->image) }}" alt="" class="img-responsive">--}}
                                                    {{--</a>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-6 col-sm-6">--}}
                                            {{--<div class="trienlam-cnt-box">--}}
                                                {{--<div class="row ">--}}
                                                    {{--<div class="col-md-12">--}}
                                                        {{--<h5><a href="{{ $submit->link }}">{{ $submit->title }}</a></h5>--}}
                                                        {{--<p>{{ $submit->introtext }}</p>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-12 like-share-checked">--}}
                                                        {{--<div class="like-number">--}}
                                                            {{--<a onclick="return $.app.contest.vote({{ $submit->id }});" href="#">--}}
                                                                {{--<img src="{{url('assets/images/trienlamhanhphuc/like-icon.png')}}" alt="">--}}
                                                                {{--<span class="submit-votes-{{ $submit->id }}">{{ $submit->votes }}</span>--}}
                                                            {{--</a>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="share-number">--}}
                                                            {{--<a onclick="return $.app.contest.share('{{ url($submit->link) }}', '{{ $submit->id }}');" href="#">--}}
                                                                {{--<img src="{{url('assets/images/trienlamhanhphuc/share-icon.png')}}" alt="">--}}
                                                                {{--<span class="submit-shares-{{ $submit->id }}">{{ $submit->shares }}</span>--}}
                                                            {{--</a>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="checked-number hidden">--}}
                                                            {{--<a href="#">--}}
                                                                {{--<img src="{{url('assets/images/trienlamhanhphuc/checked-icon.png')}}" alt="">--}}
                                                                {{--1000--}}
                                                            {{--</a>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="clear"></div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--@endif--}}
                            {{--@endforeach--}}
                        {{--</div>--}}
                        {{--<div class="col-md-5 col-sm-5">--}}
                            {{--<div class="bg-pink post-lg">--}}
                                {{--@foreach($submits as $j => $submit)--}}
                                    {{--@if( ($j == 5 || $j == 11) && $j <= $k+2 )--}}
                                    {{--<div class="trienlam-post-box">--}}
                                        {{--<div class="trienlam-img-box">--}}
                                            {{--<a href="{{ $submit->link }}">--}}
                                                {{--<img src="{{ url($submit->image)  }}" alt="" class="img-responsive">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="trienlam-cnt-box">--}}
                                            {{--<div class="col-md-9 no-padding">--}}
                                                {{--<h5><a href="{{ $submit->link }}">{{ $submit->title }}</a></h5>--}}
                                                {{--<p>{{ $submit->introtext }}</p>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-3 no-padding padding-15 like-share-checked">--}}
                                                {{--<div class="like-number">--}}
                                                    {{--<a onclick="return $.app.contest.vote({{ $submit->id }});" href="#">--}}
                                                        {{--<img src="{{url('assets/images/trienlamhanhphuc/like-icon.png')}}" alt="">--}}
                                                        {{--<span class="submit-votes-{{ $submit->id }}">{{ $submit->votes }}</span>--}}
                                                    {{--</a>--}}
                                                {{--</div>--}}
                                                {{--<div class="share-number">--}}
                                                    {{--<a onclick="return $.app.contest.share('{{ url($submit->link) }}', '{{ $submit->id }}');" href="#">--}}
                                                        {{--<img src="{{url('assets/images/trienlamhanhphuc/share-icon.png')}}" alt="">--}}
                                                        {{--<span class="submit-shares-{{ $submit->id }}">{{ $submit->shares }}</span>--}}
                                                    {{--</a>--}}
                                                {{--</div>--}}
                                                {{--<div class="checked-number hidden">--}}
                                                    {{--<a href="#">--}}
                                                        {{--<img src="{{url('assets/images/trienlamhanhphuc/checked-icon.png')}}" alt="">--}}
                                                        {{--1000--}}
                                                    {{--</a>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="clear"></div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--@endif--}}
                                {{--@endforeach--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--@endif--}}
            {{--@endforeach--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="content content__two">
        <div class="container">
            <div class="row slick-config">
                <div class="col-sm-12">
                    <div class="row cnt-bdt">
                        @foreach($submits as $submit)
                            <div class="col-sm-4 col-xs-6 box-two">
                                <a href="{{ $submit->link }}">
                                    <div style="border-radius:20px"><img src="{{ url($submit->image) }}" width="100%" class="slick4-img"></div>
                                </a>
                                <div class="row">
                                    <div class="col-sm-9 col-xs-9">
                                        <div class="box-two__text">
                                            <a href="{{ $submit->link }}">{{ $submit->introtext }}</a>
                                        </div>
                                        <div class="box-two__name">
                                            <div class="circle"></div>
                                            {{ $submit->username }}
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 text-center pl-0">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <a class="hover-vote" onclick="return $.app.contest.vote('{{ $submit->id }}');" href="#"><img src="{{ url('assets/img_home/vote.png') }}" alt="image" width="100%" class="mt-10"></a>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="box-two__number">{{ $submit->votes }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
@stop