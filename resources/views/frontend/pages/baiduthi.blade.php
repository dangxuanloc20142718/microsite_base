@extends('layouts.default')
@section('header')
    <title>{{ config('custom.seo_title') }}</title>
    <meta name="description" content="{{ config('custom.seo_description') }}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}"/>
    <meta property="og:image" content="{{ url($submit->image) . '?v='.config('custom.version') }}"/>
    <meta property="og:image:secure_url" content="{{ url($submit->image) . '?v='.config('custom.version') }}"/>
    <meta property="og:image:width" content="600"/>
    <meta property="og:image:height" content="600"/>
    <meta property="og:title" content="{{ $submit->title }} - {{ config('custom.seo_title') }}"/>
    <meta property="og:description" content="{{ config('custom.seo_description') }}"/>

    <link type="text/css" href="{{ url('assets/css/home.css?'.config('custom.version')) }}" rel="stylesheet"/>
    <link type="text/css" href="{{ url('assets/css/bai-du-thi-new.css?'.config('custom.version')) }}" rel="stylesheet"/>
    <script src="{{ url('assets/lib/slick/slick.min.js?'.config('custom.version')) }}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.1/jquery.fancybox.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.1/jquery.fancybox.min.js"></script>
    <script src="{{ url('assets/js/jquery.sticky-kit.js?'.config('custom.version')) }}"></script>
@stop

@section('content')
    <link rel="stylesheet" href="{{ url('assets/lib/mediaplayer/mediaelementplayer.min.css') }}" type="text/css"/>
    <script src="{{ url('assets/lib/mediaplayer/mediaelement-and-player.js') }}"></script>

    <div class="bai-du-thi">
        <div class="top-img-banner">
            <img src="{{ url($submit->image) }}" alt="" width="100%">
            <img src="{{ url('assets/images/baiduthi/FRAME COVER.png') }}" alt="" width="100%" class="frame">

        </div>
        <div class="bdt-chi-tiet">
            <div class="bdt-chi-tiet-section1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-5">
                            <div class="border-img">
                                <div class="avatar-box">
                                    <img src="{{ url($user->avatar) }}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-0 col-sm-6 col-sm-offset-1 col-xs-7">
                            <div class="quote-of-user">
                                <h5>{{ $user->name }}</h5>
                            </div>
                            <div class="quote-cnt">
                                <p>{{ $submit->introtext }}</p>
                            </div>
                        </div>
                        <div class="col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-0 col-xs-7 col-xs-offset-5">
                            <div class="bdt-like-share-vote">
                                <div class="like-bdt-btn">
                                    <a onclick="return $.app.contest.vote({{ $submit->id }});" href="#">
                                        <img src="{{url('assets/images/baiduthi/like-icon-white.png')}}" alt="">
                                        Vote
                                    </a>
                                    <p class="like-share-vote-nb">{{ $submit->votes }}</p>
                                </div>
                                <div class="share-bdt-btn">
                                    <a onclick="return $.app.contest.share('{{ url($submit->link) }}', '{{ $submit->id }}');"
                                       href="#">
                                        <img src="{{url('assets/images/baiduthi/share-icon-white.png')}}" alt="">
                                        Share
                                    </a>
                                    <p class="like-share-vote-nb">{{ $submit->shares }}</p>
                                </div>
                                {{--<div class="vote-bdt-btn">--}}
                                {{--<a href="#">--}}
                                {{--<img src="{{url('assets/images/baiduthi/check-icon-white.png')}}" alt="">--}}
                                {{--Vote--}}
                                {{--</a>--}}
                                {{--<p class="like-share-vote-nb">350</p>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bdt-chi-tiet-section2">
                <div class="container">
                    <div class="row ">
                        <div class="col-md-6 col-sm-6 sticky-kit">
                            <div class="cnt-left">
                                <h5>Câu chuyện hạnh phúc của tôi</h5>
                                <div class="like-share-voted-number">
                                    <div class="voted-number">
                                        <a  onclick="return $.app.contest.vote({{ $submit->id }});" href="#">
                                            <div class="voted">
                                                Vote
                                            </div>
                                            <p>{{ $submit->votes }}</p>
                                        </a>
                                    </div>
                                    {{--<div class="like-number">--}}
                                    {{--<a href="#">--}}
                                    {{--<div class="like">--}}
                                    {{--Like--}}
                                    {{--</div>--}}
                                    {{--<p>350</p>--}}
                                    {{--</a>--}}
                                    {{--</div>--}}
                                    <div class="share-number">
                                        <a onclick="return $.app.contest.share('{{ url($submit->link) }}', '{{ $submit->id }}');" href="#">
                                            <div class="share">
                                                Share
                                            </div>
                                            <p>{{ $submit->shares }}</p>
                                        </a>
                                    </div>
                                </div>
                                <p style="white-space: pre-wrap;">{{ $submit->fulltext }}</p>
                            </div>

                        </div>
                        <div class="col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1">
                            <div class="cnt-right">
                                @if ($submit->youtube)
                                    <div class="ifram-container">
                                        <video class="youtube_player_detail" playsinline id="youtube_player_detail">
                                            <source src="{{ $submit->youtube }}" type="video/youtube">
                                            Your browser does not support the video tag.
                                        </video>
                                    </div>
                                @endif

                                @if ($submit->desc_images)
                                    <?php $descImgs = explode(',', $submit->desc_images); ?>
                                    @foreach ($descImgs as $i)
                                        <a data-fancybox="gallery" href="{{ url($i) }}"><img src="{{ url($i) }}" width="100%"></a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="bdt-other">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="others-title">
                            <div class="title-container">
                                <img src="{{url('assets/images/baiduthi/Các bài dự thi khác.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content content__two">
                <div class="container">
                    <div class="row slick-config">
                        <div class="col-sm-12">
                            <div class="row cnt-bdt">
                                @foreach($submits as $submit)
                                    <div class="col-sm-4 col-xs-6 box-two">
                                        <a href="{{ $submit->link }}">
                                            <div style="border-radius:20px"><a href="{{ $submit->link }}"><img src="{{ url($submit->image) }}" width="100%" class="slick4-img"></a></div>
                                        </a>
                                        <div class="row">
                                            <div class="col-sm-9 col-xs-9">
                                                <div class="box-two__text">
                                                    <a href="{{ $submit->link }}">{{ $submit->introtext }}</a>
                                                </div>
                                                <div class="box-two__name">
                                                    <div class="circle"></div>
                                                    {{ isset($users[$submit->user_id]) ? $users[$submit->user_id]->name : '' }}
                                                </div>
                                            </div>
                                            <div class="col-sm-3 col-xs-3 text-center pl-0">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <a onclick="return $.app.contest.vote({{ $submit->id }});" href="#"><img src="{{ url('assets/img_home/vote.png') }}" alt="image" width="100%" class="mt-10"></a>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="box-two__number submit-votes-{{ $submit->id }}">{{ $submit->votes }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pagination-container">
                            <ul class="pagination">
                                {{--<li class="prev-part"><a href="#"><img--}}
                                                {{--src="{{url('assets/images/baiduthi/left-pagination.png')}}" alt=""></a>--}}
                                {{--</li>--}}
                                {{--<li><a href="#">1</a></li>--}}
                                {{--<li class="active"><a href="#">2</a></li>--}}
                                {{--<li><a href="#">3</a></li>--}}
                                {{--<li><a href="#">4</a></li>--}}
                                {{--<li><a href="#">5</a></li>--}}
                                {{--<li class="next-part"><a href="#"><img--}}
                                                {{--src="{{url('assets/images/baiduthi/right-pagination.png')}}" alt=""></a>--}}
                                {{--</li>--}}
                            </ul>
                            <a href="{{ url('bai-du-thi') }}" class="findall">Xem tất cả</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    {{--<script>--}}
    {{--(function ($) {--}}
    {{--jQuery('.sticky-kit').stick_in_parent();--}}
    {{--if(jQuery(window).width() < 768 ){--}}
    {{--jQuery('.sticky-kit').trigger("sticky_kit:detach");--}}
    {{--}--}}
    {{--})(jQuery);--}}
    {{--</script>--}}
@stop
@section('footer')
@stop