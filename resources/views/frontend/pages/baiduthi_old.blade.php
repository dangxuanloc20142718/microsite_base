@extends('layouts.default')
@section('header')
    <title>{{ $submit->title }} - {{ config('custom.seo_title') }}</title>
    <meta name="description" content="{{ config('custom.seo_description') }}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}" />
    <meta property="og:image" content="{{ url($submit->image) . '?v='.config('custom.version') }}" />
    <meta property="og:image:secure_url" content="{{ url($submit->image) . '?v='.config('custom.version') }}" />
    <meta property="og:image:width" content="600" />
    <meta property="og:image:height" content="600" />
    <meta property="og:title" content="{{ $submit->title }} - {{ config('custom.seo_title') }}" />
    <meta property="og:description" content="{{ config('custom.seo_description') }}" />

    <link type="text/css" href="{{ url('assets/css/home.css?'.config('custom.version')) }}" rel="stylesheet"/>
    <link type="text/css" href="{{ url('assets/css/baiduthi.css?'.config('custom.version')) }}" rel="stylesheet"/>
    <script src="{{ url('assets/lib/slick/slick.min.js?'.config('custom.version')) }}"></script>
@stop

@section('content')
<div id="bdt" class="baiduthi">
    <div class="baiduthi-container">
        <div class="bg-left"><img src="{{url('assets/images/baiduthi/left-img.png')}}" width="75%" alt=""></div>
        <div class="bg-top-right"><img src="{{url('assets/images/baiduthi/top-right-img.png')}}" width="75%" alt=""></div>
        <div class="bg-bottom-right"><img src="{{url('assets/images/baiduthi/right-bottom-img.png')}}" width="75%" alt=""></div>
        <div class="container">
            <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 ">
                <div class="bg-pink bdt-bg post-xs post-sm post-lg">
                    <div class="baiduthi-box">
                        <div class="baiduthi-img-box">
                            <a href="#">
                                <img src="{{url($submit->image)}}" alt="" class="img-responsive">
                            </a>
                        </div>
                        <div class="baiduthi-ctn-box">
                            <div class="col-md-9 no-padding">
                                <h5><a href="#">{{ $submit->title }}</a></h5>
                                <p>{{ $submit->introtext }}</p>
                            </div>
                            <div class="col-md-3 no-padding padding-15">
                                <div class="like-number">
                                    <a onclick="return $.app.contest.vote({{ $submit->id }});" href="#">
                                        <img src="{{url('assets/images/baiduthi/like-icon.png')}}" alt="">
                                        <span class="submit-votes-{{ $submit->id }}">{{ $submit->votes }}</span>
                                    </a>
                                </div>
                                <div class="share-number">
                                    <a onclick="return $.app.contest.share('{{ url($submit->link) }}', '{{ $submit->id }}');" href="#">
                                        <img src="{{url('assets/images/baiduthi/share-icon.png')}}" alt="">
                                        <span class="submit-shares-{{ $submit->id }}">{{ $submit->shares }}</span>
                                    </a>
                                </div>
                                <div class="voted-number hidden">
                                    <a href="#">
                                        <img src="{{url('assets/images/baiduthi/voted-icon.png')}}" alt="">
                                        1000
                                    </a>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="bdt-like-share like-share-checked">
                        {{--<a href="#">
                            <img src="{{url('assets/images/baiduthi/like2.png')}}" alt="">
                            Like
                        </a>--}}
                        <a onclick="return $.app.contest.vote({{ $submit->id }});" href="#">
                            <img src="{{url('assets/images/baiduthi/vote2.png')}}" alt="">
                            Vote
                        </a>
                        <a onclick="return $.app.contest.share('{{ url($submit->link) }}', '{{ $submit->id }}');" href="#">
                            <img src="{{url('assets/images/baiduthi/share2.png')}}" alt="">
                            Share
                        </a>

                    </div>
                    <div class="join-btn">
                        <a href="#" onclick="return $.app.checkLogin('{{ url('bai-du-thi/create') }}');"><span>Chia sẻ hạnh phúc của bạn ngay nhé</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('footer')
@stop