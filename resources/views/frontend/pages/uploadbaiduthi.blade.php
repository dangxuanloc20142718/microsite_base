@extends('layouts.default')
@section('header')
    <title>{{ config('custom.seo_title') }} - Gửi bài dự thi</title>
    <meta name="description" content="{{ config('custom.seo_description') }}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}" />
    <meta property="og:image" content="{{ url('media/system/fbimage.jpg?'.config('custom.version'))  }}" />
    <meta property="og:title" content="{{ config('custom.seo_title') }}" />
    <meta property="og:description" content="{{ config('custom.seo_description') }}" />
    <link type="text/css" href="{{ url('assets/css/home.css?'.config('custom.version')) }}" rel="stylesheet"/>
    <link type="text/css" href="{{ url('assets/css/uploadbaiduthi.css?'.config('custom.version')) }}" rel="stylesheet"/>
    <script src="{{ url('assets/lib/slick/slick.min.js?'.config('custom.version')) }}"></script>

    <link rel="stylesheet" href="{{ url('assets/lib/cropper/cropper.css') }}"/>
    <script src="{{ url('assets/lib/cropper/cropper.js') }}"></script>
@stop

@section('content')

<style type="text/css">
    .btn-remove-img {
        position: absolute;
        top: 5px;
        right: 5px;
        color: red;
        font-size: 20px;
        cursor: pointer;
    }
</style>

<div id="upload-bdt" class="upload-baiduthi">
    <div class="upload-container">
        <div class="container">
            <form id="uploadForm" class="form-horizontal" autocomplete="off">
                <div class="row">
                    <div class="cover-box">
                        <img style="position: relative" src="{{url('assets/images/baiduthi/frame-cover.png')}}" class="img-responsive">
                        <img id="image_value2" style="position: absolute; left:0; top:0; z-index: -1; display:none;height: 100%;
    margin: auto; " src="" class="img-responsive">
                        <div class="section-upload" >
                            <div style="width:100%; float: left">
                                <a href="#" onclick="$('#image_file1').trigger('click'); return false;">
                                    <img src="{{ url('assets/images/hanhphuccuatoila/cam.png') }}" style="width:20%;cursor:pointer"/><br/>
                                    Tải ảnh Cover của bạn lên<br/>Kích thước tối thiểu 1900px..
                                </a>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="cover" value="" id="image_value1"/>

                    <!-- Modal -->
                    <div id="cropModal" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Chọn ảnh Cover</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="alert alert-success alert-cover">Bạn vui lòng đợi, file đang được xử lý...</div>
                                    <div class="clearfix"></div>

                                    <div>
                                        <input class="pull-left" type="file" name="image_file1" id="image_file1">
                                        <button onclick="$('#image_value2').attr('src', $('#image_value1').val()).show();" type="button" class="btn-ok pull-right btn btn-primary" data-dismiss="modal">OK, sử dụng ảnh này <i class="fa fa-chevron-right"></i></button>
                                    </div>
                                    <div class="clearfix"><br/><br/></div>

                                    <div class="image_crop1_wrap">
                                        <img id="image_crop1" src="{{url('assets/images/baiduthi/frame-cover.png')}}" class="img-responsive">
                                    </div>
                                    <div class="clearfix"><br/></div>
                                </div>
                                <div class="modal-footer">
                                    <button onclick="$('#image_value2').attr('src', $('#image_value1').val()).show();" type="button" class="hidden btn-ok btn btn-primary" data-dismiss="modal">OK, sử dụng ảnh này</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<div class="first-section">
                        <div class="col-md-12 col-sm-12 col-xs-12 cover">
                            <div class="cover-box" style="margin-bottom:0px;">
                                <div class="cover-frame ">
                                    <img src="{{url('assets/images/baiduthi/frame-cover.png')}}" width="100%" alt="">
                                </div>
                                <div class="click-upload">
                                    <img onclick="return $.app.upload.uploadImage('.progressbar', _cbCover, {minWidth: 1500});" src="{{url('assets/images/hanhphuccuatoila/cam.png')}}" width="100%" alt="">
                                    <h6>Yêu cầu ảnh chất lượng cao có chiều rộng tối thiểu 1500px
                                        <div class="progressbar"></div>
                                    </h6>
                                    <script>
                                        function _cbCover(res) {
                                            if(res.code){
                                                $('.img-ctn img').attr('src', $.app.vars.url + res.link).show();
                                                $('#cover_image').val(res.link);
                                            }
                                        }
                                    </script>
                                </div>
                                <div class="img-ctn">
                                    <img src="{{url('assets/images/baiduthi/img.png')}}" style="display:none" width="100%" alt="">
                                    <input type="hidden" name="cover" id="cover_image" value="">
                                </div>
                            </div>
                        </div>
                    </div>--}}
                </div>
                <div class="row">
                    <div class="second-section">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-7 col-sm-7 col-xs-12 emotion-part">
                                <div class="row top-section">
                                    <div class="emotion-title"> <h5>Hạnh phúc trong bạn là gì? *</h5></div>
                                    <div class="happy-view-box">
                                        <!-- <img src="{{url('assets/images/hanhphuccuatoila/frame1.png')}}" class="img-responsive" alt=""> -->
                                        <div class="happy-text"> 
                                            <textarea style="width:100%" placeholder="Tối đa 80 kí tự..." name="introtext" id="lined" cols="50" rows="5"></textarea>
                                            <img src="{{url('assets/images/hanhphuccuatoila/ddvspn2.png')}}" class="happy-img" alt="image">
                                        </div>
                                    </div>
                                </div>
                                <div class="row bottom-section">
                                    <div class="emotion2-title"> <h5>Câu chuyện hạnh phúc của bạn *</h5></div>
                                    <div class="emotion-box">
                                        <!-- <img src="{{url('assets/images/hanhphuccuatoila/frame1.png')}}" class="img-responsive" alt=""> -->
                                        <div class="happy-text"> 
                                            <textarea style="width:100%" placeholder="Chi tiết câu chuyện hạnh phúc của bạn..." name="fulltext" id="lined" cols="50" rows="8"></textarea>
                                            <img src="{{url('assets/images/hanhphuccuatoila/ddvspn2.png')}}" class="happy-img" alt="image">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-12">
                                <div class="row top-section">
                                    <div class="upload-part">
                                        <div class="upload-video-title"><h5>Video</h5></div>
                                        <div class="col-xs-12 upload-video-ctn upload-ctn">
                                            <div class="col-sm-5 col-xs-5 left-part">
                                                <img src="{{url('assets/images/baiduthi/camera-video.png')}}" width="100%" alt="">
                                                <h6>Youtube Video<br><span>(Dán link youtube video của bạn vào ô bên cạnh)</span></h6>
                                            </div>
                                            <div class="col-md-7 col-sm-7 col-xs-7 right-part no-padding">
                                                <textarea  name="youtube" id="txtbox" cols="50" rows="5"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="row bottom-section">
                                    <div class="upload-part">
                                        <div class="upload-image-title"><h5>Ảnh mô tả cho câu chuyện của bạn <span class="small-text"></span></h5></div>

                                        <div class="col-xs-12">
                                            <div class="progressbar-desc"></div>
                                        </div>

                                        <div class="col-xs-12 upload-image-ctn upload-ctn">
                                            <div class="col-sm-6 col-xs-6 left-part">
                                                <img src="{{url('assets/images/hanhphuccuatoila/cam.png')}}" width="100%" alt="">
                                                <h6>File ảnh<br><span>(JPG, JPEG, PNG, dung lượng không quá 5MB)</span></h6>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6 right-part no-padding">
                                                <div class="upload-img-btn">
                                                    <a onclick="return $.app.upload.uploadImage('.progressbar-desc', _cbDesc, {maxSize: '5MB'});" href="javascript:void(0);"><i class="fa fa-plus-circle"></i> Thêm ảnh</a>
                                                </div>
                                            </div>

                                            <script>
                                                var descImgs = [];

                                                function _cbDesc(res) {
                                                    if (res.code) {
                                                        $('.img-desc-uploaded').append('<div style="position: relative;" class=""><img class="img-preview" src="' + $.app.vars.url + res.link + '"><a data-path="' + res.link + '" onclick="return $.app.ajax(null, \'upload\', {action: \'delete\', file: $(this).data(\'path\')}, _cbRemoveImg, \'POST\');" href="javascript:void(0);"><i class="btn-remove-img fa fa-close"></i></a></div>');

                                                        descImgs.push(res.link);
                                                    }
                                                }

                                                function _cbRemoveImg(res) {
                                                    if (res.code) {
                                                        $('a[data-path="' + res.link + '"]').closest('div').fadeOut(1000, function () {$(this).remove()});
                                                    }
                                                }
                                            </script>
                                        </div>

                                        <div class="col-xs-12 img-desc-uploaded"></div>
                                        <textarea class="hidden" id="desc-imgs" name="desc_imgs"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="third-section">
                        <div class="join-btn">
                            <a onclick="$('#image_value1').val( $('#image_value2').attr('src') ); $('#desc-imgs').val(descImgs.join(',')); return $.app.ajax(this, 'bai-du-thi', $('#uploadForm').serialize(), _callbackSubmit, 'POST', '.alert-submit');"><span>Gửi bài dự thi</span></a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="alert alert-submit d-none"></div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
@section('footer')
    <div class="modal fade" id="completedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close hidden" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h1 class="modal-title" id="exampleModalLabel">Xin chúc mừng!</h1>
                </div>
                <div class="modal-body">
                    <b>Bạn đã đăng bài thành công</b><br/>
                    Hãy cập nhật các thông tin cá nhân dưới đây để chúng tôi có thể liên hệ với bạn trong trường hợp nhận được giải thưởng của chương trình:<br/><br/>
                    <form class="form-horizontal" id="infoForm">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Họ và tên</label>
                            <div class="col-sm-8">
                                <input type="text" name="name" value="{{ Auth::user()->name }}" class="form-control" id="inputEmail3" placeholder="Họ và tên...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Email</label>
                            <div class="col-sm-8">
                                <input type="email" name="email" value="{{ Auth::user()->email }}" readonly="readonly" class="form-control" id="inputEmail3" placeholder="Địa chỉ email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Số điện thoại</label>
                            <div class="col-sm-8">
                                <input type="text" name="mobile" value="{{ Auth::user()->mobile }}" class="form-control" id="inputEmail3" placeholder="Số điện thoại">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Địa chỉ</label>
                            <div class="col-sm-8">
                                <input type="text" name="address" value="{{ Auth::user()->address }}" class="form-control" id="inputEmail3" placeholder="Địa chỉ nhà riêng...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Link Facebook</label>
                            <div class="col-sm-8">
                                <input type="text" name="facebook" value="{{ Auth::user()->facebook }}" class="form-control" id="inputEmail3" placeholder="Địa chỉ Facebook">
                            </div>
                        </div>
                    </form>
                    <div class="alert alert-info d-none"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary hidden" data-dismiss="modal">Đóng lại</button>
                    <button type="button" onclick="return _callbackInfo(this);" class="btn btn-primary"><i class="fa fa-send"></i> Hoàn tất</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        var submitId = 0;
        function _callbackSubmit(data){
            if(data.code){
                submitId = data.data.id;

                $('#completedModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });

                var $form = $('#uploadForm');
                $('input, textarea', $form).val('');
                $('.img-preview', $form).remove();
                $('.img-ctn img').attr('src', '').hide();
            }
        }

        function _callbackInfo(obj){
            var _cb = function(data){
                if(data.code){
                    setTimeout(function () {
                        window.location.href = '{{ url('bai-du-thi') }}/' + submitId;
                    }, 3000);
                }
            };

            $.app.ajax(obj, 'member/{{ Auth::user()->id }}', $('#infoForm').serialize(), _cb, 'POST', '.alert-info');
            return false;
        }

        $(function () {
            // Set width = height upload image
            var cw = $('.wrapper-image-crop').width();
            $('.wrapper-image-crop').css({'height': cw + 'px'});

            function buttonStartLoading(element) {
                $button = $(element);
                if($button.prop('disabled'))
                    return false;
                $button.find('.btn-loading').toggleClass('d-none');
                $button.find('.btn-text').toggleClass('d-none');
                $button.prop('disabled', true);
                return true;
            }

            function buttonEndLoad(element) {
                $button = $(element);
                $button.find('.btn-loading').toggleClass('d-none');
                $button.find('.btn-text').toggleClass('d-none');
                $button.prop('disabled', false);
            }

            $('.btn-file1').click(function(){
                $('#image_file1').trigger('click');
                return false;
            });

            $('#image_file1').on('change', function (e) {
                if(typeof this.files[0] == 'undefined' || !buttonStartLoading('.btn-file1')) {
                    return false;
                }
                var file = this.files[0];
                var reader = new FileReader();
                //console.log(file.type);

                $('#cropModal').modal('show');
                $('.image_crop1_wrap').hide();
                $('.alert-cover').html('Bạn vui lòng chờ, file đang được xử lý...');
                $('.btn-ok').hide();

                reader.addEventListener("load", function (e) {
                    var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
                    var header = "";
                    for (var i = 0; i < arr.length; i++) {
                        header += arr[i].toString(16);
                    }

                    function mimeType(headerString) {
                        switch (headerString) {
                            case "89504e47":
                                type = "image/png";
                                break;
                            case "47494638":
                                type = "image/gif";
                                break;
                            case "ffd8ffe0":
                            case "ffd8ffe1":
                            case "ffd8ffe2":
                                type = "image/jpeg";
                                break;
                            default:
                                type = "unknown";
                                break;
                        }
                        return type;
                    }

                    var mimeType = mimeType(header).split('/');
                    if(mimeType[0] != 'image'){
                        $('#image_file1').val('');
                        alert('Bạn phải upload file ảnh!');

                        buttonEndLoad('.btn-file1');
                        $('.more-opt1').removeClass('d-none');
                        $('.opacity-image1').addClass('d-none');
                        $('.btn-file1 .btn-text').html('Tải ảnh lên');
                        return false;
                    }

                    function callbackCompress(src) {
                        $('.alert-cover').html('Bạn hãy di chuyển ảnh về vị trí thích hợp nhất');
                        $('.image_crop1_wrap').show();
                        $('.btn-ok').show();

                        $('#image_crop1').attr("src", src);
                        $('#image_crop1').cropper('destroy');

                        buttonEndLoad('.btn-file1');
                        $('.more-opt1').removeClass('d-none');
                        $('.opacity-image1').addClass('d-none');
                        $('.btn-file1 .btn-text').html('Đổi ảnh');

                        var cropper = $('#image_crop1').cropper({
                            viewMode: 3,
                            aspectRatio: 1.91/1,
                            autoCropArea: 1,
                            guides: true,
                            highlight: false,
                            zoomable: false,
                            cropBoxResizable: false,
                            responsive: true,
                            minContainerWidth: cw,
                            setCropBoxData: {
                                width: 1910,
                                height: 1000
                            },
                            crop: function (e) {
                                var imageData = $('#image_crop1').cropper('getCroppedCanvas',
                                        {
                                            width: 1910,
                                            height: 1000,
                                            fillColor: '#fff',
                                            imageSmoothingEnabled: false,
                                            imageSmoothingQuality: 'high'
                                        }
                                ).toDataURL();
                                // $('#image_preview1').attr('src', imageData);
                                $('#image_value1').val(imageData);
                            }
                        });

                        $('.btn-rotate1').click(function(){
                            var rotate = $(this).data('option');
                            $('#image_crop1').cropper('rotate', Number(rotate));
                        });
                    }


                    var objImage = new Image();
                    objImage.onload = function () {
                        var width = objImage.naturalWidth,
                                height = objImage.naturalHeight;
                        window.URL.revokeObjectURL(objImage.src);

                        //if ((width <= 800 && height <= 800) && file.size < 1048576) {
                        if (0) {
                            callbackCompress(reader.result);
                        }
                        else {
                            var $data = new FormData();
                            $data.append('image_upload', file);

                            $.ajax({
                                type: 'POST',
                                url: '/upload/compress',
                                data: $data,
                                processData: false,
                                contentType: false,
                                success: function (response) {
                                    callbackCompress('data:image/jpeg;base64,' + response.image);
                                    $('#image_file1').val('');
                                    $('.btn-file2').hide();
                                },
                                error: function (response) {
                                }
                            });
                        }
                    };

                    objImage.src = window.URL.createObjectURL(file);
                }, false);

                if (file) {
                    reader.readAsArrayBuffer(file);
                }
            });
        });
    </script>
@stop