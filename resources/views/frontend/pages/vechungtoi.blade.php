@extends('layouts.default')
@section('header')
    <title>{{ config('custom.seo_title') }}</title>
    <meta name="description" content="{{ config('custom.seo_description') }}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}" />
    <meta property="og:image" content="{{ url('media/system/fbimage.jpg?'.config('custom.version'))  }}" />
    <meta property="og:title" content="{{ config('custom.seo_title') }}" />
    <meta property="og:description" content="{{ config('custom.seo_description') }}" />
    <link type="text/css" href="{{ url('assets/css/home.css?'.config('custom.version')) }}" rel="stylesheet"/>
    <link type="text/css" href="{{ url('assets/css/about-us.css?'.config('custom.version')) }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ url('assets/lib/mediaplayer/mediaelementplayer.min.css') }}" type="text/css"/>
    <script src="{{ url('assets/lib/mediaplayer/mediaelement-and-player.js') }}"></script>
    <script src="{{ url('assets/lib/slick/slick.min.js?'.config('custom.version')) }}"></script>
@stop

@section('content')
    <div class="about-us">
        <div class="about-us-title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="about-us-img-title">
                            <img src="{{url('assets/images/vechungtoi/vechungtoi-title.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="about-us-post">
            <div class="cont-container">
                <div class="container">
                    <div class="col-sm-6">
                        <h1>CHẶNG ĐƯỜNG 15 NĂM <br>
                            LAN TỎA HẠNH PHÚC</h1>
                        <p>Trong hành trình phát triển, Dạ Hương luôn là thương hiệu tiên phong trong việc chăm sóc, bảo vệ sức khỏe phụ nữ Việt.
                        </p>
                        <p>15 năm phát triển cũng là chặng đường Dạ Hương mong muốn đem tới cho phụ nữ nhiều giá trị hơn nữa, không chỉ là giá
                            trị sức khỏe về mặt thể chất mà còn là những giá trị tinh thần đầy ý nghĩa. </p>
                        <p>Đó là hạnh phúc được theo đuổi đam mê; là hạnh phúc khi yêu và được yêu, được vun vén và giữ lửa hôn nhân gia đình
                            hay chỉ đơn giản là được chứng kiến và góp phần lưu truyền những giá trị truyền thống tốt đẹp của phụ nữ Việt Nam
                            đến những thế hệ sau.</p>
                        <p>Và hạnh phúc của Dạ Hương là vinh dự được đồng hành chăm sóc sức khỏe của các thế hệ phụ nữ Việt Nam. Chúng tôi
                            luôn nâng niu, trân trọng hạnh phúc của phụ nữ dù là bé nhỏ nhất. Chúng tôi mong muốn lan tỏa cảm hứng sống tích
                            cực đến thật nhiều phụ nữ Việt Nam tạo động lực cho họ mở rộng tâm hồn đón nhận và trải nghiệm hạnh phúc.</p>
                    </div>
                    <div class="col-sm-6">
                        {{--<img src="{{url('assets/images/vechungtoi/about-us1_02.png')}}" alt="" width="100%">--}}
                    </div>
                </div>
            </div>
            <div class="img-container-fluid mt">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6 no-padding"></div>
                        <div class="col-sm-6 no-padding">
                            <img src="{{url('assets/images/vechungtoi/about-us1_02.png')}}" alt="" width="100%">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="about-us-post">
            <div class="cont-container">
                <div class="container">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <h1>TẦM NHÌN, SỨ MỆNH <br>
                            CỦA CHIẾN DỊCH</h1>
                        <p>“Lan tỏa hạnh phúc” là chiến dịch đầy ý nghĩa với mong muốn giúp phụ nữ chia sẻ những điều làm nên hạnh phúc của riêng họ.
                            Từ đó, Dạ hương sẽ là nhịp cầu mang những thông điệp đó truyền cảm hứng đến mọi đối tượng phụ nữ trong xã hội.
                            Thúc đẩy phụ nữ đồng hành cùng Dạ Hương trong hành trình bảo vệ sức khỏe và trải nghiệm hạnh phúc.</p>
                    </div>
                </div>
            </div>
            <div class="img-container-fluid">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6 no-padding">
                            <img src="{{url('assets/images/vechungtoi/about-us2_02.png')}}" alt="" width="100%">
                        </div>
                        <div class="col-sm-6 no-padding">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="about-us-post">
            <div class="cont-container">
                <div class="container">
                    <div class="col-sm-6">
                        <h1>“LAN TỎA HẠNH PHÚC” –<br>
                            CHIẾN DỊCH ĐẾN TỪ TRÁI TIM</h1>
                        <p>“Lan tỏa hạnh phúc” như là nốt thăng đẹp mà Dạ Hương chọn để đánh dấu mốc 15 năm đáng nhớ của mình.
                            Cuộc sống vốn nhiều bộn bề và bất trắc, cuối cùng thì chúng ta vẫn gom góp những điều bé nhỏ,
                            bình dị để tự tay vẽ lên bức tranh hạnh phúc cho riêng mình. Và thông qua “Lan tỏa hạnh phúc”,
                            Dạ hương muốn đồng hành cùng phụ nữ Việt tôn vinh, chia sẻ hạnh phúc dù bé nhỏ và lan tỏa thông điệp về hạnh phúc,
                            về cảm hứng sống tích cực đến toàn xã hội.
                        </p>
                        <p>Sức khỏe và hạnh phúc của phụ nữ Việt Nam là động lực lớn nhất để Dạ Hương không ngừng hoàn thiện và đổi mới.</p>
                    </div>
                    <div class="col-sm-6">
                        {{--<img src="{{url('assets/images/vechungtoi/about-us1_02.png')}}" alt="" width="100%">--}}
                    </div>
                </div>
            </div>
            <div class="img-container-fluid">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6 no-padding"></div>
                        <div class="col-sm-6 no-padding">
                            <img src="{{url('assets/images/vechungtoi/about-us3_02.png')}}" alt="" width="100%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="viral-clip">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="viral-clip-title">
                            <h5>Viral Clip: Phút giây hạnh phúc</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" container home-video ">
                <video class="youtube_player2 youtube_player1_config" playsinline id="youtube_player2">
                    <source src="https://www.youtube.com/watch?v=A-pDVJP-4MQ" type="video/youtube">
                    Your browser does not support the video tag.
                </video>
                <div class=" container--config container--posi">
                    <div class="">
                        <div class="col-sm-5 home-video__icon hidden-mb">
                            <div class="row">
                                <div class="col-sm-12 home-video__icon__1">
                                    <img src="{{ url('assets/img_home/img44.png') }}" alt="image" width="120%">
                                </div>
                                <div class="col-sm-12 mt-10">
                                    <img src="{{ url('assets/img_home/icon1.png') }}" alt="image">
                                    <div class="home-text__1">
                                        <h2>1300</h2>
                                        <p>Câu chuyện hạnh phúc</p>
                                    </div>
                                </div>
                                <div class="col-sm-12 mt-10">
                                    <img src="{{ url('assets/img_home/icon2.png') }}" alt="image">
                                    <div class="home-text__1">
                                        <h2>350000</h2>
                                        <p>Hạnh phúc được lan tỏa</p>
                                    </div>
                                </div>
                                <div class="col-sm-12 home-video__icon__2_1">
                                    <img src="{{ url('assets/img_home/icon3.png') }}" alt="image">
                                    <img src="{{ url('assets/img_home/sound.png') }}" alt="image" class="sound1">
                                </div>
                                {{--<div class="col-sm-12 home-video__icon__3">--}}
                                    {{--<a href="#content"><img src="{{ url('assets/img_home/icon5.png') }}" alt="image"></a>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="home-video__play">
                    <img src="{{ url('assets/img_home/icon_play.png') }}" alt="image" class="player">
                    <div class="home-video__play__text">Xem video</div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="join-btn">
                            <a onclick="return $.app.checkLogin('{{ url('bai-du-thi/create') }}');" href="#"><span>Tham gia ngay cuộc thi</span></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop

@section('footer')
@stop