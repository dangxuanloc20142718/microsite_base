@extends('layouts.default')
@section('header')
    <title>{{ config('custom.seo_title') }}</title>
    <meta name="description" content="{{ config('custom.seo_description') }}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}" />
    <meta property="og:image" content="{{ url('media/system/fbimage.jpg?'.config('custom.version'))  }}" />
    <meta property="og:title" content="{{ config('custom.seo_title') }}" />
    <meta property="og:description" content="{{ config('custom.seo_description') }}" />
    <link type="text/css" href="{{ url('assets/css/home.css?'.config('custom.version')) }}" rel="stylesheet"/>
    <link type="text/css" href="{{ url('assets/css/thelegiaithuong.css?'.config('custom.version')) }}" rel="stylesheet"/>
    <script src="{{ url('assets/lib/slick/slick.min.js?'.config('custom.version')) }}"></script>
@stop

@section('content')
    <div class="thelegt">
    {{-----------------------------SECTION123-DESKTOP------------------------}}
        <div class="thele-title">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="thele-img-title">
                            <img src="{{url('assets/images/thelegiaithuong/thele-title.png')}}" alt="">
                            <h5>Hạnh phúc của tôi</h5>
                        </div>
                        <div class="join-btn">
                            <a onclick="return $.app.checkLogin('{{ url('bai-du-thi/create') }}');"><span>Chia sẻ hạnh phúc của bạn ngay nhé</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="section1" class="thelegiaithuong-content">
            <div class="content-ctn">
                <div class="container">
                    <div class="col-sm-6">
                        <span class="gt-gt"><h1>Giới thiệu</h1><span>
                        <p>Bạn có tin không, những điều tưởng như bé nhỏ, giản dị trong cuộc sống đôi khi lại là nguồn cảm hứng khiến ta mỉm cười và sống vui vẻ hơn.Dạ Hương tin điều đó và mong muốn là cầu nối để lan tỏa nguồn năng lượng tích cực, lan tỏa hạnh phúc đến nhiều người hơn nữa. </p>
                        <p>Tháng 9 này, Dạ Hương vinh dự đông hành cùng Afamily mang đến cho bạn cuộc thi "Hạnh phúc của tôi". Hãy kể cho chúng tôi nghe những kỉ niệm-những câu chuyện- những con người đáng nhớ và truyền cảm hứng sống hạnh phúc cho bạn. Đừng quên chia sẻ cảm nhận cá nhân về thương hiệu Dạ Hương nhé! Hãy cùng chúng tôi lan tỏa hạnh phúc đến nhiều phụ nữ hơn nữa và nhận những giải thưởng giá trị như lời cảm ơn của Dạ Hương. </p>
                    </div>
                    <div class="col-sm-6">
                        {{--<img src="{{url('assets/images/thelegiaithuong/momgirl.png')}}" alt="" width="100%">
                        <span><img src="{{url('assets/images/thelegiaithuong/lavender-right.png')}}" width="70%"alt=""><span>--}}
                    </div>
                </div>
            </div>
            <div class="img-container-fluid mt">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6 no-padding"></div>
                        <div class="col-sm-6 no-padding right-img-box">
                            <img src="{{url('assets/images/thelegiaithuong/momgirl.png')}}" alt="" width="100%">
                            <span><img src="{{url('assets/images/thelegiaithuong/lavender-right.png')}}" width="70%"alt=""><span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="section2" class="thelegiaithuong-content">
            <div class="content-ctn">
                <div class="container">
                    <div class="col-sm-6">
                        <h1>đối tượng tham dự</h1>
                        <p>Phụ nữ Việt Nam tuổi từ 16 đến 46</p>
                    </div>
                    <div class="col-sm-6">
                        <h1>Thời gian tham dự</h1>
                        <p>Thời gian dự thi: <b> Từ ngày 12/09 đến 15/10</b></p>
                        <p>Công bố giải thưởng: <b> 16/10</b></p>
                    </div>
                </div>
            </div>
        </div>

        <div id="section3" class="thelegiaithuong-content">
            <div class="content-ctn">
                <div class="container">
                    <div class="col-md-12 col-sm-12">
                        <span class="gt-gt"><h1>Giải thưởng</h1></span>
                        <img src="{{url('assets/images/thelegiaithuong/giaithuong.png')}}" width="100%"alt="">
                    </div>
                </div>
            </div>
        </div>
        {{-----------------------------END-SECTION123-DESKTOP------------------------}}
        <!-- section123-mobile -->
        {{-----------------------------SECTION123-MOBILE------------------------}}
       
        <div id="section1-mb" class="thelegiaithuong-content">
            <div class="content-ctn">
                <div class="container">
                    <div class="col-sm-12">
                        <img src="{{url('assets/images/thelegiaithuong/momgirl2.png')}}" alt="">
                        <span class="gt-gt"><h1>Giới thiệu</h1><span>
                        <p>Bạn có tin không, những điều tưởng như bé nhỏ, giản dị trong cuộc sống đôi khi lại là nguồn cảm hứng khiến ta mỉm cười và sống vui vẻ hơn.Dạ Hương tin điều đó và mong muốn là cầu nối để lan tỏa nguồn năng lượng tích cực, lan tỏa hạnh phúc đến nhiều người hơn nữa. </p>
                        <p>Tháng 8 này, Dạ Hương vinh dự đông hành cùng Afamily mang đến cho bạn cuộc thi "Hạnh phúc của tôi". Hãy kể cho chúng tôi nghe những kỉ niệm-những câu chuyện- những con người đáng nhớ và truyền cảm hứng sống hạnh phúc cho bạn. Đừng quên chia sẻ cảm nhận cá nhân về thương hiệu Dạ Hương nhé! Hãy cùng chúng tôi lan tỏa hạnh phúc đến nhiều phụ nữ hơn nữa và nhận những giải thưởng giá trị như lời cảm ơn của Dạ Hương. </p>
                    </div>
                </div>
            </div>
            
        </div>

        <div id="section2-mb" class="thelegiaithuong-content">
            <div class="content-ctn">
                <div class="container">
                    <div class="col-xs-6">
                        <h1>đối tượng tham dự</h1>
                        <p>Phụ nữ Việt Nam tuổi từ 16 đến 46</p>
                    </div>
                    <div class="col-xs-6">
                        <h1>Thời gian tham dự</h1>
                        <p>Thời gian dự thi: <b> Từ ngày 12/09 đến 15/10</b></p>
                        <p>Công bố giải thưởng: <b> 16/10</b></p>
                    </div>
                </div>
            </div>
        </div>

        <div id="section3-mb" class="thelegiaithuong-content">
            <div class="content-ctn">
                <div class="container">
                    <div class=" col-xs-12">
                        <span class="gt-gt"><h1>Giải thưởng</h1></span>
                        <img src="{{url('assets/images/thelegiaithuong/giaithuong-mb.png')}}" width="100%"alt="">
                    </div>
                </div>
            </div>
        </div>

        {{-----------------------------END-SECTION123-MOBILE------------------------}}
        <div id="section4"class="thelegiaithuong-content">
            <div class="content-ctn">
                <div class="container">
                    <div class="col-sm-12">
                    <span class="cachthuc"><h1>Cách thức tham gia</h1></span>
                        <div class="babuoc">
                            <div class="step step1">
                                <img src="{{url('assets/images/thelegiaithuong/arrow-step.png')}}" width="1%"alt=""><span><a>Bước 1</a></span> 
                                <p>Đăng kí thông tin dự thi tại link sau :<a onclick="return $.app.checkLogin('{{ url('bai-du-thi/create') }}');" style="color:#dc5ca1;text-decoration:none" href="#">[Link]</a> </p>
                            </div>
                            <div class="step step2">
                                <img src="{{url('assets/images/thelegiaithuong/arrow-step.png')}}" width="1%"alt=""><span><a>Bước 2</a></span> 
                                <p> -Thí sinh đăng tải hình ảnh </p>
                                <p>- Chia sẻ câu chuyện hoặc chỉ đơn giản là câu nói đơn giản, tâm đắc về hạnh phúc của mình. Sử dụng template "Hạnh phúc của tôi ..." 
                                    theo một trong các chủ đề : <span style="color:#c097ce">Đam mê, Gia đình và Bản thân.</span>
                                </p>
                                <p>Lưu ý:
                                    <i> Thí sinh đăng ảnh chụp có sự hiện diện của sản phẩm Dạ Hương sẽ là điểm cộng</i>
                                    <div class="dahuong-img">
                                        <img src="{{url('assets/images/thelegiaithuong/dahuong.png')}}" width="100%"alt="">
                                    </div>
                                </p>  
                            </div>  
                            <div class="step step3">
                                <img src="{{url('assets/images/thelegiaithuong/arrow-step.png')}}" width="1%"alt=""><span><a>Bước 3</a></span> 
                                <p>Người chơi sẽ chia sẻ công khai về trang cá nhân của mình kèm theo hashtag<span style="color:#dc5ca1">#DaHuong, #lantoahanhphuc</span> và kêu gọi bình chọn trên trang web chính thức của chiến dịch tại <a target="_blank" style="color:#dc5ca1;text-decoration:none" href="{{ url('bai-du-thi') }}">Link</a>. </p>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="section5" class="thelegiaithuong-content">
            <div class="content-ctn">
                <div class="container">
                    <div class="col-sm-12">
                        <div class="tieuchi">
                            <div class="list-tc tc-pd">
                                <img src="{{url('assets/images/thelegiaithuong/arrow2.png')}}" width="1%"alt=""><span><a>Tiêu chí chấm giải từ BGK</a></span> 
                                <p><b> -Về hình ảnh:</b> </p> 
                                <p>Hình ảnh thể hiện được câu chuyện hạnh phúc được chia sẻ.<br> Hình ảnh rõ nét,chất lượng, thể hiện sự đầu tư cao.</p>
                                <p><b> -Về nội dung bài chia sẻ:</b> </p>
                                <p>Nội dung bài viết súc tích, ngắn gọn, cách truyền đạt tự nhiên.<br>Nội dung truyền được thông điệp về những niềm hạnh phúc nhỏ bé và giản dị trong cuộc sống.</p>
                            </div> 
                            <div class="list-tc tc-pd">
                                <img src="{{url('assets/images/thelegiaithuong/arrow2.png')}}" width="1%"alt=""><span><a>Bài dự thi hợp lệ</a></span> 
                                <p>- Bài chia sẻ về facebook phải kèm hashtag <span style="color:#dc5ca1">#DaHuong, #lantoahanhphuc</span> và để chế độ công khai.  </p> 
                                <p>- Hình ảnh và bài viết phải thuộc quyền sở hữu của người tham gia (BTC sẽ không công nhận những hình ảnh và bài viết được sao chép từ internet hoặc từ những cá nhân khác.)</p>
                                <p>- Các bài dự thi sử dụng công cụ like/share tự động sẽ được xem là gian lận và bị loại.</p>
                            </div> 
                            <div class="list-tc tc-pd">
                                <img src="{{url('assets/images/thelegiaithuong/arrow2.png')}}" width="1%"alt=""><span><a>Hình thức trao giải</a></span> 
                                <p>- Sau khi giải thưởng được công bố, BTC sẽ liên lạc để xác nhận thông tin với các cá nhân đoạt giải.</p> 
                                <p>- BTC có quyền từ chối trao giải trong trường hợp người nhận giải thưởng không cung cấp đủ thông tin của từng thành viên nhóm, bao gồm:<br>
                                họ tên, địa chỉ thường chú, số điện thoại cá nhân, bản sao CMND.
                                </p>
                                <p>- Giải thưởng sẽ được trao cho các cá nhân đoạt giải trong vòng 30 ngày làm việc kể từ khi công bố kết quả chung cuộc. Các giả thưởng sẽ được trao thông qua đường bưu điện và chuyển khoản ngân hàng.</p>
                                <p>- Ban Tổ Chức không chịu trách nhiệm nếu người thắng giải không nhận được giải thưởng do cung cấp thông tin nhận thưởng sai hay thay đổi thông tin mà không thông báo cho Ban Tổ Chức.</p>
                            </div> 
                            <div class="list-tc tc-pd">
                                <img src="{{url('assets/images/thelegiaithuong/arrow2.png')}}" width="1%"alt=""><span><a>Lưu ý khác</a></span> 
                                <p>- Người tham gia cuộc thi chịu hoàn toàn trách nhiệm về tác quyền hay tranh chấp liên quan đến hình ảnh và bài viết dự thi của mình.</p> 
                                <p>- Những bài dự thi vi phạm sẽ không được chọn và Ban Tổ Chức không chịu trách nhiệm về các vấn đề có liên quan.</p>
                                <p>- Bài dự thi không vi phạm thuần phong mỹ tục Việt Nam, không gây ảnh hưởng đến uy tín và danh dự của người khác; gây kích động bạo lực, vũ trang; là văn hóa phẩm đồi trụy hay xâm phạm quyền tự do cá nhân của người khác.</p>
                                <p>- Ban tổ chức có quyền từ chối công nhận bài dự thi bất kì lúc nào nếu nhận thấy bài dự thi không phù hợp tiêu chí cuộc thi.</p>
                                <p>- Khi tham gia vào cuộc thi, người chơi đã mặc nhiên đồng ý cho phép Ban tổ chức được toàn quyền sử dụng tất cả các hình ảnh và bài viết tham gia dự thi và thông tin người dự thi (bao gồm tên, địa chỉ, hình ảnh) mà không phải thông báo hay trả bất kỳ chi phí nào.</p> 
                                <p>- Trong suốt quá trình cuộc thi diễn ra, nếu có vấn đề phát sinh ngoài ý muốn thì quyết định của Ban tổ chức sẽ là quyết định cuối cùng. </p>
                                <p>- Tùy theo nhu cầu thực tế nhằm cải thiện chất lượng cuộc thi, Ban tổ chức giữ quyền điều chỉnh thể lệ tại bất cứ thời điểm nào trước khi kết thúc cuộc thi bằng cách đăng tải thể lệ đã chỉnh sửa lên website của cuộc thi. </p>
                                <p>- Tất cả người chơi tham gia cuộc thi phải thể hiện tính trung thực. Mọi hành vi gian dối khi bị phát hiện sẽ không được tiếp tục tham gia cuộc thi.</p>
                                <p>- Ban Tổ Chức không chịu trách nhiệm cho bất kỳ sự cố nào do lỗi kĩ thuật khách quan về đường truyền internet, server... gây ra.</p>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="img-container-fluid mt">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 no-padding tc-img-bg">
                            <img src="{{url('assets/images/thelegiaithuong/bg-lavender.png')}}" alt="" width="100%">
                        </div>
                    </div>
                </div>
            </div>   
        </div>
    </div>
@stop

@section('footer')
@stop