@foreach($news as $new)
    <div class="col-md-3 news-item">
        <div class="card border-0 card--transparent">
            <a class="d-block mb-3 news-link" href="{{ url($new->alias . 'n' . $new->id) }}.html">
                <img class="thumbnail" style="max-width: 100%;" src="{{ url($new->image) }}" alt="{{ $new->title }}">
            </a>
            <div class="card-block p-0">
                <h5 class="fw-5">
                    <a class="text-gray-dark news-link" href="{{ url($new->alias . 'n' . $new->id) }}.html">
                        {{ $new->title }}
                    </a>
                </h5>
                <p class="text-paragraph hidden-md-down">
                    {{ $new->introtext }}
                </p>
            </div>
        </div>
    </div>
@endforeach