@extends('layouts.default')

@section('header')
    <title>Tin tức - {{ config('custom.seo_title') }}</title>
    <meta name="description" content="Tin tức - {{ config('custom.seo_description') }}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}" />
    <meta property="og:image" content="{{ url('media/system/fbimage.jpg?'.config('version.version'))  }}" />
    <meta property="og:title" content="Tin tức - {{ config('custom.seo_title') }}" />
    <meta property="og:description" content="Tin tức - {{ config('custom.seo_description') }}" />
@stop

@section('content')
    <div class="container">
        <h1>Tin tức</h1>

        <div class="news-list">
            @include('frontend.news.index_items')
        </div><div class="clearfix"></div>

        <div class="text-center">
            <a class="btn btn-block btn-outline-gray-dark w-100 w-md-auto px-5" href="#" onclick="return loadNews(this, '.news-list');">
                Xem thêm
            </a>
        </div>
    </div>

@stop

@section('footer')
    <script type="text/javascript">
        var indexNews = 2;
        function loadNews(obj, divClass){
            var $div = $(divClass);
            var _success = function(data){
                if(data.status){
                    if(obj) {
                        indexNews++;
                        $div.append(data.html);
                    }
                }

                if(data.status == 2 && obj){
                    $(obj).html('Không còn tin tức nào!');
                }
            };
            funcHelp.helperAjax.ajax(obj, 'news?ajax=1&page='+indexNews , {}, _success, 'GET');
            return false;
        }
    </script>
@stop
