@extends('layouts.default')

@section('header')
    <title>{{ ($item->seo_title) ? $item->seo_title : $item->title }}</title>
    <meta name="description" content="{{ ($item->seo_keyword) ? $item->seo_keyword : $item->introtext }}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}"/>
    <meta property="og:title" content="{{ ($item->seo_title) ? $item->seo_title : $item->title }}"/>
    <meta property="og:description" content="{{ ($item->seo_keyword) ? $item->seo_keyword : $item->introtext }}"/>
@stop

@section('content')
    <div class="container">
        <h1>{{ $item->title }}</h1>

        <b>{{ $item->introtext }}</b>

        <p>{{ $item->fulltext }}</p>

        <div class="fblike-container">
            <div class="fb-share-button" data-href="{{ url()->current() }}" data-layout="button_count" data-action="share" data-size="small" data-show-faces="false" data-share="true"></div>
        </div>
    </div>
@stop

@section('footer')

@stop
