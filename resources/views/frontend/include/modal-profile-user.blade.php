<div class="modal fade" id="modal-profile" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Thông tin user</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal frm-profile" role="form" method="POST" action="">
                    <div class="left pull-left">
                        <img src="@if($user->avatar) {{ url($user->avatar) }} @else {{ url('uploads/img/no-image.png') }}@endif" class="" title="Ảnh đại diện" alt="">
                    </div>
                    <div class="right pull-right">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nickname</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="{{ $user->name }}" name="name" placeholder="Username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Fullname</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="{{ $user->fullname }}" name="fullname" placeholder="Fullname">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Birthday</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="{{ $user->birthday }}" name="birthday" placeholder="Birthday">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Email</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="email" value="{{ $user->email }}" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Mobile</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="mobile" value="{{ $user->mobile }}" placeholder="Mobile">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Facebook</label>
                            <div class="col-md-9">
                                <a target="_blank" href="https://facebook.com/{{ $user->facebook_id }}" title="Nhấn vào để chuyển sang facebook cá nhân">https://facebook.com/{{ $user->facebook_id }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary btn-submit">Cập nhật</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    $(function(){
        $('#modal-profile .btn-submit').on('click', function() {
            var formEle = $('#modal-profile .frm-profile')[0];
            funcHelp.helperAjax.ajax(this, '/user/{{ $user->id }}', new FormData(formEle));
        });
    });
</script>