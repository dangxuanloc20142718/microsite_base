@extends('layouts.default')

@section('header')
    <title>{{ $submit->title }} - {{ config('custom.seo_title') }}</title>
    <meta name="description" content="{{ config('custom.seo_description') }}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}" />
    <meta property="og:image" content="{{ url($submit->image) . '?v='.config('custom.version') }}" />
    <meta property="og:image:secure_url" content="{{ url($submit->image) . '?v='.config('custom.version') }}" />
    <meta property="og:image:width" content="600" />
    <meta property="og:image:height" content="600" />
    <meta property="og:title" content="{{ $submit->title }} - {{ config('custom.seo_title') }}" />
    <meta property="og:description" content="{{ config('custom.seo_description') }}" />
@stop

@section('content')

    <section>
        <div class="game-frame2">
            <div class="container">
                <div class="row">
                    @if($submit->status == 1)
                    <div class="col-md-12">
                        <div class="game-frame2-box">
                            <div class="game-frame2-content-box">
                                <div class="row vote-item">
                                    <div class="col-md-4">
                                        <div class="game-frame2-firstbox">
                                            <img src="/assets/images/tuyetvoi.png" alt="">
                                            @if($type == 'done')
                                            <p>Chúc mừng bạn đã chia sẻ <br>
                                                công thức kem tuyệt đỉnh <br>
                                                của gia đình mình <br>
                                                thành công! Hãy *Share* <br>
                                                để kêu gọi bạn bè <br>
                                                *Thả tim* cho công thức <br>
                                                kem của mình nào!</p>
                                            @else
                                                <p>Công thức kem tuyệt vời <br/> của {{ $user->name }}</p>
                                                <p>Hãy tham gia chia sẻ ngay <br>
                                                    công thức kem của Gia <br>
                                                    đình bạn nhé !!!
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="game-frame2-secondbox">
                                            <div class="game-frame2-box-final vote-thumb vote-thumb-{{ $submit->id }}">
                                                <img id="fakeheight" src="/assets/images/img-done.png" alt="">
                                                <img class="chosen-image"src="{{ $submit->image }}" alt="">
                                                {{--<img class="chosen-khung" src="/assets/images/khung-act.png" alt="">--}}
                                            </div><br/>

                                            <div class="like-share text-center">
                                                <span class="like" style="margin-right: 15px;">
                                                    <a @if(Auth::check()) onclick="return _voteCallback({{ $submit->id }});" @else onclick="return funcHelp.helperFunction.fblogin('{{ url()->current() }}', this);" @endif data-vote-id="{{ $submit->id }}"  href="#">
                                                        <span class="vote-count-{{ $submit->id }}">{{ $submit->votes }}</span>
                                                        <img src="{{ url('/') }}/assets/images/heart.png" alt=""/>
                                                    </a>
                                                </span>
                                                <span class="share">
                                                    <a class="facebook_share" href="{{ url('bai-du-thi/'.$submit->id) }}" data-id="{{ $submit->id }}" >
                                                        <span class="vote-share-{{ $submit->id }}">{{ $submit->shares }}</span>
                                                        <img src="{{ url('/') }}/assets/images/share-btn.png" alt="">
                                                    </a>
                                                </span>
                                            </div>

                                            <div class="share-fb">
                                                <a class="facebook_share" href="{{ url('bai-du-thi/'.$submit->id) }}" data-id="{{ $submit->id }}" ><img src="/assets/images/fb-btn.png" alt=""></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="game-frame2-secondbox">
                                            @if(time() < config('custom.round_1_end_create'))
                                            <div class="send-new-post">
                                                <a class="@if(!Auth::check()) btn-fblogin @endif" data-redirect="{{ url('bai-du-thi/create') }}" href="{{ url('bai-du-thi/create') }}">GỬI BÀI DỰ THI</a>
                                            </div>
                                            @endif
                                            <div class="ranking">
                                                <a href="{{ url('bai-du-thi') }}">Bảng xếp hạng</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <img class="game-frame2-bottom-img" src="/assets/images/three-ice-cream.png" alt="">
                            </div>
                        </div>
                    </div>
                    @else
                        <div class="" style="background-color: #fff; border-radius:5px; min-height: 600px; padding: 15px; margin-top: 30px;">
                            <h1>Ops</h1>
                            Bài dự thi này không hợp lệ!<br/><br/>
                            <a class="btn btn-primary" href="{{ url('bai-du-thi') }}">Xem tất cả Bài dự thi <i class="fa fa-arrow-right"></i></a>
                        </div>
                    @endif
                </div>
            </div>
        </div>

    </section>

    <script>
        $(document).ready(function(){
            // var wHeight = $(window).height();
            // $("body").css('height', wHeight);

            $(".toggle-menu-mb").click(function(){
                $(".menu-mobile-list").toggle("slide", { direction: "right" }, 200);
                $(".layout-toggle-menu").fadeIn(200);
            })
            $(".layout-toggle-menu").click(function(){
                $(".menu-mobile-list").toggle("slide", { direction: "right" }, 200);
                $(".layout-toggle-menu").fadeOut(200);
            })
            function checkMenu(){
                if($(window).width() > 768){
                    $(".layout-toggle-menu").fadeOut(200);
                    $(".menu-mobile-list").css("display", "none");
                }
            }
            checkMenu();
            window.onresize = function(e){
                checkMenu();
            };

            function checkScroll(){
                var $height = $('body > section').outerHeight() + $('body > header').outerHeight();

                $("body").css('height', $height - 100);
                $("body").css('background-size', '100% '+ ($height + 50) + 'px');
            }
            checkScroll();
            window.onresize = function(e){
                checkScroll();
            }

        });
        
    </script>

    <div class="container">
        {{--<h1>{{ $submit->title }}</h1>
        <img src="{{ $submit->image }}">

        <div class="like-share">
            <div class="like">
                <a @if(Auth::check()) onclick="return _voteCallback({{ $submit->id }});" @else onclick="return funcHelp.helperFunction.fblogin('{{ url()->current() }}', this);" @endif data-id="{{ $submit->id }}"  data-from="vote" data-redirect="{{ url()->current() }}" href="#">
                    <span class="count_vote_{{ $submit->id }}">{{ $submit->votes }}</span>
                    <img src="{{ url('/') }}/assets/images/heart.png" alt=""/>
                </a>
            </div>
            <div class="share">
                <a class="facebook_share" href="{{ url('bai-du-thi/'.$submit->id) }}" data-id="{{ $submit->id }}" >
                    <span class="count_share_{{ $submit->id }}">{{ $submit->shares }}</span>
                    <img src="{{ url('/') }}/assets/images/share-btn.png" alt="">
                </a>
            </div>
        </div>--}}

        {{--<div>
            {{ $submit->introtext }}
        </div>

        <div class="fblike-container">
            <div onclick="return false;" class="fb-share-button" data-href="{{ url()->current() }}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
            <a href="{{ url()->current() }}" data-id="{{ $submit->id }}" class="hidden btn btn-sm btn-primary facebook_share"><i class="fa fa-facebook"></i> Share on Facebook</a>
        </div>--}}
    </div>
@stop

@section('footer')
@stop