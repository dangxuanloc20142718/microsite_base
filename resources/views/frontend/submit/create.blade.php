@extends('layouts.default')
@section('header')
    <title>{{ config('custom.seo_title') }} - Gửi bài dự thi</title>
    <meta name="description" content="{{ config('custom.seo_description') }}  - Gửi bài dự thi">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}"/>
    <meta property="og:image" content="{{ url('media/system/fbimage.jpg?'.config('custom.version'))  }}"/>
    <meta property="og:title" content="{{ config('custom.seo_title') }}"/>
    <meta property="og:description" content="{{ config('custom.seo_description') }}"/>

    <link rel="stylesheet" href="{{ url('assets/lib/cropper/cropper.css') }}"/>

    <link type="text/css" href="{{ url('/') }}/assets/css/styletranganh.css?{{ config('custom.version') }}" rel="stylesheet"/>
    <style>
        .swiper-button-next, .swiper-button-prev { display: block; }
    </style>
@stop

@section('content')

    @if($user && time() < config('custom.round_1_end_create') && !$limit)
    <section>
        <div class="baiduthi">
            <div class="container">
                <div class="col-md-12">
                    <div class="baiduthi-box">
                        <div class="topbaiduthi tranganh">
                            <div>
                                <h3 class="text-center pd">cùng chia sẻ những công thức kem tuyệt đỉnh <br> tạo nên khoảnh khắc vui vẻ, ấm áp bên gia đình nhé!!!
                                    {{--<br/>Bạn có thể gửi tối đa <span class="red">{{ config('custom.round_1_limit') }}</span> bài dự thi--}}
								</h3>
                                <p class="absolute">
                                    <img src="{{ url('/') }}/assets/images/Trang-Gamecook_03.png" class="img-responsive">
                                </p>
                                <div class="col-xs-12 visible-xs rules text-center" >
                                    <a style="font-family: Stabile-Regular; font-size: 18px;" href="{{ url('page/the-le') }}">Thể lệ cuộc thi ></a>
                                </div>
                                <div class="col-md-6 col-xs-12 kemngon" style="">
                                    <div>
                                        <span style="font-family: Stabile-Regular; font-size: 18px;">Bước 1: Tải ảnh lên và chọn vị trí phù hợp<br/></span>
                                        <form id="submitForm">
                                            <div style="position: relative">
                                                <img id="image_crop1" src="{{ url('/')  }}/assets/images/kem-ngon.jpg" class="img-responsive">
                                                {{--<img id="image_crop1" class="img-fluid" src="{{ url('assets/img/no-image.svg') }}" alt="" style="border-radius: 4px !important;">--}}
                                                <div class="opacity-image1" style="width: 100%;height: 100%;position: absolute;top: 0;"></div>
                                                <input class="d-none" type="file" name="image_file1" id="image_file1">
                                                <input type="hidden" name="image1" value="" id="image_value1"/>
                                                <input type="hidden" name="frame" class="input_frame"/>
                                                <div id="image_frame" style="z-index: -1; position: absolute; top:0; left:0; width: 100%; height: 100%; background-image: url('{{ url('assets/img/frame.png') }}'); background-size: 100% 100%; background-repeat:   no-repeat; background-position: center center;  "></div>
                                                <div class="clearfix"></div>
                                                <div class="btn-group more-opt1 d-none" style="position: absolute; right: 0px; bottom: 0px; text-align: right; margin-left: auto;margin-right: auto;display: table;">
                                                    <button type="button" class="btn btn-default btn-rotate1" data-method="rotate" data-option="-90" title="Rotate Left">
                                                        <span class="docs-tooltip" data-toggle="tooltip" title=""><span class="fa fa-rotate-left"></span></span>
                                                    </button>
                                                    <button type="button" class="btn btn-default btn-rotate1" data-method="rotate" data-option="90" title="Rotate Right">
                                                        <span class="docs-tooltip" data-toggle="tooltip" title=""><span class="fa fa-rotate-right"></span></span>
                                                    </button>
                                                    <a href="#" class="btn btn-default btn-file1">
                                                        <img class="btn-loading d-none" src="/assets/img/loading.gif"/>
                                                        <span class="btn-text">
                                                            TẢI ẢNH LÊN
                                                        </span>
                                                        {{--Tải ảnh lên--}}
                                                    </a>
                                                </div>
                                                {{--<p class="fs-15 text-focus my-1 more-opt1 d-none">Bạn có thể di chuyển ảnh về vị trí mong muốn</p>--}}
                                            </div>
                                        </form>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-submit alert d-none"></div>
                                        </div>
										<div class="col-md-12">
											<div class="alert alert-warning">
												Lưu ý: Bài dự thi cần chụp ảnh gia đình bạn và món kem bạn đã thực hiện.<br/> Ảnh không hợp lệ sẽ không được nhận giải thưởng.
											</div>
										</div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 flright">
                                        <a href="#" class="btn-file1 btn-file2">
                                            <img class="btn-loading d-none" src="/assets/img/loading.gif"/>
                                            <span class="btn-text">
                                                TẢI ẢNH LÊN
                                            </span>
                                            {{--Tải ảnh lên--}}
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 flleft hidden-xs">
                                        <a onclick="funcHelp.helperAjax.ajax(this, 'bai-du-thi', $('#submitForm').serialize(), _callbackSubmit, 'POST', '.alert-submit'); return false;" href="#">
                                            Gửi bài dự thi
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="col-md-6 nenkem hidden-xs">
                                    <div class="bg-dungquen hidden-xs">
                                        <p class="dungquen">
                                            Đừng quên <br>
                                            trang trí công thức kem <br> tuyệt  đỉnh của mình với những <br> khung ảnh siêu độc đáo <br>
                                            dưới đây nhé!
                                        </p>
                                    </div>
                                    <div>
                                        <b style="font-family: Stabile-Regular; font-size: 18px; margin-left: 18px;">Bước 2: Chọn Frame trang trí<br/></b>
                                    </div>

                                    @for($i=1; $i<=4; $i++)
                                        <div class="col-md-6 col-sm-6 pd-bt">
                                            <a onclick="return selectFrame(this);" data-url="/assets/images/frame/frame-0{{ $i }}.png" data-id="{{ $i }}" href="#">
                                                <img src="/assets/images/frame/frame-0{{ $i }}.png" class="img-responsive">
                                            </a>
                                        </div>
                                    @endfor

                                    <div class="col-md-12 text-center">
                                        <ul class="pagination changer hidden">
                                            <li><a href="#"><<</a></li>
                                            <li><a href="#" class="active">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">>></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-xs-12 select_frame visible-xs">
                                    <div class="qt-list">
                                        <div class="clearfix"></div>
                                        <b style="font-family: Stabile-Regular; font-size: 18px;">Bước 2: Chọn Frame trang trí<br/></b>
                                        <div class="clearfix"></div>
                                        <!-- Swiper -->
                                        <div class="swiper3">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    @for($i=1; $i<=4; $i++)
                                                        <div class="swiper-slide">
                                                            <div class=" col-sm-12 pd-bt">
                                                                <a onclick="return selectFrame(this);" data-url="/assets/images/frame/frame-0{{ $i }}.png" data-id="{{ $i }}" href="#">
                                                                    <img src="{{ url('/') }}/assets/images/frame/frame-0{{ $i }}.png" class="img-responsive">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    @endfor
                                                </div>
                                                <!-- Add Pagination -->
                                                <div class="swiper-pagination"></div>
                                                <!-- Add Arrows -->
                                                
                                            </div>
                                            <div class="swiper-button-next"></div>
                                            <div class="swiper-button-prev"></div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="col-xs-12 flleft hidden-lg hidden-md hidden-sm visible-xs text-center">
                                        <a class="sysbtn" onclick="funcHelp.helperAjax.ajax(this, 'bai-du-thi', $('#submitForm').serialize(), _callbackSubmit, 'POST', '.alert-submit'); return false;" href="#">
                                            Gửi bài dự thi
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @else
        <section>
            <div class="baiduthi">
                <div class="container">
                    <div class="col-md-12">
                        <div class="baiduthi-box">
                            <div class="topbaiduthi tranganh">
                                <h3 class="text-center pd">
                                    <br/><br/><br/><br/>

                                    @if(time() < config('custom.round_1_end_create'))
                                        @if($limit)
                                            Bạn đã gửi đủ {{ config('custom.round_1_limit') }} bài dự thi
                                        @else
                                            Bạn phải đăng nhập để tham gia cuộc thi <br/><br/>

                                            <a class="@if(!Auth::check()) btn-fblogin @endif" data-redirect="{{ url('bai-du-thi/create') }}" href="{{ url('bai-du-thi/create') }}">
                                                Đăng nhập bằng Facebook
                                            </a>

                                            <p class="absolute">
                                                <img src="{{ url('/') }}/assets/images/Trang-Gamecook_03.png" class="img-responsive">
                                            </p>
                                        @endif
                                    @else
                                        Đã hết thời gian gửi bài dự thi!
                                    @endif
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
    {{--<section>
        <div class="baiduthi">
            <div class="container">
                <div class="col-md-12">
                    <div class="baiduthi-box">
                        <div class="topbaiduthi tranganh">

                            <div>
                                <h3 class="text-center pd">cùng chia sẻ những công thức kem tuyệt đỉnh <br> tạo nên khoảnh khắc vui vẻ, ấm áp bên gia đình nhé!!!</h3>
                                <p class="absolute">
                                    <img src="{{ url('/')  }}/assets/images/Trang-Gamecook_03.png" class="img-responsive">
                                </p>
                                <div class="col-md-6 kemngon">
                                    <form id="submitForm">
                                        <div style="position: relative">
                                            <img id="image_crop1" src="{{ url('/')  }}/assets/images/kem-ngon.jpg" class="img-responsive">
                                            --}}{{--<img id="image_crop1" class="img-fluid" src="{{ url('assets/img/no-image.svg') }}" alt="" style="border-radius: 4px !important;">--}}{{--
                                            <div class="opacity-image1" style="width: 100%;height: 100%;position: absolute;top: 0;"></div>
                                            <input class="d-none" type="file" name="image_file1" id="image_file1">
                                            <input type="hidden" name="image1" value="" id="image_value1"/>
                                            <div id="image_frame" style="z-index: -1; position: absolute; top:0; left:0; width: 100%; height: 100%; background-image: url('{{ url('assets/img/frame.png') }}'); background-size: 100% 100%; background-repeat:   no-repeat; background-position: center center;  "></div>
                                        </div>
                                    </form>
                                    <div class="col-md-6 col-sm-6 flright">
                                        <a href="#" class="btn-file1">
                                            Tải ảnh lên
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 flleft">
                                        <a onclick="funcHelp.helperAjax.ajax(this, 'bai-du-thi', $('#submitForm').serialize(), _callbackSubmit, 'POST', '.alert-submit'); return false;" href="#">
                                            Gửi bài dự thi
                                        </a>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-submit alert d-none"></div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6 nenkem">
                                    <div class="bg-dungquen">
                                        <p class="dungquen">
                                            Đừng quên <br>
                                            trang trí công thức kem <br> tuyệt  đỉnh của mình với những <br> khung ảnh siêu độc đáo <br>
                                            dưới đây nhé!
                                        </p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pd-bt">
                                        <a href="">
                                            <img src="{{ url('/')  }}/assets/images/Vector-.png" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pd-bt">
                                        <a href="">
                                            <img src="{{ url('/')  }}/assets/images/Vector-.png" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pd-bt">
                                        <a href="">
                                            <img src="{{ url('/')  }}/assets/images/Vector-.png" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pd-bt">
                                        <a href="">
                                            <img src="{{ url('/')  }}/assets/images/Vector-.png" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="col-md-12 hidden">
                                        <ul class="pagination changer">
                                            <li><a href="#"><<</a></li>
                                            <li><a href="#" class="active">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">>></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>--}}

     <script>
  $(document).ready(function(){
      var swiper = new Swiper('.swiper-container', {
          slidesPerView: 4,
          spaceBetween: 0,
          slidesPerGroup: 1,
          loop: true,
          loopFillGroupWithBlank: true,
          navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev',
          },
          breakpoints: {
              // when window width is <= 320px
              320: {
                  slidesPerView: 2,
                  spaceBetween: 0
              },
              // when window width is <= 480px
              480: {
                  slidesPerView: 2,
                  spaceBetween: 0
              },
              // when window width is <= 640px
              940: {
                  slidesPerView: 2,
                  spaceBetween: 0
              }
          }
      });

      function checkScroll(){
          /*$('.kemngon').css('height', $('.kemngon').outerHeight() + 10);
          $('.kemngon').css('clear', 'both');

          $('.select_frame').css('height', $('.select_frame .swiper3').outerHeight() + 10);*/

          var $height = $('body > section').outerHeight() + $('body > header').outerHeight();
          $("body").css('height', $height - 10);
          $("body").css('background-size', '100% '+ ($height + 150) + 'px');
      }
      checkScroll();
      window.onresize = function(e){
          //checkScroll();
      };

      // $("body").css('height', wHeight); 
      $(".toggle-menu-mb").click(function(){
          $(".menu-mobile-list").toggle("slide", { direction: "right" }, 200);
          $(".layout-toggle-menu").fadeIn(200);
      })
      $(".layout-toggle-menu").click(function(){
          $(".menu-mobile-list").toggle("slide", { direction: "right" }, 200);
          $(".layout-toggle-menu").fadeOut(200);
      })
      function checkMenu(){
          if($(window).width() > 768){
              $(".layout-toggle-menu").fadeOut(200);
              $(".menu-mobile-list").css("display", "none");
          }
      }
      checkMenu();
      window.onresize = function(e){
          checkMenu();
      };
  })
</script>

    {{--<form id="submitForm" action="/submit" method="post">
        <!-- signup content -->
        <div class="container">
            <div class="">
                <h1 class="">ĐĂNG KÝ DỰ THI</h1>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="wrapper-image-crop" style="position: relative;">
                        <img id="image_crop1" class="img-fluid" src="{{ url('assets/img/no-image.svg') }}" alt="" style="border-radius: 4px !important;">
                        <div class="opacity-image1" style="width: 100%;height: 100%;position: absolute;top: 0;"></div>
                        <input class="d-none" type="file" name="image_file1" id="image_file1">
                        <input type="hidden" name="image1" value="" id="image_value1"/>
                        <div id="image_frame" style="z-index: -1; position: absolute; top:0; left:0; width: 100%; height: 100%; background-image: url('{{ url('assets/img/frame.png') }}'); background-size: 100% 100%; background-repeat: no-repeat; background-position: center center;"></div>
                    </div>

                    <div class="btn-group more-opt1 d-none" style="margin-left: auto;margin-right: auto;display: table;">
                        <button type="button" class="btn btn-primary btn-rotate1" data-method="rotate" data-option="-90" title="Rotate Left">
                            <span class="docs-tooltip" data-toggle="tooltip" title=""><span class="fa fa-rotate-left"></span></span>
                        </button>
                        <button type="button" class="btn btn-primary btn-rotate1" data-method="rotate" data-option="90" title="Rotate Right">
                            <span class="docs-tooltip" data-toggle="tooltip" title=""><span class="fa fa-rotate-right"></span></span>
                        </button>
                    </div>
                    <p class="fs-15 text-focus my-1 more-opt1 d-none">Bạn có thể di chuyển ảnh về vị trí mong muốn</p>

                    <button class="btn btn-block btn-secondary-2 btn-file btn-file1 fw-7 fs-18 text-gray-dark text-uppercase" type="button">
                        <img class="btn-loading d-none" src="/assets/img/loading.gif"/>
                        <span class="btn-text">TẢI ẢNH LÊN</span>
                    </button>
                </div>
                <div class="col-md-6">
                    <img onclick="return selectFrame(this);" src="{{ url('assets/img/frame.png') }}" style="width: 100px;"/>
                    <img onclick="return selectFrame(this);" src="{{ url('assets/img/frame2.png') }}" style="width: 100px;"/>
                    <div class="clearfix"></div>
                    <textarea name="content"></textarea><div class="clearfix"></div>
                    <div class="alert-submit alert d-none"></div>

                    <a class="btn btn-primary" onclick="funcHelp.helperAjax.ajax(this, 'bai-du-thi', $('#submitForm').serialize(), _callbackSubmit, 'POST', '.alert-submit');" href="#">Gửi bài dự thi</a>
                </div>
            </div>
        </div>
    </form>--}}
@stop

@section('footer')
    <div class="modal fade" id="completedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close hidden" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h1 class="modal-title" id="exampleModalLabel">Xin chúc mừng!</h1>
                </div>
                <div class="modal-body">
                    <b>Bạn đã đăng bài thành công</b><br/>
                    Hãy cập nhật các thông tin cá nhân dưới đây để chúng tôi có thể liên hệ với bạn trong trường hợp nhận được giải thưởng của chương trình:<br/><br/>
                    <form class="form-horizontal" id="infoForm">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Email</label>
                            <div class="col-sm-8">
                                <input type="email" name="email" value="{{ Auth::user()->email }}" readonly="readonly" class="form-control" id="inputEmail3" placeholder="Địa chỉ email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Số điện thoại</label>
                            <div class="col-sm-8">
                                <input type="text" name="mobile" value="{{ Auth::user()->mobile }}" class="form-control" id="inputEmail3" placeholder="Số điện thoại">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Link Facebook</label>
                            <div class="col-sm-8">
                                <input type="text" name="facebook" value="{{ Auth::user()->facebook }}" class="form-control" id="inputEmail3" placeholder="Địa chỉ Facebook">
                            </div>
                        </div>
                    </form>
                    <div class="alert alert-info d-none"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary hidden" data-dismiss="modal">Đóng lại</button>
                    <button type="button" onclick="return _callbackInfo(this);" class="btn btn-primary"><i class="fa fa-send"></i> Hoàn tất</button>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ url('assets/lib/cropper/cropper.js') }}"></script>
    {{--<script src="{{ url('assets/js/caman.full.min.js') }}"></script>--}}

    <script>
        var submitId = 0;
        function _callbackSubmit(data){
            if(data.code){
                submitId = data.data.id;

                @if(Auth::user()->mobile)
                    window.location.href = '{{ url('bai-du-thi') }}/' + data.data.id + '?type=done';
                @else
                    $('#completedModal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                @endif
            }
        }

        function _callbackInfo(obj){
            var _cb = function(data){
                if(data.code){
                    setTimeout(function () {
                        window.location.href = '{{ url('bai-du-thi') }}/' + submitId + '?type=done';
                    }, 2000);
                }
            };

            funcHelp.helperAjax.ajax(obj, 'member/{{ Auth::user()->id }}', $('#infoForm').serialize(), _cb, 'POST', '.alert-info');
            return false;
        }

        function selectFrame(obj) {
            var $img = $(obj);
            var $url = $img.data('url');

            $('.input_frame').val( $img.data('id') );
            $('.cropper-face').css({
                'background-image': "url('"+ $url +"')",
                'background-size': '100% 100%',
                'background-repeat': 'no-repeat',
                'background-position': 'center center',
                'opacity': 1,
                'background-color': 'transparent'
            });

            return false;
        }

        $(function () {
            // Set width = height upload image
            var cw = $('.wrapper-image-crop').width();
            $('.wrapper-image-crop').css({'height': cw + 'px'});

            function buttonStartLoading(element) {
                $button = $(element);
                if($button.prop('disabled'))
                    return false;
                $button.find('.btn-loading').toggleClass('d-none');
                $button.find('.btn-text').toggleClass('d-none');
                $button.prop('disabled', true);
                return true;
            }

            function buttonEndLoad(element) {
                $button = $(element);
                $button.find('.btn-loading').toggleClass('d-none');
                $button.find('.btn-text').toggleClass('d-none');
                $button.prop('disabled', false);
            }

            $('.btn-file1').click(function(){
                $('#image_file1').trigger('click');
                return false;
            });

            $('#image_file1').on('change', function (e) {
                if(typeof this.files[0] == 'undefined' || !buttonStartLoading('.btn-file1')) {
                    return false;
                }
                var file = this.files[0];
                var reader = new FileReader();
                //console.log(file.type);

                reader.addEventListener("load", function (e) {
                    var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
                    var header = "";
                    for (var i = 0; i < arr.length; i++) {
                        header += arr[i].toString(16);
                    }

                    function mimeType(headerString) {
                        switch (headerString) {
                            case "89504e47":
                                type = "image/png";
                                break;
                            case "47494638":
                                type = "image/gif";
                                break;
                            case "ffd8ffe0":
                            case "ffd8ffe1":
                            case "ffd8ffe2":
                                type = "image/jpeg";
                                break;
                            default:
                                type = "unknown";
                                break;
                        }
                        return type;
                    }

                    var mimeType = mimeType(header).split('/');
                    if(mimeType[0] != 'image'){
                        $('#image_file1').val('');
                        alert('Bạn phải upload file ảnh!');

                        buttonEndLoad('.btn-file1');
                        $('.more-opt1').removeClass('d-none');
                        $('.opacity-image1').addClass('d-none');
                        $('.btn-file1 .btn-text').html('Tải ảnh lên');
                        return false;
                    }

                    function callbackCompress(src) {
                        $('#image_crop1').attr("src", src);
                        $('#image_crop1').cropper('destroy');

                        buttonEndLoad('.btn-file1');
                        $('.more-opt1').removeClass('d-none');
                        $('.opacity-image1').addClass('d-none');
                        $('.btn-file1 .btn-text').html('Đổi ảnh');

                        var cropper = $('#image_crop1').cropper({
                            viewMode: 3,
                            aspectRatio: 3/2,
                            autoCropArea: 1,
                            guides: true,
                            highlight: false,
                            zoomable: false,
                            cropBoxResizable: false,
                            responsive: true,
                            minContainerWidth: cw,
                            setCropBoxData: {
                                width: 600,
                                height: 400
                            },
                            crop: function (e) {
                                var imageData = $('#image_crop1').cropper('getCroppedCanvas',
                                    {
                                        width: 600,
                                        height: 400,
                                        fillColor: '#fff',
                                        imageSmoothingEnabled: false,
                                        imageSmoothingQuality: 'high'
                                    }
                                ).toDataURL();
                                // $('#image_preview1').attr('src', imageData);
                                $('#image_value1').val(imageData);
                            }
                        });

                        $('.btn-rotate1').click(function(){
                            var rotate = $(this).data('option');
                            $('#image_crop1').cropper('rotate', Number(rotate));
                        });
                    }


                    var objImage = new Image();
                    objImage.onload = function () {
                        var width = objImage.naturalWidth,
                            height = objImage.naturalHeight;
                        window.URL.revokeObjectURL(objImage.src);

                        //if ((width <= 800 && height <= 800) && file.size < 1048576) {
                        if (0) {
                            callbackCompress(reader.result);
                        }
                        else {
                            var $data = new FormData();
                            $data.append('image_upload', file);

                            $.ajax({
                                type: 'POST',
                                url: '/upload/compress',
                                data: $data,
                                processData: false,
                                contentType: false,
                                success: function (response) {
                                    callbackCompress('data:image/jpeg;base64,' + response.image);
                                    $('#image_file1').val('');
                                    $('.btn-file2').hide();
                                },
                                error: function (response) {
                                }
                            });
                        }
                    };

                    objImage.src = window.URL.createObjectURL(file);
                }, false);

                if (file) {
                    reader.readAsArrayBuffer(file);
                }
            });
        });
    </script>

@stop