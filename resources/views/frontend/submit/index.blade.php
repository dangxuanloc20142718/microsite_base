@extends('layouts.default')
@section('header')
    <title>Bài dự thi - {{ config('custom.seo_title') }}</title>
    <meta name="description" content="Bài dự thi - {{ config('custom.seo_description') }}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}"/>
    <meta property="og:image" content="{{ url('media/system/fbimage.jpg?'.config('version.version'))  }}"/>
    <meta property="og:title" content="Bài dự thi - {{ config('custom.seo_title') }}"/>
    <meta property="og:description" content="Bài dự thi - {{ config('custom.seo_description') }}"/>

    <link type="text/css" href="{{ url('/') }}/assets/css/baiduthi.css?{{ config('custom.version') }}" rel="stylesheet"/>
@stop

@section('content')
    <section>
        <div class="baiduthi">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-md-12"> -->
                    <div class="col-12 col-md-8 col-sm-11 col-sx-1">
                        <div class="topbaiduthi baithi1">
                            <div>
                                <div class="text-center pd" style="padding-left: 0px;"><p>đã có <span class="headtext-small">{{ $totalSubmit }}</span> công thức kem hè tuyệt đỉnh được chia sẻ. Hãy tham gia dự thi ngay để nhận những phần quà <br/>
                                        thật hấp dẫn từ nhãn hàng kem wall's nhé!<br/>
                                        <span class="headtext-small">150</span> công thức kem được *thả tim* nhiều nhất sẽ nhận  ngay <span class="headtext-big">voucher</span> coopmart <span class="headtext-big">200k</span> ăn kem wall's miễn phí. Hình ảnh dự thi hợp lệ phải thể hiện đúng chủ đề của cuộc thi, gồm cả hình ảnh công thức kem và các thành viên trong gia đình.</p>
                                    <div class="sub-title"><img src="/assets/images/big-heart.png" alt=""> <a class="top-favorite">Danh sách bài dự thi mới nhất </a><img src="/assets/images/big-heart.png" alt=""></div></div>
                                <div class="row">
                                    @include('frontend.submit.index_items')
                                    <div class="clearfix"></div>

                                </div>
                                
                            </div>
                        </div>
                    </div>

                    <!-- </div> -->
                    <div class="col-6 col-md-4">
                        <div class="bxh baithi2">
                            <div>
                                <div class="search-section"><h1 class="bxh-title">Bảng xếp hạng</h1>
                                    <form action="#">
                                        <input class="input_keyword input-search" type="text" placeholder="Nhập tên của bạn">
                                        <button onclick="return searchSubmit(this);" class="button-search" type="submit"><img src="/assets/images/search-ic.png" alt=""></button>
                                    </form>
                                </div>
                                <table class="table table_top">
                                    @include('frontend.submit.index_top')
                                </table>

                                <div class="thamgia hidden">
                                    <a class="@if(!Auth::check()) btn-fblogin @endif" data-redirect="{{ url('bai-du-thi/create') }}" href="{{ url('bai-du-thi/create') }}">Tham gia ngay</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <script>
        $(document).ready(function(){

            function checkScroll(){
                var $height = $('body > section').outerHeight() + $('body > header').outerHeight();

                $("body").css('height', $height + 100);
                $("body").css('background-size', '100% '+ ($height + 150) + 'px');
            }
            checkScroll();
            window.onresize = function(e){
                checkScroll();
            }

            // $("body").css('height', wHeight);
            $(".toggle-menu-mb").click(function(){
                $(".menu-mobile-list").toggle("slide", { direction: "right" }, 200);
                $(".layout-toggle-menu").fadeIn(200);
            })
            $(".layout-toggle-menu").click(function(){
                $(".menu-mobile-list").toggle("slide", { direction: "right" }, 200);
                $(".layout-toggle-menu").fadeOut(200);
            })
            function checkMenu(){
                if($(window).width() > 768){
                    $(".layout-toggle-menu").fadeOut(200);
                    $(".menu-mobile-list").css("display", "none");
                }
            }
            checkMenu();
            window.onresize = function(e){
                checkMenu();
            };
        });
    </script>




{{--<div class="container">
    <h1>Bài dự thi</h1>
    @if(count($submits))
        <div class="row submit-list">
            @include('frontend.submit.index_items')
        </div><div class="clearfix"></div>

        <div class="text-center">
            <a class="btn btn-block btn-outline-gray-dark w-100 w-md-auto px-5" href="#" onclick="return loadSubmits(this, '.submit-list');">
                Xem thêm
            </a>
        </div>
    @else
        Chưa có bài dự thi
    @endif
</div>--}}

@stop

@section('footer')
    <script type="text/javascript">
        var indexSubmit = 2;
        function loadSubmits(obj, divClass){
            var $div = $(divClass);
            var _success = function(data){
                if(data.status){
                    if(obj) {
                        indexSubmit++;
                        $div.append(data.html);
                    }
                }

                if(data.status == 2 && obj){
                    $div.html('Không còn bài dự thi nào!');
                }
            };

            funcHelp.helperAjax.ajax(obj, 'bai-du-thi?ajax=1&page='+indexSubmit , {}, _success, 'GET');
            return false;
        }

        pageTop = 1;
        function searchSubmit(obj) {
            var $div = $('.table_top');
            var keyword = $('.input_keyword').val();

            var _success = function(data){
                if(data.status){
                    //pageTop++;
                    $div.html(data.html);

                    if(data.status === 2){
                        $div.html('Không tìm thấy kết quả nào');
                    }
                }
            };

            funcHelp.helperAjax.ajax(obj, 'bai-du-thi?ajax=1&filter=vote&limit=10&layout=top&keyword='+keyword+'&page='+pageTop , {}, _success, 'GET');
            return false;
        }
    </script>
@stop