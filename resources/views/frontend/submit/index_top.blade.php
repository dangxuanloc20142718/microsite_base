<tbody>
<tr>
    <td>STT</td>
    <td>Họ tên</td>
    <td>Like</td>
    <td>Share</td>
</tr>
@if(count($topVote))
    @foreach($topVote as $k => $s)
        <tr>
            <td>{{ $k + 1 }}</td>
            <td><a href="{{ url('bai-du-thi/' . $s->id) }}">{{ $s->title }}</a></td>
            <td>{{ $s->votes }}</td>
            <td>{{ $s->shares }}</td>
        </tr>
    @endforeach
@else
    <tr><td colspan="4">Chưa có bài dự thi</td></tr>
@endif
</tbody>

@if(count($topVote))
<tfoot class="top-vote-footer">
    <tr>
        <td colspan="4" class="text-center">
            {!! $topVote->links() !!}
        </td>
    </tr>
</tfoot>
    <script>
        $('.top-vote-footer .pagination a').click(function(){
            pageTop = getParameterByName('page', $(this).attr('href')) || 1;
            searchSubmit(this);
            return false;
        });

        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }
    </script>
@endif