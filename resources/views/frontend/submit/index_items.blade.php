@foreach( $submits as $s )
<div class="col-md-3 col-sm-6 col-sx-1 vote-item" data-tag='web'>
    <a href="{{ url('bai-du-thi/'.$s->id) }}">
        <div class="thumbnail vote-thumb vote-thumb-{{ $s->id }}">
            <img src="{{ url($s->image) }}" alt="">
        </div>
        <div class="like-share">
            <div class="like">
                <a @if(Auth::check()) onclick="return _voteCallback({{ $s->id }});" @else onclick="return funcHelp.helperFunction.fblogin('{{ url()->current() }}', this);" @endif data-vote-id="{{ $s->id }}" href="#">
                    <span class="vote-count-{{ $s->id }}">{{ $s->votes }}</span>
                    <img src="{{ url('/') }}/assets/images/heart.png" alt=""/>
                </a>
            </div>
            <div class="share">
                <a class="facebook_share" href="{{ url('bai-du-thi/'.$s->id) }}" data-id="{{ $s->id }}" >
                    <span class="vote-share-{{ $s->id }}">{{ $s->shares }}</span>
                    <img src="{{ url('/') }}/assets/images/share-btn.png" alt="">
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </a>
    <div class="clearfix"></div>
</div>
@endforeach

<div class="clearfix"></div>
<div class="vote-footer text-center">
    {!! $submits->links() !!}
</div>

<script>


    /*$('.vote-footer .pagination a').click(function(){
        pageTop = getParameterByName('page', $(this).attr('href')) || 1;
        searchSubmit(this);
        return false;
    });

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }*/
</script>