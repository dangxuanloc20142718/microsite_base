@extends('layouts.default')
@section('header')
    <title>Sửa bài dự thi {{ $submit->title }}</title>
    <meta name="description" content="Sửa bài dự thi {{ $submit->title }}">
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="{{ url('/')  }}"/>
    <meta property="og:url" content="{{ Request::url()  }}" />
    <meta property="og:image" content="{{ url('media/system/fbimage.jpg?'.config('custom.version'))  }}" />
    <meta property="og:title" content="Sửa bài dự thi {{ $submit->title }}" />
    <meta property="og:description" content="Sửa bài dự thi {{ $submit->title }}" />
@stop

@section('content')
    <div class="edit-submit py-6">
        <div class="container py-md-5">
            <div class="row">
                <h3 class="bdt__info-name">Hello {{$user->name}}</h3>
                <div id="s3-screen-2" class="s3__screen s3__screen-2">
                    <a name="round"></a>
                    <form id="submitForm">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
                        <div class="row mx-0 h-100">
                            <div class="col-xl-4 px-0">
                                <div class="p-35px">
                                    <p class="text-primary fs-16 fs-xl-24 mb-4">Profile</p>

                                    <form class="form-material">
                                        <div class="form-group mb-4">
                                            <div class="upload rounded-circle d-flex justify-content-center align-items-center m-auto" id="avatar_image" @if($user->avatar) style="background-image: url('{{ url($user->avatar) }}')" @endif>
                                                <i class="hidden icon-user d-inline-block-"></i>
                                            </div>
                                            <a href="#" class="btn btn-xs btn-success pull-right" data-name="avatar" onClick="CreatedUploadFiles(this, '#msglog-image', '#avatar_value', '#avatar_image', null, null); return false;" style="padding:3px; margin-top:-20px;  position: relative; z-index: 1;">Đổi ảnh</a>
                                            <div id="msglog-image" style="margin-top: 25px;"></div>
                                            <input type="hidden" name="avatar" value="{{ $user ? $user->avatar : '' }}" id="avatar_value"/>
                                            <div class="clearfix"></div>
                                        </div>

                                        {{--<script>--}}
                                            {{--function cb_upload_avatar(data) {--}}
                                                {{--if (data.code == 1) {--}}
                                                    {{----}}
                                                {{--}--}}
                                            {{--}--}}
                                        {{--</script>--}}

                                        <div class="form-group">
                                            <label class="mb-0 fs-15 text-gray-lighter" for="fc-1">Nickname</label>
                                            <input type="text" name="name" readonly value="{{ $user ? $user->name : '' }}" class="form-control form-control--material" id="fc-1">
                                            <small id="emailHelp" class="form-text text-danger hidden">Error message.</small>
                                        </div>

                                        <div class="form-group hidden">
                                            <label class="mb-0 fs-15 text-gray-lighter" for="fc-2">Mật khẩu</label>
                                            <input type="password" class="form-control form-control--material" id="fc-2">
                                        </div>

                                        <div class="form-group">
                                            <label class="mb-0 fs-15 text-gray-lighter" for="fc-3">Email</label>
                                            <input type="email" name="email" value="{{ $user ? $user->email : '' }}" class="form-control form-control--material" id="fc-3">
                                        </div>

                                        <div class="form-group">
                                            <label class="mb-0 fs-15 text-gray-lighter" for="fc-4">Mobile</label>
                                            <input type="tel" name="mobile" value="{{ $user ? $user->mobile : '' }}" class="form-control form-control--material" id="fc-4">
                                        </div>

                                        <div class="form-group">
                                            <label class="mb-0 fs-15 text-gray-lighter" for="fc-5">Năm sinh</label>
                                            <input type="text" name="birthday" value="{{ ($user && $user->birthday) ? date('Y', strtotime($user->birthday)) : '' }}" class="form-control form-control--material" id="fc-5">
                                        </div>

                                        <div class="form-group hidden">
                                            <label class="mb-0 fs-15 text-gray-lighter" for="fc-6">Facebook</label>
                                            <input type="text" name="facebook" value="{{ $user ? $user->facebook : '' }}" class="form-control form-control--material" id="fc-6">
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="col-xl-8 px-0 d-flex flex-column border-left">
                                <div class="media align-items-stretch">
                                    <div class="media-body p-35px">
                                        <input type="hidden" name="attach" id="attach_value" value="{{ $submit->attach }}"/>
                                        <p class="text-primary fs-16 fs-xl-24 mb-3">CV</p>
                                        @if(!$submit->attach)
                                            <p class="mb-0">
                                                Hãy gửi CV của bạn cho chúng tôi.<br/>
                                                File CV có dạng PDF, DOC, DOCX, PPT, PPTX, JPG, JPEG.<br/>
                                                Dung lượng không quá 10MB.
                                            </p>
                                        @else
                                            <p class="mb-0">
                                                File CV của bạn: <a href="{{ url($submit->attach) }}" target="_blank">Xem CV</a><br/>
                                                Bạn có thể gửi lại CV của mình.<br/>
                                                File CV có dạng PDF, DOC, DOCX, PPT, PPTX, JPG, JPEG.<br/>
                                                Dung lượng không quá 10MB.
                                            </p>
                                        @endif
                                        <span id="msglog-cv"></span>
                                    </div>
                                    <div class="upload d-flex justify-content-center align-items-center d-flex">
                                        <label for="upload-cv" class="upload__link fs-16 mb-0 text-center" href="#" data-name="cv" onclick="CreatedUploadFiles(this, '#msglog-cv', '#attach_value', null, null, null); return false;">
                                            <i class="icon-upload d-inline-block"></i><br/>
                                            Tải lên
                                        </label>
                                    </div>
                                </div>

                                <div class="dbai d-flex flex-column text-white">
                                    <div class="dbai__body row gutter-35px w-100 mx-0">
                                        <div class="col-md-6 px-35px py-4 border-right-light">
                                            <p class="fs-16 fs-xl-24">Đề bài</p>
                                            <h2 class="dbai__title mb-4">
                                                <label class="mb-0" for="fc-ta-1">Tôi từng nghĩ</label>
                                            </h2>
                                            <textarea name="introtext" class="form-control dbai__form-control" id="fc-ta-1" placeholder="Sẽ mua một cái máy ảnh để làm phóng viên...">{{ $submit->introtext }}</textarea>
                                        </div>
                                        <div class="col-md-6 px-35px py-4">
                                            <p class="fs-24">&nbsp;</p>
                                            <h2 class="dbai__title mb-4">
                                                <label class="mb-0" for="fc-ta-2">Cho đến khi</label>
                                            </h2>
                                            <textarea name="fulltext" class="form-control dbai__form-control" id="fc-ta-2" placeholder="biết rằng dùng điện thoại chụp ảnh cũng có thể trở thành một phóng viên...">{{ $submit->fulltext }}</textarea>
                                        </div>
                                    </div>
                                    <div class="alert alert-danger alert-submit pull-left hidden"></div>
                                    <div class="dbai__footer d-flex justify-content-end p-4">
                                        <a onclick="return ajaxEditSubmit(this, $('#submitForm').serialize(), {{ $submit->id }});" id="dbai-btn-send-" class="dbai__btn-send btn btn-secondary" href="#">Cập nhật Bài thi & Profile</a>
                                    </div>

                                    <div class="dbai__nav nav1 d-flex">
                                        <a class="nav1__arrow nav1__prev d-flex justify-content-center align-items-center px-3 sel_bg_submit" href="#back">
                                            <i class="icon-chevron-left"></i>
                                        </a>
                                        <div class="nav1__num icon-square d-flex justify-content-center align-items-center">
                                            <strong class="fs-20 index_bg_submit">{{ $submit->image }}</strong>
                                            <input type="hidden" name="image" value="{{ $submit->image }}">
                                        </div>
                                        <a class="nav1__arrow nav1__next d-flex justify-content-center align-items-center px-3 sel_bg_submit" href="#next">
                                            <i class="icon-chevron-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <script type="text/javascript">
                        var class_cat = $('.cat_class').val();
                        var style_bg = '';
                        // Thay đổi màu sắc cho button đăng bài submit
                        $('.sel_bg_submit').on('click', function(){
                            var num = Number($('.index_bg_submit').text());
                            if(isNaN(num)) num = 1;
                            if($(this).attr('href') == '#next') {
                                if(num == 3) num = 1; else ++num;
                            } else {
                                if(num == 1) num = 3; else --num;
                            }
                            style_bg = 'background-image: url("/assets/images/themes/'+ class_cat +'/bg/bg-dbai-'+ num +'.jpg")';
                            $('.dbai').attr('style', style_bg);
                            $('.bg-img-submit').attr('style', style_bg);
                            $('.index_bg_submit').text(num);
                            $('.index_bg_submit ~ input[name="image"]').val(num);
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer')

@stop