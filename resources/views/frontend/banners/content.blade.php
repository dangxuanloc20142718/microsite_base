<div class="row">
@foreach($bannercontent as $key=>$item)
    @if($item->image)
        <div class="bottom15 col-xs-6 col-sm-6 col-md-12">
            <a href="{{$item->link}}" title="{{$item->title}}">{{ Html::image($item->image, $item->title, array('class'=>'')) }}</a>
        </div>
    @endif
@endforeach
</div>