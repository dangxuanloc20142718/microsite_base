<div class="row" style="padding: 0px 15px;">
    <div class="customer">
        @foreach($customer as $key=>$item)
            @if($item->image)
                <div class="">
                    <a href="{{$item->link}}" title="{{$item->title}}">
                        {{ Html::image($item->image, $item->title, array('class'=>'')) }}
                    </a>
                </div>
            @endif
        @endforeach
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.customer').slick({
            slidesToShow: 5,
            infinite: true,
            speed: 300,
            dots: false,
            arrows: true
        });
    });
</script>