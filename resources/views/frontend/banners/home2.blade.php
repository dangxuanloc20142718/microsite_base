<div class="row">
@foreach($bannerhome2 as $key=>$item)
    @if($item->image)
        <div class="bottom15 col-xs-12 col-sm-6 col-md-6">
            <a href="{{$item->link}}" title="{{$item->title}}">{{ Html::image($item->image, $item->title, array('class'=>'')) }}</a>
        </div>
    @endif
@endforeach
</div>