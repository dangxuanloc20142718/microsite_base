@foreach($bannerhome1 as $key=>$item)
    @if($item->image)
        <div class="bottom15">
            <a href="{{$item->link}}" title="{{$item->title}}">{{ Html::image($item->image, $item->title, array('class'=>'')) }}</a>
        </div>
    @endif
@endforeach