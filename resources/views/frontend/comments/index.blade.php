@foreach( $items as $k => $item )
    <div class="item {{ (($k+1)%2 == 0) ? '' : 'odd' }}">
        <div class="content">
            {{ $item->comment }}<br/>
            <span class="text-gray"><b>{!! $item->fullname !!}</b> - {{ \App\Helper\StringHelper::date($item->created_at) }}</span>
        </div>
        @if($item->reply)
        <div class="reply">
            <i class="glyphicon glyphicon-user"></i> {{ $item->reply }}<br/>
            <span class="text-gray"><b>Admin</b></span>
        </div>
        @endif
    </div>
@endforeach
{!! $items->links() !!}