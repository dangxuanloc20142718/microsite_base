<html>
<body>
<div class="mailbox">
    <h3 class="title"></h3>
    <div class="contents">
        <p>Thân chào bạn,</p>
        <p>Ban Tổ Chức xin chúc mừng bạn đã dũng cảm bắt đầu <strong> “BƯỚC ĐI ĐẦU TIÊN - VÒNG 1”</strong> của cuộc thi <strong>KỲ THỰC TẬP TRONG MƠ MÙA 2 – ĐI RỒI SẼ ĐẾN! </strong>và là những ứng cử viên hoàn toàn hợp lệ để chính thức bước tiếp vào <strong>VÒNG  2 – CỨ ĐI THÔI!</strong>
        </p>
        <p>Tại vòng này, bạn sẽ phải hoàn thành thử thách tương ứng với ngành nghề bạn muốn thực tập do Ban Giám Khảo đề ra nhằm giúp Ban Giám Khảo chọn được những thí sinh xuất sắc nhất với những tố chất và kỹ năng phù hợp với nghề thể hiện qua thử thách đặc biệt của chương trình.</p>
        <p><strong>ĐỢT 1: 00:00’ 03/08/2017 - 23:59’ 10/08/2017, DO BGK CHẤM, CHIẾM 70% </strong></p>
        <p><strong>TỔNG SỐ ĐIỂM</strong></p>
        <p>Sau khi hoàn thành vòng đăng ký, mỗi thí sinh sẽ chọn 1 ngành nghề và nhận một thử thách từ “Kì thực tập trong mơ” trong vòng 08 ngày.</p>
        <ul>
            <li>
                <p><strong>BƯỚC 1</strong></p>
                <p>Thí sinh đăng nhập vào tài khoản đã được tạo khi tham gia Vòng 1, để nhận đề thi, chỉ có thí sinh tham gia Vòng 1 mới hợp lệ để tham gia Vòng 2 (thí sinh hợp lệ sẽ được nhận email thông báo)</p>
            </li>
            <li>
                <p><strong>BƯỚC 2</strong></p>
                <p>Thí sinh tiến hành thực hiện thử thách theo yêu cầu của chương trình, và nộp lại bài theo đúng định dạng quy định tại website của chương trình. Sau 23:59’ ngày 10/08/2017 hệ thống sẽ đóng lại và bạn không thể nộp bài dự thi của mình.</p>
            </li>
        </ul>
        <p><strong>ĐỢT 2: 00:00’ 11/08/2017 - 23:59’ 13/08/2017, DO KHÁN GIẢ BÌNH CHỌN, CHIẾM 30% TỔNG SỐ ĐIỂM</strong></p>
        <ul>
            <li>
                <p><strong>BƯỚC 1</strong></p>
                <p>Thí sinh chia sẻ bài dự thi của mình trên trang mạng xã hội cá nhân với hashtag #take1stmove #diroiseden #BitisHunter #kythuctaptrongmo</p>

            </li>
            <li>
                <p><strong>BƯỚC 2</strong></p>
                <p>Thí sinh thể hiện sự ảnh hưởng của mình bằng cách kêu gọi bạn bè và cộng đồng vote cho bài dự thi của mình.</p>
            </li>
        </ul>
        <p><em>BGK chấm điểm và chọn ra Top 15 bài dự thi xuất sắc nhất để đến với Vòng 3. <br>
                Top 3 bạn có lượt vote cao nhất sẽ được đặt cách tham gia phỏng vấn,
            </em></p>
        <p>Hãy tự tin và hoàn thành thử thách theo cách của chính bạn. Vì Biti’s Hunter tin rằng Đi rồi sẽ đến, còn chần chờ gì mà không bắt tay vào thực hiện thử thách ngay nào!</p>
        <p>Chúc bạn sẽ có những trải nghiệm tuyệt vời trong quá trình thực hiện thử thách nhé! <br>
            Ban Tổ Chức chúc bạn sẽ xuất sắc vượt qua thử thách tại vòng 2. <br>
            Kết quả vòng 2 sẽ được công bố vào ngày 23/08/2017. <br>
        </p>
        <p>#take1stmove #diroiseden #BitisHunter #kythuctaptrongmo</p>
        <hr>
        <p>Còn chần chờ gì nữa, cùng xem phần thử thách dành riêng cho bạn – một ứng cử viên của Kỳ thực tập trong mơ mùa 2 nào!</p>
        <p><em>“Những Multimedia Reporter biết rằng tài năng không đến từ việc đọc lý thuyết suông, họ luôn là những người không ngừng rèn luyện chính mình với đôi bàn tay vừa biết viết, biết chụp để tạo ra những bài viết lôi cuốn, hấp dẫn cho người đọc. bằng chính lòng nhiệt huyết, sẵn sàng xông pha, “săn” tin cực chất. <br>
                Là một Multimedia Reporter trẻ, hãy chứng tỏ khả năng trở thành một Multimedia Reporter của bạn bằng việc thực hiện một bài báo thuộc 1 trong 3 nhóm chủ đề: Giải Trí, Đời Sống hoặc Tin tức Xã hội để chia sẻ về tin tức mà bạn quan tâm và muốn truyền tải đến mọi người.. <br>
                <mark>Các bạn thí sinh viết 01 bài báo về 1 trong 3 nhóm chủ đề trên,độ dài bài viết từ 800-1000 từ, trong bài dự thi  phải thể hiện được kĩ năng báo chí và góc nhìn khách quan của chủ đề bạn khai thác và hình thức bài dự thi phải đáp ứng đầy đủ gồm ảnh minh hoạ rõ nét cho bài viết hoặc video ngắn, tối đa 1 phút.”</mark></em></p>
        <p><em><strong><ins>Lưu ý, hãy nghiên cứu kỹ tiêu chí chấm điểm để khai thác và giành số điểm tối đa trong 70% điểm đến từ BGK cho bài dự thi của bạn nhé, cụ thể như sau:</ins></strong></em></p>
        <ul>
            <strong><em>Phần nội dung:</em></strong>
            <li><em>Tiêu đề bài viết hay, kích thích người đọc, đánh vào cảm giác hiếu kì của khách hàng: 10 điểm</em></li>
            <li><em>Bố cục bài viết trình bày rõ ràng, nổi bật ý chính của từng phần: 15 điểm</em></li>
            <li><em>Nội dung bài viết thể hiện được quan điểm cá nhân của thí sinh đối với chủ đề bạn đang viết: 20 điểm</em></li>
            <li><em>Tính sáng tạo trong bài viết 10 điểm</em></li>
        </ul>
        <ul>
            <strong><em>Phần hình thức:</em></strong>
            <li><em>Hình ảnh chất lượng, phù hợp với nội dung bài viết: 10 điểm </em></li>
            <li><em>Tính thẩm mỹ: 5 điểm </em></li>
        </ul>
    </div>
</div>
</body>
</html>