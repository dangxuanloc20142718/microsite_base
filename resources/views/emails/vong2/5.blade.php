<html>
<body>
<div class="mailbox">
    <h3 class="title"></h3>
    <div class="contents">
        <p>Thân chào bạn,</p>
        <p>Ban Tổ Chức xin chúc mừng bạn đã dũng cảm bắt đầu <strong> “BƯỚC ĐI ĐẦU TIÊN - VÒNG 1” </strong> của cuộc thi <strong>KỲ THỰC TẬP TRONG MƠ MÙA 2 – ĐI RỒI SẼ ĐẾN! </strong> và là những ứng cử viên hoàn toàn hợp lệ để chính thức bước tiếp vào <strong>VÒNG  2 – CỨ ĐI THÔI! </strong>
        </p>
        <p>Tại vòng này, bạn sẽ phải hoàn thành thử thách tương ứng với ngành nghề bạn muốn thực tập do Ban Giám Khảo đề ra nhằm giúp Ban Giám Khảo chọn được những thí sinh xuất sắc nhất với những tố chất và kỹ năng phù hợp với nghề thể hiện qua thử thách đặc biệt của chương trình.</p>
        <p><strong>ĐỢT 1: 00:00’ 03/08/2017 - 23:59’ 10/08/2017, DO BGK CHẤM, CHIẾM 70% </strong></p>
        <p><strong>TỔNG SỐ ĐIỂM</strong></p>
        <p>Sau khi hoàn thành vòng đăng ký, mỗi thí sinh sẽ chọn 1 ngành nghề và nhận một thử thách từ “Kì thực tập trong mơ” trong vòng 08 ngày.</p>
        <ul>
            <li>
                <p><strong>BƯỚC 1</strong></p>
                <p>Thí sinh đăng nhập vào tài khoản đã được tạo khi tham gia Vòng 1, để nhận đề thi, chỉ có thí sinh tham gia Vòng 1 mới hợp lệ để tham gia Vòng 2 (thí sinh hợp lệ sẽ được nhận email thông báo)</p>
            </li>
            <li>
                <p><strong>BƯỚC 2</strong></p>
                <p>Thí sinh tiến hành thực hiện thử thách theo yêu cầu của chương trình, và nộp lại bài theo đúng định dạng quy định tại website của chương trình. Sau 23:59’ ngày 10/08/2017 hệ thống sẽ đóng lại và bạn không thể nộp bài dự thi của mình.</p>
            </li>
        </ul>
        <p><strong>ĐỢT 2: 00:00’ 11/08/2017 - 23:59’ 13/08/2017, DO KHÁN GIẢ BÌNH CHỌN, CHIẾM 30% TỔNG SỐ ĐIỂM</strong></p>
        <ul>
            <li>
                <p><strong>BƯỚC 1</strong></p>
                <p>Thí sinh chia sẻ bài dự thi của mình trên trang mạng xã hội cá nhân với hashtag #take1stmove #diroiseden #BitisHunter #kythuctaptrongmo</p>
            </li>
            <li>
                <p><strong>BƯỚC 2</strong></p>
                <p>Thí sinh thể hiện sự ảnh hưởng của mình bằng cách kêu gọi bạn bè và cộng đồng vote cho bài dự thi của mình.</p>
            </li>
        </ul>
        <p><em>BGK chấm điểm và chọn ra Top 15 bài dự thi xuất sắc nhất để đến với Vòng 3.</em></p>
        <p>Hãy tự tin và hoàn thành thử thách theo cách của chính bạn. Vì Biti’s Hunter tin rằng Đi rồi sẽ đến, còn chần chờ gì mà không bắt tay vào thực hiện thử thách ngay nào!</p>
        <p>Chúc bạn sẽ có những trải nghiệm tuyệt vời trong quá trình thực hiện thử thách nhé! <br>
            Ban Tổ Chức chúc bạn sẽ xuất sắc vượt qua thử thách tại vòng 2. <br>
            Kết quả vòng 2 sẽ được công bố vào ngày 23/08/2017. <br>
        </p>
        <p>#take1stmove #diroiseden #BitisHunter #kythuctaptrongmo</p>
        <hr>
        <p>Còn chần chờ gì nữa, cùng xem phần thử thách dành riêng cho bạn – một ứng cử viên của Kỳ thực tập trong mơ mùa 2 nào!</p>
        <p><em>“Việt Nam là nước trồng cà phê lớn thứ 2 thế giới sau Brazil, mỗi năm thu về 3 tỷ USD từ cà phê. Hương vị café đậm đà đã trở nên quen thuộc trong nhịp sống mỗi ngày của người dân Việt. Sự tinh tế của cafe Việt thể hiện ở nét văn hóa và phong cách thưởng thức café khác lạ của người Việt. Từ cốc café trứng lạ vị đến ly café sữa đá vỉa hè đều mang hương vị thời gian tinh tuý, một nét phong cách Á Đông rát Việt Nam. Tuy nhiên, với sự đa dạng ở nhiều cách chế biến và văn hoá khác nhau, café Việt vẫn là một ẩn số đối với bạn bè bốn phương, đặc biệt là châu Âu và các nước phương Tây. </em></p>

        <p><em>Với mục tiêu đưa văn hoá thưởng thức cafe Việt gần hơn với giới trẻ trong và ngoài nước. Đưa Việt Nam trở thành một trong những quốc gia xứng đáng thưởng thức cafe số một thế giới. Biến café truyền thống Việt trở thành một trong những món đáng thưởng thức nhất khi đến Việt Nam nói riêng và Đông Nam Á nói chung <br>
                Thử thách: Với vai trò là một Barista, bạn hãy thực hiện một video tối đa 3' về một món cafe Việt Nam. </em></p>

        <p><em><strong>Mục tiêu:</strong> Truyền cảm hứng với người xem <br>

                Đối tượng mục tiêu: từ 18 đến 30 tuổi <br>
                <br>
                <mark>Thí sinh nộp bài dự thi dưới hình thức link Youtube để chế độ công khai tại <a target="_blank" href="https://www.youtube.com/">https://www.youtube.com/</a>(đặt tên theo công thức: Kỳ Thực Tập Trong Mơ 2 – Barista - Tên thí sinh) <br>
                    Sau khi hoàn tất, bạn gửi link vào microsite chương trình (chỉ có thí sinh đã tham gia vòng 1 mới được nộp bài vòng 2)</mark>
                Ngôn ngữ bài dự thi có thể bằng tiếng Anh hoặc tiếng Việt <br>
                Bài thi nghiêm cấm việc đạo ý tưởng, đạo nhạc, sử dụng hình ảnh, footage không rõ nguồn gốc trên Google, thí sinh có thể sử dụng hình trên các trang ShutterStock, GettyImages và giữ nguyên watermark để làm minh họa cho bài làm”
            </em></p>
        <p><em><strong><ins>Lưu ý, hãy nghiên cứu kỹ tiêu chí chấm điểm để khai thác và giành số điểm tối đa trong 70% điểm đến từ BGK cho bài dự thi của bạn nhé, cụ thể như sau:</ins></strong></em></p>
        <ul>
            <strong><em>Phần nội dung:</em></strong>
            <li><em>Clip giới thiệu về 1 món cafe truyền thống cụ thể, rõ ràng của Việt Nam, có đề cập đến văn hoá/ nét riêng độc đáo của món café, lý do chọn món café để giới thiệu:: 20 điểm</em></li>
            <li><em>Clip phù hợp để truyền bá trong và ngoài nước: 15 điểm</em></li>
            <li><em>Cách kể chuyện/ giới thiệu món cafe truyền thống lôi cuốn, hấp dẫn người xem: 10 điểm</em></li>
            <li><em>Thể hiện được cá tính, quan điểm của thí sinh, tình yêu của thí sinh đối với café và đam mê mang ra quốc tế: 10 điểm</em></li>
            <li><em>Tính sáng tạo: 5 điểm</em></li>
        </ul>
        <ul>
            <strong><em>Phần hình thức:</em></strong>
            <li>Hình ảnh chất lượng cao, rõ ràng: 5 điểm</li>
            <li>Tính thẩm mỹ: 5 điểm </li>
        </ul>
    </div>
</div>
</body>
</html>