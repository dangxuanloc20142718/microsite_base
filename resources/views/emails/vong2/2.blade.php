<html>
<body>
<div class="mailbox">
    <h3 class="title"></h3>
    <div class="contents">
        <p>Thân chào bạn,</p>
        <p>Ban Tổ Chức xin chúc mừng bạn đã dũng cảm bắt đầu <strong>“BƯỚC ĐI ĐẦU TIÊN - VÒNG 1”</strong> của cuộc thi <strong> KỲ THỰC TẬP TRONG MƠ MÙA 2 –
                ĐI RỒI SẼ ĐẾN!</strong> và là những ứng cử viên hoàn toàn hợp lệ để chính thức bước tiếp vào <strong>VÒNG  2 – CỨ ĐI THÔI!</strong>
        </p>
        <p>Tại vòng này, bạn sẽ phải hoàn thành thử thách tương ứng với ngành nghề bạn muốn thực tập do Ban Giám Khảo đề ra nhằm giúp Ban Giám Khảo chọn được những thí sinh xuất sắc nhất với những tố chất và kỹ năng phù hợp với nghề thể hiện qua thử thách đặc biệt của chương trình.</p>
        <p><strong>ĐỢT 1: 00:00’ 03/08/2017 - 23:59’ 10/08/2017, DO BGK CHẤM, CHIẾM 70% </strong></p>
        <p><strong>TỔNG SỐ ĐIỂM</strong></p>
        <p>Sau khi hoàn thành vòng đăng ký, mỗi thí sinh sẽ chọn 1 ngành nghề và nhận một thử thách từ “Kì thực tập trong mơ” trong vòng 08 ngày.</p>
        <ul>
            <li>
                <p><strong>BƯỚC 1</strong></p>
                <p>Thí sinh đăng nhập vào tài khoản đã được tạo khi tham gia Vòng 1, để nhận đề thi, chỉ có thí sinh tham gia Vòng 1 mới hợp lệ để tham gia Vòng 2 (thí sinh hợp lệ sẽ được nhận email thông báo)</p>
            </li>
            <li>
                <p><strong>BƯỚC 2</strong></p>
                <p>Thí sinh tiến hành thực hiện thử thách theo yêu cầu của chương trình, và nộp lại bài theo đúng định dạng quy định tại website của chương trình. Sau 23:59’ ngày 10/08/2017 hệ thống sẽ đóng lại và bạn không thể nộp bài dự thi của mình.</p>
            </li>
        </ul>
        <p><strong>ĐỢT 2: 00:00’ 11/08/2017 - 23:59’ 13/08/2017, DO KHÁN GIẢ BÌNH CHỌN, CHIẾM 30% TỔNG SỐ ĐIỂM</strong></p>
        <ul>
            <li>
                <p><strong>BƯỚC 1</strong></p>
                <p>Thí sinh chia sẻ bài dự thi của mình trên trang mạng xã hội cá nhân với hashtag #take1stmove #diroiseden #BitisHunter #kythuctaptrongmo</p>
            </li>
            <li>
                <p><strong>BƯỚC 2</strong></p>
                <p>Thí sinh thể hiện sự ảnh hưởng của mình bằng cách kêu gọi bạn bè và cộng đồng vote cho bài dự thi của mình.</p>
            </li>
        </ul>
        <p><em>BGK chấm điểm và chọn ra Top 15 bài dự thi xuất sắc nhất để đến với Vòng 3.</em></p>
        <p>Hãy tự tin và hoàn thành thử thách theo cách của chính bạn. Vì Biti’s Hunter tin rằng Đi rồi sẽ đến, còn chần chờ gì mà không bắt tay vào thực hiện thử thách ngay nào!</p>
        <p>Chúc bạn sẽ có những trải nghiệm tuyệt vời trong quá trình thực hiện thử thách nhé! <br>
            Ban Tổ Chức chúc bạn sẽ xuất sắc vượt qua thử thách tại vòng 2. <br>
            Kết quả vòng 2 sẽ được công bố vào ngày 23/08/2017.
        </p>
        <p>#take1stmove #diroiseden #BitisHunter #kythuctaptrongmo</p>
        <hr>
        <p>Còn chần chờ gì nữa, cùng xem phần thử thách dành riêng cho bạn – một ứng cử viên của Kỳ thực tập trong mơ mùa 2 nào!</p>
        <p><em>“Giải trí là ngành tiềm năng thể hiện được văn hoá đặc sắc của mỗi quốc gia, là cầu nối cho sự phát triển và văn minh nhân loại. Việt Nam, đất nước đang trên đà phát triển và nhận được sự giao thoa của nhiều nền văn hoá thần tượng khác nhau trên thế giới, thử thách đặt ra cho những nghệ sĩ Việt là làm sao để hoà nhập nhưng vẫn tạo được chỗ đứng và cá tính riêng của mình trên đấu trường giải trí. Với mong muốn những nghệ sĩ Việt được đào tạo bài bản với lộ trình phát triển rõ ràng, nhằm đưa giải trí Việt Nam vươn xa ra tầm khu vực, giúp nghệ sĩ Việt đến gần hơn với khu vực và Thế giới. Giữa thị trường giải trí đầy cơ hội nhưng không ít những thách thức bất ngờ, Talent Management Specialist không chỉ là đưa người nghệ sĩ tới sân khấu với ánh đèn và khán giả của họ. Một Talent Management Specialist phải là người có thể giúp người nghệ sĩ vượt ra khỏi hình ảnh của chính mình và tìm tới những nấc thang thành công cao hơn, xa hơn.</em></p>
        <p><em><strong>Thử thách</strong>: Với vai trò là một Talent Management Specialist, bạn hãy lên một kế hoạch định hướng phong cách mới, tạo sự khác biệt cho ca sĩ Soobin Hoàng Sơn (*). <br>
                Soobin Hoàng Sơn tên thật là Nguyễn Hoàng Sơn - một nam ca sĩ, nhạc sĩ, rapper , từng giành được 2 đề cử tại giải Cống hiến, sở hữu những bản hit "Phía sau một cô gái", “Đi để trở về”, "Anh đã quen với cô đơn", “Xin đừng lặng im”.
            </em></p>
        <p><em><strong>Mục tiêu:</strong><br>
                Thuyết phục được Soobin Hoàng Sơn theo định hướng của mình
            </em></p>
        <p><em> <strong>Đối tượng mục tiêu</strong>: 18- 25 tuổi </em></p>
        <p><em><mark>Thí sinh nộp bài dự thi dưới hình thức PDF - tối đa 10 trang, khổ 16:9 ngang đăng tải trực tiếp, chế độ công khai tại <a target="_blank" href=" https://www.slideshare.net/"> https://www.slideshare.net/</a> (đặt tên theo công thức: Kỳ Thực Tập Trong Mơ 2 – Talent Management Specialist - Tên thí sinh) <br>
                    Sau khi hoàn tất, bạn gửi link vào microsite chương trình (chỉ có thí sinh đã tham gia vòng 1 mới được nộp bài vòng 2)
                </mark></em></p>
        <p><em>Ngôn ngữ bài dự thi có thể bằng tiếng Anh hoặc tiếng Việt <br>
                Bài thi nghiêm cấm việc đạo ý tưởng, sử dụng hình ảnh không rõ nguồn gốc trên Google, thí sinh có thể sử dụng hình trên các trang ShutterStock, GettyImages và giữ nguyên watermark để làm minh họa cho bài làm. Hình ảnh Soobin Hoàng Sơn phải có chú thích minh họa bên dưới”
            </em></p>
        <p><em>(*)Nghệ sĩ chỉ tham gia với vai trò để tìm kiếm ra những thực tập sinh xuất sắc nhất trong khuôn khổ Vòng 2 của cuộc thi Kỳ thực tập trong mơ mùa 2, không phải là nghệ sĩ chính thức mà thực tập sinh sẽ phụ trách quản lý sau khi giành chiến thắng vào tháng 9&10.</em></p>
        <p><em><strong><ins>Lưu ý, hãy nghiên cứu kỹ tiêu chí chấm điểm để khai thác và giành số điểm tối đa trong 70% điểm đến từ BGK cho bài dự thi của bạn nhé, cụ thể như sau:</ins></strong></em></p>

        <ul>
            <strong><em>Phần nội dung:</em></strong>
            <li><em>Thể hiện sự am hiểu về ca sĩ:15 điểm</em></li>
            <li><em>Kế hoạch đính hướng có yếu tố đột phá hay không?15 điểm</em></li>
            <li><em>Kế hoạch định hướng phù hợp với phong cách của ca sĩ hay không? 15 điểm</em></li>
            <li><em>Kế hoạch đính hướng có khả thi/ ứng dụng được hay không? 15 điểm</em></li>
        </ul>

        <ul>
            <strong><em>Phần hình thức:</em></strong>
            <li><em>Hình ảnh phù hợp với định hướng của ca sĩ: 5 điểm</em></li>
            <li><em>Tính thẩm mỹ, có đầu tư: 5 điểm </em></li>
        </ul>
    </div>
</div>
</body>
</html>