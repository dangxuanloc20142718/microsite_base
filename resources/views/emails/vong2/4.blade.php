<html>
<body>
<div class="mailbox">
    <h3 class="title"></h3>
    <div class="contents">
        <p>Thân chào bạn,</p>
        <p>Ban Tổ Chức xin chúc mừng bạn đã dũng cảm bắt đầu <strong> “BƯỚC ĐI ĐẦU TIÊN - VÒNG 1” </strong> của cuộc thi <strong>KỲ THỰC TẬP TRONG MƠ MÙA 2 – ĐI RỒI SẼ ĐẾN! </strong> và là những ứng cử viên hoàn toàn hợp lệ để chính thức bước tiếp vào <strong>VÒNG  2 – CỨ ĐI THÔI! </strong>
        </p>
        <p>Tại vòng này, bạn sẽ phải hoàn thành thử thách tương ứng với ngành nghề bạn muốn thực tập do Ban Giám Khảo đề ra nhằm giúp Ban Giám Khảo chọn được những thí sinh xuất sắc nhất với những tố chất và kỹ năng phù hợp với nghề thể hiện qua thử thách đặc biệt của chương trình.</p>
        <p><strong>ĐỢT 1: 00:00’ 03/08/2017 - 23:59’ 10/08/2017, DO BGK CHẤM, CHIẾM 70% </strong></p>
        <p><strong>TỔNG SỐ ĐIỂM</strong></p>
        <p>Sau khi hoàn thành vòng đăng ký, mỗi thí sinh sẽ chọn 1 ngành nghề và nhận một thử thách từ “Kì thực tập trong mơ” trong vòng 08 ngày.</p>
        <ul>
            <li>
                <p><strong>BƯỚC 1</strong></p>
                <p>Thí sinh đăng nhập vào tài khoản đã được tạo khi tham gia Vòng 1, để nhận đề thi, chỉ có thí sinh tham gia Vòng 1 mới hợp lệ để tham gia Vòng 2 (thí sinh hợp lệ sẽ được nhận email thông báo)</p>
            </li>
            <li>
                <p><strong>BƯỚC 2</strong></p>
                <p>Thí sinh tiến hành thực hiện thử thách theo yêu cầu của chương trình, và nộp lại bài theo đúng định dạng quy định tại website của chương trình. Sau 23:59’ ngày 10/08/2017 hệ thống sẽ đóng lại và bạn không thể nộp bài dự thi của mình.</p>
            </li>
        </ul>
        <p><strong>ĐỢT 2: 00:00’ 11/08/2017 - 23:59’ 13/08/2017, DO KHÁN GIẢ BÌNH CHỌN, CHIẾM 30% TỔNG SỐ ĐIỂM</strong></p>
        <ul>
            <li>
                <p><strong>BƯỚC 1</strong></p>
                <p>Thí sinh chia sẻ bài dự thi của mình trên trang mạng xã hội cá nhân với hashtag #take1stmove #diroiseden #BitisHunter #kythuctaptrongmo</p>
            </li>
            <li>
                <p><strong>BƯỚC 2</strong></p>
                <p>Thí sinh thể hiện sự ảnh hưởng của mình bằng cách kêu gọi bạn bè và cộng đồng vote cho bài dự thi của mình.</p>
            </li>
        </ul>
        <p>BGK chấm điểm và chọn ra Top 15 bài dự thi xuất sắc nhất để đến với Vòng 3.</p>
        <p>Hãy tự tin và hoàn thành thử thách theo cách của chính bạn. Vì Biti’s Hunter tin rằng Đi rồi sẽ đến, còn chần chờ gì mà không bắt tay vào thực hiện thử thách ngay nào!</p>
        <p>Chúc bạn sẽ có những trải nghiệm tuyệt vời trong quá trình thực hiện thử thách nhé! <br>
            Ban Tổ Chức chúc bạn sẽ xuất sắc vượt qua thử thách tại vòng 2. <br>
            Kết quả vòng 2 sẽ được công bố vào ngày 23/08/2017. <br>
        </p>
        <p>#take1stmove #diroiseden #BitisHunter #kythuctaptrongmo</p>
        <hr>
        <p>Còn chần chờ gì nữa, cùng xem phần thử thách dành riêng cho bạn – một ứng cử viên của Kỳ thực tập trong mơ mùa 2 nào!</p>
        <p><em>“Năm 2016, Biti’s tiến hành cuộc cách mạng để mang thương hiệu quay trở lại với nhóm khách hàng trẻ, năng động và phong cách với việc ra mắt dòng sản phẩm giày thể thao cao cấp Biti’s Hunter. Thương hiệu đã tạo được tiếng vang về sự đột phá trong sản phẩm và ấn tượng ở mặt trận truyền thông với chiến dịch đình đám Tết Nguyên Đán 2017: Đi Để Trở Về. <br>
                Năm 2017 là năm Biti’s Hunter đặt bước tiến xa hơn trên con đường phát triển với sự đồng hành của đại sứ thương hiệu Sơn Tùng M-TP cùng với những đầu tư cho các chiến dịch truyền thông ý nghĩa như Đi và Yêu, Đi Rồi Sẽ Đến; nhưng quan trọng nhất vẫn là sự tập trung vào việc phát triển những sản phẩm chất lượng tốt hơn để phục vụ cho người tiêu dùng.
            </em></p>
        <p><strong>Về Biti’s Hunter X:</strong></p>
        <p><em>- Là dòng cao cấp của Biti’s Hunter với chất lượng vượt trội được ra mắt vào quý 3 năm 2017 - chính là một bước đi chiến lược quan trọng trong việc thực hiện triết lý tập trung vào chất lượng sản phẩm, thể hiện nỗ lực không ngừng của Biti’s trong việc ứng dụng những công nghệ mới để nâng tầm trải nghiệm của người sử dụng, cũng như thiết kế ấn tượng và thời trang hơn</em></p>
        <p><em>- Phiên bản đầu tiên – 2017: </em></p>
        <ul>
            <li>
                <p><em><strong>Tính năng: </strong> <br>
                        Upper (mũ quai): công nghệ liteknit của Biti’s nâng tầm với công thức kháng khuẩn tạo độ thông thoáng tuyệt đối trong lòng giày, triệt tiêu các yếu tố gây mùi khi sử dụng thời gian dài. <br>
                        6 zones cushioning: công nghệ 06 điểm massage trong lòng giày nâng cao trải nghiệm êm ái cho người dùng. <br>
                        Cage đinh hình form chân: thiết kế cập nhật cage đắp nổi hai bên hông bàn chân giúp định hình dáng chân trong những hoạt động di chuyển phức tạp.</em></p>
            </li>
            <li>
                <p><em><strong>Thiết kế: </strong> <br>
                        Nhấn vào yếu tố thời trang, cập nhật các xu hướng thiết kế giày thể thao từ quốc tế, chú trọng vào khâu chọn lựa chất liệu đạt những tiêu chuẩn khắt khe để tạo trải nghiệm tốt nhất cho khách hàng.</em></p>
            </li>
        </ul>
        <p> <strong>Thử thách: </strong> Với vai trò là 1 thành viên team marketing cho Biti’s Hunter, bạn hãy lên kế hoạch ra mắt sản phẩm Biti’s Hunter X với những thông tin bên dưới:</p>
        <p><em><strong>Mục tiêu:</strong> <br>
                - Tạo được cơn sốt mua Biti’s Hunter X với số lượng 20,000 đôi trong 1 tháng ra mắt sản phẩm. <br>
                - Thu hút nhóm khách hàng mục tiêu phân khúc trung cấp & thuyết phục nhóm khách hàng hiện có của Biti’s Hunter sử dụng dòng cao cấp này. <br>
                - Tạo độ nhận biết thương hiệu (brand awareness) của nhóm khách hàng mục tiêu cho Biti’sHunter X & xây dựng nhận thức về yếu tố“ chất lượng cải tiến & vượt trội” và “thời trang hơn” cho dòng Biti’s Hunter X này.</em></p>
        <p><strong><em>Đối tượng mục tiêu:</strong> <br>
            - 18-30, thu nhập AB+, ở 6 thành phố lớn HCM, Hà Nội, Đà Nẵng, Cần Thơ, Hải Phòng, Nha Trang. <br>
            - Ưu tiên nhóm trung cao cấp (upper mainstream) chưa mua Biti’s Hunter, sau đó là nhóm khách hàng hiện tại của Biti’s Hunter. <br>
            - Thời gian: 1 tháng từ ngày tung sản phẩm là 08/08/2017 <br>
            - Ngân sách: 500,000,000 VNĐ, chủ yếu cho các hoạt động digital marketing & trải nghiệm sản phẩm.</em></p>
        <p><em><mark>Thí sinh nộp bài dự thi dưới hình thức PDF - tối đa 10 trang, khổ 16:9 ngang đăng tải trực tiếp, chế độ công khai tại <a target="_blank" href="https://www.slideshare.net/">https://www.slideshare.net/</a>  (đặt tên theo công thức: Kỳ Thực Tập Trong Mơ 2 – Marketing Specialist - Tên thí sinh) <br>
                    Sau khi hoàn tất, bạn gửi link vào microsite chương trình (chỉ có thí sinh đã tham gia vòng 1 mới được nộp bài vòng 2)
                </mark> <br>
                Ngôn ngữ bài dự thi có thể bằng tiếng Anh hoặc tiếng Việt. <br>
                Bài thi nghiêm cấm việc đạo ý tưởng, sử dụng hình ảnh không rõ nguồn gốc trên Google, thí sinh có thể sử dụng hình trên các trang ShutterStock, GettyImages và giữ nguyên watermark để làm minh họa cho bài làm.
            </em></p>
        <p><em><strong><ins>Lưu ý, hãy nghiên cứu kỹ tiêu chí chấm điểm để khai thác và giành số điểm tối đa trong 70% điểm đến từ BGK cho bài dự thi của bạn nhé, cụ thể như sau:</ins></strong></em></p>
        <ul>
            <strong><em>Phần nội dung:</em></strong>
            <li><em>Insight:</em>
                <ul>
                    <li><em>Cách thức tìm ra insight của đối tượng mục tiêu: 15 điểm</em></li>
                    <li><em>Sức mạnh của insight: 15 điểm</em></li>
                </ul>

            </li>
            <li><em>Big idea</em>
                <ul>
                    <li>Idea có được phát triển từ insight hay không? 6 điểm</li>
                    <li>Idea có đủ mạnh để giải quyết bài toán đưa ra hay không? 6 điểm</li>
                    <li>Idea có yếu tố đột phá hay không? 6 điểm</li>
                    <li>Idea có phù hợp với Hunter hay không? 6 điểm</li>
                    <li>Idea có khả thi/ ứng dụng được hay không? 6 điểm</li>
                </ul>
            </li>
        </ul>
        <ul>
            <strong><em>Phần hình thức:</em></strong>
            <li>Rõ ràng, cô động, logic: 5 điểm</li>
            <li>Thẩm mỹ, có đầu tư: 5 điểm</li>
        </ul>
    </div>
</div>
</body>
</html>