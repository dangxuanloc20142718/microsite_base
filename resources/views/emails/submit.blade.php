<html>
    <body>
        <div class="mailbox">
            <h3 class="title"></h3>
            <div class="contents">
                <p><strong>Thân chào bạn,</strong></p>

                <p>Ngay khi bạn đăng ký tham gia cuộc thi Kỳ Thực Tập Trong Mơ mùa 2 – Đi rồi sẽ đến chính là bạn đã bắt đầu đồng hành cùng Biti’s Hunter,
                    cùng chung niềm tin rằng ước mơ sẽ chẳng thể thành hiện thực nếu ta cứ mãi quẩn quanh những nỗi sợ mơ hồ,
                    khoá chặt đôi chân sau những rào cản tự tạo. Khoảng cách giữa xuất phát điểm và vạch đích, đôi khi chỉ cách nhau ở bước đầu tiên – bời lẽ, ĐI RỒI SẼ ĐẾN!
                </p>


                <p>Biti’s Hunter mong rằng, không chỉ bạn dành đủ nhiệt huyết, chân thành và cả lòng quyết
                    tâm để đặt bước chân đầu tiên trên hành trình chinh phục ước mơ của mình mà còn hy
                    vọng bạn sẽ là cầu nối khuyến khích các bạn trẻ khác cùng tìm hiểu, lắng nghe và có một
                    niềm tin chung <em>“Đi Rồi Sẽ Đến”</em> này.
                    Hãy hiện thực hóa điều đó ngay bằng cách chia sẻ bài dự thi của mình về trang mạng xã
                    hội cá nhân kèm theo hashtag <a href="https://www.facebook.com/hashtag/thefirstmove" target="_blank" title="#thefirstmove">#thefirstmove</a> <a href="https://www.facebook.com/hashtag/diroiseden" target="_blank" title="#diroiseden">#diroiseden</a> <a href="https://www.facebook.com/hashtag/BitisHunter" target="_blank" title="#BitisHunter">#BitisHunter</a> để có cơ hội sở
                    hữu 01 đôi giày Biti’s Hunter cực chất trong bộ sưu tập <em>“Đi, Trải Nghiệm mùa hè”</em> mới
                    nhất dành cho 50 bài dự thi có số lượt vote cao nhất.</p>

                <p>Mùa hè này, hãy cất bước chân trải nghiệm Kỳ thực tập trong mơ mùa 2, hãy để bước đi
                    đầu tiên đưa bạn tới giấc mơ mang tên mình và hãy luôn tin rằng Đi Rồi Sẽ Đến!</p>

                <p>Cùng đón chờ những thử thách hấp dẫn tại vòng 2 sẽ được Bạn Tổ Chức thông báo vào
                    ngày 03/08/2017 nhé!
                <p>Cảm ơn bạn đã đồng hành cùng cuộc thi Kỳ Thực Tập Trong Mơ mùa 2 – Đi rồi sẽ đến.</p>
                <p>Thân mến,</p>
                <p>Biti’s Hunter-Kỳ thực tập trong mơ mùa 2 – Đi rồi sẽ đến</p>
                <p><a href="https://www.facebook.com/hashtag/thefirstmove" target="_blank" title="#thefirstmove">#thefirstmove</a> <a href="https://www.facebook.com/hashtag/diroiseden" target="_blank" title="#diroiseden">#diroiseden</a> <a href="https://www.facebook.com/hashtag/BitisHunter" target="_blank" title="#BitisHunter">#BitisHunter</a></p>

                <br/>
                Tái bút:<br/>
                Để tiếp thêm sức mạnh cho đôi chân bạn vững vàng trên chặng đường phía trước, Ban tổ chức  gửi tặng bạn món quà là mã code <b>{{ $code }}</b> giảm giá 10% khi mua sắm sản phẩm Biti’s Hunter tại website <a href="http://bitis.com.vn/35nam/product/search/hunter" target="_blank">www.bitis.com.vn</a><br/>
                Lưu ý: quy chế sử dụng mã code chỉ áp dụng khi mua sắm 01 sản phẩm Biti’s Hunter cho 01 hoá đơn mua hàng có giá trị đến hết ngày 30/08/2017.
            </div>
        </div>
    </body>
</html>