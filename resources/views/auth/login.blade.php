<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title>Login Form</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="{{ url('/') }}/assets/admin/login/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ url('/') }}/assets/admin/login/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ url('/') }}/assets/admin/login/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ url('/') }}/assets/admin/login/css/style-metro.css" rel="stylesheet" type="text/css"/>
    <link href="{{ url('/') }}/assets/admin/login/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="{{ url('/') }}/assets/admin/login/css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="{{ url('/') }}/assets/admin/login/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="{{ url('/') }}/assets/admin/login/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="{{ url('media/system/favicon.ico?'.config('custom.version')) }}"/>
{{--<link rel="stylesheet" type="text/css" href="{{ url('/') }}/assets/admin/login/plugins/select2/select2_metro.css" />--}}
<!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{ url('/') }}/assets/admin/login/css/pages/login-soft.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    <!-- PUT YOUR LOGO HERE -->
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="form-vertical login-form" action="{{ url('auth/login') }}" method="post">
        <h3 class="form-title">Đăng nhập</h3>
        {{ csrf_field() }}

        <div class="alert alert-error @if(!Session::has('message')) {{ 'hide' }} @endif">
            <button class="close" data-dismiss="alert"></button>
            <span>@if(Session::has('message')) {{ Session::get('message') }} @else {{ 'Enter any email and password.' }} @endif</span>
        </div>
        <div class="control-group"{{ $errors->has('email') ? ' has-error' : '' }}>
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Email</label>
            <div class="controls">
                <div class="input-icon left">
                    <i class="icon-user"></i>
                    <input class="m-wrap placeholder-no-fix" type="text" value="@if(Session::has('email')){{ Session::get('email') }}@else{{ old('email') }}@endif" autocomplete="off" placeholder="Email" name="email"/>
                </div>
                @if ($errors->has('email'))
                    <span class="help-block text-danger" style="color:#953b39;">
                        {{ $errors->first('email') }}
                    </span>
                @endif
            </div>
        </div>
        <div class="control-group"{{ $errors->has('password') ? ' has-error' : '' }}>
            <label class="control-label visible-ie8 visible-ie9">Mật khẩu</label>
            <div class="controls">
                <div class="input-icon left">
                    <i class="icon-lock"></i>
                    <input class="m-wrap placeholder-no-fix" type="password" value="@if(Session::has('password')){{ Session::get('password') }}@else{{ old('password') }}@endif" autocomplete="off" placeholder="Mật khẩu" name="password"/>
                </div>
                @if ($errors->has('password'))
                    <span class="help-block text-danger" style="color:#953b39;">
                        {{ $errors->first('password') }}
                    </span>
                @endif
            </div>
        </div>
        <div class="form-actions" style="margin-left: -10px;">
            <label class="checkbox">
                <input type="checkbox" name="remember" value="1" @if(Session::has('remember')){{ 'checked' }}@endif /> Ghi nhớ
            </label>
            <button type="submit" class="btn blue pull-right">
                Đăng nhập <i class="icon-arrow-right"></i>
            </button>
        </div>
        <div class="create-account hidden">
            <script src='https://www.google.com/recaptcha/api.js'></script>
            <label class="col-sm-2 control-label required">Mã bảo vệ</label>
            <div class="col-sm-6">
                <div class="g-recaptcha" data-sitekey="{{ config('custom.captcha_sitekey') }}"></div>
            </div>
        </div>
        <div class="forget-password">
            <h4>Bạn quên mật khẩu?</h4>
            <p>
                Hãy click <a href="javascript:;"  id="forget-password">vào đây</a> để lấy lại
            </p>
        </div>
    </form>
    <!-- END LOGIN FORM -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="form-vertical forget-form" action="{{ url('/password/email') }}" method="post">
        <h3 >Quên mật khẩu?</h3>
        <p>Vui lòng nhập địa chỉ email của bạn</p>
        {{ csrf_field() }}
        <div class="control-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <div class="controls">
                <div class="input-icon left">
                    <i class="icon-envelope"></i>
                    <input class="m-wrap placeholder-no-fix" type="text" placeholder="Email" value="{{ old('email') }}" autocomplete="off" name="email" />
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn">
                <i class="icon-arrow-left"></i> Quay lại
            </button>
            <button type="submit" class="btn blue pull-right">
                Hoàn tất <i class="icon-arrow-right"></i>
            </button>
        </div>
    </form>
    <!-- END FORGOT PASSWORD FORM -->
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
    2017 &copy; Admicro
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<script src="{{ url('/') }}/assets/admin/login/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/assets/admin/login/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{ url('/') }}/assets/admin/login/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/assets/admin/login/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/assets/admin/login/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
<!--[if lt IE 9]>
<script src="{{ url('/') }}/assets/admin/login/plugins/excanvas.min.js"></script>
<script src="{{ url('/') }}/assets/admin/login/plugins/respond.min.js"></script>
<![endif]-->
<script src="{{ url('/') }}/assets/admin/login/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/assets/admin/login/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/assets/admin/login/plugins/jquery.cookie.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/assets/admin/login/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ url('/') }}/assets/admin/login/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/assets/admin/login/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
{{--<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>--}}
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ url('/') }}/assets/admin/login/scripts/app.js" type="text/javascript"></script>
<script src="{{ url('/') }}/assets/admin/login/scripts/login-soft.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {
        App.init();
        Login.init();
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>