<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title>Reset Password</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="/assets/admin/login/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/admin/login/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/admin/login/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/admin/login/css/style-metro.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/admin/login/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/admin/login/css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/admin/login/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="/assets/admin/login/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="/assets/admin/login/css/pages/login-soft.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    <!-- PUT YOUR LOGO HERE -->
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN RESET FORM -->
    <form class="form-vertical login-form" role="form" action="{{ url('/password/reset') }}" method="post">
        <h3 class="form-title">Reset Password</h3>
        {{ csrf_field() }}

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="alert alert-error @if(!Session::has('message')) {{ 'hide' }} @endif">
            <button class="close" data-dismiss="alert"></button>
            <span>@if(Session::has('message')) {{ Session::get('message') }} @else {{ 'Enter any username and password.' }} @endif</span>
        </div>

        <div class="control-group"{{ $errors->has('email') ? ' has-error' : '' }}>
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">E-Mail Address</label>
            <div class="controls">
                <div class="input-icon left">
                    <i class="icon-user"></i>
                    <input id="email" type="email" class="m-wrap placeholder-no-fix" name="email" value="{{ $email or old('email') }}" placeholder="E-Mail Address">
                </div>
                @if ($errors->has('email'))
                    <span class="help-block text-danger" style="color:#953b39;">
                        {{ $errors->first('email') }}
                    </span>
                @endif
            </div>
        </div>
        <div class="control-group"{{ $errors->has('password') ? ' has-error' : '' }}>
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="controls">
                <div class="input-icon left">
                    <i class="icon-lock"></i>
                    <input id="password" type="password" class="m-wrap placeholder-no-fix" name="password" placeholder="Password">
                </div>
                @if ($errors->has('password'))
                    <span class="help-block text-danger" style="color:#953b39;">
                        {{ $errors->first('password') }}
                    </span>
                @endif
            </div>
        </div>
        <div class="control-group"{{ $errors->has('password_confirmation') ? ' has-error' : '' }}>
            <label class="control-label visible-ie8 visible-ie9">Confirm Password</label>
            <div class="controls">
                <div class="input-icon left">
                    <i class="icon-lock"></i>
                    <input id="password-confirm" type="password" class="m-wrap placeholder-no-fix" name="password_confirmation" placeholder="Confirm Password">
                </div>
                @if ($errors->has('password_confirmation'))
                    <span class="help-block text-danger" style="color:#953b39;">
                        {{ $errors->first('password_confirmation') }}
                    </span>
                @endif
            </div>
        </div>
        <div class="form-actions" style="margin-left: -10px;">
            <button type="submit" class="btn blue pull-right">
                Reset Password <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>
    </form>

    <!-- END LOGIN FORM -->
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
    2017 &copy; <a href="http://www.kenh14.vn">Kenh14</a> - Admicro
</div>

<script src="/assets/admin/login/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
<script src="/assets/admin/login/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="/assets/admin/login/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="/assets/admin/login/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/assets/admin/login/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
<!--[if lt IE 9]>
<script src="/assets/admin/login/plugins/excanvas.min.js" type="text/javascript"></script>
<script src="/assets/admin/login/plugins/respond.min.js" type="text/javascript"></script>
<![endif]-->
<script src="/assets/admin/login/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/assets/admin/login/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/assets/admin/login/plugins/jquery.cookie.min.js" type="text/javascript"></script>
<script src="/assets/admin/login/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>

<script src="/assets/admin/login/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="/assets/admin/login/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>

<script src="/assets/admin/login/scripts/app.js" type="text/javascript"></script>
<script src="/assets/admin/login/scripts/login-soft.js" type="text/javascript"></script>

<script>
    jQuery(document).ready(function() {
        App.init();
        Login.init();
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>