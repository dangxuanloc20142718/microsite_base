<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title>Opsss !</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="/assets/admin/login/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/admin/login/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/admin/login/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/admin/login/css/style-metro.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/admin/login/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/admin/login/css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/admin/login/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="/assets/admin/login/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    {{--<link rel="stylesheet" type="text/css" href="/assets/admin/login/plugins/select2/select2_metro.css" />--}}
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="/assets/admin/login/css/pages/login-soft.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    <!-- PUT YOUR LOGO HERE -->
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="form-vertical login-form" action="{{ url('auth/login') }}" method="post">
        <h3 class="form-title">Opssss !</h3>
    
        <div class="forget-password">
            <h4>Thông tin bạn tìm kiếm không tồn tại hoặc có lỗi xảy ra khi xử lý yêu cầu.</h4>
            <p>
                Click tại <a href="/">đây</a>
                để trở lại ..
            </p>
        </div>
    </form>
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
    2017 &copy; <a href="http://www.kenh14.vn">Kenh14</a> - Admicro
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<script src="/assets/admin/login/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
<script src="/assets/admin/login/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="/assets/admin/login/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="/assets/admin/login/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/assets/admin/login/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
<!--[if lt IE 9]>
<script src="/assets/admin/login/plugins/excanvas.min.js"></script>
<script src="/assets/admin/login/plugins/respond.min.js"></script>
<![endif]-->
<script src="/assets/admin/login/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/assets/admin/login/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/assets/admin/login/plugins/jquery.cookie.min.js" type="text/javascript"></script>
<script src="/assets/admin/login/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/assets/admin/login/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="/assets/admin/login/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
{{--<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>--}}
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/assets/admin/login/scripts/app.js" type="text/javascript"></script>
<script src="/assets/admin/login/scripts/login-soft.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {
        App.init();
        Login.init();
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>