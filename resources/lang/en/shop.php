<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'category' => 'Category',
    'keyword' => 'Keywords...',
    'buy' => 'Buy',
    'details' => 'Details',
    'featured' => 'Featured',
    'promotion' => 'Promotion',
    'new_arrival' => 'New arrival',
    'hot' => 'Hot sale',
    'sale_price' => 'Price',
    'contact' => 'Contact us',
    'order_by' => 'Order by:',
    'order_latest' => 'Latest products',
    'order_price_up' => 'Price asc',
    'order_price_down' => 'Price desc',
    'vat' => '(VAT included)',
    'add_cart' => 'Add to cart',
    'related' => 'Related products',

    'cart' => 'Cart',
    'cart_title' => 'Cart & Checkout',
    'cart_product' => 'Product title',
    'cart_number' => 'Quantity',
    'cart_total' => 'Total',
    'cart_close' => 'Close',
    'cart_order' => 'Checkout',
    'cart_back' => 'Back',
    'cart_finish' => 'Send',
    'cart_guide' => 'Please fill following information'
];
