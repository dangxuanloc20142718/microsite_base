
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'more' => 'View more',
    'views' => 'Views',
    'fullname' => 'Full name',
    'mobile' => 'Mobile',
    'address' => 'Address',
    'address_desc' => 'Your delivery address',
    'note' => 'Note',
    'note_desc' => 'Your note for admin',
    'comments' => 'Comments',
    'comments_content' => 'Your content',
    'captcha' => 'Code',
    'send' => 'Send',
    'news_related_tag' => 'Related topics',
    'news_related' => 'Related news',

    'member'=>'Member',
    'member_welcome'=>'Welcome',
    'member_manage'=>'Manage',
    'member_logout'=>'Logout',
    'member_login'=>'Login',
    'member_login_via'=>'Login via',
    'member_reg'=>'Register',
    'member_reg_via'=>'Register via',
    'member_customer'=> 'Customer',
    'member_password'=> 'Password',
    'member_password_retype'=> 'Retype password',
    'member_remember'=> 'Remember',
    'member_forgot'=> 'Forgot password?',
    'member_gender'=> 'Gender',
    'member_male'=> 'Male',
    'member_female'=> 'Female',

    'newsletter_title' => 'Newsletter',
    'newsletter_email' => 'Your email',

    'contact' => 'Contact',
    'contact_or' => 'OR',
    'contact_content' => 'Your contact content',

    'ct_project'=>'Featured projects',
    'ct_partners' => 'Our partners',
    'ct_customer'=>'Customers',
    'ct_hotline1'=>'Hotline',
    'ct_hotline2'=>'(8h-20h every weekdays)',
    'ct_links'=>'Other links',
];
