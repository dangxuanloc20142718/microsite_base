<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'more' => 'Xem thêm',
    'views' => 'Lượt xem',
    'fullname' => 'Họ và tên',
    'mobile' => 'Điện thoại',
    'address' => 'Địa chỉ',
    'address_desc' => 'Địa chỉ để chúng tôi giao hàng cho bạn',
    'note' => 'Ghi chú',
    'note_desc' => 'Lời nhắn cho Admin về đơn hàng này',
    'comments' => 'Bình luận',
    'comments_content' => 'Nội dung bình luận',
    'captcha' => 'Mã bảo vệ',
    'send' => 'Gửi',
    'news_related_tag' => 'Bài viết cùng chủ đề',
    'news_related' => 'Bài viết liên quan',

    'member'=>'Thành viên',
    'member_welcome'=>'Xin chào',
    'member_manage'=>'Quản lý Tài khoản',
    'member_logout'=>'Thoát',
    'member_login'=>'Đăng nhập',
    'member_login_via'=>'Đăng nhập qua',
    'member_reg'=>'Đăng ký',
    'member_reg_via'=>'Đăng ký qua',
    'member_customer'=> 'Khách hàng',
    'member_password'=> 'Mật khẩu',
    'member_password_retype'=> 'Nhắc lại mật khẩu',
    'member_remember'=> 'Ghi nhớ',
    'member_forgot'=> 'Quên mật khẩu?',
    'member_login'=> 'Đăng nhập',
    'member_gender'=> 'Giới tính',
    'member_male'=> 'Nam',
    'member_femail'=> 'Nữ',


    'newsletter_title' => 'Đăng ký nhận tin khuyến mại',
    'newsletter_email' => '"Email của bạn',

    'contact' => 'Liên hệ',
    'contact_or' => 'HOẶC',
    'contact_content' => 'Nội dung liên hệ',

    'ct_project'=>'Sự kiện Bán hàng',
    'ct_partners' => 'Đối tác của chúng tôi',
    'ct_customer'=>'Đối tác',
    'ct_hotline1'=>'TỔNG ĐÀI GÓP Ý KHIẾU NẠI',
    'ct_hotline2'=>'(8h-20h tất cả các ngày trong tuần)',
    'ct_links'=>'Liên kết khác',
];