<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'category' => 'Danh mục sản phẩm',
    'keyword' => 'Bạn muốn tìm sản phẩm...',
    'buy' => 'Mua hàng',
    'details' => 'Chi tiết',
    'featured' => 'Nổi bật',
    'promotion' => 'Khuyến mại',
    'new_arrival' => 'Hàng mới về',
    'hot' => 'Hàng bán chạy',
    'sale_price' => 'Giá bán',
    'contact' => 'Liên hệ',
    'order_by' => 'Sắp xếp theo:',
    'order_latest' => 'Sản phẩm mới nhất',
    'order_price_up' => 'Giá tăng dần',
    'order_price_down' => 'Giá giảm dần',
    'vat' => '(Đã bao gồm VAT)',
    'add_cart' => 'Cho vào Giỏ hàng',
    'related' => 'Sản phẩm tương tự',

    'cart' => 'Giỏ hàng',
    'cart_title' => 'Giỏ hàng & Đặt hàng',
    'cart_product' => 'Sản phẩm',
    'cart_number' => 'Số lượng',
    'cart_total' => 'Tổng',
    'cart_close' => 'Đóng lại',
    'cart_order' => 'Đặt hàng',
    'cart_back' => 'Quay lại',
    'cart_finish' => 'Hoàn tất',
    'cart_guide' => 'Để thuận tiện cho việc giao hàng và tiết kiệm thời gian của quý khách, vui lòng điền các thông tin dưới đây.<br/>Quý khách chỉ cần thực hiện 1 lần duy nhất.<br/><br/>',
];
